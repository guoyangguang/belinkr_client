describe "ScrapbookView", ->
  require ["jquery","underscore", "backbone", "cs!scrapbook/model",
  "cs!scrapbook/collection", "cs!scrapbook/view.model",
  "cs!../spec/support/fixtures/scraps_fixture"],
  ($, _, Backbone, Model, Collection, ViewModel, Fixture) ->
    describe "ScrapbookView", ->
      beforeEach ->
        @fixture       = new Fixture

        setFixtures("<div id=content><div id=maintest></div><div id=sidebar></div></div>")
        @clock = sinon.useFakeTimers()
      afterEach ->
        @clock.restore()
 
      it "is defined", ->
        expect(ViewModel).toBeDefined()

      it "has a name", ->
        expect(ViewModel.name).toEqual "ScrapbookView"

      describe "Instantiation", ->
        it "creates a root element", ->
          model = new Model {user_id: 1, entity_id: 1, name:'test text1'}
          view = new ViewModel(model: model)
          expect(view.el.nodeName).toEqual 'LI'

        it "bind events", ->
          model = new Backbone.Model()
          model_spy = sinon.spy(model,"bind")
          view = new ViewModel(model: model)
          expect(model_spy).toHaveBeenCalledOnce()
          expect(model_spy).toHaveBeenCalledWith("change")
          expect(_.keys(view.events)).toContain "click a"
          model.bind.restore()
          
      describe "Rendering", ->
        it "return the view object", ->
          model = new Model {user_id: 1, entity_id: 1, name:'test text1'}
          view = new ViewModel(model: model)
          itself = view.render()
          expect(itself).toEqual view
 
        it "product the correct html", ->
          model = new Model {user_id: 1, entity_id: 1, name:'test text1'}
          view = new ViewModel(model: model)
          html_output = view.render().$el
          expect(html_output.find("a").length).toEqual(1)
          expect(html_output.find("a")).toHaveText("test text1")

      describe "UI event", ->
        it "hide the view when click delete", ->
          #$("div#main").html("")
          #model = new Model {user_id: 1, id: 1, text:'test text1'}
          #view = new ViewModel(model: model)
          #model.bind 'destroy', view.fadeOutremove
          #$("div#main").html(view.render().el)
          #expect($("div#main div.scrapbook")).toBeVisible()
          #$("li.links a.delete").click()
          #@clock.tick(600)
          #expect($("div#main div.scrapbook")).not.toBeVisible()
 
#      describe "UI event show", ->
#        it "show the scrap view when click show", ->
#          model = new Model {user_id: 1, id: 1, text:'test text1'}
#          view = new ViewModel(model: model)
#
#          $("div#maintest").html("")
#          $("div#maintest").html(view.render().el)
#          show_inside_spy = sinon.spy(view,"renderChildInto")
#          @server = sinon.fakeServer.create()
#          @server.respondWith Fixture.validResponse(@fixture.fetch_scraps())
#          $("a").click()
#          @server.respond()
#          expect(show_inside_spy).toHaveBeenCalledOnce()
#          # FIXME: fail the test, need to fix later
#          # expect($("div ul.scraps li div.scrap").length).toEqual(@fixture.fetch_scraps().length)
#
#          @server.restore()
