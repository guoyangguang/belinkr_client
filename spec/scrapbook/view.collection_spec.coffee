describe "ScrapbooksView", ->
  require ["jquery","underscore", "backbone", "cs!scrapbook/model", 
  "cs!scrapbook/collection", "cs!scrapbook/view.collection"],
  ($, _, Backbone, Model, Collection, ViewCollection) ->
    describe "ScrapbooksView", ->
      beforeEach ->
        setFixtures("<div id=content></div>")
      it "is defined", ->
        expect(ViewCollection).toBeDefined()

      it "has a name", ->
        expect(ViewCollection.name).toEqual 'ScrapbooksView'

      describe "Instantiation", ->
        it "bind reset event to the collection", ->
          collection = new Backbone.Collection()
          collection_spy = sinon.spy(collection, "bind")
          view = new ViewCollection(collection: collection)
          expect(collection_spy).toHaveBeenCalledTwice()
          expect(collection_spy).toHaveBeenCalledWith("add")
          expect(collection_spy).toHaveBeenCalledWith("reset")
          collection.bind.restore()

      describe "UI Event", ->
        it "invoke render_one method to render the model view", ->
          #collection = new Collection()
          #model1 = new Model {user_id: 1, entity_id: 1, text:'test text1'}
          #model2 = new Model {user_id: 1, entity_id: 1, text:'test text2'}
          #collection.add [model1, model2]
          #view = new ViewCollection(collection: collection, el:$('div#content'))
          #render_one_spy = sinon.spy(view,"render_one")
          #view.render()
          #expect(render_one_spy).toHaveBeenCalledWith(model1)
          #expect(render_one_spy).toHaveBeenCalledWith(model2)
          #expect($("#content ul.scrapbook").find('li.text').length).toEqual 2 

        it "attach the el to the dom", ->
          #collection1 = new Collection()
          #collection1.add {user_id: 1, name:'test scrapbook'}
          #view = new ViewCollection(collection: collection1, el:$('div#content'))
          #view.render()
          #expect($("#content ul.scrapbook").find('li.name')).toHaveText("test scrapbook")

