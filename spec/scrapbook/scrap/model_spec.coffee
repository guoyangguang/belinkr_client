describe "Scrap.Model", ->
  require ["underscore", "cs!scrapbook/scrap/model"],
  (_, Model) ->
    describe "Scrap.Model", ->

      it "is defined", ->
        expect(Model).toBeDefined()

      it "has a name", ->
        expect(Model.name).toEqual "Scrap"

      it "has a urlRoot attribute", ->
        a_model = new Model(scrapbook_id: 2)
        expect(a_model.urlRoot()).toEqual "/scrapbooks/2/scraps"

      describe 'default values', ->
        it 'set created_at and updated_at when created', ->
          scrapbook = new Model text: 'Test Text'
          expect(scrapbook.get('created_at')).toBeDefined()
          expect(scrapbook.get('updated_at')).toBeDefined()
          expect(scrapbook.get('text')).toEqual 'Test Text'
