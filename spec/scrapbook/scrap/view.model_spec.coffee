# will be refactored

#describe "ScrapbookView", ->
#  require ["jquery","underscore", "backbone", "cs!scrapbook/scrap/model",
#    "cs!scrapbook/scrap/collection", "cs!scrapbook/scrap/view.model"],
#  ($,_, Backbone, Model, Collection, ViewModel) ->
#    describe "ScrapbookView", ->
#      beforeEach ->
#        setFixtures("<div id=content><div id=maintest></div></div>")
#        @clock = sinon.useFakeTimers()
#      afterEach ->
#        @clock.restore()
#      it "is defined", ->
#        expect(ViewModel).toBeDefined()
#
#      it "has a name", ->
#        expect(ViewModel.name).toEqual "ScrapView"
#
#      describe "Instantiation", ->
#        it "creates a root element", ->
#          model = new Model {user_id: 1, scrapbook_id: 1, text:'test text1'}
#          view = new ViewModel(model: model)
#          expect(view.el.nodeName).toEqual 'LI'
#
#        it "bind events", ->
#          model = new Backbone.Model()
#          model_spy = sinon.spy(model,"bind")
#          view = new ViewModel(model: model)
#          expect(model_spy).toHaveBeenCalledOnce()
#          expect(model_spy).toHaveBeenCalledWith("change")
#          expect(_.keys(view.events)).toContain "click .links .delete"
#          model.bind.restore()
#
#      describe "Rendering", ->
#        it "return the view object", ->
#          model = new Model {user_id: 1, scrapbook_id: 1, text:'test text1'}
#          view = new ViewModel(model: model)
#          itself = view.render()
#          expect(itself).toEqual view
#
#        it "product the correct html", ->
#          model = new Model {user_id: 1, scrapbook_id: 1, text:'test text1'}
#          view = new ViewModel(model: model)
#          html_output = view.render().$el
#          expect(html_output.find("div ul li.id").length).toEqual(1)
#          expect(html_output.find("div ul li.updated_at").length).toEqual(1)
#          expect(html_output.find("div ul li.links a").length).toEqual 2
#          expect(_.pluck(html_output.find("div ul li.links a"),"text"))
#            .toEqual(['show','delete'])
#          expect(html_output.find("div ul li.text")).toHaveText("test text1")
#
#
#      describe "UI event", ->
#        it "hide the view when click delete", ->
#          model = new Model {user_id: 1, scrapbook_id: 1, text:'test text1'}
#          view = new ViewModel(model: model)
#          model.bind 'destroy', view.fadeOutremove
#          $("div#maintest").html(view.render().el)
#          expect($("div#maintest div.scrap")).toBeVisible()
#          $("li.links a.delete").click()
#          @clock.tick(600)
#          expect($("div#maintest div.scrap")).not.toBeVisible()
#
