describe "Scrap.Collection", ->
  require [
    "underscore",
    "backbone",
    "cs!scrapbook/scrap/model",
    "cs!scrapbook/scrap/collection",
    "cs!../spec/support/fixtures/scraps_fixture"
  ], (_, Backbone, Model, Collection, Fixture) ->
    describe "Scrap.Collection", ->
      beforeEach ->
        @collection     = new Collection([],scrapbook_id:1)
        window.Fixture    = Fixture
        @fixture          = new Fixture

        @scrap1 = new Backbone.Model text: "hihi", updated_at: 1
        @scrap2 = new Backbone.Model text: "hello", updated_at: 2
        @scrap3 = new Backbone.Model text: "goodbye", updated_at: 3

        @server = sinon.fakeServer.create()

      afterEach ->
        @server.restore()


      it "is defined", ->
        expect(Collection).toBeDefined()

      it "has url", ->
        expect(@collection.url()).toEqual '/scrapbooks/1/scraps'

      it "has a scrapbook model", ->
        expect(@collection.model).toBeDefined()
        expect(@collection.model.name).toEqual 'Scrap'

      it "should add a model", ->
        @collection.add {text: 'ddyy', scrapbook_id:1 }
        expect(@collection.length).toEqual(1)

      it "sorts by updated_at", ->
        @collection.model = Backbone.Model 
        @collection.add [@scrap2,@scrap1,@scrap3]
        expect(@collection.length).toEqual 3
        expect(@collection.at(0)).toEqual @scrap1
        expect(@collection.at(1)).toEqual @scrap2
        expect(@collection.at(2)).toEqual @scrap3

      it "make the correct request", ->
        @server.respondWith Fixture.validResponse(@fixture.fetch_scraps())
        @collection.fetch()
        @server.respond()
        expect(@server.requests.length).toEqual 1
        expect(@server.requests[0].method).toEqual "GET"
        expect(@server.requests[0].url).toEqual "/scrapbooks/1/scraps"
        expect(@collection.get(1).get("text"))
          .toEqual @fixture.fetch_scraps()[0].text

