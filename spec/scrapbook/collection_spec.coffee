describe "Scrapbook.Collection", ->
  require [
    "underscore",
    "backbone",
    "cs!scrapbook/model",
    "cs!scrapbook/collection",
    "cs!../spec/support/fixtures/scrapbooks_fixture"
  ], (_, Backbone, Model, Collection, Fixture) ->
    describe "Scrapbook.Collection", ->

      beforeEach ->
        @collection     = new Collection
        @fixture          = new Fixture


        @scrapbook1 = new Backbone.Model text: "hihi", updated_at: 1
        @scrapbook2 = new Backbone.Model text: "hello", updated_at: 2
        @scrapbook3 = new Backbone.Model text: "goodbye", updated_at: 3

        @server = sinon.fakeServer.create()

      afterEach ->
        @server.restore()


      it "is defined", ->
        expect(Collection).toBeDefined()

      it "has url", ->
        expect(@collection.url).toEqual '/scrapbooks'

      it "has a scrapbook model", ->
        expect(@collection.model).toBeDefined()
        expect(@collection.model.name).toEqual 'Scrapbook'

      it "should add a model", ->
        @collection.model = Backbone.Model 
        @collection.add { entity_id: 1, user_id: 1 }
        expect(@collection.length).toEqual(1)

      it "sorts by updated_at", ->
        @collection.model = Backbone.Model 
        @collection.add [@scrapbook2,@scrapbook1,@scrapbook3]
        expect(@collection.length).toEqual 3
        expect(@collection.at(0)).toEqual @scrapbook1
        expect(@collection.at(1)).toEqual @scrapbook2
        expect(@collection.at(2)).toEqual @scrapbook3

      it "make the correct request", ->
        @server.respondWith Fixture.validResponse(@fixture.fetch_scrapbooks())
        @collection.fetch()
        @server.respond()
        expect(@server.requests.length).toEqual 1
        expect(@server.requests[0].method).toEqual "GET"
        expect(@server.requests[0].url).toEqual "/scrapbooks"
        expect(@collection.get(1).get("name"))
          .toEqual @fixture.fetch_scrapbooks()[0].name

