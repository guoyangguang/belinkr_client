describe "Scrapbook.Model", ->
  require ["underscore", "cs!scrapbook/model"],
  (_, Model) ->
    describe "Scrapbook.Model", ->
      it "is defined", ->
        expect(Model).toBeDefined()

      it "has a name", ->
        expect(Model.name).toEqual "Scrapbook"

      it "has a urlRoot attribute", ->
        a_model = new Model
        expect(a_model.urlRoot).toEqual "/scrapbooks"

      describe 'default values', ->
        it 'set created_at and updated_at when created', ->
          scrapbook = new Model title: 'Test Text'
          expect(scrapbook.get('created_at')).toBeDefined()
          expect(scrapbook.get('updated_at')).toBeDefined()
