describe "Status.Collection", ->
  require ["jquery", "backbone", "underscore","cs!status/model",
  "cs!status/collection",  "cs!../spec/support/fixtures/statuses_fixture"],
  ($, Backbone, _, Model, Collection, Fixture) ->
    describe "Status.Collection", ->
      beforeEach ->
        @collection     = new Collection()
        @fixture          = new Fixture()
        #spyOn(window, 'StubStatus').andCallFake (value)=>
        #  new Backbone.Model(value)
        @status1 = new Backbone.Model text: "hihi", updated_at: 1
        @status2 = new Backbone.Model text: "hello", updated_at: 2
        @status3 = new Backbone.Model text: "goodbye", updated_at: 3
        @server = sinon.fakeServer.create()

      afterEach ->
        @server.restore()

      it "is defined", ->
        expect(Collection).toBeDefined()

      it "has url", ->
        collection  = new Collection()
        expect(@collection.url).toEqual '/statuses'

      it "has a Status model", ->
        expect(@collection.model).toBeDefined()
        expect(@collection.model.name).toEqual 'Status'

      it "should add a model", ->
        @collection.model = Backbone.Model #StubStatus
        @collection.add { entity_id: 1, user_id: 1 }
        expect(@collection.length).toEqual(1)

      it "sorts by updated_at", ->
        @collection.model = Backbone.Model #StubStatus
        @collection.add [@status2,@status1,@status3]
        expect(@collection.length).toEqual 3
        expect(@collection.at(0)).toEqual @status1
        expect(@collection.at(1)).toEqual @status2
        expect(@collection.at(2)).toEqual @status3

      it "make the correct request", ->
        @server.respondWith Fixture.validResponse(@fixture.fetch_statuses())
        @collection.fetch()
        @server.respond()
        expect(@server.requests.length).toEqual 1
        expect(@server.requests[0].method).toEqual "GET"
        expect(@server.requests[0].url).toEqual "/statuses"
        expect(@collection.get(1).get("text"))
          .toEqual @fixture.fetch_statuses()[0].text


