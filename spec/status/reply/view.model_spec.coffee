describe "ReplyView", ->
  require ["jquery","underscore", "backbone", "cs!status/reply/model",
    "cs!status/reply/collection", "cs!status/reply/view.model"],
  ($,_, Backbone, Model, Collection, ViewModel) ->

    describe "ReplyView", ->
      beforeEach ->
        setFixtures("<div id=content><div id=maintest></div></div>")
        @clock = sinon.useFakeTimers()
      afterEach ->
        @clock.restore()
      it "is defined", ->
        expect(ViewModel).toBeDefined()

      it "has a name", ->
        expect(ViewModel.name).toEqual "ReplyView"

      describe "Instantiation", ->
        it "creates a root element", ->
          model = new Model {user_id: 1, status_id: 1, text:'test text1'}
          view = new ViewModel(model: model)
          expect(view.el.nodeName).toEqual 'LI'

        it "bind events", ->
          model = new Backbone.Model()
          model_spy = sinon.spy(model,"bind")
          view = new ViewModel(model: model)
          expect(model_spy).toHaveBeenCalledTwice()
          expect(model_spy).toHaveBeenCalledWith("change")
          expect(model_spy).toHaveBeenCalledWith("destroy")
          expect(_.keys(view.events)).toContain "click .links .delete"
          model.bind.restore()
          
      describe "Rendering", ->
        beforeEach ->
          setFixtures("<div id=content><div id=maintest></div></div>")
          @clock = sinon.useFakeTimers()
        afterEach ->
          @clock.restore()

        it "return the view object", ->
          model = new Model {user_id: 1, status_id: 1, text:'test text1'}
          view = new ViewModel(model: model)
          itself = view.render()
          expect(itself).toEqual view
 
        it "product the correct html", ->
          require ["cs!prefetch_data/prefetch_models"], (PrefetchModels)=>
            stub = sinon.stub(PrefetchModels, "current_user", ()->
              id:1
              entity_id:1
            )
            model = new Model {user_id: 1, status_id: 1, text:'test text1'}
            view = new ViewModel(model: model)
            html_output = view.render().$el
            expect(html_output.find("div.reply div.timeago").length).toEqual(1)
            expect(html_output.find("div.reply ul.links a").length).toEqual(3)
            expect(_.map(html_output.find("div.reply ul.links a"),(i)->$(i).attr("class")))
              .toEqual(['delete','retweet', 'share'])
            expect(html_output.find("div.reply div.text")).toHaveText("test text1")
            stub.restore()

      describe "UI event", ->
        it "hide the view when click delete", ->
          require ["cs!prefetch_data/prefetch_models"], (PrefetchModels)=>
            stub = sinon.stub(PrefetchModels, "current_user", ()->
              id:1
              entity_id:1
            )
            model = new Model {user_id: 1, status_id: 1, text:'test text1'}
            view = new ViewModel(model: model)
            model.bind 'destroy', view.fadeOutremove
            $("div#maintest").html(view.render().el)
            expect($("div#maintest div.reply")).toBeVisible()
            $(".links .delete").click()
            @clock.tick(600)
            expect($("div#maintest div.reply")).not.toBeVisible()
            stub.restore()
 
