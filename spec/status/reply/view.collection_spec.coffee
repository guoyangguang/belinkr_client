describe "RepliesView", ->
  require ["jquery","underscore", "backbone", "cs!status/reply/model", "cs!status/reply/collection", "cs!status/reply/view.collection"],
  ($, _, Backbone, Model, Collection, ViewCollection) ->
    describe "RepliesView", ->
      beforeEach ->
        setFixtures("<ul class=replies></ul>")
 
      it "is defined", ->
        expect(ViewCollection).toBeDefined()

      it "has a name", ->
        expect(ViewCollection.name).toEqual 'RepliesView'

      describe "Instantiation", ->
        it "bind reset event to the collection", ->
          collection = new Backbone.Collection()
          collection_spy = sinon.spy(collection, "bind")
          view = new ViewCollection(collection: collection)
          expect(collection_spy).toHaveBeenCalledTwice()
          expect(collection_spy).toHaveBeenCalledWith("add")
          expect(collection_spy).toHaveBeenCalledWith("reset")
          collection.bind.restore()

      describe "UI Event", ->
        it "invoke render_one method to render the model view", ->
          collection     = new Collection([],status_id:1)
          model1 = new Model {user_id: 1, status_id:1, text:'test text1'}
          model2 = new Model {user_id: 1, status_id:1, text:'test text2'}
          collection.add [model1, model2]
          view = new ViewCollection(collection: collection)
          render_one_spy = sinon.spy(view,"render_one")
          view.render()
          expect(render_one_spy).toHaveBeenCalledWith(model1)
          expect(render_one_spy).toHaveBeenCalledWith(model2)
          expect($("ul.replies",view.el).find('div.text').length).toEqual 2 

        it "attach the el to the dom", ->
          collection1 = new Collection([],status_id:3)
          collection1.add {user_id: 1, text:'test reply'}
          collection1.add {user_id: 1, text:'test reply 2'}
          view = new ViewCollection(collection: collection1)
          view.render()
          expect($("ul.replies",view.el).find('div.text').first()).toHaveText("test reply")
          view.remove()

