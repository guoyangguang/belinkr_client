describe "Reply.Model", ->
  require ["underscore", "cs!status/reply/model"],
  (_, Model) ->
    describe "Reply.Model", ->
      it "is defined", ->
        expect(Model).toBeDefined()

      it "has a name", ->
        expect(Model.name).toEqual "Reply"

      it "has a urlRoot attribute", ->
        a_model = new Model(status_id:2)
        expect(a_model.urlRoot()).toEqual "/statuses/2/replies"

      describe 'default values', ->
        it 'set created_at and updated_at when created', ->
          status = new Model title: 'Test Text'
          expect(status.get('created_at')).toBeDefined()
          expect(status.get('updated_at')).toBeDefined()
