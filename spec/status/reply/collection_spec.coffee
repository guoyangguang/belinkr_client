describe "Reply.Collection", ->
  require [
    "underscore",
    "backbone",
    "cs!status/reply/model",
    "cs!status/reply/collection",
    "cs!../spec/support/fixtures/replies_fixture"
  ], (_, Backbone, Model, Collection, Fixture) ->
    describe "Reply.Collection", ->
      beforeEach ->
        @collection     = new Collection([],status_id:1)
        @fixture          = new Fixture

        @reply1 = new Backbone.Model text: "hihi", updated_at: 1
        @reply2 = new Backbone.Model text: "hello", updated_at: 2
        @reply3 = new Backbone.Model text: "goodbye", updated_at: 3

        @server = sinon.fakeServer.create()

      afterEach ->
        @server.restore()


      it "is defined", ->
        expect(Collection).toBeDefined()

      it "has url", ->
        expect(@collection.url()).toEqual '/statuses/1/replies'

      it "has a reply model", ->
        expect(@collection.model).toBeDefined()
        expect(@collection.model.name).toEqual 'Reply'

      it "should add a model", ->
        @collection.add {text: 'ddyy', status_id:1 }
        expect(@collection.length).toEqual(1)

      it "sorts by updated_at", ->
        @collection.model = Backbone.Model 
        @collection.add [@reply2,@reply1,@reply3]
        expect(@collection.length).toEqual 3
        expect(@collection.at(0)).toEqual @reply1
        expect(@collection.at(1)).toEqual @reply2
        expect(@collection.at(2)).toEqual @reply3

      it "make the correct request", ->
        @server.respondWith Fixture.validResponse(@fixture.fetch_replies())
        @collection.fetch()
        @server.respond()
        expect(@server.requests.length).toEqual 1
        expect(@server.requests[0].method).toEqual "GET"
        expect(@server.requests[0].url).toEqual "/statuses/1/replies"
        expect(@collection.get(1).get("text"))
          .toEqual @fixture.fetch_replies()[0].text

