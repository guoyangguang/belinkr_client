describe "StatusesView", ->
  require ["jquery","underscore", "backbone", "cs!status/model", "cs!status/collection", "cs!status/view.collection"],
  ($, _, Backbone, Model, Collection, ViewCollection) ->
    describe "StatusesView", ->
      beforeEach ->
        setFixtures("<div id=maintest></div>")
      it "is defined", ->
        expect(ViewCollection).toBeDefined()

      it "has a name", ->
        expect(ViewCollection.name).toEqual 'StatusesView'

      describe "Instantiation", ->
        it "bind both add and reset event to the collection", ->
          collection = new Backbone.Collection()
          collection_spy = sinon.spy(collection, "bind")
          view = new ViewCollection(collection: collection)
          expect(collection_spy).toHaveBeenCalledTwice()
          expect(collection_spy).toHaveBeenCalledWith("add")
          expect(collection_spy).toHaveBeenCalledWith("reset")
          collection.bind.restore()

      describe "UI Event", ->
        it "invoke add_one method to render the model view", ->
          collection = new Collection()
          model1 = new Model {user_id: 1, entity_id: 1, text:'test text1'}
          model2 = new Model {user_id: 1, entity_id: 1, text:'test text2'}
          collection.add [model1, model2]
          view = new ViewCollection(collection: collection)
          render_one_spy = sinon.spy(view,"render_one")
          view.render()
          $("div#maintest").html(view.el)
          expect(render_one_spy).toHaveBeenCalledWith(model1)
          expect(render_one_spy).toHaveBeenCalledWith(model2)
          expect($("#maintest ul li.status").find('div.text').length).toEqual 2 

        it "attach the el to the dom", ->
          collection1 = new Collection()
          collection1.add {user_id: 1, entity_id: 1, text:'test text'}
          view = new ViewCollection(collection: collection1)
          view.render()
          $("div#maintest").html(view.el)
          expect($("#maintest ul li.status").find('div.text')).toHaveText("test text")

        it "click the submit button", ->
          collection1 = new Collection()
          collection1.add {user_id: 1, entity_id: 1, text:'test text'}
          spy = sinon.spy(ViewCollection.prototype, "create")
          view = new ViewCollection(collection: collection1)
          view.render()
          $("div#maintest").html(view.el)
          server = sinon.fakeServer.create()
          server.respondWith([201,"Content-Type": "application/json",
            '{"id":61,"text":"test text2"}'])
          view.$('textarea').val('test text2')
          view.$('.submit').click()
          server.respond()
          expect(spy).toHaveBeenCalled()
          expect(collection1.first().get("text")).toEqual "test text"
          expect(collection1.last().get("text")).toEqual "test text2"
          spy.restore()
          server.restore()


