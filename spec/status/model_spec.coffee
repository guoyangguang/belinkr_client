describe "Status.Model", ->
  require ["jquery", "underscore","cs!status/model","cs!status/reply/collection"],
  ($, _, Model, Replies) ->
    describe "Status.Model", ->
 
      it "is defined", ->
        expect(1).toEqual 1
        expect(Model).toBeDefined()
  
      it "has a name", ->
        expect(Model.name).toEqual "Status"
  
      it "has a urlRoot attribute", ->
        a_model = new Model
        expect(a_model.urlRoot).toEqual "/statuses"
  
      describe 'default values', ->
        it 'set created_at and updated_at when created', ->
          status = new Model text: 'Test Text'
          expect(status.get('created_at')).toBeDefined()
          expect(status.get('updated_at')).toBeDefined()
      describe 'get_replies', ->
        it 'get replies', ->
          status = new Model(id: 1, text: 'Test Text', replies: [
            {text: 'a reply'},
            {text: '2nd reply'}
          ])
          expect(status.get_replies().constructor.name).toEqual('ReplyCollection')
          expect(status.get_replies().length).toEqual(2)
          expect(status.get_replies().at(0).get 'text').toEqual 'a reply'
          expect(status.get_replies().at(0).constructor.name).toEqual 'Reply'
          expect(status.get_replies().at(1).get 'text').toEqual '2nd reply'
        it 'get empty replies when there is no replies attributes', ->
          status = new Model(id: 1, text: 'Test Text')
          expect(status.get_replies().constructor.name).toEqual('ReplyCollection')
          expect(status.get_replies().length).toEqual(0)
 
