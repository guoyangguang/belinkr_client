describe "StatusView", ->
  require ["jquery","underscore", "backbone", "cs!status/model",
  "cs!status/collection", "cs!status/view.model",
  "cs!status/reply/collection"],
  ($,_, Backbone, Model, Collection, ViewModel,Replies) ->
    describe "StatusView", ->
      beforeEach ->
        setFixtures("<div id=main></div>")
      it "is defined", ->
        expect(ViewModel).toBeDefined()

      it "has a name", ->
        expect(ViewModel.name).toEqual "StatusView"

      describe "Instantiation", ->
        it "creates a root element", ->
          model = new Model {user_id: 1, entity_id: 1, text:'test text1'}
          view = new ViewModel(model: model)
          expect(view.el.nodeName).toEqual 'LI'

        it "bind events", ->
          model = new Backbone.Model()
          model_spy = sinon.spy(model,"bind")
          view = new ViewModel(model: model)
          expect(model_spy).toHaveBeenCalledTwice()
          expect(model_spy).toHaveBeenCalledWith("change")
          expect(model_spy).toHaveBeenCalledWith("destroy")
          expect(_.keys(view.events)).toContain "click .links .delete"
          expect(_.keys(view.events)).toContain "click .links .new_reply"
          expect(_.keys(view.events)).toContain "click .submit"
          expect(_.keys(view.events)).toContain "click .links .retweet"
          model.bind.restore()
          
      describe "Rendering", ->
        it "return the view object", ->
          model = new Model {user_id: 1, entity_id: 1, text:'test text1'}
          view = new ViewModel(model: model)
          itself = view.render()
          expect(itself).toEqual view
 
        it "produce the correct html if the current_user is author", ->
          require ["cs!prefetch_data/prefetch_models"], (PrefetchModels)=>
            stub = sinon.stub(PrefetchModels, "current_user", ()->
              id: 1,
              entity_id: 1
            )
            model = new Model {user_id: 1, entity_id: 1, text:'test text1'}
            view = new ViewModel(model: model)
            html_output = view.render().$el
            expect(html_output.find("div ul div.user_id")).toHaveText("1")
            expect(html_output.find("div ul div.timeago").length).toEqual(1)

            expect(html_output.find("div ul.links li a").length).toEqual 3
            expect(_.map(html_output.find("div ul.links li a"),(i)->$(i).attr('class')))
               .toEqual(['delete', 'new_reply', 'share'])

            expect(html_output.find("div div.text")).toHaveText("test text1")
            stub.restore()

        it "produce the correct html if the current_user is not author", ->
          require ["cs!prefetch_data/prefetch_models"], (PrefetchModels)=>
            stub = sinon.stub(PrefetchModels, "current_user", ()->
              id: 4321,
              entity_id: 1
            )
            model = new Model {user_id: 1, entity_id: 1, text:'test text1'}
            view = new ViewModel(model: model)
            html_output = view.render().$el
            expect(html_output.find("div ul div.user_id")).toHaveText("1")
            expect(html_output.find("div ul div.timeago").length).toEqual(1)

            expect(html_output.find("div ul.links li a").length).toEqual 3
            expect(_.map(html_output.find("div ul.links li a"),(i)->$(i).attr('class')))
            .toEqual(['new_reply', 'retweet', 'share'])

            expect(html_output.find("div div.text")).toHaveText("test text1")
            stub.restore()
 
        it "render the correct reply form", ->
          model = new Model {user_id: 1, entity_id: 1, text:'test text1'}
          spy_event=sinon.spy(ViewModel.prototype,'submit_new_reply')
          spy_create=sinon.spy(Replies.prototype,'create')
          view = new ViewModel(model: model)
          html_output = view.render().$el
          view.$el.find(".links a.new_reply").trigger('click')
          expect(html_output.find("form.reply").length).toEqual(1)
          view.$el.find("textarea").val('sample reply')

          spy_get_replies=sinon.spy(view.model,'get_replies')
          view.$el.find(".submit").trigger('click')
          expect(spy_event).toHaveBeenCalled()
          expect(spy_get_replies).toHaveBeenCalled()
          expect(spy_create).toHaveBeenCalledWith(
            text: 'sample reply'
            status_id:view.model.id
            user_id:1
            files: []
          )

          spy_event.restore()
          spy_get_replies.restore()
          spy_create.restore()

        it "render files", ->
          model = new Model
            user_id: 1
            entity_id: 1
            text:'test text1'
            files: [{id: 'fileid', original_filename: 'filename.pdf', mime_type: 'application/pdf' }]
          view = new ViewModel(model: model)
          view.render()
          expect(_.isEmpty(view.$el.find("ul.files").find("li"))).toBeFalsy()
          expect(view.$el.find("ul.files")).toHaveText(/filename.pdf/)

        it "render replies", ->

      describe "viewer_is_author", ->
        it "return true if status author is current_user", ->
          require ["cs!prefetch_data/prefetch_models"], (PrefetchModels)=>
            stub = sinon.stub(PrefetchModels, "current_user", ()->
              id:1,
              entity_id:1
            )
            model = new Model
              user_id: 1
              entity_id: 1
              text:'test text1'
              files: []
            view = new ViewModel(model: model)
            expect(view.viewer_is_author()).toBeTruthy()
            stub.restore()

        it "return false if current_user is not author", ->
          model = new Model
            user_id: 1
            entity_id: 1
            text:'test text1'
            files: []
          view = new ViewModel(model: model)
          expect(view.viewer_is_author()).toBeFalsy()

      describe "display_enforcer", ->
        it "wont display the retweet if the current_user is author", ->
          require ["cs!prefetch_data/prefetch_models"], (PrefetchModels)=>
            stub = sinon.stub(PrefetchModels, "current_user", ()->
              id:1
              entity_id:1
            )
            model = new Model
              user_id: 1
              entity_id: 1
              text:'test text1'
              files: []
            view = new ViewModel(model: model)
            view.render()
            expect(view.$el.find("a.retweet").length).toEqual(0)
            stub.restore()

        it "wont display the trash if the current_user is not author", ->
          model = new Model
            user_id: 1
            entity_id: 1
            text:'test text1'
            files: []
          view = new ViewModel(model: model)
          view.render()
          expect(view.$el.find("a.delete").length).toEqual(0)

      describe "retweet", ->
        it "status forwarded", ->
          require ["cs!prefetch_data/prefetch_models"], (PrefetchModels)=>
            stub = sinon.stub(PrefetchModels, "current_user", ()->
              id:1234
              entity_id:1
            )
            spy       = sinon.spy(ViewModel.prototype, 'appendChildInto')
            spy_event = sinon.spy(ViewModel.prototype, 'retweet')
            server = sinon.fakeServer.create()
            server.respondWith("PUT", "/statuses/forwarded/1",
              [200, { "Content-Type": "application/json" },
              '{ "id": 1, "user_id": 1, "text": "test text1", "forwarder_id": 1}'])
            model = new Model
              id: 1
              user_id: 1
              entity_id: 1
              text:'test text1'
              files: []
            view = new ViewModel(model: model)
            view.render()
            view.$el.find("a.retweet").trigger('click')
            expect(spy_event).toHaveBeenCalled()
            server.respond()
            server.restore()
            expect(spy).toHaveBeenCalled()
            spy.restore()
            spy_event.restore()
            stub.restore()
