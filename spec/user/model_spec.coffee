describe "User.Model", ->
  require ["jquery", "underscore","cs!user/model"],
  ($, _, Model) ->
    describe "User.Model", ->
 
      it "is defined", ->
        expect(1).toEqual 1
        expect(Model).toBeDefined()
  
      it "has a name", ->
        expect(Model.name).toEqual "User"
  
      it "has a urlRoot attribute", ->
        a_model = new Model
        expect(a_model.urlRoot).toEqual "/users"
  
      describe 'default values', ->
        it 'set created_at and updated_at when created', ->
          user = new Model first: 'First', last: 'Last'
          expect(user.get('created_at')).toBeDefined()
          expect(user.get('updated_at')).toBeDefined()

