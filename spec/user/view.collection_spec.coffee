describe "UsersView", ->
  require ["jquery","underscore", "backbone", "cs!user/model", "cs!user/paginator.collection", "cs!user/view.collection"],
  ($, _, Backbone, Model, Collection, ViewCollection) ->
    describe "UsersView", ->
      beforeEach ->
        setFixtures("<div id=maintest></div>")
      it "is defined", ->
        expect(ViewCollection).toBeDefined()

      it "has a name", ->
        expect(ViewCollection.name).toEqual 'UsersView'

      describe "Instantiation", ->
        it "bind both add and reset event to the collection", ->
          collection = new Backbone.Collection()
          collection_spy = sinon.spy(collection, "bind")
          view = new ViewCollection(collection: collection)
          expect(collection_spy).toHaveBeenCalledTwice()
          expect(collection_spy).toHaveBeenCalledWith("add")
          expect(collection_spy).toHaveBeenCalledWith("reset")
          collection.bind.restore()

      describe "UI Event", ->
        it "invoke add_one method to render the model view", ->
          collection = new Collection()
          model1 = new Model {entity_id: 1, first: 'F', last: 'L', email:'a@b.c'}
          model2 = new Model {entity_id: 1, first: 'f', last: 'l', email:'b@b.c'}
          collection.add [model1, model2]
          view = new ViewCollection(collection: collection)
          render_one_spy = sinon.spy(view,"render_one")
          view.render()
          $("div#maintest").html(view.el)
          expect(render_one_spy).toHaveBeenCalledWith(model1)
          expect(render_one_spy).toHaveBeenCalledWith(model2)
          expect($("#maintest ul li.card").find('li.email').length).toEqual 2 

        it "attach the el to the dom", ->
          collection1 = new Collection()
          collection1.add {entity_id: 1, first: 'F', last: 'L', email:'a@b.c'}
          view = new ViewCollection(collection: collection1)
          view.render()
          $("div#maintest").html(view.el)
          expect($("#maintest ul li.card").find('li.email')).toHaveText("a@b.c")

        it "click the show more button", ->
          collection1 = new Collection()
          collection1.add {entity_id: 1, first: 'F', last: 'L', email:'a@b.c'}
          spy = sinon.spy(ViewCollection.prototype, "nextResultPage")
          view = new ViewCollection(collection: collection1)
          view.render()
          $("div#maintest").html(view.el)

          expect(collection1.page).toEqual 0
          server = sinon.fakeServer.create()
          server.respondWith([201,"Content-Type": "application/json",
            '{"id":61,"first":"F2", "last":"L2", "email":"a2@b.c"}'])
          view.$('.btn').click()
          server.respond()
          expect(spy).toHaveBeenCalled()

          #it is a paginator, so get the next page collections
          expect(collection1.page).toEqual 1
          expect(collection1.length).toEqual 1
          expect(collection1.first().get("first")).toEqual "F2"

          spy.restore()
          server.restore()


