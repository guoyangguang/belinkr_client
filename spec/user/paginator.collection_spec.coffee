describe "User.Collection", ->
  require ["jquery", "backbone", "underscore","cs!user/model",
  "cs!user/paginator.collection",  "cs!../spec/support/fixtures/users_fixture"],
  ($, Backbone, _, Model, Collection, Fixture) ->
    describe "User.Collection", ->
      beforeEach ->
        @collection     = new Collection()
        @fixture          = new Fixture()
        #spyOn(window, 'StubUser').andCallFake (value)=>
        #  new Backbone.Model(value)
        @server = sinon.fakeServer.create()

      afterEach ->
        @server.restore()

      it "is defined", ->
        expect(Collection).toBeDefined()

      it "has url", ->
        collection  = new Collection()
        expect(@collection.url).toEqual '/users?'

      it "has a User model", ->
        expect(@collection.model).toBeDefined()
        expect(@collection.model.name).toEqual 'User'

      it "should add a model", ->
        @collection.model = Backbone.Model #StubUser
        @collection.add { entity_id: 1, first: 'First', last: 'Last'}
        expect(@collection.length).toEqual(1)

      it "make the correct request", ->
        @server.respondWith Fixture.validResponse(@fixture.fetch_users())
        @collection.fetch()
        @server.respond()
        expect(@server.requests.length).toEqual 1
        expect(@server.requests[0].method).toEqual "GET"
        expect(@server.requests[0].url).toMatch "/users?"
        expect(@collection.get(1).get("first"))
          .toEqual @fixture.fetch_users()[0].first


