describe "UserView", ->
  require ["jquery","underscore", "backbone", "cs!user/model",
  "cs!user/collection", "cs!user/view.model",
  "cs!../spec/support/fixtures/users_fixture"],
  ($,_, Backbone, Model, Collection, ViewModel, Fixture) ->
    describe "UserView", ->
      beforeEach ->
        setFixtures("<div id=main></div>")
        @fixture = new Fixture()
      it "is defined", ->
        expect(ViewModel).toBeDefined()

      it "has a name", ->
        expect(ViewModel.name).toEqual "UserView"

      describe "Instantiation", ->
        it "creates a root element", ->
          model = new Model {entity_id: 1, first:'F', last:'L', email:'e@c.cn'}
          view = new ViewModel(model: model)
          expect(view.el.nodeName).toEqual 'LI'
          expect(view.el.className).toEqual 'card'

      describe "Rendering", ->
        it "return the view object", ->
          model = new Model {entity_id: 1, first:'F', last:'L', email:'e@c.cn'}
          view = new ViewModel(model: model)
          itself = view.render()
          expect(itself).toEqual view
 
        it "product the correct html", ->
          model = new Model {entity_id: 1, first:'F', last:'L', email:'e@c.cn'}
          view = new ViewModel(model: model)
          html_output = view.render().$el
          expect(html_output.find("li.first").length).toEqual(1)
          expect(html_output.find("li.first")).toHaveText("F")
          expect(html_output.find("li.last")).toHaveText("L")
          expect(html_output.find("li.email")).toHaveText("e@c.cn")
          expect(html_output.find("button label")).toHaveText("follow")
          

      describe "Follow a User", ->
        beforeEach ->
          @spy = sinon.spy(ViewModel.prototype,"follow")
          @spy_model = sinon.spy(Model.prototype,"set")
          @server = sinon.fakeServer.create()
          @server.respondWith Fixture.validResponse(@fixture.follow_user())
          
        afterEach ->
          @spy.restore()
          @spy_model.restore()
          @server.restore()

        it "click follow butten", ->
          model = new Model 
            entity_id: 1
            first:'FF' 
            last:'LL'
            relationship: 'none'
            email:'e@c'
          view = new ViewModel(model: model)
          html_output = view.render().$el
          expect(html_output.find("button label")).toHaveText("follow")

          view.$el.find('button.btn-primary').trigger('click')
          @server.respond()
          expect(@spy).toHaveBeenCalled()
          expect(@spy_model).toHaveBeenCalled()

          
