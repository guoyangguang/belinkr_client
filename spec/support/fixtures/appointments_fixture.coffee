define ["underscore","json2"],(_,JSON)->
  class Fixture extends RequestMixin
    fetch_appointments: ->
      appointment_fb = new FixtureBuilder(Specimens.Appointment)
      appointment_list= _.map _.range(1, 11), (i)->
        appointment_fb.validFixture
          defaults:
            id: i
