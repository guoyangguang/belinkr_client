define ["underscore","json2"],(_,JSON)->
  class Fixture extends RequestMixin
    fetch_scrapbooks: ->
      scrapbook_fb = new FixtureBuilder(Specimens.Scrapbook)
      scrapbook_list= _.map _.range(1, 11), (i)->
        scrapbook_fb.validFixture
          defaults:
            id: i
