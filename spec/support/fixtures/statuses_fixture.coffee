define ["underscore","json2"],(_,JSON)->
  class Fixture extends RequestMixin
    fetch_statuses: ->
      status_fb = new FixtureBuilder(Specimens.Status)
      status_list= _.map _.range(1, 11), (i)->
        status_fb.validFixture
          defaults:
            id: i