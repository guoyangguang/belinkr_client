define ["underscore","json2"],(_,JSON)->
  class Fixture extends RequestMixin
    fetch_replies: ->
      reply_fb = new FixtureBuilder(Specimens.Reply)
      reply_list= _.map _.range(1, 11), (i)->
        reply_fb.validFixture
          defaults:
            id: i
