define ["underscore","json2"],(_,JSON)->
  class Fixture extends RequestMixin
    fetch_scraps: ->
      scrap_fb = new FixtureBuilder(Specimens.Scrap)
      scrap_list= _.map _.range(1, 11), (i)->
        scrap_fb.validFixture
          defaults:
            id: i
