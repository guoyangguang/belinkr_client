define ["underscore","json2"],(_,JSON)->
  class Fixture extends RequestMixin
    fetch_users: ->
      user_fb = new FixtureBuilder(Specimens.User)
      user_list= _.map _.range(1, 11), (i)->
        user_fb.validFixture
          defaults:
            id: i
    follow_user: ->
      user_fb = new FixtureBuilder(Specimens.User)
      user_fb.validFixture
        defaults:
          relationship: 'follower'
