class RequestMixin
  @validResponse: (responseText)->
    [
      200,
      "Content-Type": "applilcation/json",
      JSON.stringify responseText
    ]
  @invalidResponse: (responseText)->
    [
      404,
      "Content-Type": "applilcation/json",
      JSON.stringify responseText
    ]

window.RequestMixin = RequestMixin

class FixtureBuilder
  constructor: (specimen) ->
    @specimen = specimen

  validFixture: (options) ->
    specimenClone = _.clone(@specimen.defaults)
    if options.defaults
      specimenClone = _.extend(specimenClone, options.defaults)

    Specimens.extractAttrs(specimenClone, specimenClone)

  invalidFixture: (options) ->
    specimenClone = _.clone(@specimen.defaults)

    if options.defaults
      specimenClone = _.extend(specimenClone, options.defaults)

    fixture = Specimens.extractAttrs(specimenClone, specimenClone)

    excepts = options.excepts || @specimen.mildewy

    if _.isArray(excepts)
      if excepts.length > 0
#        seed = Math.floor(Math.random()*excepts.length + 1) - 1
        castrated_fixture = {}
        left_keys = _.without(_.keys(fixture), excepts)
        _.each(left_keys, (_key)->
          castrated_fixture[_key] = fixture[_key]
        )
        castrated_fixture
    else
      {}
window.FixtureBuilder = FixtureBuilder