window.Specimens ||= {}
Specimens.Workspace =
  defaults:
    "id": 1,
    "name": "Workspace {{id}}",
    "entity_id": 1,
    "created_at": new Date(),
    "updated_at": new Date(),
    "deleted_at": null,
    "statuses": "/workspaces/{{id}}/statuses",
    "collaborators": "/workspaces/{{id}}/collaborators",
    "administrators": "/workspaces/{{id}}/administrators",
    "links":
      "self": "/workspaces/{{id}}"
  mildewy: ["name", "entity_id"]

Specimens.Status =
  defaults:
    "id": 1,
    "entity_id": 1,
    "user_id": 1,
    "text": "my status{{id}}",
    "created_at": new Date(),
    "updated_at": new Date(),
  mildewy: ["user_id", "entity_id", "text"]

Specimens.Scrapbook =
  defaults:
    "id": 1,
    "name": "my name",
    "user_id": 1,
    "created_at": new Date(),
    "updated_at": new Date(),
  mildewy: ["user_id", "name"]

Specimens.Scrap =
  defaults:
    "id": 1,
    "text": "my name",
    "user_id": 1,
    "scrapbook_id": 1,
    "created_at": new Date(),
    "updated_at": new Date(),
  mildewy: ["user_id","scrapbook_id"]

Specimens.Reply =
  defaults:
    "id": 1,
    "text": "my reply",
    "user_id": 1,
    "status_id": 1,
    "created_at": new Date(),
    "updated_at": new Date(),
  mildewy: ["user_id","status_id"]

Specimens.Appointment =
  defaults:
    "id": 1,
    "entity_id": 1,
    "user_id": 1,
    "text": "my appointment{{id}}",
    "start_at": "test start at",
    "created_at": new Date(),
    "updated_at": new Date()
  mildewy: ["user_id", "entity_id", "text"]

Specimens.User =
  defaults:
    "id": 1,
    "entity_id": 1,
    "first": "F",
    "last": "L",
    "email": "e@c.cn",
    "created_at": new Date(),
    "updated_at": new Date()
  mildewy: ["entity_id"]



Specimens.extractAttrs = (pairs, upLevel) ->
  _.each(_.keys(pairs), (_key) ->
    if _.isString(pairs[_key])
      reg = /\{\{.[^/]*\}\}/ig
      if reg.test(pairs[_key])
        matches = pairs[_key].match(reg)
        if matches.length > 0
          _.each(matches, (m) ->
            field = _.clone(m).replace(/\{/g, '').replace(/\}/g, '')
            if field.indexOf(".") > 0
              field_nested = field.split(".").toString()
              field_nested = field_nested.replace(/,/g, '"]["')
              value1 = null
              value2 = null
              try
                value1 = eval('pairs["' + field_nested + '"]')
              catch e
                e
              try
                value2 = eval('upLevel["' + field_nested + '"]')
              catch e
                e
              fieldValue = value1 || value2
              if fieldValue
                pairs[_key] = pairs[_key].replace(m, fieldValue)
            else
              fieldValue = pairs[field] || upLevel[field]
              pairs[_key] = pairs[_key].replace(m, fieldValue)
          )
    else
      try
        if _.keys(pairs[_key]).length > 0
          Specimens.extractAttrs(pairs[_key], pairs)
      catch e
        e
  )
  pairs
