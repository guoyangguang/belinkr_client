describe "FileView", ->
  require ["jquery","underscore", "backbone", "cs!file/view.model"],
  ($,_, Backbone, ViewModel) ->
    describe "FileView", ->
      beforeEach ->
        setFixtures("<div id=main></div>")
      it "is defined", ->
        expect(ViewModel).toBeDefined()

      it "has a name", ->
        expect(ViewModel.name).toEqual "FileView"

      describe "Instantiation", ->
        it "raise an exception if templdate directive data not present", ->
          expect(()->
            new ViewModel
          ).toThrow()

        it "create a root element", ->
          view = new ViewModel(
            data:
              id: 'foo_bar'
          )
          expect(view.el.nodeName).toEqual 'LI'

      describe "Render", ->
        it "render the form template", ->
          mimes = {}
          mimes['image/jpg'] = 'image'
          mimes['application/pdf'] = 'pdf'
          mimes['inode/x-empty'] = 'other'
          _.each mimes, (css, mime)->
            view = new ViewModel(
              data:
                id: 'foo_bar'
                mime_type: mime
                original_filename: 'foo_bar.ext'
            )
            expect(view.$el.find("[id$='foo_bar']").length > 0).toBeTruthy()
            expect(view.$el).toHaveText(/foo_bar.ext/)
            tag = view.$el.find("a")
            expect(tag).toHaveClass(css)
            expect(tag.attr("href").indexOf('foo_bar') > 0).toBeTruthy()