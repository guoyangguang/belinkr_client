describe "App", ->
  require [
    "underscore",
    "backbone",
    "cs!router",
    "cs!status/collection",
    "cs!status/view.collection"
  ], (_, Backbone, Router,Collection,ViewCollection) ->
    describe "App", ->
      describe "the the routing", ->
        beforeEach ->
          setFixtures("<div><div id=main></div><div id=sidebar></div><div id=content></div></div>")
    
        # the following specs are not needed because it is testing against the
        # backbone route lib, not testing against the app we are developing
        it "fires the default route with any other nonempty hash", ->
          @routeSpy = sinon.spy(Router.prototype,"defaultRoute")
          @router = new Router()
          @router.navigate "anyaction", true
          expect(@routeSpy).toHaveBeenCalledOnce()
          @routeSpy.restore()
    
        it "fires the statuses route", ->
          @routeSpy = sinon.spy(Router.prototype,"statuses")
          @router = new Router()
          @router.navigate "statuses", true
          expect(@routeSpy).toHaveBeenCalled()
          @routeSpy.restore()
    
        it "fires the scrapbooks route", ->
          @routeSpy = sinon.spy(Router.prototype,"scrapbooks")
          @router = new Router()
          @router.navigate "scrapbooks", true
          expect(@routeSpy).toHaveBeenCalled()
          @routeSpy.restore()
    
        it "fires the appointments route", ->
          @routeSpy = sinon.spy(Router.prototype,"appointments")
          @router = new Router()
          @router.navigate "appointments", true
          expect(@routeSpy).toHaveBeenCalled()
          @routeSpy.restore()
    
      describe "render Statuses View", ->
        beforeEach ->
          @router = new Router()
        it "create a Statuses list view and collection and return the view", ->
          view = @router.statuses(1)
          expect(view.collection).toBeDefined()
          expect(view.collection.model).toBeDefined()
    
      describe "render Scrapbooks View", ->
        beforeEach ->
          @router = new Router()
        it "create a Scrapbooks list view and collection and return the view", ->
          view = @router.scrapbooks(1)
          expect(view.collection).toBeDefined()
          expect(view.collection.model).toBeDefined()
    
      describe "render Appointments View", ->
        beforeEach ->
          @router = new Router()
        it "create a Appointments list view and collection and return the view", ->
          view = @router.appointments(1)
          expect(view.collection).toBeDefined()
          expect(view.collection.model).toBeDefined()
    
