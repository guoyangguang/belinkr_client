describe "StatusesView", ->
  require ["jquery","underscore", "backbone", "cs!appointment/model", "cs!appointment/collection", "cs!appointment/view.collection"],
  ($, _, Backbone, Model, Collection, ViewCollection) ->
    describe "StatusesView", ->
      beforeEach ->
        setFixtures("<div id=main></div>")
      it "is defined", ->
        expect(ViewCollection).toBeDefined()

      it "has a name", ->
        expect(ViewCollection.name).toEqual 'AppointmentsView'

      describe "Instantiation", ->
        it "bind add, reset, chage and destroy event to the collection", ->
          collection = new Backbone.Collection()
          collection_spy = sinon.spy(collection, "bind")
          view = new ViewCollection(collection: collection)
          expect(collection_spy).toHaveBeenCalledWith("add")
          expect(collection_spy).toHaveBeenCalledWith("reset")
          expect(collection_spy).toHaveBeenCalledWith("change")
          expect(collection_spy).toHaveBeenCalledWith("destroy")
          collection_spy.restore()

      describe "UI Event", ->
        it "create a new appointment view when click a day", ->
          #FullCalendar doesn't respond to $().click()
          #So all UI Event Spec in fullcalendar need to test
          #by its methods http://arshaw.com/fullcalendar/docs/
          collection = new Collection()
          select_spy = sinon.spy(ViewCollection.prototype,"select")
          view = new ViewCollection(collection: collection)
          view.render()
          $("#main").append(view.el)
          view.onShow()
          expect($("#eventDialog").length).toEqual 0
          $('#calendar').fullCalendar('select', new Date(), new Date())
          expect(select_spy).toHaveBeenCalled()
          expect($("#eventDialog").length).toEqual 1
     
