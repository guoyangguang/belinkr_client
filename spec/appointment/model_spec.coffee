describe "Appointment.Model", ->
  require ["underscore", "cs!appointment/model"],
  (_, Model) ->
    describe "Appointment.Model", ->
      it "is defined", ->
        expect(Model).toBeDefined()
    
      it "has a name", ->
        expect(Model.name).toEqual "Appointment"
    
      it "has a urlRoot attribute", ->
        a_model = new Model
        expect(a_model.urlRoot).toEqual "/appointments"
    
      describe 'default values', ->
        it 'set created_at and updated_at when created', ->
          appointment = new Model text: 'Test Text'
          expect(appointment.get('created_at')).toBeDefined()
          expect(appointment.get('updated_at')).toBeDefined()

