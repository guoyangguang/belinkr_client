describe "Appointment.Collection", ->
  require [
    "underscore",
    "backbone",
    "cs!appointment/model",
    "cs!appointment/collection",
    "cs!../spec/support/fixtures/appointments_fixture"
  ], (_, Backbone, Model, Collection, Fixture) ->
    describe "Appointment.Collection", ->
      beforeEach ->
        @collection     = new Collection
        @fixture          = new Fixture

        #  new Backbone.Model(value)
        @appointment1 = new Backbone.Model text: "hihi", updated_at: 1
        @appointment2 = new Backbone.Model text: "hello", updated_at: 2
        @appointment3 = new Backbone.Model text: "goodbye", updated_at: 3

        @server = sinon.fakeServer.create()

      afterEach ->
        @server.restore()

      it "is defined", ->
        expect(Collection).toBeDefined()

      it "has url", ->
        expect(@collection.url).toEqual '/appointments'

      it "has a Appointment model", ->
        expect(@collection.model).toBeDefined()
        expect(@collection.model.name).toEqual 'Appointment'

      it "should add a model", ->
        @collection.model = Backbone.Model 
        @collection.add { entity_id: 1, user_id: 1 }
        expect(@collection.length).toEqual(1)

      it "sorts by updated_at", ->
        @collection.model = Backbone.Model
        @collection.add [@appointment2,@appointment1,@appointment3]
        expect(@collection.length).toEqual 3
        expect(@collection.at(0)).toEqual @appointment1
        expect(@collection.at(1)).toEqual @appointment2
        expect(@collection.at(2)).toEqual @appointment3

      it "make the correct request", ->
        @server.respondWith Fixture.validResponse(@fixture.fetch_appointments())
        @collection.fetch()
        @server.respond()
        expect(@server.requests.length).toEqual 1
        expect(@server.requests[0].method).toEqual "GET"
        expect(@server.requests[0].url).toEqual "/appointments"
        expect(@collection.get(1).get("text"))
          .toEqual @fixture.fetch_appointments()[0].text

