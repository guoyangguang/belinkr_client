describe "UploaderFormView", ->
  require ["jquery","underscore", "backbone", "cs!uploader/view.form"],
  ($,_, Backbone, ViewModel) ->
    describe "UploaderFormView", ->
      beforeEach ->
        setFixtures("<div id=main></div>")
      it "is defined", ->
        expect(ViewModel).toBeDefined()

      it "has a name", ->
        expect(ViewModel.name).toEqual "UploaderFormView"

      describe "Instantiation", ->
        it "raise an exception if templdate directive data not present", ->
          expect(()->
            new ViewModel
          ).toThrow()

        it "create a root element", ->
          view = new ViewModel(
            data:
              id: 'foo_bar'
          )
          expect(view.el.nodeName).toEqual 'LI'

      describe "Render", ->
        it "render the form template", ->
          view = new ViewModel(
            data:
              id: 'foo_bar'
          )
          view.render()
          expect(view.$el.find("[id$='foo_bar']").length > 0).toBeTruthy()