describe "UploaderItemView", ->
  require ["jquery","underscore", "backbone", "cs!uploader/view.item"],
  ($,_, Backbone, ViewModel) ->
    describe "UploaderItemView", ->
      beforeEach ->
        setFixtures("<div id=main></div>")
      it "is defined", ->
        expect(ViewModel).toBeDefined()

      it "has a name", ->
        expect(ViewModel.name).toEqual "UploaderItemView"

      describe "Instantiation", ->
        it "raise an exception if templdate directive data not present", ->
          expect(()->
            new ViewModel
          ).toThrow()

        it "create a root element", ->
          view = new ViewModel(
            data:
              id: 'foo_bar'
              filename: 'test.txt'
              filesize: '100'
          )
          expect(view.el.nodeName).toEqual 'LI'

      describe "Render", ->
        it "render the form template", ->
          view = new ViewModel(
            data:
              id: 'foo_bar'
              filename: 'test.txt'
              filesize: '100'
          )
          expect(view.$el.find("[id$='foo_bar']").length > 0).toBeTruthy()
          expect(view.$el).toHaveText(/test.txt/)
          expect(view.$el).toHaveText(/100/)