define [
  'jquery',
  'json!../locales/en.json',
  'json!../locales/zh.json',
  'json!../locales/es.json'
], ($, EN, ZH, ES) ->
  ($el, locale) ->
    locales =
      'en' : EN
      'zh' : ZH
      'es' : ES

    $el.find("[data-localize]").each (index, element) =>
      key         = $(element).attr('data-localize')
      keys        = key.split(/\./)
      translation = locales[locale]

      while keys.length > 0 and (translation != undefined)
        translation = translation[keys.shift()]

      translation = null unless typeof(translation) is 'string'
#      for key in keys
#        if (translation != null)
#          translation = translation[key]
#        else
#          translation = null
#      translation = null unless translation

      if translation
        $(element).html(translation) unless $(element).is(':empty')

        if $(element).attr('placeholder')
          $(element).attr('placeholder', translation)

        # add this for invite user to workspace
        if $(element).attr('data-placeholder')
          $(element).attr('data-placeholder', translation)

        if $(element).attr('rel') is 'tooltip'
          if $(element).attr('title')
            $(element).attr('title', translation)

        if $(element).attr('data-filler')
          $(element).attr('data-filler', translation)
    $el