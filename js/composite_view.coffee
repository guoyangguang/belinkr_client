define [
  "order!jquery",
  "underscore",
  "backbone",
  "text!../templates/html_editor.html",
  "cs!./localize",
  "timeago",
  "order!chosen"
], ($, _, Backbone, HtmlEditorTemplate, localizer) ->
  class CompositeView extends Backbone.View
    constructor: ->
      @children = _([])
      @static_children = _([])
      super

    leave: ->
      @unbind()
      @remove()
      @_leaveChildren()
      @_leaveStaticChildren()
      @_removeFromParent()

    renderChild: (view)->
      view.parent = @
      view.render()
      @children.push(view)

    renderStaticChild: (view)->
      view.parent = @
      view.render()
      @static_children.push(view)

    replaceWithStaticChildInto: (view, container)->
      @renderStaticChild(view)
      container.replaceWith(view.el)

    appendChild: (view)->
      @renderChild(view)
      @$el.append(view.el)

    appendChildInto: (view, container)->
      @renderChild(view)
      container.append(view.el)

    appendStaticChildInto: (view, container)->
      @renderStaticChild(view)
      container.append(view.el)


    prependChildInto: (view, container)->
      @renderChild(view)
      $(container).prepend(view.el)

    renderChildInto: (view, container)->
      @renderChild(view)
      $(container).empty().append(view.el)

    _leaveChildren: ->
      @children.chain().clone().each (view)-> view.leave?()

    _leaveStaticChildren: ->
      @static_children.chain().clone().each (view)-> view.leave?()

    _removeFromParent: ->
      @parent?._removeChild(@)

    _removeChild: (view)->
      index = @children.indexOf(view)
      @children.splice(index,1)

    invoke_timeago: () ->
      @$(".timeago").timeago() # or just $('.timeago')

    invoke_chosen: () ->
      #FIXME: @$ would break the router spec
      #$(".chosen").chosen()

    setObjectName: (funcstr) ->
      reg = /function (.{1,})\(/i
      results = reg.exec(funcstr)
      @_objectName = results[1] || ""

    render_html_editor: (options = {})->
      editor = jQuery(HtmlEditorTemplate)
      editor.attr('id', options.id)
      if options.editorContainer
        if typeof(options.editorContainer) is "object"
          options.editorContainer.replaceWith(editor)
        else
          @$el.find(options.editorContainer).replaceWith(editor)

    localize: (el)->
      lang = $.cookie('belinkr.locale')
      localizer el, lang

    getLocalizeTextByKey: (key) ->
      return '' unless typeof(key) is 'string'
      div = $("<div />")
      span = $("<span />",
        "data-localize": key
        "text": 'empty'
      )
      div.append(span)
      @localize(div)
      text = span.html()
      div.empty()
      text
