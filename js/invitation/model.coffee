define ["underscore", "backbone"], (_, Backbone) ->
  class Invitation extends Backbone.Model
    urlRoot: "/invitations"
