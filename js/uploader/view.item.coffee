define [
  'jquery',
  'underscore',
  'backbone',
  'cs!../composite_view',
  '../../templates/directives',
  'text!../../templates/uploader_item.html',
  '../aura/mediator',
  '../libs/pure/pure'
], ($, _, Backbone, CompositeView, Directives, Template, mediator) ->
  class UploaderItemView extends CompositeView
    template: jQuery(Template)
    tagName: 'li'

    events:
      "click .file_remove" : "remove"

    initialize: () ->
      throw new Error('template directive missing') unless @options.data

      @uploader = @options.uploader
      @file     = @options.file
      _.bindAll @, "render"

      @template.directives(Directives.uploader_item)
      @$el.attr('id', 'uploader_file_id_' + @options.data.id)
      @$el.html(@template.render(@options.data).html())

    render: ()->
      @_leaveChildren()
      @

    remove: ()->
      @$el.fadeOut('fast', => @$el.remove())
      @uploader.removeFile(@file)
