define [
  'jquery',
  'underscore',
  'backbone',
  'cs!../composite_view',
  "../../templates/directives",
  'text!../../templates/uploader_form.html',
  '../aura/mediator',
  "../libs/pure/pure"
], ($, _, Backbone, CompositeView, Directives, Template, mediator) ->

  class UploaderFormView extends CompositeView
    template: jQuery(Template)
    tagName:    'div'
    className:  'uploader_form'
    events:
      'click button' : 'button_click'
      'click button span' : 'button_click'
      'click button i' : 'button_click'

    initialize: () ->
      throw new Error('template directive missing') unless @options.data
      _.bindAll @

      @template.directives(Directives.uploader_form)
      @$el.attr('id', 'upload_container_' + @options.data.id)
      @$el.html(@template.render(@options.data).html())
      @$el = @localize(@$el)

    render: () ->
      @_leaveChildren()
      @

    button_click: () ->
      false
