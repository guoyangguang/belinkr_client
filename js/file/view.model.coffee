define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../composite_view",
  "../../templates/directives",
  "text!../../templates/file.html",
  "../aura/mediator",
  "cs!../modules",
  "../libs/pure/pure"
], ($, _, Backbone, CompositeView, Directives, Template, mediator) ->
  class FileView extends CompositeView
    template: jQuery(Template)
    tagName:  'li'

#    events for preview etc
#    events:
#      "click .links .delete"    : "delete"

    initialize: () ->
      throw new Error('the template directive data missing') unless @options.data
      _.bindAll @, "render"
      @template.directives(Directives.file)
      @$el.html(@template.render(@options.data))

    render: () ->
      @_leaveChildren()
      @
