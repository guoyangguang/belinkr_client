define [
  "jquery",
  "underscore",
  "backbone",
  'cs!../sidebar/view.sidebar.search',
  "cs!./view.model",
  "cs!./collection",
  "cs!../composite_view",
  'cs!../alerter/helper',
  "text!../../templates/scrapbooks_sidebar_partial.html",
  "../aura/mediator",
  "cs!../modules"
], ($, _, Backbone, SearchSidebar, ScrapbookView, ScrapbookCollection, CompositeView, Alerter, Template, mediator) ->
  class ScrapbooksSidebar extends CompositeView
    className: "scrapbooks_sidebar row-fluid"
    events:
      "click .modal button.submit"   : "create"
      "click button.all_scrapbooks"  : 'nav_scrapbooks'
      "click a.scrapbook"             : 'enter_scrapbook'

    initialize:() ->
      _.bindAll @
      @collection = new ScrapbookCollection([], perPage: 5)
      @collection.perPage = 5
      @collection.bind("add", @render_one)
      @collection.bind("reset", @render)
      @collection.fetch()
      @$el.html(Template)
      @$el = @localize(@$el)

      # Markup hooks
      @form           = @$("#create_scrapbook_modal form")
      @name_field     = @$("input[name='name']")
      @name_filler    = @name_field.data("filler")
      @submit_button  = @$("button.submit")
      @scrapbook_list = @$("ul.scrapbooks")

      @reset_form()

    render: () =>
      @collection.each(@render_one)
      @render_search_sidebar()
      @

    render_search_sidebar: ()->
      search_sidebar = new SearchSidebar()
      @replaceWithStaticChildInto(search_sidebar, @$("#sidebar div.header"))

    render_one: (model) ->
      view = new ScrapbookView(model:model, parent:@)
      view.render()
      view.$el.undelegate('a', 'click')
      view.$el.delegate('a', 'click', (e)->
        window.App.navigate($(@).attr("href"), trigger: true)
        e.preventDefault()
      )
      model.bind("remove", view.fadeOutremove)
      @prependChildInto(view, @scrapbook_list)

    create: () ->
      @submit_button.attr("disabled", "disabled")
      @$("#create_scrapbook_modal ul.alert").remove()

      if (@name_field.attr("value") != @name_filler)  &&
      (@name_field.attr("value") != "")

        @collection.create({ name: @name_field.val() }, {
          wait:     true,
          success:  @success,
          error:    @errors
        })

      @submit_button.removeAttr("disabled")
      false

    success: (a, model)->
      alerter = new Alerter()
      div = $("<div />")
      link = $("<a />", class: 'scrapbook', href: "scrapbooks/#{model.id}")
      link.html(@getLocalizeTextByKey("scrapbook.alerter.new.here"))
      div.append(link)
      message = @getLocalizeTextByKey("scrapbook.alerter.new.your_scrapbook")
      message += model.name
      message += @getLocalizeTextByKey("scrapbook.alerter.new.created")
      message += div.html()
      message += @getLocalizeTextByKey("scrapbook.alerter.new.enter")
      div.empty()
      @form.before(alerter.success(message).render())
      @reset_form()

    errors: (model, response, xhr) ->
      if response && response.responseText
        attrs_with_errors = JSON.parse(response.responseText).errors
        alerter = new Alerter()
        @form.before(alerter.error(attrs_with_errors).render())

    nav_scrapbooks: ()->
      window.App.navigate("scrapbooks", trigger: true)

    enter_scrapbook: (e)->
      e.preventDefault()
      @$el.find('div.modal-footer a').trigger('click')
      window.App.navigate($(e.target).attr('href'), trigger: true)

    reset_form: () ->
      @name_field.attr("value", @name_filler)
        .focusin (event) =>
          $(event.target).attr("value", "")
        .focusout (event) =>
          if $(event.target).attr("value") == ""
            $(event.target).attr("value", @name_filler)
