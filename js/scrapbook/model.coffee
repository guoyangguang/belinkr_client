define ["underscore", "backbone"], (_, Backbone) ->
  class Scrapbook extends Backbone.Model
    urlRoot: "/scrapbooks"
    defaults: {
      created_at: new Date()
      updated_at: new Date()
    }
