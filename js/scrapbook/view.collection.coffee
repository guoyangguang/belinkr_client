define [
  "jquery",
  "underscore",
  "backbone",
  'cs!../sidebar/view.sidebar.search',
  "cs!./model",
  "cs!./view.model.main",
  "cs!./toolbar/view.scrapbook.header",
  "cs!../composite_view",
  'cs!../alerter/helper',
  "text!../../templates/scrapbooks_main.html",
  "../aura/mediator",
  "cs!../modules"
], ($, _, Backbone, SearchSidebar, Scrapbook, ScrapbookView, ScrapbookHeaderView, CompositeView, Alerter, Template, mediator) ->
  class ScrapbooksView extends CompositeView
    events:
      "click button.submit" : "create"
      "click a.scrapbook"             : 'enter_scrapbook'

    initialize:() ->
      @scrapbook_id = @options.scrapbook_id
      _.bindAll @
      @collection.bind("add", @render_one_with_active)
      @collection.bind("reset", @render)
      @$el.html(jQuery(Template))
      @$el = @localize(@$el)

      # Markup hooks
      @form           = @$("#create_scrapbook_modal form")
      @name_field     = @$("input[name='name']")
      @name_filler    = @name_field.data("filler")
      @submit_button  = @$("button.submit")
      @scrapbook_list = @$("ul.scrapbooks")
      @error_list     = @$("ul.errors")

      @reset_form()

    render_search_sidebar: ()->
      search_sidebar = new SearchSidebar()
      @replaceWithStaticChildInto(search_sidebar, @$("#sidebar div.header"))

    render: () ->
      @collection.each(@render_one)
      @active_one() if @_demonstrated is true
      @render_search_sidebar()
      @

    onShow: ()->
      @_demonstrated = true

    active_one: ()->
      if @scrapbook_id
        views = @children.select (view)=>
          view.model.id.toString() == @scrapbook_id

        view = _(views).first()
        if view
          model = view.model
          @render_header(model)
          view.$el.find('a').trigger('click')
        else
          model = new Scrapbook(id: @scrapbook_id)
          view = new ScrapbookView(model: model)
          model.fetch()
          @prependChildInto(view, @scrapbook_list)
          view.$el.find('a').trigger('click')
      else
        if view = @children.last()
          model = view.model
          @render_header(model)
          view.$el.find('a').trigger('click')

    remove_actives: ()->
      @scrapbook_list.find("li").removeClass('active')

    render_header: (model)->
      header_view = new ScrapbookHeaderView(
        model: model
      )
      @replaceWithStaticChildInto(header_view, @$("#scrapbook-header"))

    render_one_with_active: (model)->
      @render_one(model)
      @scrapbook_id = null # the newly as the active one
      @active_one()

    render_one: (model) ->
      view = new ScrapbookView(model:model, parent:@)
      model.bind("remove", view.fadeOutremove)
      @prependChildInto(view, @scrapbook_list)

    create: () ->
      @submit_button.attr("disabled", "disabled")
      @$("#create_scrapbook_modal ul.alert").remove()

      if (@name_field.attr("value") != @name_filler)  &&
         (@name_field.attr("value") != "")

        @collection.create({ name: @name_field.val() }, {
          wait:     true,
          success:  @success,
          error:    @errors
        })

      @submit_button.removeAttr("disabled")
      false

    success: (a, model)->
      alerter = new Alerter()
      div = $("<div />")
      link = $("<a />", class: 'scrapbook', href: "scrapbooks/#{model.id}")
      link.html(@getLocalizeTextByKey("scrapbook.alerter.new.here"))
      div.append(link)
      message = @getLocalizeTextByKey("scrapbook.alerter.new.your_scrapbook")
      message += model.name
      message += @getLocalizeTextByKey("scrapbook.alerter.new.created")
      message += div.html()
      message += @getLocalizeTextByKey("scrapbook.alerter.new.enter")
      div.empty()
      @form.before(alerter.success(message).render())
      @reset_form()

    errors: (model, response, xhr) ->
      if response && response.responseText
        attrs_with_errors = JSON.parse(response.responseText).errors
        alerter = new Alerter()
        @form.before(alerter.error(attrs_with_errors).render())

    enter_scrapbook: (e)->
      e.preventDefault()
      @$el.find('div.modal-footer a').trigger('click')
      window.App.navigate($(e.target).attr('href'), trigger: true)

    reset_form: () ->
      @name_field.attr("value", @name_filler)
        .focusin (event) =>
          $(event.target).attr("value", "")
        .focusout (event) =>
          if $(event.target).attr("value") == ""
            $(event.target).attr("value", @name_filler)