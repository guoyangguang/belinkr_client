define [
  "jquery",
  "underscore",
  "backbone",
  "cs!./view.model.box",
  "cs!../toolbar/view.search_results.toolbar",
  "cs!../prefetch_data/prefetch_models",
  "cs!../workspace/view.sidebar",
  "cs!../composite_view",
  "text!../../templates/scrapbooks_search.html",
  "../aura/mediator",
  "cs!../modules"
], ($, _, Backbone, ScrapbookView, SearchResultsToolbarView, PrefetchModels,WorkspacesSidebarView, CompositeView, Template, mediator) ->
  class ScrapbooksSearchView extends CompositeView
    template: jQuery(Template)

    initialize: ()->
      _.bindAll @
      @collection.bind("add", @render_one)
      @collection.bind("reset", @render)

      @collection.pager = @collection_pager

      @$el.html(@template)

      @scrapbook_list = @$("ul.scrapbooks")
      @error_list     = @$("ul.errors")
      @render_sidebar()
      @render_toolbar()

    render: () ->
      @collection.each(@render_one)
      @endless_pagination()
      if @collection.length <= @collection.perPage
        @hide_next_page_button()
      else
        @show_next_page_button()
      @

    render_toolbar: () ->
      search_results = PrefetchModels.search_results()
      toolbar_view = new SearchResultsToolbarView()
      @appendStaticChildInto(toolbar_view, @$(".btn-toolbar"))

    show_next_page_button: ()->
      @$el.find("button.servernext").removeClass("hide")

    hide_next_page_button: ()->
      @$el.find("button.servernext").addClass("hide")

    disable_next_page_button: ()->
      @$el.find("button.servernext").attr("disabled", "disabled")
      @$el.find("button.servernext").html("No more results")

    enable_next_page_button: ()->
      @$el.find("button.servernext").removeAttr("disabled")
      @$el.find("button.servernext").html("The next page")

    render_one: (model) ->
      view = new ScrapbookView(model:model, parent:@)
      view.render()
      model.bind("remove", view.fadeOutremove)
      if @collection._added_to_prepend is true
        @prependChildInto(view, @scrapbook_list)
        @collection._added_to_prepend = false
      else
        @appendChildInto(view, @scrapbook_list)

    render_sidebar: () ->
      sidebar_view = new WorkspacesSidebarView
      @replaceWithStaticChildInto(sidebar_view, @$("#sidebar"))

    nav_scrapbooks: (e)->
      el = $(e.target)
      el = el.parent() if el.is('span')
      window.App.navigate(el.attr("href"), trigger: true)
      false

    endless_pagination: ()->
      @collection.no_more_results = false
      $(window).unbind('scroll')
      $(window).scroll ()=>
        return if @collection.no_more_results
        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10)
          @collection.requestNextPage()

    collection_pager: ()->
      @collection.fetch success: (collection, resp)=>
        if collection.length == 0
          @show_next_page_button()
          @disable_next_page_button()
          @collection.page -= 1
          @collection.no_more_results = true

    next_page: () ->
      @hide_next_page_button()
      @collection.requestNextPage()
      $ () =>
        if $(document).height() > $(window).height()
          @endless_pagination()
        else
          @show_next_page_button()
          @enable_next_page_button()
      false