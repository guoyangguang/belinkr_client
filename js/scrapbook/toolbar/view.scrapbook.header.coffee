define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../../composite_view",
  "text!../../../templates/scrapbook_header.html",
  "../../../templates/directives",
  "cs!../../alerter/helper",
  "../../aura/mediator",
  "cs!../../modules",
  "../../libs/pure/pure"
], ($, _, Backbone, CompositeView, Template, Directives, Alerter, mediator) ->
  class ScrapbookHeaderView extends CompositeView
    attributes:
      id: "scrapbook-header"
    template: jQuery(Template)

    events:
      "click #update_scrapbook_modal button.submit" : "update_scrapbook"
      "click #delete_scrapbook_modal a[data-context='delete-scrapbook']" : "delete_scrapbook"

    initialize: () ->
      _.bindAll @
      @model.bind 'change', @render_header

    render: () ->
      @template = jQuery(Template)
      @render_header()
      @template.find("#update_scrapbook_modal form input")
      .val(@model.get("name"))

      @$el.html(@template)
      @$el = @localize(@$el)
      @

    render_header: ()->
      scrapbook_header = @getLocalizeTextByKey("scrapbook.header")

      scrapbook_name = @model.get('name')
      if (scrapbook_name is 'favorites') or (scrapbook_name is 'drafts')
        if $.cookie('belinkr.locale') == 'zh'
          scrapbook_name = @getLocalizeTextByKey("scrapbook." + scrapbook_name)

      @template.find("#header h1").html(
        scrapbook_header + " : " + scrapbook_name
      )

    update_scrapbook: (e)->
      e.preventDefault()
      e.stopPropagation()
      @model.save(
        name: @$el.find("#update_scrapbook_modal form input").val(),
        {
          wait: true
          success: ()=>
            alerter = new Alerter()
            message = @getLocalizeTextByKey("scrapbook.alerter.update.succeed")
            @$el.find("#update_scrapbook_modal form")
            .before(alerter.success(message).render())

          error: (model, response, xhr)=>
            if response && response.responseText
              attrs_with_errors = JSON.parse(response.responseText).errors
              alerter = new Alerter()
              @$el.find("#update_scrapbook_modal form")
              .before(alerter.error(attrs_with_errors).render())
        }
      )

    delete_scrapbook: (e)->
      e.preventDefault()
      e.stopPropagation()
      @model.destroy(
        success: ()=>
          @$el.find('#delete_scrapbook_modal div.modal-footer a').trigger('click')
          window.setTimeout ()->
            window.App.navigate('scrapbooks', trigger: true)
          ,1500

        error: (model, response, xhr)=>
          if response && response.responseText
            attrs_with_errors = JSON.parse(response.responseText).errors
            alerter = new Alerter()
            @$el.find("#delete_scrapbook_modal form")
            .before(alerter.error(attrs_with_errors).render())
      )
