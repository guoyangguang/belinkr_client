define ["underscore", "backbone", "cs!./model", "paginator"], (_, Backbone, Scrapbook) ->
  class ScrapbookCollection extends Backbone.Paginator.requestPager
    model:      Scrapbook
    url:        => "/scrapbooks/search"
    comparator: (model) ->
      model.get("updated_at")

    pageAttribute: 'page'
    perPageAttribute: 'perPage'
    queryAttribute: 'name'

    page: 0
    firtPage: 0
    perPage: 20

    initialize: (models, options) ->
      unless _.isEmpty(arguments)
        @options = _(arguments).last()
        @perPage = @options.perPage if @options.perPage
        @query=options['query']
