define [
  "jquery",
  "underscore",
  "backbone",
  "../../templates/directives",
  "text!../../templates/scrapbook_left.html",
  "cs!./scrap/collection",
  "cs!./scrap/view.collection",
  "cs!../composite_view",
  "../aura/mediator",
  "cs!../modules",
  "../libs/pure/pure"
], ($, _, Backbone, Directives, Template,Scraps,ScrapsView,CompositeView,mediator) ->
  class ScrapbookView extends CompositeView
    template:             jQuery(Template)
    tagName:              "li"

    events:
#      "click .links .delete" : "delete"
      "click a"              : "show"

    initialize: () ->
      _.bindAll @, "render", "fadeOutremove", "show"
      @model.bind("change", @render)
      @template.directives(Directives.scrapbook)

    render: () ->
      @$el.html(@template.render(@model.toJSON()).html())
      @localize_defaults_name()
      @

    fadeOutremove: () ->
      $(@el).fadeOut(250,=>@remove())

    localize_defaults_name: ()->
      scrapbook_name = @model.get("name")
      if (scrapbook_name is 'favorites') or (scrapbook_name is 'drafts')
        if $.cookie('belinkr.locale') == 'zh'
          text = @getLocalizeTextByKey("scrapbook." + scrapbook_name)
          @$el.find('a').text(text)

#    delete: () ->
#      @model.destroy({},{wait:true})
#      false

    show: (e) ->
      e.preventDefault()

      @$el.siblings().removeClass('active')
      @$el.addClass('active')
      @parent.render_header(@model)

      @parent?.currentView?.scraps_view?.leave?()
      scraps = new Scraps([], scrapbook_id: @model.id)
      @scraps_view = new ScrapsView(collection: scraps)
      scraps.fetch()
      @parent?.currentView = @
      mediator.publish "showScrapbook", @
      false
