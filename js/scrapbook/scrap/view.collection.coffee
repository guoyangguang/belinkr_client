define [
  "jquery",
  "underscore",
  "backbone",
  "../../../templates/directives"
  "cs!./view.model",
  "cs!../../composite_view",
  "text!../../../templates/scraps.html",
  'cs!../../uploader',
  "../../aura/mediator",
  "cs!../../modules"
], ($, _, Backbone, Directives, ScrapView, CompositeView, Template, Uploader, mediator) ->
  class ScrapsView extends CompositeView
    template: jQuery(Template)
    
    events:
      "click button[type='submit']" : "create"
      "click button.servernext"     : "next_page"

    initialize: () ->
      @_demonstrated = false
      _.bindAll @
      @collection.bind "add", @render_one
      @collection.bind "reset", @render

      @collection.pager = @collection_pager

      @template.directives(Directives.scrap_form)
      @$el.html(
        @template.render(scrapbook_id: @collection.scrapbook_id).html()
      )
      @$el = @localize(@$el)

#      @render_html_editor(
#        id: "editor-toolbar-scrap"
#        editorContainer: "#editor-toolbar-scrap"
#      )

      @scraps = @$("ul.scraps")
      @submit_button  = @$("button[type='submit']")
      @error_list     = @$("ul.errors")
      @text_field     = @$('form textarea')
      @text_filler    = @text_field.data('filler')

    render: () ->
      @collection.each(@render_one)
      if @collection.length <= @collection.perPage
        @hide_next_page_button()
      else
        @show_next_page_button()
      @_after()
      @

    onShow: ()->
      @render_upload_form()

    _after: ()->
      if @_demonstrated is false
        @_demonstrated = true
      else
#        @editor = new wysihtml5.Editor @$el.find("#scrap")[0],
#          toolbar:      "editor-toolbar-scrap",
#          parserRules:  wysihtml5ParserRules
        @render_upload_form()
        @endless_pagination()

    show_next_page_button: ()->
      @$el.find("button.servernext").removeClass("hide")

    hide_next_page_button: ()->
      @$el.find("button.servernext").addClass("hide")

    disable_next_page_button: ()->
      @$el.find("button.servernext").attr("disabled", "disabled")
#      @$el.find("button.servernext").html("No more results")

    enable_next_page_button: ()->
      @$el.find("button.servernext").removeAttr("disabled")
#      @$el.find("button.servernext").html("The next page")

    render_upload_form: ()=>
      view_options=
        data:
          id: 'scrapbooks_' + @collection.scrapbook_id + "_scraps"

      uploader_options=
        parent_form: @$el.find("form").first()
        settings:
          token: '?auth_token=' + $.cookie('belinkr.auth_token')

      built = Uploader.build(view_options, uploader_options)
      @replaceWithStaticChildInto(built.view, @$("div.uploader"))
      $ ()->
        built.uploader.init()

    render_one: (model)->
      view = new ScrapView(model:model)
      view.render()
      model.bind("destroy", view.fadeOutremove)
      if @collection._added_to_prepend is true
        @prependChildInto(view, @scraps)
        @collection._added_to_prepend = false
      else
        @appendChildInto(view, @scraps)

    create: ()->
      @collection._added_to_prepend = true
      mediator.publish "createScrap", @
      @editor_reset()
      @endless_pagination()
      false

    editor_reset: ()->
      @editor?.clear()
      @editor?.fire("set_placeholder")

    reset_form: () ->
      @$el.find("input[name='files']").val('')
      @$el.find(".uploader_form ul.file_list").empty()

      @text_field.val(@text_filler)
        .focusin (event) =>
          $(event.target).attr("value", "")
        .focusout (event) =>
          if $(event.target).attr("value") == ""
            $(event.target).attr("value", @text_filler)

    errors: (model, response, xhr) ->
      if response && response.responseText
        attrs_with_errors = JSON.parse(response.responseText).errors
        _.each(attrs_with_errors, (errors) ->
          _.each(errors, (error) ->
            $("ul.errors").append("<li>" + error + "</li>")
          )
        )

    endless_pagination: ()->
      @collection.no_more_results = false
      $(window).unbind('scroll')
      $(window).scroll ()=>
        return if @collection.no_more_results
        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10)
          @collection.requestNextPage()

    collection_pager: ()->
      @collection.fetch success: (collection, resp)=>
        if collection.length == 0
          @show_next_page_button()
          @disable_next_page_button()
          @collection.page -= 1
          @collection.no_more_results = true

    next_page: () ->
      @hide_next_page_button()
      @collection.requestNextPage()
      $ () =>
        if $(document).height() > $(window).height()
          @endless_pagination()
        else
          @show_next_page_button()
          @enable_next_page_button()
      false