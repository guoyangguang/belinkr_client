define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../../composite_view",
  "cs!../../file/view.model",
  "../../../templates/directives",
  "text!../../../templates/scrap.html",
  "cs!./reply/view.model",
  "../../aura/mediator",
  "cs!../../modules",
  "../../libs/pure/pure"
], ($, _, Backbone, CompositeView, FileView, Directives, Template, ScrapReplyView, mediator) ->
  class ScrapView extends CompositeView
    template:             jQuery(Template)
    tagName:              "li"
    className:            "scrap article span12 row-fluid"

    events:
      "click .links .delete"    : "delete"

    initialize: () ->
      @_demonstrated = false
      _.bindAll(@, "render","fadeOutremove","delete", "leave")
      @model.bind("change", @render)
      @template.directives(Directives.scrap)

    render: () ->
      @$el.html( @template.render(@model.toJSON()).html() )
      @$el = @localize(@$el)
      @display_enforcer()
      @_after()
      @

    _after: ()->
      if @_demonstrated is false
        @_demonstrated = true
      else
        @render_files()
        @render_replies()
        @invoke_timeago()

    render_files: () ->
      files = @model.get('files')
      @file_list = @$("ul.files")
      unless _.isEmpty(files)
        _.each files, (data)=>
          file_view = new FileView(data: data)
          @appendChildInto(file_view, @file_list)

    render_replies: ()->
      replies = @model.get('replies')
      @reply_list = @$("ul.replies")
      unless _.isEmpty(replies)
        _.each replies, (data)=>
          scrap_reply_view = new ScrapReplyView(data: data)
          @appendChildInto(scrap_reply_view, @reply_list)

    delete: () ->
      mediator.publish "deleteScrap", @
      false

    fadeOutremove: () ->
      @$el.fadeOut(250,=> this.remove())

    leave: () ->
      @unbind()
      @remove()

    viewer_is_author: () ->
      true
#      current_user = PrefetchModels.current_user()
#      current_user.id == @model.get("user_id")

    display_enforcer: () ->
      unless @viewer_is_author()
        @$el.find("a.delete").parent().remove()
