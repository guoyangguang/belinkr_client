define ["underscore", "backbone", "cs!./model", "paginator"], (_, Backbone, Scrap) ->
  class ScrapCollection extends Backbone.Paginator.requestPager
    model:      Scrap
    url:        ->
      "/scrapbooks/" + @scrapbook_id + "/scraps"

    pageAttribute: 'page'
    perPageAttribute: 'perPage'

    page: 0
    firtPage: 0
    perPage: 20

    comparator: (model) ->
      model.get("updated_at")

    initialize: (models,options)->
      if options.scrapbook_id
        @scrapbook_id = options.scrapbook_id
      
