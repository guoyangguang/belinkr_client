define ["underscore", "backbone"], (_, Backbone) ->
  class Scrap extends Backbone.Model
    urlRoot: -> "/scrapbooks/" + @get("scrapbook_id") + "/scraps"
    defaults: {
      created_at: new Date()
      updated_at: new Date()
    }

    # must provide user_id and scrapbook_id when create a new scrap
    #initialize: (options) ->


