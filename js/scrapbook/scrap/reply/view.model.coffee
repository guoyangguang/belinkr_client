define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../../../composite_view",
  "../../../../templates/directives",
  "text!../../../../templates/scrap_reply.html",
  "cs!../../../file/view.model",
  "../../../aura/mediator",
  "cs!../../../modules",
  "../../../libs/pure/pure"
], ($, _, Backbone, CompositeView, Directives, Template, FileView, mediator) ->
  class ScrapReplyView extends CompositeView
    template:             jQuery(Template)
    tagName:              "li"
    className:            "reply span12 row-fluid"

    initialize: () ->
      throw new Error('the template directive data missing') unless @options.data
      _.bindAll @, "render"
      @template.directives(Directives.scrap_reply)
      @$el.html(@template.render(@options.data).html())

    render: () ->
      @_leaveChildren()
      @render_files()
      @

    render_files: () ->
      files = @options.data.files
      @file_list = @$("ul.files")
      unless _.isEmpty(files)
        _.each files, (data)=>
          file_view = new FileView(data: data)
          @appendChildInto(file_view, @file_list)
