define [
  "jquery",
  "underscore",
  "backbone",
  "../../templates/directives",
  "text!../../templates/scrapbook_box.html",
  "cs!../composite_view",
  'cs!../alerter/helper',
  "../aura/mediator",
  "cs!../modules",
  "../libs/pure/pure"
], ($, _, Backbone, Directives, Template,CompositeView, Alerter, mediator) ->
  class ScrapbookViewBox extends CompositeView
    template:             jQuery(Template)
    tagName:              "li"
    className:            "workspace"

    events:
      "click button"        : 'nav_scrapbook'
      "click h3.truncate a" : 'nav_scrapbook'

    initialize: () ->
      @_demonstrated = false
      _.bindAll(@, "render", "fadeOutremove", "delete", "show")
      @model.bind("change", @render)
      @template.directives(Directives.scrapbook_box)

    render: () ->
      @_after()
      @

    _after: ()->
      if @_demonstrated is false
        @_demonstrated = true
      else
        $(@el).html( @template.render @model.toJSON() )


    fadeOutremove: () ->
      $(@el).fadeOut(250,=>@remove())

    delete: () ->
      @model.destroy({},{wait:true})
      false
    show: () ->
      false

    nav_scrapbook: (e)->
      @_navigate()
      false

    _navigate: ()->
      window.App.navigate('/scrapbooks/' + @model.id, trigger: true)
