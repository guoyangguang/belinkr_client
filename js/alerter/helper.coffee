define [
  'jquery',
  'underscore',
  'text!../../templates/alert_success.html',
  'text!../../templates/alert_error.html'
], ($, _, SuccessTemplate, ErrorTemplate) ->
  class Alerter
    success_alert:  jQuery(SuccessTemplate).html()
    error_alert:    jQuery(ErrorTemplate).html()

    initialize: () ->
      _.bindAll @

    render: () ->
      @markup

    success: (message) ->
      @markup = $(@success_alert)
      @markup.append("<li>" + message + "</li>")
      @

    error: (messages) ->
      @markup = $(@error_alert)
      _.each messages, (message) =>
        @markup.append("<li>" + message + "</li>")
      @
