define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../composite_view",
  "cs!./model.summary",
  "../../templates/directives",
  "text!../../templates/user_profile.html",
  "cs!../status/view.sidebar",
  "cs!./status/collection",
  "cs!./status/view.collection",
  "cs!../prefetch_data/prefetch_collections",
  "../libs/pure/pure"
], ($, _, Backbone, CompositeView, Model, Directives, Template, SidebarView, Statuses, StatusesView, PrefetchCollections) ->
  class UserProfileView extends CompositeView
    template:             jQuery(Template)

    events:
      'click div.picture p button' : 'follow_or_not'
      "click p[data-context='workspaces'] a" : 'nav_workspaces'
      "click p[data-context='followers'] a"  : 'nav_followers'
      "click p[data-context='following'] a"  : 'nav_following'

    initialize: () ->
      @_demonstrated = false
      _.bindAll @
      @model = new Model(id: @options.user_id)
      @model.fetch()
      @model.bind "change", @render
      @template.directives(Directives.profile)

    render: () ->
      if @model.hasChanged()
        @$el.html(@template.render(@model.toJSON()))
        @hook_doms()
        @render_summary()
        @$el = @localize(@$el)
      @_after()
      @

    _after: ()->
      if @_demonstrated is false
        @_demonstrated = true
      else
        @render_sidebar()
        @render_statuses()

    hook_doms: ()->
      @workspace_list  = @$el.find("ul.workspaces")
      @followers_list  = @$el.find("ul.followers")
      @following_list  = @$el.find("ul.following")

      _.each [@workspace_list, @followers_list, @following_list], (dom)->
        dom.undelegate('li', 'click')
        dom.delegate('li', 'click', (e)->
          link = $(@).find("a").attr('href')
          window.App.navigate(link, trigger: true)
          e.preventDefault()
        )

    render_sidebar: () ->
      sidebar_view = new SidebarView()
      @replaceWithStaticChildInto(sidebar_view, @$("#sidebar"))

    render_statuses: () ->
      collection = new Statuses([], user_id: @options.user_id)
      view = new StatusesView(collection: collection)
      collection.fetch()
      @replaceWithStaticChildInto(view, @$("ul.statuses"))

    render_summary: ()->
      @workspace_list.empty()
      @followers_list.empty()
      @following_list.empty()
      @_render_workspaces_summary()
      @_render_followers_summary()
      @_render_following_summary()

    _render_workspaces_summary: ()->
      _.each @model.get("summary").workspaces, (workspace)=>
        li = $('<li />')
        link = $('<a />',
          href: '/workspaces/' + workspace.id
        )
        link.html(workspace.name)
        li.append(link)
        @workspace_list.append(li)

    _render_followers_summary: ()->
      _.each @model.get("summary").followers, (user) =>
        li = $('<li />')
        link = $('<a />',
          href: '/users/' + user.id
        )
        img = $('<img />',
          src: user.links.avatar
        )
        link.append(img)
        li.append(link)
        @followers_list.append(li)

    _render_following_summary: ()->
      _.each @model.get("summary").following, (user) =>
        li = $('<li />')
        link = $('<a />',
          href: '/users/' + user.id
        )
        img = $('<img />',
          src: user.links.avatar
        )
        link.append(img)
        li.append(link)
        @following_list.append(li)

    follow_or_not: ()->
      switch @model.get('relationship')
        when "follower"
          @unfollow()
        when "following", "none"
          @follow()
        else
          ''
      false

    nav_workspaces: (e)->
      e.preventDefault()
      link = $(e.target).attr("href")
      window.App.navigate(link, trigger: true)

    nav_followers: (e) ->
      e.preventDefault()
      link = $(e.target).attr("href")
      window.App.navigate(link, trigger: true)

    nav_following: (e) ->
      e.preventDefault()
      link = $(e.target).attr("href")
      window.App.navigate(link, trigger: true)

    follow: () ->
      $.ajax
        url: '/users/' + @model.id + '/followers'
        type: "POST"
        success: (response) =>
          @model.set($.parseJSON(response))
          counter = PrefetchModels.counter()
          counter.set("following", counter.get("following") + 1 )
          following = PrefetchCollections.following()
          following.add @model
        error: ()=>
      false

    unfollow: () ->
      $.ajax
        url: '/users/' + @model.id + '/followers/' + PrefetchModels.current_user().get("id")
        type:'DELETE'
        success: (response)=>
          @model.set(
            relationship    : 'none'
            followers_count : @model.get('followers_count') - 1
          )
          counter = PrefetchModels.counter()
          counter.set("following", counter.get("following") - 1 )
          following = PrefetchCollections.following()
          following.remove @model
        error: ()=>
      false