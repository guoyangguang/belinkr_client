define ["underscore", "backbone"], (_, Backbone) ->
  class User extends Backbone.Model
    urlRoot: "/users"
    defaults: {
      created_at: new Date()
      updated_at: new Date()
    }


