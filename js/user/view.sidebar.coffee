define [
  "jquery",
  "underscore",
  "backbone",
  'cs!../sidebar/view.sidebar.search',
  "cs!../sidebar/view.counter.sidebar.partial",
  "cs!../sidebar/view.sidebar.followers",
  "cs!../sidebar/view.sidebar.following",
  "cs!../composite_view",
  "text!../../templates/users_sidebar.html",
  "../aura/mediator",
  "cs!../modules"
], ($, _, Backbone, SearchSidebar, CounterSidebar, FollowersSidebar, FollowingSidebar, CompositeView,Template,mediator) ->
  class UsersSidebarView extends CompositeView
    initialize: () ->
      _.bindAll @
      @$el.html(Template)
      #@render_scrapbooks_sidebar()
      @render_search_sidebar()
      @render_counter_sidebar()
      @render_following_sidebar()
      @render_followers_sidebar()
      # Markup hooks
    render: ()->
      @

    render_search_sidebar: ()->
      search_sidebar = new SearchSidebar()
      @replaceWithStaticChildInto(search_sidebar, @$("#sidebar div.header"))

    render_counter_sidebar: ->
      counter_sidebar = new CounterSidebar()
      @replaceWithStaticChildInto(counter_sidebar, @$("ul.counters"))

    render_following_sidebar: ->
      following_sidebar = new FollowingSidebar()
      @replaceWithStaticChildInto(following_sidebar, @$("div.following"))

    render_followers_sidebar: ->
      followers_sidebar = new FollowersSidebar()
      @replaceWithStaticChildInto(followers_sidebar, @$("div.followers"))

    render_scrapbooks_sidebar: ()=>
      scrapbooks = new ScrapbookCollection()

      scrapbooks_sidebar = new ScrapbooksSidebar
        collection: scrapbooks
      @replaceWithStaticChildInto(scrapbooks_sidebar, @$(".scrapbooks_sidebar"))
      scrapbooks.fetch()

    errors: (model, response, xhr) =>
      if response && response.responseText
        attrs_with_errors = JSON.parse(response.responseText).errors
        _.each(attrs_with_errors, (errors) ->
          _.each(errors, (error) ->
            @$("ul.errors").append("<li>" + error + "</li>")
          )
        )

