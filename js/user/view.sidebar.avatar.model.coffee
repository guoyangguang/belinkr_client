define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../composite_view",
  "cs!./model",
  "../../templates/directives",
  "text!../../templates/user_sidebar_avatar.html",
  "../aura/mediator",
  "cs!../modules",
  "../libs/pure/pure"
], ($, _, Backbone, CompositeView, Model, Directives, Template, mediator) ->
  class UserView extends CompositeView
    template:             jQuery(Template)
    tagName:              "li"

    events:
      "click a" : "nav_user"

    initialize: () ->
      _.bindAll @
      @model.bind "change", @render
      @template.directives(Directives.user_sidebar_avatar)

    render: () ->
      @$el.html(@template.render(@model.toJSON()))
      @

    leave: ()->
      @unbind()
      @remove()

    nav_user: (e)->
      e.preventDefault()
      window.App.navigate("/users/" + @model.id, trigger: true)
      false

