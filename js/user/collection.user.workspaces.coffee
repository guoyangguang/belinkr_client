define ["underscore", "backbone", "cs!../workspace/model", "paginator"], (_, Backbone, Workspace) ->
  class UserWorkspaceCollection extends Backbone.Paginator.requestPager
    model:      Workspace
    url: ()->
      "/users/" + @user_id + "/workspaces"

    pageAttribute: 'page'
    perPageAttribute: 'perPage'

    page: 0
    firtPage: 0
    perPage: 20

    comparator: (model) ->
      model.get("updated_at")

    initialize: () ->
      unless _.isEmpty(arguments)
        @options = _(arguments).last()
        @user_id = @options.user_id