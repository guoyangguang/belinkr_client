define ["underscore", "backbone", "cs!./model", "paginator"], (_, Backbone, Status) ->
  class UserStatuses extends Backbone.Paginator.requestPager
    model:      Status
    url:        ->
#      "/users/" + @user_id + "/statuses"
      "/statuses"

    pageAttribute: 'page'
    perPageAttribute: 'perPage'

    page: 0
    firtPage: 0
    perPage: 20

    comparator: (model) ->
      model.get("updated_at")

    initialize: (models,options)->
      @options = options
      @user_id = @options.user_id
