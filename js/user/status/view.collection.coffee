define [
  "jquery",
  "underscore",
  "backbone",
  "../../../templates/directives"
  "cs!./view.model",
  "cs!../../composite_view",
  "text!../../../templates/user_statuses.html",
  "../../aura/mediator",
  "cs!../../modules"
], ($, _, Backbone, Directives, StatusView, CompositeView, Template, mediator) ->
  class UserStatusesView extends CompositeView
    template: jQuery(Template)

    events:
      "click button.servernext" : "next_page"

    initialize: () ->
      @_demonstrated = false
      _.bindAll @
      @collection.bind "add", @render_one
      @collection.bind "reset", @render
      @collection.pager = @collection_pager
      @$el.html(@template)
      @$el = @localize(@$el)

      @status_list = @$("ul.statuses")

    render: () ->
      @collection.each(@render_one)
      if @collection.length <= @collection.perPage
        @hide_next_page_button()
      else
        @show_next_page_button()
      @_after()
      @

    _after: ()->
      if @_demonstrated is false
        @_demonstrated = true
      else
        @endless_pagination()

    show_next_page_button: ()->
      @$el.find("button.servernext").removeClass("hide")

    hide_next_page_button: ()->
      @$el.find("button.servernext").addClass("hide")

    disable_next_page_button: ()->
      @$el.find("button.servernext").attr("disabled", "disabled")
#      @$el.find("button.servernext").html("No more results")

    enable_next_page_button: ()->
      @$el.find("button.servernext").removeAttr("disabled")
#      @$el.find("button.servernext").html("The next page")

    render_one: (model)->
      view = new StatusView(model: model)
      view.render()
      @appendChildInto(view, @status_list)

    endless_pagination: ()->
      @collection.no_more_results = false
      $(window).unbind('scroll')
      $(window).scroll ()=>
        return if @collection.no_more_results
        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10)
          @collection.requestNextPage()

    collection_pager: ()->
      @collection.fetch success: (collection, resp)=>
        if collection.length == 0
          @show_next_page_button()
          @disable_next_page_button()
          @collection.page -= 1
          @collection.no_more_results = true

    next_page: () ->
      @hide_next_page_button()
      @collection.requestNextPage()
      $ () =>
        if $(document).height() > $(window).height()
          @endless_pagination()
        else
          @show_next_page_button()
          @enable_next_page_button()
      false