define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../../composite_view",
  "cs!../../file/view.model",
  "../../../templates/directives",
  "text!../../../templates/user_status.html",
  "cs!./reply/model",
  "cs!./reply/view.model",
  "text!../../../templates/status_delete_click_modal.html",
  "../../aura/mediator",
  "cs!../../modules",
  "../../libs/pure/pure"
], ($, _, Backbone, CompositeView, FileView, Directives, Template, Reply, ReplyView, StatusDeleteClickModal, mediator) ->
  class UserStatusView extends CompositeView
    template:             jQuery(Template)
    tagName:              "li"
    className:            "status article span12 row-fluid"

    events:
#      "click .links .delete"    : "delete"
      "click .author li span[class^='label']" : 'nav_workspace'
      "click img.avatar"        : "nav_user"
      "click span.user_name"    : "nav_user"

    initialize: () ->
      @_demonstrated = false
      _.bindAll @
      @model.bind("change", @render)
      @template.directives(Directives.user_status)

    render: () ->
      @$el.html( @template.render(@model.toJSON()).html() )
      @$el = @localize(@$el)
      @click_modal()
      @_after()
      @display_enforcer()
      @

    click_modal: ()->
      @$el.find(".links .delete").click_modal
        parent: @
        modal:
          target: "for_status"
          template: @localize($(StatusDeleteClickModal))
          events:
            "button.btn-warning" : "delete"

    _after: ()->
      if @_demonstrated is false
        @_demonstrated = true
      else
        @render_files()
        @render_replies()
        @invoke_timeago()

    render_files: () ->
      files = @model.get('files')
      @file_list = @$("ul.files")
      unless _.isEmpty(files)
        _.each files, (data)=>
          file_view = new FileView(data: data)
          @appendChildInto(file_view, @file_list)

    render_replies: ()->
      replies = @model.get('replies')
      @reply_list = @$("ul.replies")
      unless _.isEmpty(replies)
        _.each replies, (data)=>
          reply_view = new ReplyView(model: new Reply(data))
          @appendChildInto(reply_view, @reply_list)

    delete: () ->
      @model.destroy()
      @fadeOutremove()
      false

    fadeOutremove: () ->
      @$el.fadeOut(250,=> @remove())

    leave: () ->
      @unbind()
      @remove()

    nav_workspace: ()->
      if @model.get('workspace_id')
        window.App.navigate("/workspace/" + @model.get('workpace_id'), trigger: true)
      false

    viewer_is_author: () ->
      current_user = PrefetchModels.current_user()
      current_user.id == @model.get("user_id")

    display_enforcer: () ->
      unless @viewer_is_author()
        @$el.find("a.delete").parent().remove()

    nav_user:(e) ->
      e.preventDefault()
      window.App.navigate("/users/" + @model.get('user_id'), trigger: true)
      false
