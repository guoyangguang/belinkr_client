define ["underscore", "backbone"], (_, Backbone) ->
  class UserStatus extends Backbone.Model
    urlRoot: -> "/statuses"

    defaults: {
      created_at: new Date()
      updated_at: new Date()
    }