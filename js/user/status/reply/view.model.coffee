define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../../../composite_view",
  "../../../../templates/directives",
  "text!../../../../templates/user_status_reply.html",
  "cs!../../../file/view.model",
  "text!../../../../templates/reply_delete_click_modal.html",
  "../../../aura/mediator",
  "cs!../../../modules",
  "../../../libs/pure/pure"
], ($, _, Backbone, CompositeView, Directives, Template, FileView, ReplyDeleteClickModal, mediator) ->
  class UserStatusReplyView extends CompositeView
    template:             jQuery(Template)
    tagName:              "li"
    className:            "reply span12 row-fluid"

    events:
      "click img.avatar"        : "nav_user"
      "click span.user_name"    : "nav_user"

    initialize: () ->
      _.bindAll @
      @template.directives(Directives.user_status_reply)
      @$el.html(@template.render(@model.toJSON()).html())

    render: () ->
      @$el = @localize(@$el)
      @_leaveChildren()
      @click_modal()
      @display_enforcer()
      @render_files()
      @

    click_modal: ()->
      link = @$el.find(".links .delete")
      link.click_modal
        parent: @
        modal:
          target: "for_status_reply"
          template: @localize($(ReplyDeleteClickModal))
          events:
            "button.btn-warning" : "delete"

    delete: () ->
      @model.destroy()
      @fadeOutremove()
      false

    fadeOutremove: () ->
      @$el.fadeOut(250, => @remove())

    render_files: () ->
      files = @model.get("files")
      @file_list = @$("ul.files")
      unless _.isEmpty(files)
        _.each files, (data)=>
          file_view = new FileView(data: data)
          @appendChildInto(file_view, @file_list)

    viewer_is_author: () ->
      current_user = PrefetchModels.current_user()
      current_user.id == @model.get("user_id")

    display_enforcer: () ->
      unless @viewer_is_author()
        @$el.find("a.delete").parent().remove()

    nav_user:(e) ->
      e.preventDefault()
      third_party = @model.get('third_party')
      unless third_party?.user
        window.App.navigate("/users/" + @model.get('user_id'), trigger: true)
      false
