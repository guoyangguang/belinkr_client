define ["underscore", "backbone"], (_, Backbone) ->
  class UserStatusReply extends Backbone.Model
    urlRoot: -> "/statuses/" + @get("status_id") + "/replies"
    defaults:
      created_at: new Date()
      updated_at: new Date()