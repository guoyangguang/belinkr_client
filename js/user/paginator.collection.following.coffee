define ["underscore", "backbone","cs!./paginator.collection"], (_, Backbone, UserPaginatorCollection) ->
  class Following extends UserPaginatorCollection
    url: "/following"

    pageAttribute: 'page'

    page:0
    firtPage:0
    perPage:5

