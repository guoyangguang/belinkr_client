define ["underscore", "backbone"], (_, Backbone) ->
  class UserSummary extends Backbone.Model
    url: ()->
      "/users/" + @id + "/summary"
    defaults: {
      created_at: new Date()
      updated_at: new Date()
    }