define ["underscore", "backbone","cs!./paginator.collection"], (_, Backbone, UserPaginatorCollection) ->
  class Followers extends UserPaginatorCollection
    url: "/followers"

    pageAttribute: 'page'

    page:0
    firtPage:0
    perPage:5

