define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../composite_view",
  "cs!./model",
  "../../templates/directives",
  "text!../../templates/user.html",
  "cs!../prefetch_data/prefetch_models",
  "cs!../prefetch_data/prefetch_collections",
  "../aura/mediator",
  "cs!../modules",
  "../libs/pure/pure"
], ($, _, Backbone, CompositeView, Model, Directives, Template, PrefetchModels, PrefetchCollections, mediator) ->
  class UserView extends CompositeView
    events:
      "click button.btn-primary" : "follow"
      "click button.btn-success" : "unfollow"
      "click img.avatar"         : "nav_user"
      "click li.first"           : "nav_user"
      "click li.last"            : "nav_user"

    template:             jQuery(Template)
    tagName:              "li"
    className:            "card"
    initialize: () ->
      _.bindAll @
      @model.bind "change", @render
      @template.directives(Directives.user)

    render: () ->
      @$el.html(@template.render(@model.toJSON()).html())
      @update_button_text()
      @$el = @localize(@$el)
      @

    update_button_text: ()->
      text = @getLocalizeTextByKey(
        "button.user_relationship.#{@model.get("relationship")}"
      )
      @$el.find('button.btn span').html(text)

    follow: () ->
      $.post('/users/'+@model.id+'/followers',
        (response)=> 
          @model.set($.parseJSON(response))
          counter = PrefetchModels.counter()
          counter.set("following",counter.get("following")+1 )
          following = PrefetchCollections.following()
          following.add @model
      )

    unfollow: () ->
      $.ajax(
        url: '/users/'+@model.id+'/followers/id',
        type:'DELETE'
        success: (response)=> 
          @model.set('relationship','none')
          @model.set('followers_count',@model.get('followers_count')-1)
          counter = PrefetchModels.counter()
          counter.set("following",counter.get("following")-1 )
          following = PrefetchCollections.following()
          following.remove @model
      )

    follow_success: ()->
      @$el.addClass('following')
      @$('button').removeClass('btn-primary').addClass('btn-success')
      @$('button i').removeClass('icon-plus-sign').addClass('icon-ok-sign')
      @$('button').text('following')

    nav_user: (e) ->
      e.preventDefault()
      window.App.navigate("/users/" + @model.get("id"), trigger: true )
      false
    leave: () ->
      @unbind()
      @remove()


