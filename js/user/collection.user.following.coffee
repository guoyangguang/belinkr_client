define ["underscore", "backbone","cs!./paginator.collection"], (_, Backbone, UserPaginatorCollection) ->
  class UserFollowingCollection extends UserPaginatorCollection
    url: ()->
      "/users/" + @user_id + "/following"

    pageAttribute: 'page'

    page:0
    firtPage:0
    perPage:20

    initialize: () ->
      unless _.isEmpty(arguments)
        @options = _(arguments).last()
        @user_id = @options.user_id