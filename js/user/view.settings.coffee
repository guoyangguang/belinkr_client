define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../composite_view",
  "cs!./model",
  "../../templates/directives",
  "text!../../templates/user_settings.html",
  "cs!../status/view.sidebar",
  'cs!../alerter/helper',
  'cs!../uploader',
  "../libs/pure/pure"
], ($, _, Backbone, CompositeView, Model, Directives, Template, SidebarView, Alerter, Uploader) ->
  class UserSettingsView extends CompositeView
    template:             jQuery(Template)

    _avatar_error: (uploader, error)->
      alerter = new Alerter()
      $('form h4').after(alerter.error([error.message]).render())

    _avatar_added: (uploader, files)->
      uploader.refresh()
      if uploader.settings.auto_upload
        window.setTimeout(()->
          uploader.start()
        ,100)

    _avatar_uploaded: (uploader, file, resp)->
      data = JSON.parse(resp.response)
      $("div.avatar img").attr("src", data.links.mini)
      $("input[name='avatar']").val(data.links.mini)
      @model.set("avatar", data.links.mini)
      $("button.btn-primary").trigger('click')

    upload_avatar_settings:
        filters: [
          title: "Image files"
          extensions : "jpg,gif,png"
        ]
        browse_button: "pick_file"

    events:
      'click button.btn-primary'       : 'save'
      'click div.avatar button'        : 'button_click'
      "change input[name='first']"     : "first_change",
      "change input[name='last']"      : "last_change",
      "change input[name='position']"  : "position_change",
      "change input[name='mobile']"    : "mobile_change",
      "change input[name='landline']"  : "landline_change",
      "change input[name='fax']"       : "fax_change",
      "change select[name='locale']"   : "locale_change"
      "change select[name='timezone']" : "timezone_change"

    initialize: () ->
      _.bindAll @
      @_demonstrated = false
#      @model = new Model(id: PrefetchModels.current_user().id)
#      @model.fetch()
      @model = PrefetchModels.current_user()
#      @model.bind "change", @render
      @template.directives(Directives.settings)

    render: () ->
      @$el.html(@template.render(@model.toJSON()))
      @$el = @localize(@$el)
      @choose_locale()
      @choose_timezone()
      @_after()
      @

    onShow: ()->
      @init_uploader()
      @render_sidebar()
      $ () ->
        jQuery('input, textarea').placeholder()

    choose_locale: ()->
      _.each @$el.find("select[name='locale'] option"), (option)=>
        if $(option).attr("value") == @model.get("locale")
          $(option).attr("selected", "selected")

    choose_timezone: ()->
      _.each @$el.find("select[name='timezone'] option"), (option)=>
        if $(option).attr("value") == @model.get("timezone")
          $(option).attr("selected", "selected")

    _after: ()->
      if @_demonstrated is false
        @_demonstrated = true
      else
        @render_sidebar()

    render_sidebar: () ->
      sidebar_view = new SidebarView()
      @replaceWithStaticChildInto(sidebar_view, @$("#sidebar"))

    first_change: (e)->
      @model.set("first", $(e.currentTarget).val())
      false

    last_change: (e)->
      @model.set("last", $(e.currentTarget).val())
      false

    position_change: (e)->
      @model.set("position", $(e.currentTarget).val())
      false

    mobile_change: (e)->
      @model.set("mobile", $(e.currentTarget).val())
      false

    landline_change: (e)->
      @model.set("landline", $(e.currentTarget).val())
      false

    fax_change: (e)->
      @model.set("fax", $(e.currentTarget).val())
      false

    locale_change: ()->
      selected = @$el.find("select[name='locale'] option:selected")
      @model.set("locale", selected.attr("value"))
      false

    timezone_change: ()->
      selected = @$el.find("select[name='timezone'] option:selected")
      @model.set("timezone", selected.attr("value"))
      false

    save: ()->
      $('form .alert').remove()
      $('button.btn-primary').attr('disabled', true)
      @model.save({ wait: true },
        success: (model, response) =>
          alerter = new Alerter()
          message = @getLocalizeTextByKey("settings.alerter.update.succeed")
          $('form h4').html(alerter.success(message).render())
          @model.set(model)
          window.localStorage.setItem('user_data', JSON.stringify(model))
          PrefetchModels.current_user_model = @model
        error: (model, response) =>
          alerter = new Alerter()
          message = @getLocalizeTextByKey("settings.alerter.update.failed")
          $('form h4').html(alerter.error([message]).render())
      )
      $('button.btn-primary').removeAttr('disabled')
      false

    button_click: (e)->
      false

    init_uploader: () ->
      @uploader = Uploader.getUploader(@upload_avatar_settings)
      @uploader.bind('Error',        @_avatar_error)
      @uploader.bind('FilesAdded',   @_avatar_added)
      @uploader.bind('FileUploaded', @_avatar_uploaded)
      $ () =>
        @uploader.init()
