define [
  "jquery",
  "underscore",
  "backbone",
  "cs!./model",
  "cs!./view.model",
  "cs!../status/view.sidebar",
  "cs!../toolbar/view.users.toolbar",
  "cs!../toolbar/view.search_results.toolbar",
  "cs!../prefetch_data/prefetch_models",
  "cs!../composite_view",
  "text!../../templates/users.html",
  "../aura/mediator",
  "cs!../modules"
], ($, _, Backbone, Model, UserView, SidebarView, ToolbarView, SearchResultsToolbarView, PrefetchModels, CompositeView,Template,mediator) ->
  class UsersView extends CompositeView
    events:
      "click button.servernext" : "next_page"

    initialize: () ->
      _.bindAll @
      @collection.bind "reset", @render_all
      @collection.bind "add",   @render_one
      @collection.pager = @collection_pager
      translation_key = Backbone.history.fragment
      @$el.html(Template)
      @$el.find("#header h1").attr('data-localize', "heading.#{translation_key}")
      @$el = @localize(@$el)
      @render_sidebar()
      @render_toolbar()
      @reset_paging()

    reset_paging: =>
      if @collection.page?  and @collection.page > 0
        @collection.page = 0
        @collection.reset()
        @collection.fetch()

    render: () ->
      @render_all()
      if @collection.length <= @collection.perPage
        @hide_next_page_button()
      else
        @show_next_page_button()
      @endless_pagination()
      @

    render_sidebar: () ->
      sidebar_view = new SidebarView()
      @replaceWithStaticChildInto(sidebar_view, @$("#sidebar"))

    render_toolbar: () ->
      search_results = PrefetchModels.search_results()
      if search_results.get('search_terms')
        toolbar_view = new SearchResultsToolbarView()
      else
        toolbar_view = new ToolbarView()
      @appendStaticChildInto(toolbar_view, @$(".btn-toolbar"))

    render_all: () =>
      @collection.each(@render_one)

    render_one: (model) ->
      view = new UserView(model: model)
      @appendChildInto(view, @$("ul.cards"))

    show_next_page_button: ()->
      @$el.find("button.servernext").removeClass("hide")

    hide_next_page_button: ()->
      @$el.find("button.servernext").addClass("hide")

    disable_next_page_button: ()->
      @$el.find("button.servernext").attr("disabled", "disabled")
#      @$el.find("button.servernext").html("No more results")

    enable_next_page_button: ()->
      @$el.find("button.servernext").removeAttr("disabled")
#      @$el.find("button.servernext").html("The next page")

    endless_pagination: ()->
      @collection.no_more_results = false
      $(window).unbind('scroll')
      $(window).scroll ()=>
        return if @collection.no_more_results
        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10)
          @collection.requestNextPage()

    collection_pager: ()->
      @collection.fetch success: (collection, resp)=>
        if collection.length == 0
          @show_next_page_button()
          @disable_next_page_button()
          @collection.page -= 1
          @collection.no_more_results = true

    next_page: () ->
      @hide_next_page_button()
      @collection.requestNextPage()
      $ () =>
        if $(document).height() > $(window).height()
          @endless_pagination()
        else
          @show_next_page_button()
          @enable_next_page_button()
      false