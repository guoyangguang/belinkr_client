define ["underscore", "backbone", "cs!./model","paginator"], (_, Backbone, User) ->
  class UserPaginatorCollection extends Backbone.Paginator.requestPager
    model:      User
    url:        "/users"

    pageAttribute: 'page'

    page:0
    firtPage:0
    perPage:5

