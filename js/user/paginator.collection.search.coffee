define ["underscore", "backbone", "cs!./model","paginator"], (_, Backbone, User) ->
  class SearchUserPaginatorCollection extends Backbone.Paginator.requestPager
    model:      User
    url:        => "/users/search"

    pageAttribute: 'page'
    perPageAttribute: 'perPage'
    queryAttribute: 'name'

    page:0
    firtPage:0
    perPage:20

    initialize: (models, options)->
      super
      @query=options['query']


