define ["underscore", "backbone","cs!../counter/model","cs!../user/model"], (_, Backbone, Counter, User) ->
  class PrefetchModels
    @counter: ->
      unless @counter_model
        @counter_model = new Counter()
        @counter_model.fetch()
      @counter_model

    @current_user: ->
      unless @current_user_model
        #user_data contain the user data
        store = localStorage.getItem('user_data')
        if store
          @current_user_model = new User(JSON.parse(store))
        else
          @current_user_model = {}
        
      @current_user_model
    @search_results: ->
      unless @search_result
        @search_result = new Backbone.Model()
      @search_result

    @current_user_is_admin: ()=>
      user = @current_user()
      user.get('role') is 'admin'