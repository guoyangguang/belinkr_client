define ["underscore", "backbone","cs!../user/paginator.collection.following", "cs!../user/paginator.collection.followers"], (_, Backbone, PaginatorFollowing, PaginatorFollowers) ->
  class PrefetchCollections
    @following: ->
      unless @following_collections
        @following_collections = new PaginatorFollowing()
        @following_collections.fetch()
      @following_collections
    @followers: ->
      unless @followers_collections
        @followers_collections = new PaginatorFollowers()
        @followers_collections.fetch()
      @followers_collections

