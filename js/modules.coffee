define ["jquery", "underscore", "aura/facade"], ($, _, facade)->
  # Subscription 'modules' for our views. These take the 
  # the form facade.subscribe( subscriberName, notificationToSubscribeTo , callBack )

  # Update view with latest todo content
  # Subscribes to: newContentAvailable

  facade.subscribe 'statuses_view','createStatus', (statuses_view)-> 

    statuses_view.submit_button.attr("disabled", "disabled")
    statuses_view.error_list.empty()

    if (statuses_view.text_field.val() != statuses_view.text_filler) && (statuses_view.text_field.val() != "") 
      statuses_view.collection.create({ "text": statuses_view.text_field.val() }, {
        wait:    true,
        success: statuses_view.reset_form,
        error:   statuses_view.errors
      })

    statuses_view.submit_button.removeAttr("disabled")
 
  facade.subscribe 'scraps_view','createScrap', (scraps_view)-> 
    callback = ->
      @submit_button.attr("disabled", "disabled")
      @error_list.empty()

      if (@text_field.attr("value") != @text_filler)  &&
         (@text_field.attr("value") != "")
        files = @$("input[name='files']").val()
        files = '[]' if (files is null) || (files is '')
        @collection.create({
          text: @text_field.val(),
          files: $.parseJSON(files),
          scrapbook_id:@collection.scrapbook_id,
          user_id: PrefetchModels.current_user().get("id")
        },{
          wait:     true,
          success:  @reset_form,
          error:    @errors
        })

      @submit_button.removeAttr("disabled")
    callback.call scraps_view

  facade.subscribe 'scrap_view','deleteScrap', (scrap_view)-> 
    scrap_view.model.destroy()

  facade.subscribe 'scrapbook_view','showScrapbook', (scrapbook_view)->
    window.App.navigate("/scrapbooks/" + scrapbook_view.model.id, { trigger: false })
    scrapbook_view.renderChildInto(scrapbook_view.scraps_view,"div#main")

  facade.subscribe 'status_view','deleteReply', (reply_view)-> 
    reply_view.model.destroy()

  facade.subscribe 'status_view','deleteStatus', (status_view)-> 
    status_view.model.destroy({}, { wait: true})

  facade.subscribe 'status_view','newReply', (status_view)-> 
    callback = ->
    callback.call status_view

  facade.subscribe 'workspace_statuses_view','createWorkspaceStatus', (workspace_statuses_view)-> 
    callback = ->
      @submit_button.attr("disabled", "disabled")
      @error_list.empty()

      if (@text_field.attr("value") != @text_filler)  &&
         (@text_field.attr("value") != "")
        files = @$("input[name='files']").val()
        files = '[]' if (files is null) || (files is '')
        @collection.create({
          text: @text_field.val(),
          files: $.parseJSON(files),
          workspace_id:@collection.workspace_id,
          user_id: PrefetchModels.current_user().get("id")
        }, {
          wait:     true,
          success:  @reset_form,
          error:    @errors
        })

      @submit_button.removeAttr("disabled")
    callback.call workspace_statuses_view


