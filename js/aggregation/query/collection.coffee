define ["underscore", "backbone", "cs!./model", "paginator"], (_, Backbone, Status) ->
  class AggregationQueryCollection extends Backbone.Paginator.requestPager
    model:      Status
    url:        "/microblog/aggregations/query"
    comparator: (model) ->
      model.get("create_date")

    pageAttribute: 'page'
    perPageAttribute: 'perPage'
    queryAttribute: 'q'

    page: 1
    firtPage: 1
    perPage: 20

    initialize: (models, options)->
      super
      @query=options.query
