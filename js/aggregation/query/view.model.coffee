define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../../composite_view",
  "cs!./model",
  "../../../templates/directives",
  'text!../../../templates/aggregation/query/status.html',
  "../../libs/pure/pure"
], ($, _, Backbone, CompositeView, Status, Directives, Template) ->
  class AggregationQueryStatusView extends CompositeView
    template:             jQuery(Template)
    tagName:              "li"
    className:            "status article span12 row-fluid"

    initialize: () ->
      _.bindAll @
      @template.directives(Directives.aggregation)

    render: () ->
      @$el.html(@template.render(@model.toJSON()).html())
#      @$el = @localize(@$el)
      @invoke_timeago()
      @