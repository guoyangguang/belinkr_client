define [
  'jquery',
  'underscore',
  'backbone',
  'cs!./model',
  'cs!./view.model',
  'cs!../../composite_view',
  'text!../../../templates/aggregation/query/statuses.html'
], ($, _, Backbone, Model, StatusView, CompositeView, Template) ->
  class AggregationQueryStatusesView extends CompositeView

    template: jQuery(Template)

    initialize: () ->
      _.bindAll @
      @collection.bind 'reset', @render
      @collection.bind 'add',   @render_one
      @collection.pager = @collection_pager

      @$el.html(@template.html())
      @$el = @localize(@$el)

      @status_list    = @$('ul.statuses')

    render: () =>
      @render_all()
      if @collection.length <= @collection.perPage
        @hide_next_page_button()
      else
        @show_next_page_button()
      @endless_pagination()
      @

    render_all: () ->
      @collection.each(@render_one)

    render_one: (model) =>
      view = new StatusView(model: model)
      @appendChildInto(view, @status_list)

    show_next_page_button: ()->
      @$el.find("button.servernext").removeClass("hide")

    hide_next_page_button: ()->
      @$el.find("button.servernext").addClass("hide")

    disable_next_page_button: ()->
      @$el.find("button.servernext").attr("disabled", "disabled")

    enable_next_page_button: ()->
      @$el.find("button.servernext").removeAttr("disabled")

    render_sidebar: () =>
      sidebar_view = new StatusesSidebarView()
      @replaceWithStaticChildInto(sidebar_view, @$("#sidebar"))


    endless_pagination: ()->
      @collection.no_more_results = false
      $(window).unbind('scroll')
      $(window).scroll ()=>
        return if @collection.no_more_results
        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10)
          @collection.requestNextPage()

    collection_pager: ()->
      @collection.fetch success: (collection, resp)=>
        if collection.length == 0
          @show_next_page_button()
          @disable_next_page_button()
          @collection.page -= 1
          @collection.no_more_results = true

    next_page: () ->
      @hide_next_page_button()
      @collection.requestNextPage()
      $ () =>
        if $(document).height() > $(window).height()
          @endless_pagination()
        else
          @show_next_page_button()
          @enable_next_page_button()
      false

