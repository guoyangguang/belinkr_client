define [
  "order!jquery",
  "underscore",
  "backbone",
  "cs!./localize",
  "timeago",
  'order!polyfills/jquery.placeholder',
], ($, _, Backbone, localizer) ->
  class ExternalView extends Backbone.View

    locale_events:
      'click a.en'                : 'set_locale_en'
      'click a.zh'                : 'set_locale_zh'

    onShow: ()->
      $ () ->
        jQuery('input, textarea').placeholder()

    constructor: ()->
      super

    invoke_timeago: () ->
      @$(".timeago").timeago()

    localize: (el)->
      lang = $.cookie('belinkr.locale')
      localizer el, lang

    getLocalizeTextByKey: (key) ->
      return '' unless typeof(key) is 'string'
      div = $("<div />")
      span = $("<span />",
        "data-localize": key
        "text": 'empty'
      )
      div.append(span)
      @localize(div)
      text = span.html()
      div.empty()
      text


    _set_locale: (locale) ->
      $.cookie('belinkr.locale', locale, { expires: null, path: '/' })
      Backbone.history.loadUrl(Backbone.history.fragment)
      false

    set_locale_zh: (e) ->
      e.preventDefault()
      @_set_locale('zh') unless $.cookie('belinkr.locale') is 'zh'

    set_locale_en: (e) ->
      e.preventDefault()
      @_set_locale('en') unless $.cookie('belinkr.locale') is 'en'