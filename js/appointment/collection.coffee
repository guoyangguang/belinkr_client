define ["underscore", "backbone", "cs!./model"], (_, Backbone, Appointment) ->
  class AppointmentCollection extends Backbone.Collection
    model:      Appointment
    url:        "/appointments"
    comparator: (model) ->
      model.get("updated_at")
    toFullCalendarFormat: ->
      @models.map (model)-> model.toFullCalendarFormat()


