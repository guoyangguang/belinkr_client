define [
  "jquery",
  "underscore",
  "backbone",
  "cs!./model",
  "cs!./view.model",
  "cs!../composite_view",
  "text!../../templates/appointments.html",
  "../aura/mediator",
  "cs!../modules",
  "order!fullcalendar"
], ($, _, Backbone, Appointment, AppointmentView,CompositeView,Template,mediator) ->
  class AppointmentsView extends CompositeView
    template: jQuery(Template)
    id: "mainview_container"
    
    initialize: () ->
      _.bindAll(@, "render", "onShow", "addAll", "addOne","change", "destroy",
        "eventClick", "eventDropOrResize", "errors")
      @collection.bind "reset", @render
      @collection.bind "add",   @addOne
      @collection.bind "change",@change            
      @collection.bind "destroy",@destroy            
      @$el.html(Template)
      
      # Markup hooks
    addAll: ()->
      $("#calendar").fullCalendar("addEventSource",@collection
        .toFullCalendarFormat())
    render: ->
      @addAll()
      @
    onShow: ->
      @render_calendar_template()
      @addAll()

    render_calendar_template:()=>
 
      $("#calendar").fullCalendar                              
        header:                                                
          left: 'prev,next today',                        
          center: 'title',                                
          right: 'month,basicWeek,basicDay'
          ignoreTimezone: false
        selectable: true
        selectHelper: true
        select: @select
        editable: true

        eventClick: @eventClick,
        eventDrop: @eventDropOrResize,        
        eventResize:@eventDropOrResize

    create: () =>
      false
    select: (startDate, endDate)=>
      model = new Appointment
        start_at: startDate
        end_at: endDate
      view = new AppointmentView
        model:model 
      view.collection =@collection
      @renderChild(view)

    addOne: (event)->
      
      $("#calendar").fullCalendar('renderEvent',event.toFullCalendarFormat())

    eventClick:(fcEvent) ->
      model = @collection.get(fcEvent.id)
      view = new AppointmentView
        model:model 
      view.collection =@collection
      @renderChild(view)

    change: (appointment)->
      fcEvent = $("#calendar").fullCalendar('clientEvents',appointment.get('id'))[0]
      
      appointment_json = appointment.toFullCalendarFormat()
      fcEvent.title = appointment_json.title
      fcEvent.color = appointment_json.color
      $("#calendar").fullCalendar('updateEvent',fcEvent) 

    eventDropOrResize:(fcEvent) ->
      @collection.get(fcEvent.id).save({start_at: fcEvent.start, end_at: fcEvent.end})
      
    destroy: (event)->
      $("#calendar").fullCalendar('removeEvents', event.id)

    render_all: () ->
      #@$("ul.events li").remove()
      @collection.each(@render_one)

    render_one: (model) =>
      view = new AppointmentView(model: model)
      view.render()
      #@status_list.prepend(view.el)
      @prependChildInto(view, @event_list)
      model.bind("remove", view.remove)

    errors: (model, response, xhr) =>
      if response && response.responseText
        attrs_with_errors = JSON.parse(response.responseText).errors
        _.each(attrs_with_errors, (errors) ->
          _.each(errors, (error) ->
            @$("ul.errors").append("<li>" + error + "</li>")
          )
        )

