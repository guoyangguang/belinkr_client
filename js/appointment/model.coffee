define ["underscore", "backbone"], (_, Backbone) ->
  class Appointment extends Backbone.Model
    urlRoot: "/appointments"
    defaults: {
      created_at: new Date()
      updated_at: new Date()
      description: 'sample description'
      #TODO: assign the id only for test
      user_id: 1
      entity_id: 1
    }
    toFullCalendarFormat: ->
      json=
        id    : @get('id')
        title : @get('name')
        color : @get('description')
        start : @get('start_at')
        end   : @get('end_at')


