define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../composite_view",
  "cs!./model",
  "../../templates/directives",
  "text!../../templates/appointment.html",
  "../aura/mediator",
  "cs!../modules",
  "order!jqueryui/dialog"
], ($, _, Backbone, CompositeView, Model, Directives, Template,mediator) ->
  class AppointmentView extends CompositeView
    template: jQuery(Template)
    id: "eventDialog"

    initialize: ->
      _.bindAll @
      @template.directives(Directives.appointment)
      @_save_text   = @getLocalizeTextByKey("action.save")
      @_delete_text = @getLocalizeTextByKey("action.delete")
      @_cancel_text = @getLocalizeTextByKey("action.cancel")

    render: =>
      buttons = {}
      buttons[@_save_text] = @save
      if !@model.isNew()
        buttons[@_delete_text] = @destroy
      buttons[@_cancel_text] = @close
      
      @$el.html(@template.render(@model.toFullCalendarFormat()))
      @$el = @localize(@$el)
      
      $(@$el).dialog(
        modal: true
        title: @getLocalizeTextByKey("appointment.dialog.title")
        buttons: buttons
        )
      @

    close: =>
      $(@$el).dialog('close')
      
      idx = @parent.children.indexOf(@)
      @parent.children.splice(idx,1)

    save: =>
      @model.set
        name: @$('#title').val()
        description: @$('#color').val()
      if @model.isNew() 
        @collection.create(@model,success: @close, wait: true)
      else
        @model.save {}, {success: @close, wait: true}

    destroy: =>
      @model.destroy
        success: @close
     
