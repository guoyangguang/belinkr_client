define [
  'jquery',
  'underscore',
  'cs!../libs/websocket/notification'
], ($, _, NotificationWebSocket) ->
  class NotificationSynchronizer
    @mode: 'websocket' # poll or websocket
    @running: false
    @refresh: (data)->
      window.Notifications = [] unless window.Notifications
      window.Notifications = window.Notifications.concat(data)
      dom = jQuery($.find('li.notifications span.badge'))
      if window.Notifications.length is 0
        dom.addClass('hide')
      else
        dom.removeClass('hide')
        dom.html(window.Notifications.length)

    @poll: ()=>
      $.ajax
        url: "/notifications/read/no"
        dataType: 'json'
        success: (data, status, resp)=>
          @refresh(data)
      setTimeout('NotificationSynchronizer.poll()', 3 * 60 * 1000)

    @sync: (options={})=>
      @running = true
      switch @mode
        when 'poll'
          @poll()
        when 'websocket'
          NotificationWebSocket.init(options, @)
        else
          ''

  window.NotificationSynchronizer = NotificationSynchronizer