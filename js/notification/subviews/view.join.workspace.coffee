define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../view.model.base"
], ($, _, Backbone, BaseView) ->
  class NotifcationJoinedWorkspaceView extends BaseView

    render: ()->
      @pre_render()
      @predicate_p.append(@render_actor())
      @predicate_p.append(@render_action())
      @predicate_p.append(@render_object())
      @$("div.predicate").after(@state_for_membership())
      @

    state_for_membership: ()->
      div = $("<div />",
        class: "state accepted"
        text: @getLocalizeTextByKey(
          "notification.workspace.invitation.state.joined"
        )
      )
      div