define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../view.model.base"
], ($, _, Backbone, BaseView) ->
  class NotifcationPostReplyToWorkspaceStatusView extends BaseView

    render: ()->
      @pre_render()
      @predicate_p.append(@render_actor())
      @predicate_p.append(@render_action())
      @predicate_p.append(@render_object())
      @predicate_p.append(@render_to())
      @predicate_p.append(@render_workspace())
      @predicate_p.append(@render_target())
      @$("div.predicate").after(@descrition_for_object())
      @

    render_object: ()->
      object = @model.get('object')
      text = @getLocalizeTextByKey(
        "notification.polymap." + object.object_type
      )
      $("<a />",
        class: 'object'
        href: "/workspaces/" + @model.get('target').workspace_id + object.links.self
        text: " " + text
      )

    render_workspace: ()->
      text = @getLocalizeTextByKey(
        "notification.polymap.workspace"
      ) + " [ " + @model.get('target').workspace_name + " ]"
      $("<a />",
        class: "target"
        href: @model.get('target').links.workspace
        text: " " + text + " "
      )