define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../view.model.base"
], ($, _, Backbone, BaseView) ->
  class NotifcationFollowUserView extends BaseView

    render: ()->
      @pre_render()
      @predicate_p.append(@render_actor())
      @predicate_p.append(@render_action())
      @predicate_p.append(@render_object())
      @