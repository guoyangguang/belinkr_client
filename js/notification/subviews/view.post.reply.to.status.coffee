define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../view.model.base"
], ($, _, Backbone, BaseView) ->
  class NotifcationPostReplyToStatusView extends BaseView

    render: ()->
      @pre_render()
      #@predicate_p.append(@render_actor())
      @predicate_p.append(@render_actor_for())
      @predicate_p.append(@render_action())
      @predicate_p.append(@render_object())
      @predicate_p.append(@render_to())
      @predicate_p.append(@render_target())
      @$("div.predicate").after(@descrition_for_object())
      @

    render_actor_for: ()->
      object = @model.get('object')
      if object.third_party
        logo_3rd_uri = "/img/microblog/#{object.third_party.type}/logo.png"
        logo = $("<img />",
          src: logo_3rd_uri
          class: "#{object.third_party.type}-microblog"
        )
        if object.third_party.user
          user_span = $("<span />",
            text: object.third_party.user.screen_name
          )
        else
          user_span = $("<span />")

        div = $("<div />")
        div.append(logo)
        div.append(user_span)
        div
      else
        @render_actor()

    descrition_for_object: ()->
      object = @model.get('object')
      translated = @getLocalizeTextByKey(
        "notification.polymap." + object.object_type
      )
      translated += ": "
      text = object.text || object.name

      if object.third_party
        logo_3rd_uri = "/img/microblog/#{object.third_party.type}/logo.png"
        logo = $("<img />",
          src: logo_3rd_uri
          class: "#{object.third_party.type}-microblog"
        )
        avatar = null
        if object.third_party.user
          user_span = $("<span />",
            text: object.third_party.user.screen_name
          )
          switch object.third_party.type
            when 'sina'
              img_src = object.third_party.user.profile_image_url
          if img_src
            avatar = $("<img />",
              src: img_src,
              class: 'avatar'
            )
        else
          user_span = $("<span />")

        div = $("<div />")
        div.append(logo)
        div.append(avatar) if avatar
        div.append(user_span)
        text += " -- #{div.html()}"

      if text
        div = $("<div />",
          class: 'description'
        )
        p = $("<p />")
        p.html(translated + text)
        div.append(p)
        div

