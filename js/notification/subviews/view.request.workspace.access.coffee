define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../view.model.base"
], ($, _, Backbone, BaseView) ->
  class NotifcationRequestWorkspaceAccessView extends BaseView
    events:
      "click"                : 'mark_as_read'
      "click button.decline" : 'reject_request'
      "click button.accept"  : 'accept_request'
      "click a"   : "nav_link"
      "click img" : "nav_actor"

    render: ()->
      @pre_render()
      @predicate_p.append(@render_actor())
      @predicate_p.append(@render_action())
      @predicate_p.append(@render_target())

      state = @autoinvitation_state()
      switch state
        when 'pending', 'accepted'
          css = 'accepted'
        else
          css = 'rejected'

      if @is_pending()
        if @is_administrator()
          controls = @controls_for_request()
          @$("div.predicate").after(controls) if controls
        else
          @$("div.predicate").after(
            @state_for_autoinvitation(state, css)
          )
      else
        @$("div.predicate").after(
          @state_for_autoinvitation(state, css)
        )
      @

    render_action: ()->
      action = @getLocalizeTextByKey(
        "notification.action.workspace_request_access"
      )
      $("<span />",
        class: 'action'
        text: action
      )

    autoinvitation_state: ()->
      object = @model.get('object')
      object.state

    is_pending: ()->
      @autoinvitation_state() is 'pending'

    is_administrator: ()->
      target = @model.get('target')
      target.relationship is 'administrator'

    controls_for_request: ()->
      div = $("<div />",
        class: 'controls'
      )
      btn_decline = $("<button />",
        class: 'decline'
        text: @getLocalizeTextByKey(
          "notification.workspace.invitation.action.decline"
        )
      )
      btn_accept  = $("<button />",
        class: 'accept'
        text: @getLocalizeTextByKey(
          "notification.workspace.invitation.action.accept"
        )
      )
      error_message = $("<div />",
        class: 'error_messages'
        id: "error_message_for" + @model.get('id')
      )
      div.append(btn_decline)
      div.append(btn_accept)
      div.append(error_message)
      div

    state_for_autoinvitation: (state, css)->
      div = $("<div />",
        class: "state " + css
        text: @getLocalizeTextByKey(
          "notification.workspace.invitation.state." + state
        )
      )
      div

    reject_request: (e) ->
      e.preventDefault()
      if @is_administrator() && @is_pending()
        workspace_id = @model.get('target').id
        autoinvitation_id = @model.get('object').id
        request_url = '/workspaces/' + workspace_id + '/autoinvitations/rejected/' + autoinvitation_id
        $.ajax
          url: request_url
          type:'PUT'
          dataType: 'json'
          success: (response)=>
            @model.get('object').state = 'rejected'
            @model.trigger('change')
          error: (xhr, text_status, error_message)=>
            message = '[' + @getLocalizeTextByKey(
              "notification.workspace.invitation.action.decline"
            ) + ']'
            message += ": "
            message += @getLocalizeTextByKey(
              "notification.workspace.invitation.request_failed"
            )
            @$el.find("#error_message_for" + @model.get("id"))
              .html(message)

    accept_request: (e) ->
      e.preventDefault()
      if @is_administrator() && @is_pending()
        workspace_id = @model.get('target').id
        autoinvitation_id = @model.get('object').id
        request_url = '/workspaces/' + workspace_id + '/autoinvitations/accepted/' + autoinvitation_id
        $.ajax
          url: request_url
          type:'PUT'
          dataType: 'json'
          success: (response)=>
            @model.get('object').state = 'accepted'
            @model.trigger('change')
          error: (xhr, text_status, error_message)=>
            message = '[' + @getLocalizeTextByKey(
              "notification.workspace.invitation.action.accept"
            ) + ']'
            message += ": "
            message += @getLocalizeTextByKey(
              "notification.workspace.invitation.request_failed"
            )
            @$el.find("#error_message_for" + @model.get("id"))
              .html(message)
