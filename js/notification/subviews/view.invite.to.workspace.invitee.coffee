define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../view.model.base"
], ($, _, Backbone, BaseView) ->
  class NotifcationInviteToWorkspaceInviteeView extends BaseView
    events:
      "click"                : 'mark_as_read'
      "click button.decline" : 'reject_invitation'
      "click button.accept"  : 'accept_invitation'
      "click a"   : "nav_link"
      "click img" : "nav_actor"

    render: ()->
      @pre_render()
      @predicate_p.append(@render_actor())
      @predicate_p.append(@render_action())
      @predicate_p.append(@render_object())
      @predicate_p.append(@render_to())
      @predicate_p.append(@render_target())
      if @i_can_play_with_controls()
        controls = @controls_for_invitation()
        @$("div.predicate").after(controls) if controls
      else
        target = @model.get('target')
        switch target.invitee.state
          when 'pending', 'accepted'
            css = 'accepted'
          else
            css = 'rejected'
        state = @state_for_invitation(target.invitee.state, css)
        @$("div.predicate").after(state) if state
      @

    render_to: ()->
      to = @getLocalizeTextByKey("notification.to.workspace_invitation")
      $("<span />",
        class: 'to'
        text: to
      )

    i_can_play_with_controls: ()->
      target = @model.get('target')
      if target
        target.invitee.state is 'pending'
      else
        false

    controls_for_invitation: ()->
      div = $("<div />",
        class: 'controls'
      )
      btn_decline = $("<button />",
        class: 'decline'
        text: @getLocalizeTextByKey(
          "notification.workspace.invitation.action.decline"
        )
      )
      btn_accept  = $("<button />",
        class: 'accept'
        text: @getLocalizeTextByKey(
          "notification.workspace.invitation.action.accept"
        )
      )
      error_message = $("<div />",
        class: 'error_messages'
        id: "error_message_for" + @model.get('id')
      )
      div.append(btn_decline)
      div.append(btn_accept)
      div.append(error_message)
      div

    state_for_invitation: (state, css)->
      div = $("<div />",
        class: "state " + css
        text: @getLocalizeTextByKey(
          "notification.workspace.invitation.state." + state
        )
      )
      div

    reject_invitation: (e) ->
      e.preventDefault()
      if @i_can_play_with_controls()
        $.ajax
          url: @model.get('target').links.invitation_reject
          type:'PUT'
          dataType: 'json'
          success: (response)=>
            @model.get('target').invitee.state = 'rejected'
            @model.trigger('change')
          error: (xhr, text_status, error_message)=>
            message = '[' + @getLocalizeTextByKey(
              "notification.workspace.invitation.action.decline"
            ) + ']'
            message += ": "
            message += @getLocalizeTextByKey(
              "notification.workspace.invitation.request_failed"
            )
            @$el.find("#error_message_for" + @model.get("id"))
              .html(message)

    accept_invitation: (e) ->
      e.preventDefault()
      if @i_can_play_with_controls()
        $.ajax
          url: @model.get('target').links.invitation_accept
          type:'PUT'
          dataType: 'json'
          success: (response)=>
            @model.get('target').invitee.state = 'accepted'
            @model.trigger('change')
          error: (xhr, text_status, error_message)=>
            message = '[' + @getLocalizeTextByKey(
              "notification.workspace.invitation.action.accept"
            ) + ']'
            message += ": "
            message += @getLocalizeTextByKey(
              "notification.workspace.invitation.request_failed"
            )
            @$el.find("#error_message_for" + @model.get("id"))
              .html(message)