define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../view.model.base"
], ($, _, Backbone, BaseView) ->
  class NotifcationAdminRemoveReplyView extends BaseView

    render: ()->
      @pre_render()
      @predicate_p.append(@render_admin_role())
      @predicate_p.append(@render_actor())
      if $.cookie('belinkr.locale') is 'zh'
        @predicate_p.append(@render_to())
        @predicate_p.append(@render_target())
        @predicate_p.append(@render_action())
        @predicate_p.append(@render_object())
      else
        @predicate_p.append(@render_action())
        @predicate_p.append(@render_object())
        @predicate_p.append(@render_to())
        @predicate_p.append(@render_target())

      @$("div.predicate").after(@descrition_for_object())
      @


    render_admin_role: ()->
      if @model.get('actor').role is 'admin'
        @getLocalizeTextByKey(
          "notification.entity_admin"
        )
      else
        ''

    render_to: ()->
      action = @getLocalizeTextByKey(
        "notification.to.from"
      )
      $("<span />",
        class: 'action'
        text: action
      )

    render_object: ()->
      dom = super
      yours =  @getLocalizeTextByKey(
        "notification.yours"
      )
      yours + dom.text()