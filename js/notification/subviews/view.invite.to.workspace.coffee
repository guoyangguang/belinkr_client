define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../view.model.base"
], ($, _, Backbone, BaseView) ->
  class NotifcationInviteToWorkspaceView extends BaseView

    render: ()->
      @pre_render()
      @predicate_p.append(@render_actor())
      @predicate_p.append(@render_action())
      @predicate_p.append(@render_object())
      @predicate_p.append(@render_to())
      @predicate_p.append(@render_target())
      target = @model.get('target')
      switch target.invitee.state
        when 'pending', 'accepted'
          css = 'accepted'
        else
          css = 'rejected'
      state = @state_for_invitation(target.invitee.state, css)
      @$("div.predicate").after(state) if state
      @

    render_to: ()->
      to = @getLocalizeTextByKey("notification.to.workspace_invitation")
      $("<span />",
        class: 'to'
        text: to
      )

    state_for_invitation: (state, css)->
      div = $("<div />",
        class: "state " + css
        text: @getLocalizeTextByKey(
          "notification.workspace.invitation.state." + state
        )
      )
      div