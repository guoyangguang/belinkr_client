define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../view.model.base"
], ($, _, Backbone, BaseView) ->
  class NotifcationAdminRemoveStatusView extends BaseView

    render: ()->
      @pre_render()
      @predicate_p.append(@render_admin_role())
      @predicate_p.append(@render_actor())
      @predicate_p.append(@render_action())
      @predicate_p.append(@render_object())
      @$("div.predicate").after(@descrition_for_object())
      @


    render_admin_role: ()->
      if @model.get('actor').role is 'admin'
        @getLocalizeTextByKey(
          "notification.entity_admin"
        )
      else
        ''

    render_object: ()->
      dom = super
      yours =  @getLocalizeTextByKey(
        "notification.yours"
      )
      yours + dom.text()