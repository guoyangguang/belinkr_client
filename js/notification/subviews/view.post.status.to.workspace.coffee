define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../view.model.base"
], ($, _, Backbone, BaseView) ->
  class NotifcationPostStatusToWorkspaceView extends BaseView

    render: ()->
      @pre_render()
      @predicate_p.append(@render_actor())
      @predicate_p.append(@render_action())
      @predicate_p.append(@render_object())
      @predicate_p.append(@render_to())
      @predicate_p.append(@render_workspace())
      @$("div.predicate").after(@descrition_for_object())
      @

    render_workspace: ()->
      text = @getLocalizeTextByKey(
        "notification.polymap.workspace"
      ) + " [ " + @model.get('object').workspace_name + " ]"
      $("<a />",
        class: "target"
        href: @model.get('object').links.workspace
        text: " " + text
      )
