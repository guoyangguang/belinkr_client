define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../view.model.base"
], ($, _, Backbone, BaseView) ->
  class NotifcationDemoteCollaboratorFromWorkspaceView extends BaseView

    render: ()->
      @pre_render()
      @predicate_p.append(@render_actor())
      @predicate_p.append(@render_action())
      @predicate_p.append(@render_object())
      @predicate_p.append(@render_to())
      @predicate_p.append(@render_target())
      @predicate_p.append(@render_membership())
      @

    render_to: ()->
      action = @getLocalizeTextByKey(
        "notification.to.as"
      )
      $("<span />",
        class: 'action'
        text: action
      )

    render_membership: ()->
      member = @getLocalizeTextByKey(
        "notification.workspace.membership.collaborator"
      )
      $("<span />",
        text: " " + member
      )