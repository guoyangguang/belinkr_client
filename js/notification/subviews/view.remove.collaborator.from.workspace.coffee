define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../view.model.base"
], ($, _, Backbone, BaseView) ->
  class NotifcationRemoveCollaboratorFromWorkspaceView extends BaseView

    render: ()->
      @pre_render()
      @predicate_p.append(@render_actor())
      if $.cookie('belinkr.locale') is 'zh'
        @predicate_p.append(@render_to())
        @predicate_p.append(@render_target())
        @predicate_p.append(@render_action())
        @predicate_p.append(@render_object())
      else
        @predicate_p.append(@render_action())
        @predicate_p.append(@render_object())
        @predicate_p.append(@render_to())
        @predicate_p.append(@render_target())

      @

    render_to: ()->
      action = @getLocalizeTextByKey(
        "notification.to.from"
      )
      $("<span />",
        class: 'action'
        text: action
      )