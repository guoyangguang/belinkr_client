define [
  'jquery',
  'underscore',
  'backbone',
  'cs!./model',
  'cs!./view.model.box',
  'cs!../status/view.sidebar',
  'cs!../composite_view',
  'text!../../templates/notifications.html',
  '../aura/mediator',
  'cs!../modules'
], ($, _, Backbone, Model, NotificationView, StatusesSidebarView, CompositeView, Template, mediator) ->
  class NotificationsView extends CompositeView
    template: jQuery(Template)

    initialize: () ->
      _.bindAll @
      @collection.bind 'reset', @render
      @collection.bind 'add',   @render_one
      @collection.pager     = @collection_pager

      @$el.html(@template.html())
      @$el = @localize(@$el)

      @render_sidebar()

    onShow: ()->

    render: () ->
      @render_all()
      if @collection.length <= @collection.perPage
        @hide_next_page_button()
      else
        @show_next_page_button()
      @endless_pagination()
      @invoke_timeago()
      @

    show_next_page_button: ()->
      @$el.find("button.servernext").removeClass("hide")

    hide_next_page_button: ()->
      @$el.find("button.servernext").addClass("hide")

    disable_next_page_button: ()->
      @$el.find("button.servernext").attr("disabled", "disabled")

    enable_next_page_button: ()->
      @$el.find("button.servernext").removeAttr("disabled")

    render_sidebar: () =>
      sidebar_view = new StatusesSidebarView()
      @replaceWithStaticChildInto(sidebar_view, @$("#sidebar"))

    render_all: () ->
      groups = @collection.groupBy (model)=>
        @timeago_parse(model.get("created_at")).toDateString()
      _.each (groups), (models, day) =>
        @render_day(day)
        _.each models, @render_one

      @invoke_timeago()

    render_day: (day)->
      reg = /\s/mg
      group_id = day.replace(reg, '_')
      unless @$("h3#" + group_id).length > 0
        day = new Date(day)
        if (new Date()).toDateString().replace(reg, '_') is group_id
          tran_key = "notification.date.today"
        else
          tran_key = "notification.date.d" + day.getDay()
        translated = @getLocalizeTextByKey(tran_key)
        date_string = day.getFullYear() + "." + (day.getMonth() + 1) + "."  + day.getDate()
        @$("#main #notifications").append("
          <h3 id='" + group_id + "'>" + translated + " - " + date_string + "</h3>" +
          "<ul class='activities' id='ul_" + group_id + "'></ul>"
        )

    render_one: (model) ->
      reg = /\s/mg
      created_at = @timeago_parse(model.get("created_at"))
      created_at = created_at.toDateString()
      group_id = created_at.replace(reg, '_')
      view = new NotificationView(model: model)
      @appendChildInto(view, @$("ul#ul_" + group_id))

    endless_pagination: ()->
      $(window).unbind('scroll')
      $(window).scroll ()=>
        return if @collection.no_more_results
        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10)
          @collection.requestNextPage()

    collection_pager: ()->
      @collection.fetch success: (collection, resp)=>
        if collection.length == 0
          @show_next_page_button()
          @disable_next_page_button()
          @collection.page -= 1
          @collection.no_more_results = true

    next_page: () ->
      @hide_next_page_button()
      @collection.requestNextPage()
      $ () =>
        if $(document).height() > $(window).height()
          @endless_pagination()
        else
          @show_next_page_button()
          @enable_next_page_button()
      false

    timeago_parse: (timeago)->
      if timeago instanceof Date
        timestamp
      else
        s = $.trim(timeago);
        s = s.replace(/\.\d\d\d+/,"")
        s = s.replace(/-/,"/").replace(/-/,"/")
        s = s.replace(/T/," ").replace(/Z/," UTC")
        s = s.replace(/([\+\-]\d\d)\:?(\d\d)/," $1$2")
        new Date(s);