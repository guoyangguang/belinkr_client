define [
  "jquery",
  "underscore",
  "backbone",
  "../../templates/directives"
  "text!../../templates/notification.html"
  "cs!../composite_view",
  "cs!../alerter/helper",
  "../libs/pure/pure"
], ($, _, Backbone, Directives, Template, CompositeView, Alerter) ->
  class NotificationBaseView extends CompositeView
    template : jQuery(Template)
    tagName: 'li'
    events:
      "click"     : "mark_as_read"
      "click a"   : "nav_link"
      "click img" : "nav_actor"

    initialize: () ->
      _.bindAll @
      @model.bind("change", @render)
      @template.directives(Directives.notification)

    pre_render: ()->
      @$el.html( @template.render @model.toJSON() )
      @predicate_p = @$("div.predicate p")
      @clean_up()

      css = 'read' if @model.get('read')
      @$el.addClass(css || '')
      @predicate_p.empty()
      @invoke_timeago()

    render: ()->
      @

    clean_up: ()->
      @$("div.description").remove()
      @$("div.controls").remove()
      @$("div.state").remove()

    leave: ()->
      @unbind()
      @remove()

    remove: () ->
      @$el.remove()

    fadeOutremove: () ->
      $(@el).fadeOut(250,=>@remove())


    # polymorphic objects
    polymorphic_object: (flag)->
      object = @model.get(flag)
      if object
        if object.name
          text = object.name
          if object.object_type is 'workspace'
            text = @getLocalizeTextByKey(
              "notification.polymap." + object.object_type
            ) + " [ " + text + " ]"
        else
          text = @getLocalizeTextByKey(
            "notification.polymap." + object.object_type
          )
        if object.object_type is 'user'
          if object.id is PrefetchModels.current_user().get('id')
            text = @getLocalizeTextByKey("notification.you")

        $("<a />",
          class: flag
          href: object.links.self
          text: " " + text
        )

    render_actor: ()->
      @polymorphic_object('actor')

    render_object: ()->
      @polymorphic_object('object')

    render_target: ()->
      @polymorphic_object('target')

    render_action: ()->
      action = @getLocalizeTextByKey(
        "notification.action." + @model.get('action')
      )
      $("<span />",
        class: 'action'
        text: action
      )

    render_to: ()->
      to = @getLocalizeTextByKey("notification.to.default")
      $("<span />",
        class: 'to'
        text: to
      )

    descrition_for_object: ()->
      object = @model.get('object')
      translated = @getLocalizeTextByKey(
        "notification.polymap." + object.object_type
      )
      translated += ": "
      text = object.text || object.name
      if text
        div = $("<div />",
          class: 'description'
        )
        p = $("<p />")
        p.html(translated + text)
        div.append(p)
        div

    #
    # events
    #
    mark_as_read: (e)->
      e.preventDefault()
      if @model.get("read") is false
        $.ajax
          url: "/notifications/" + @model.get("id")
          type: "PUT"
          success: (response) =>
            @model.set('read', true)
          error: (xhr, text_status, error_message)=>
            error_message

    nav_actor: (e) ->
      #      @mark_as_read()
      window.App.navigate(@model.get('actor').links.self, trigger: true)

    nav_link: (e) ->
      e.preventDefault()
      #      @mark_as_read()
      link = $(e.target)
      href = link.attr('href')
      window.App.navigate(href, trigger: true)