define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../composite_view",
  "cs!../alerter/helper",
  "cs!./subviews/view.post.status",
  "cs!./subviews/view.post.status.to.workspace"
  "cs!./subviews/view.post.reply.to.status"
  "cs!./subviews/view.post.reply.to.workspace.status",
  "cs!./subviews/view.invite.to.workspace"
  "cs!./subviews/view.invite.to.workspace.invitee"
  "cs!./subviews/view.join.workspace"
  "cs!./subviews/view.request.workspace.access",
  "cs!./subviews/view.remove.collaborator.from.workspace",
  "cs!./subviews/view.promote.collaborator.to.workspace",
  "cs!./subviews/view.demote.collaborator.from.workspace",
  "cs!./subviews/view.follow.user",
  "cs!./subviews/view.admin.remove.status",
  "cs!./subviews/view.admin.remove.reply",
  "../libs/pure/pure"
], ($, _, Backbone, CompositeView, Alerter,\
PostStatusView, PostStatusToWorkspaceView, PostReplyToStatusView,\
PostReplyToWorkspaceStatusView, InviteToWorkspaceView,\
InviteToWorkspaceInviteeView, JoinedWorkspaceView, RequestWorkspaceAccessView,\
RemoveCollaboratorFromWorkspaceView, PromoteCollaboratorToWorkspaceView,\
DemoteCollaboratorFromWorkspaceView, FollowUserView, AdminRemoveStatusView,\
AdminRemoveReplyView
) ->
  class NotificationBoxView extends CompositeView
    initialize: ()->
      _.bindAll @

    render: ()->
      @subview = @init_for_sub_view()
      if @subview
        @subview.render()
        @el = @subview.el

      @

    leave: ()->
      @unbind()
      @remove()

    remove: () ->
      @$el.remove()
      @subview?.$el?.remove()

    fadeOutremove: () ->
      $(@el).fadeOut(250,=>@remove())

    init_for_sub_view: ()->
      return new PostStatusView(model: @model) if @is_post_status_to_general()
      return new PostStatusToWorkspaceView(model: @model) if @is_post_status_to_workspace()

      return new PostReplyToStatusView(model: @model) if @is_post_reply_to_status_general()
      return new PostReplyToWorkspaceStatusView(model: @model) if @is_post_reply_to_workspace_status()

      if @is_workspace_invitation()
        if @i_am_the_workspace_invitee()
          return new InviteToWorkspaceInviteeView(model: @model)
        else
          return new InviteToWorkspaceView(model: @model)

      return new JoinedWorkspaceView(model: @model) if @is_joined_workspace()
      return new RequestWorkspaceAccessView(model: @model) if @is_access_workspace_request()
      return new RemoveCollaboratorFromWorkspaceView(model: @model) if @is_remove_colloabrator_from_workspace()
      return new PromoteCollaboratorToWorkspaceView(model: @model) if @is_promote_collaborator_to_workspace()
      return new DemoteCollaboratorFromWorkspaceView(model: @model) if @is_demote_collaborator_from_workspace()
      return new FollowUserView(model: @model) if @is_follow_user()
      return new AdminRemoveStatusView(model: @model) if @is_admin_remove_general_status()
      return new AdminRemoveReplyView(model: @model) if @is_admin_remove_status_timeliner_reply()
    #
    # Utils
    #
    polymorphic_type_for: (attr, type) ->
      obj = @model.get(attr)
      if obj
        obj.object_type is type
      else
        false

    actor_is_kind_of: (type) ->
      @polymorphic_type_for('actor', type)

    object_is_kind_of: (type) ->
      @polymorphic_type_for('object', type)

    target_is_kind_of: (type) ->
      @polymorphic_type_for('target', type)

    polymorphic_for: (attr) ->
      @model.get(attr)

    has_actor: ()->
      @polymorphic_for('actor')

    has_object: ()->
      @polymorphic_for('object')

    has_target: ()->
      @polymorphic_for('target')

    action_is: (action)->
      @model.get('action') is action

    is_post_status: ()->
      @actor_is_kind_of('user') &&
      @action_is('post') &&
      @object_is_kind_of('status') &&
      !@has_target()

    is_post_status_to_general: ()->
      status = @has_object()
      @is_post_status() &&
      !status.workspace_id &&
      !status.scrapbook_id

    is_post_status_to_workspace: ()->
      status = @has_object()
      @is_post_status() &&
      !!status.workspace_id &&
      !status.scrapbook_id

#    is_post_status_to_scrapbook: ()->
#      status = @has_object()
#      @is_post_status() &&
#      !status.workspace_id &&
#      !!status.scrapbook_id

    is_post_reply: ()->
      @actor_is_kind_of('user') &&
      @action_is('post') &&
      @object_is_kind_of('status::reply') &&
      @target_is_kind_of('status')

    is_post_reply_to_status_general: ()->
      status = @has_target()
      @is_post_reply() &&
      !status.workspace_id &&
      !status.scrapbook_id

    is_post_reply_to_workspace_status: ()->
      status = @has_target()
      @is_post_reply() &&
      !!status.workspace_id &&
      !status.scrapbook_id

    is_workspace_invitation: ()->
      @actor_is_kind_of('user') &&
      @action_is('invite') &&
      @object_is_kind_of('user') &&
      @target_is_kind_of('workspace')

    i_am_the_workspace_invitee: ()->
      object = @has_object()
      if object && @object_is_kind_of('user')
        object.id is PrefetchModels.current_user().get('id')
      else
        false

    is_joined_workspace: ()->
      @actor_is_kind_of('user') &&
      @action_is('join') &&
      @object_is_kind_of('workspace') &&
      !@has_target()

    is_access_workspace_request: ()->
      @actor_is_kind_of('user') &&
      @action_is('add') &&
      @object_is_kind_of('workspace::autoinvitation') &&
      @target_is_kind_of('workspace')

    is_remove_colloabrator_from_workspace: ()->
      @actor_is_kind_of('user') &&
      @action_is('remove') &&
      @object_is_kind_of('user') &&
      @target_is_kind_of('workspace')

    is_promote_collaborator_to_workspace: ()->
      @actor_is_kind_of('user') &&
      @action_is('promote') &&
      @object_is_kind_of('user') &&
      @target_is_kind_of('workspace')

    is_demote_collaborator_from_workspace: ()->
      @actor_is_kind_of('user') &&
      @action_is('demote') &&
      @object_is_kind_of('user') &&
      @target_is_kind_of('workspace')

    is_follow_user: ()->
      @actor_is_kind_of('user') &&
      @action_is('follow') &&
      @object_is_kind_of('user') &&
      !@has_target()

    is_remove_status: ()->
      @actor_is_kind_of('user') &&
      @action_is('remove') &&
      @object_is_kind_of('status')

    is_admin_remove_general_status: ()->
      actor = @has_actor()
      status = @has_object()
      @is_remove_status() &&
      !status.workspace_id &&
      !status.scrapbook_id &&
      (actor.role is 'admin' || actor.id != status.user_id)

    is_remove_reply: ()->
      @actor_is_kind_of('user') &&
      @action_is('remove') &&
      @object_is_kind_of('status::reply') &&
      @target_is_kind_of('status')

    is_admin_remove_status_timeliner_reply: ()->
      actor  = @has_actor()
      reply  = @has_object()
      status = @has_target()
      @is_remove_reply() &&
      !status.workspace_id &&
      !status.scrapbook_id &&
      (reply.timeliner is 'status') &&
      (actor.role is 'admin' || actor.id != reply.user_id)
