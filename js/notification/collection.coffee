define ["underscore", "backbone", "cs!./model", "paginator"], (_, Backbone, Notification) ->
  class NotificationCollection extends Backbone.Paginator.requestPager
    model:      Notification
    url:        "/notifications"
    comparator: (model) ->
      0 - model.get("id")

    pageAttribute: 'page'
    perPageAttribute: 'perPage'

    page: 0
    firtPage: 0
    perPage: 20
