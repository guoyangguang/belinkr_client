define ["jquery", "underscore", "backbone",
  '../../libs/backbone/backbone-localstorage',
  'jquery_cookie'
], ($, _, Backbone, LocalStore) ->
  class Card extends Backbone.Model
    name: =>
      @get('name') || @get('full_name') || @get('nick_name') ||'no name'
    avatar: ->
      if @get('type')
        "data:#{@get('type')};base64,#{@get('bin')}"
      else
        '/img/default-avatar.png'

 

      
 
