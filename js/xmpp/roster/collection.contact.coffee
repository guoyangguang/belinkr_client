define ["jquery", "underscore", "backbone",
  "cs!./model.contact",
  "cs!./collection.status",
  '../../libs/backbone/backbone-localstorage',
  'jquery_cookie'
], ($, _, Backbone, Contact, Statuses, LocalStore) ->

  class Contacts extends Backbone.Collection
    model:Contact
    comparator: (model) ->
      model.get("status") == 'offline'
    initialize: (models, options) ->
      @localStorage = new LocalStore('Roster')
      @session = options['session']
      @on 'update_presence', @update_presence
      @statuses = new Statuses()

    get_roster_list: (options)=>
      node = $iq({type:'get'}).c("query",{xmlns:"jabber:iq:roster"})
      @session.sendIQ node, (response) =>
        $('item', response).each(
          (i,item)=>
            contact = new Contact
              jid: $(item).attr('jid').split('/')[0]
              name:$(item).attr 'name'
              ack: $(item).attr 'ack'
              subscription: $(item).attr('subscription')
            
            @add(contact) unless $(item).attr('subscription') == 'none'

        )
        options['callback']?()

    update_presence: (presence)=>
      node = $(presence)
      from = node.attr 'from'
      jid  = from.split('/')[0]
      type = node.attr 'type'
      contact = @find (item)->
        item.get('jid') == jid
      @statuses.fetch()
      status = @statuses.load_status(jid)

      if contact
        if type == 'unavailable'
          contact.set 'status', 'offline'
        else
          contact.set 'status', 'online'
        status.set('status', contact.get('status'))
        contact.set('update_at', new Date())
        status.save()
        @trigger 'contacts:refresh_statuses'
      true
      
    load_contact: (jid)=>
      @find (item)-> item.get('jid') == jid


