define ["jquery", "underscore", "backbone",
  "cs!./model.message",
  '../../libs/backbone/backbone-localstorage',
  'jquery_cookie'
], ($, _, Backbone, Message, LocalStore) ->

  class Messages extends Backbone.Collection
    model:Message
    initialize: (models, options) ->
      @jid = options['jid']
 
