define ["jquery", "underscore", "backbone",
  'cs!../../composite_view',
  'cs!./collection.card',
  'cs!./collection.message',
  'cs!./model.conversation',
  'cs!./view.conversation',
  '../../../templates/directives',
  'text!../../../templates/xmpp_card.html',
  'jquery_cookie'
], ($, _, Backbone, CompositeView, Cards, \
Messages, Conversation, ConversationView, Directives, Template) ->

  class CardView extends Backbone.View
    template: jQuery(Template)
    tagName: 'li'
    className: 'truncate'
    events:
      "click div.avatar": 'open_conversation'

    initialize: (options) ->
      _.bindAll @
      @contact = options['contact']
      @card = options['card']
      @template.directives(Directives.card)
      @contact.on 'change:clicked', @open_conversation

    render: () =>
      jid = @contact.get('jid')
      @contact.set('avatar',@card.avatar())
      @contact.set('name',@card.name())
      
      @$el.html(@template.render(@contact.toJSON()).html())

      @
    open_conversation: =>
      @contact.set('clicked',false, silent:true)
      query ="div div.conversation[data-jid=\"#{@contact.get('jid')}\"]"
      if $(query).length == 0
        jid = @contact.get('jid')
        conversation = new Conversation(
          jid:jid
          name:@card.name()
        )
        @parent.session.messages[jid] ?= new Messages([],jid:jid)
        conversation_view = new ConversationView(
          jid: jid,
          model:conversation,
          session: @parent.session
          messages: @parent.session.messages[jid]
        )
        conversation_view.render()
      else
        $(query).parent().dialogr('open')
        #restore will resize the dialogr to default
        #$(query).parent().dialogr('restore')
      #scroll down to show the latest message
      $(query).parent().scrollTop $(query).height()
      @
    leave: () ->
      @unbind()
      @remove()





