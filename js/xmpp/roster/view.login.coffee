define ["jquery", "underscore", "backbone",
  'text!../../../templates/xmpp_login.html',
  "jqueryui/accordion",
  "jqueryui/autocomplete",
  "jqueryui/dialogr"
  'jquery_cookie'
], ($, _, Backbone, Template) ->

  class LoginView extends Backbone.View
    template: jQuery(Template).html()
    id: 'login'
    events:
      'click #login_button': 'login'

    initialize: (options) ->
      _.bindAll @
      @$el.html(@template)
    login: (event)=>
      
      password = @$('#login_password').val()
      @parent.trigger 'login_manually', password

    render: () =>
      @
     

 

