define ["jquery", "underscore", "backbone",
  'cs!../../composite_view',
  'cs!./view.card',
  'text!../../../templates/xmpp_cards_accordion.html',
  'jquery_cookie'
], ($, _, Backbone, CompositeView, CardView, Template) ->

  class CardsAccordionView extends CompositeView
    template: jQuery(Template).html()
    id: 'contact-list'

    initialize: (options) ->
      _.bindAll @
      @session = options['session']
      @contacts = options['contacts']
      @cards = options['cards']
      @contacts.on 'add', @render_contact
      @contacts.on 'reset', @render
      
      @$el.html(@template)

    render: =>
      @$('#accordion #contact-list-offline ul').html('')
      @$('#accordion #contact-list-online ul').html('')
      @render_all()
      @
    render_all: =>
      @contacts.each(@render_contact)
    render_contact: (contact)=>
      card_view = new CardView(
        contact:contact,
        card: @cards.load_card(contact.get('jid'))
      )
      if contact.get('status') == 'online'
        @appendChildInto(card_view,
          @$('#accordion #contact-list-online ul'))
      else
        @appendChildInto(card_view,
          @$('#accordion #contact-list-offline ul'))
      
