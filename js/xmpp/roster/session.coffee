define ["jquery", "underscore", "backbone", "strophe",
  'jquery_cookie'
],
($, _, Backbone, Strophe) ->
  class Session
    # Initializes with a BOSH URI.
    #
    # @param [String] uri
    #   The BOSH server URI.
    #
    # @api public
    constructor: (uri)->
      @xmpp = new Strophe.Connection(uri)
      @listeners =
        message: []
        roster: []
        presence: []
        card: []
      
      
  
    # Connects to the XMPP server using Strophe and calls back.
    #
    # It also attachs message handlers,
    #
    # @param [String] jid
    #   The full jid (user@domain.com)
    #
    # @param [String] password
    #   The password (1234)
    #
    # @param [Function] callback
    #   A callback to pass true or false depending on whether the connection was
    #   successful or not.
    #
    # @api public
    connect: (jid, password, callback) ->
      @xmpp.connect jid, password, (status) =>
 
        switch status
          when Strophe.Status.AUTHFAIL or Strophe.Status.CONNFAIL
            callback false
          when Strophe.Status.CONNECTED
            $.cookie('belinkr.xmpp.rid', @xmpp.rid, {path: '/'})
            # update rid when received new xmpp messages
            @xmpp.addHandler @record_rid
            # update rid when strophe.js send a post request to keep session
            # alive
            @xmpp.addTimedHandler 30000, @record_rid
            #@roster.populate =>
            #@xmpp.addHandler ((node)=> @handlePresence(node)), null, 'presence'
            #@xmpp.addHandler ((node)=> @handleMessage(node)), null, 'message'
            #@xmpp.send $pres() #XML.parse '<presence/>'
            #@roster.render()
            callback true
  
    attach: (jid, sid, rid, callback) ->
      @xmpp.attach jid, sid, rid, (status) =>
        switch status
          when Strophe.Status.DISCONNECTED
            #if attach fails, rid is invaild.
            #$.cookie('belinkr.xmpp.sid','',{path: '/'})
            $.cookie('belinkr.xmpp.rid','',{path: '/'})
            #$.cookie('belinkr.xmpp.jid','',{path: '/'})
            callback false
          when Strophe.Status.AUTHFAIL or Strophe.Status.CONNFAIL
            
            callback false
          when Strophe.Status.ATTACHED
            # update rid when received new xmpp messages
            @xmpp.addHandler @record_rid
            # update rid when strophe.js send a post request to keep session
            # alive
            @xmpp.addTimedHandler 30000, @record_rid
            #  $.cookie('belinkr.xmpp.rid', @xmpp.rid, {path: '/'})
            #  true
 
            callback true

    record_rid: =>
      $.cookie('belinkr.xmpp.rid', @xmpp.rid, {path: '/'})
      true

 
    # Disconnects from the XMPP server.
    #
    # @api public
    disconnect: ->
      @xmpp.disconnect()
      $.cookie('belinkr.xmpp.sid','', {path: '/'})
      $.cookie('belinkr.xmpp.rid','', {path: '/'})
      $.cookie('belinkr.xmpp.jid','', {path: '/'})
 
  
    # Returns whether the session is connected or not.
    #
    # @api public
    connected: -> @xmpp.jid && @xmpp.jid.length > 0
  
    # @return [String]
    #   The session's unique Id.
    #
    # @api public
    uniqueId: -> @xmpp.getUniqueId()
  
    # @return [String]
    #   The session's bare JID (without the resource part).
    #
    # @api public
    bareJid: -> @xmpp.jid.split('/')[0]

    # Sends a message to another user.
    #
    # @param [String] jid
    #   The receiver jid.
    #
    # @param [String] message
    #   The message to send.
    #
    # @api public
    sendMessage: (jid, message) ->
      node = $msg(
        to: jid
        type: "chat"
      ).c('body').t(message)
      @xmpp.send node
      @record_rid()
    sendIQ: (node, callback) ->
      @xmpp.sendIQ node, callback, callback, 5000
      @record_rid()
