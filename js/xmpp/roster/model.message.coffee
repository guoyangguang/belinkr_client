define ["jquery", "underscore", "backbone",
  '../../libs/backbone/backbone-localstorage',
  'jquery_cookie'
], ($, _, Backbone, LocalStore) ->
  class Message extends Backbone.Model
    defaults:
      body: ''
      bareJid: ''

    parse_rawdata: (rawdata)->
      xml = $(rawdata)
      from = xml.attr('from')
      to = xml.attr('to')
      type = xml.attr('type')
      body = $('body', xml).text()
      updated_at = new Date()
      composing = $('composing',xml)
      error = $('error',xml)

      @set 'direction', 'in'
      @set 'from', from
      @set 'bareJid', from.split('/')[0]
      @set 'body', body
      @set 'composing', true if composing.length > 0 and error.length == 0
      @set 'updated_at', updated_at
      @set 'error', error

    

 

