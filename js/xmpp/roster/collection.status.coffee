define ["jquery", "underscore", "backbone",
  "cs!./model.status",
  '../../libs/backbone/backbone-localstorage',
  'jquery_cookie'
], ($, _, Backbone, Status, LocalStore) ->

  class ContactStatuses extends Backbone.Collection
    model:Status
    initialize: () ->
      @localStorage = new LocalStore('Statuses')
    load_status: (jid)->
      status = @find (item)-> item.get('jid') == jid
      unless status
        status = new Status
          jid: jid
          status: 'offline'
        @add status
      status


