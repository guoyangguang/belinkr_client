define ["jquery", "underscore", "backbone",
  "cs!./model.card",
  "cs!./model.contact",
  '../../libs/backbone/backbone-localstorage',
  'jquery_cookie'
], ($, _, Backbone, Card, Contact, LocalStore) ->

  class Cards extends Backbone.Collection
    model:Card
    initialize: (models, options) ->
      @localStorage = new LocalStore('Cards')
      @session = options['session']

    load_card: (jid)->
      @find (item)-> item.get('jid') == jid

    retrive_card: (jid,callback)=>
      card = @load_card(jid)
      if card
        callback?(card)
      else
        node = $iq(
          type: "get"
          to:    jid
        ).c("vCard",{xmlns:"vcard-temp"})
        @session.sendIQ node, (result)=> @save_card(jid,result,callback)

    save_card: (jid,result,callback)=>
      card     = $('vCard', result)
      photo    = $('PHOTO', card)
      card = new Card
        jid       : jid
        name      : $('name',card).text()
        nick_name : $('NICKNAME',card).text()
        full_name : $('FN',card).text()
        type      : $('TYPE', photo).text()
        bin       : $('BINVAL', photo).text()
        updated_at: new Date()
      @create(card)
      callback?(card)

    avatar: (jid)->
      card = @load_card(jid)
      if card && card.get('type')
        "data:#{card.get('type')};base64,#{card.get('bin')}"
      else
        '/img/default-avatar.png'

 
