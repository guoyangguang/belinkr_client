define ["jquery", "underscore", "backbone",
  "cs!./model.card",
  "cs!./collection.card",
  "cs!./collection.status",
  "cs!./model.message",
  "cs!./collection.message",
  "cs!./session",
  "cs!./view.login",
  "cs!./view.roster_search",
  "cs!./view.current_user",
  "cs!./view.cards.noaccordion",
  "cs!./collection.contact",
  "cs!./collection.status",
  'cs!../../composite_view',
  'text!../../../templates/xmpp.html',
  '../../libs/backbone/backbone-localstorage',
  "jqueryui/accordion",
  "jqueryui/autocomplete",
  "jqueryui/dialogr"
  'jquery_cookie'
], ($, _, Backbone, Card, Cards, Statuses, Message, Messages, Session,\
LoginView, RosterSearchView, CurrentUserView, CardsNoAccordionView, Contacts,\
ContactStatuses, CompositeView, Template, LocalStore) ->

  class XmppView extends CompositeView
    template: jQuery(Template).html()

    initialize: (options) ->
      _.bindAll @
      uri = options['xmpp_url']
      @session = new Session(uri)
      @session.messages ={}
      window.session = @session
      @$el.html(@template)

      #@init_static_subview()

      @on 'login_manually', (password)=> @login_manually(password)
      @on 'connect_success', @connect_success
      if window.xmpp_account && window.xmpp_password
        @login(@session)
      else if $.cookie('belinkr.xmpp.sid') && $.cookie('belinkr.xmpp.jid')
        @attach(@session)
      else
        alert 'lost xmpp connection, please login again'


    render_login_static_subview: ->
      login_view = new LoginView()
      @replaceWithStaticChildInto(login_view, @$('#login'))

    render: () ->
      @

    login_manually: (password)=>
      window.xmpp_password = password
      window.xmpp_account = $.cookie('belinkr.xmpp.account')
      @login(@session)

    login: (session)->
      if window.xmpp_account && window.xmpp_password
        user = window.xmpp_account

        password = window.xmpp_password
        window.xmpp_password = ''
        #clear the online status when login
        window.localStorage.removeItem('Statuses')
        session.connect user, password, (success)=>
          @trigger('connect_success', 'login', success)
        $.cookie('belinkr.xmpp.account', window.xmpp_account, {path: '/'})
        
      else
        alert 'lost xmpp connection, please login again'

 

    attach: (session)->
      rid_str = $.cookie('belinkr.xmpp.rid')
      #rid = (parseInt(rid_str)+1).toString()
      rid = rid_str
      session.attach(
        $.cookie('belinkr.xmpp.jid'),
        $.cookie('belinkr.xmpp.sid'),
        rid,
        (success)=>
          if success
            callback = =>  @trigger('connect_success', 'attach', success)
            setTimeout(callback, 500)
          else
            # let server to create the session and return back
            $.getJSON '/sessions/xmpp', (data)=>
              session.attach(data['jid'], data['sid'], data['rid'],
                (success)=>
                  if success
                    callback = => @trigger('connect_success', 'attach', success)
                    setTimeout(callback, 500)
              )
      )

    connect_success: (type,success) =>
      @connect_type = type
      
      # save to cookie
      if success
        @init_contacts_cards_collection()
        @bind_onPresence(@session)
        @bind_onMessage(@session)

        @save_xmpp_session_ids(@session)
        @display_current_user()
        # display the name of following user from the localStorage cards
        @load_roster()
        
        @send_presence(@session)
      else
        @render_login_static_subview()



    init_contacts_cards_collection: =>
      @contacts = new Contacts([], session: @session)
      window.contacts = @contacts
      @cards =  new Cards([],session: @session)

    save_xmpp_session_ids: (session)->
      
      @$("div#login").hide()
      $.cookie('belinkr.xmpp.sid',session.xmpp.sid, {path: '/'})
      $.cookie('belinkr.xmpp.jid',session.xmpp.jid, {path: '/'})
      $.cookie('belinkr.xmpp.rid',session.xmpp.rid, {path: '/'})

    display_current_user: ->
      current_user_view = new CurrentUserView(session:@session,cards:@cards)
      @replaceWithStaticChildInto(current_user_view, @$('div#user'))

    load_roster: ->
      #get roster by sending iq:roster
      @contacts.get_roster_list(callback:@set_contact_status_load_cards)

    render_contacts_view: ->
      statuses = new Statuses()
      statuses.fetch()
      cards_noaccordion_view = new CardsNoAccordionView(
        session:@session
        contacts:@contacts
        cards:@cards
        statuses: statuses
      )

      @replaceWithStaticChildInto(cards_noaccordion_view,@$('div#contact-list'))
      #don't use accordion because we don't have online/offline groups
      #$("#accordion").accordion({collapsible: true,clearStyle: true})
      @render_roster_search_view()
    render_roster_search_view: ->
      roster_search_view = new RosterSearchView(
        session: @session
        contacts:@contacts
        cards:@cards
      )
      @replaceWithStaticChildInto(roster_search_view, @$('div#roster_search'))


    # fetch the contact's card in the localStorage, if not found, then send
    # vCard iq to xmpp server
    set_contact_status_load_cards: ->
      #if @connect_type == 'attach'
      #  # load contact online status from localStorage
      contact_statuses = new ContactStatuses()
      contact_statuses.fetch
        success: =>
          #set contacts online/offline status
          @contacts.each (contact)->
            jid = contact.get('jid')
            status = contact_statuses.load_status(jid)
            contact.set('status',status.get('status'))
          @cards.fetch
            success: @load_cards_callback

    load_cards_callback: ->
      render_contacts_view_callback = _.
        after(@contacts.length, @render_contacts_view)
      @contacts.each (contact)=>
        @cards.retrive_card(contact.get('jid'),(card)=>
          render_contacts_view_callback()
        )


    bind_onMessage: (session)->
      session.xmpp.addHandler @handleMessage, null, 'message'
    bind_onPresence: (session)->
      session.xmpp.addHandler @handlePresence, null, 'presence'
    handlePresence: (presence)->
      @contacts.trigger('update_presence',presence)
      true
    handleMessage: (message)->
      #parse message
      #insert message into the session.messages[jid] collection
      message_model = new Message()
      message_model.parse_rawdata(message)
      #ignore error message
      if message_model.get('error').length == 0
        bareJid=message_model.get('bareJid')
        @session.messages[bareJid] ?= new Messages(
          [],
          jid:bareJid)
        @session.messages[bareJid].push message_model
        #trigger the click event of the cardview
        @$("div[data-jid=\"#{bareJid}\"]").click()
      true
      
    send_presence: (session)->
      session.xmpp.send $pres()
