define ["jquery", "underscore", "backbone",
  '../../libs/backbone/backbone-localstorage',
  'jquery_cookie'
], ($, _, Backbone, LocalStore) ->
  class Contact extends Backbone.Model
    defaults:
      status: 'offline'
