define ["jquery", "underscore", "backbone",
  "cs!./collection.card",
  'cs!../../composite_view',
  'text!../../../templates/xmpp_current_user.html',
  'jquery_cookie'
], ($, _, Backbone, Cards, CompositeView, Template) ->

  class CurrentUserView extends Backbone.View
    template: jQuery(Template).html()
    id: 'user'
    initialize: (options) ->
      _.bindAll @
      @session = options['session']
      @cards = options['cards']
      @$el.html(@template)

    render: () =>
      # display self name
      jid = @session.bareJid()
      @cards.fetch
        success: =>
          @cards.retrive_card jid,(self_card)=>
            if self_card.get('full_name')
              @$("#user a").text self_card.get('full_name')
            else if self_card.get('name')
              @$("#user a").text self_card.get('name')
            else
              @$("#user a").text jid
            @$("#user div img").attr("src", @cards.avatar(jid))
      @
     
