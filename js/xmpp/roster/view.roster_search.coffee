define ["jquery", "underscore", "backbone",
  "cs!./collection.card",
  'cs!../../composite_view',
  'text!../../../templates/xmpp_roster_search.html',
  'jquery_cookie'
], ($, _, Backbone, Cards, CompositeView, Template) ->

  class RosterSearchView extends Backbone.View
    template: jQuery(Template).html()
    id: 'roster_search'
    initialize: (options) ->
      _.bindAll @
      @session = options['session']
      @cards = options['cards']
      @contacts = options['contacts']
      @$el.html(@template)

    render: () =>
      own_jid = @session.bareJid()
      other_user_cards = @cards.reject (card)-> card.get('jid') == own_jid
      contact_names = _.map other_user_cards, (card)-> card.name()
      contact_jids = _.map(other_user_cards, (card)-> card.get('jid'))
 
      @$("#roster_search_input").focus =>
        $(@$("#roster_search_input")).autocomplete
          source: contact_names,
          select: (event, ui) =>
            index = contact_names.indexOf(ui.item.value)
            jid = contact_jids[index]
            
            #can't trigger the click event in another view
            #$("div[data-jid=\"#{jid}\"]").click()
            clicked_contact = @contacts.find (contact)-> contact.get('jid')==jid
            clicked_contact.set('clicked',true)
      @
 
