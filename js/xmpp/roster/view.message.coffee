define ["jquery", "underscore", "backbone",
  '../../../templates/directives',
  'text!../../../templates/xmpp_message.html',
  'text!../../../templates/xmpp_composing_message.html',
  'jquery_cookie'
], ($, _, Backbone, Directives, Template, ComposingTemplate) ->

  class MessageView extends Backbone.View
    template: jQuery(Template)
    composing_template: jQuery(ComposingTemplate)
    initialize: (options) ->
      _.bindAll @
      @name = options['name']
      if @model.get('composing')
        @composing_template.directives(Directives.composing_message)
      else
        @template.directives(Directives.message)

    render: () =>
      @model.set('name', @name)
      if @model.get('composing')
        @$el.html(@composing_template.render(@model.toJSON()).html())
        setTimeout
        callback = =>
          @leave()
          @model.collection.remove(@model)
        setTimeout callback, 5000
      else
        @$el.html(@template.render(@model.toJSON()).html())
    leave: =>
      @unbind()
      @remove()
 
 
