define ["jquery", "underscore", "backbone",
  'cs!../../composite_view',
  'cs!./model.contact'
  'cs!./model.message'
  'cs!./view.message'
  '../../../templates/directives',
  'text!../../../templates/xmpp_conversation.html',
  'jquery_cookie'
], ($, _, Backbone, CompositeView, Contact, Message, MessageView,\
Directives, Template) ->

  class ConversationView extends CompositeView
    template: jQuery(Template)
    events:
      "keypress input.chat_input": "typing_message"

    initialize: (options) ->
      _.bindAll @
      @template.directives(Directives.conversation)
      @messages = options['messages']
      @session = options['session']
      @messages.on 'add', @push_message
      @model.on 'change:composing', @send_composing_message
      @query ="div div.conversation[data-jid=\"#{@model.get('jid')}\"]"

    render: () =>
      
      if $(@query).length == 0
        @$el.html(@template.render(@model.toJSON()).html())
        
        $(@$el).dialogr(
          title     : @model.get('name')
          maximized : false
          minimized : true
          minHeight : 140
          minWidth  : 310
          height    : 160
          width     : 330
        )
      @messages.each @push_message
      @
    push_message: (message)=>
      message_view = new MessageView(model:message, name: @model.get('name'))
      #@$('ul.messages').append message_view
      #check if there is 'composing hint', and hide it
      @$('.composing_message_body').hide()
      @appendChildInto message_view, @$('ul.messages')
      @scroll_down()
    scroll_down: =>
      $(@query).parent().scrollTop $(@query).height()


    typing_message: (event)=>
      body=@$('input.chat_input').val()
      if event.keyCode == 13 and @$()&& body.length > 0
        @messages.add new Message
          direction:'out'
          body:body
          updated_at:new Date()

        @session.sendMessage(@model.get('jid'),body)
        @$('input.chat_input').val('')
      else
        @model.set('composing', true)
        
    send_composing_message: ->
      if @model.typing = true
        composing = $msg({to: @model.get('jid'), "type": "chat"})
          .c('composing', {xmlns: "http://jabber.org/protocol/chatstates"})
        @session.xmpp.send(composing)
        setTimeout (=> @model.set('composing',false, silent:true)), 30000
        
       
