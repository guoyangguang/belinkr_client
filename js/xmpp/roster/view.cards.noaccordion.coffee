define ["jquery", "underscore", "backbone",
  'cs!../../composite_view',
  'cs!./view.card',
  'text!../../../templates/xmpp_cards_noaccordion.html',
  'jquery_cookie'
], ($, _, Backbone, CompositeView, CardView, Template) ->

  class CardsNoAccordionView extends CompositeView
    template: jQuery(Template).html()
    id: 'contact-list'

    initialize: (options) ->
      _.bindAll @
      @session = options['session']
      @contacts = options['contacts']
      @cards = options['cards']
      @statuses = options['statuses']
      @contacts.on 'add', @render_contact
      @contacts.on 'reset', @render
      @contacts.on 'contacts:refresh_statuses', @render
      
      @$el.html(@template)

    render: =>
      @$('#contact-list ul').html('')
      @_leaveChildren()
      @render_all()
      @
    render_all: =>
      @contacts.sort(silent:true)
      @contacts.each(@render_contact)
      window.contacts = @contacts
    render_contact: (contact)=>
      jid = contact.get('jid')
      card_view = new CardView(
        contact:contact,
        card: @cards.load_card(jid)
      )
      @appendChildInto(card_view,
        @$('#contact-list ul'))
     
