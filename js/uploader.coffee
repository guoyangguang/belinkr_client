define ["jquery",
  "underscore",
  "cs!uploader/view.form",
  "cs!uploader/view.item",
  "cs!file/view.model","libs/plupload/plupload.full"
], ($, _, UploaderFormView, UploaderItemView) ->
    
  class Uploader
    @defaultSettings:
      runtimes            : 'gears,html5,flash,silverlight,browserplus',
      url                 : '/files',
      max_file_size       : '100mb',
      file_data_name      : 'file',
      multipart           : true,
      unique_names        : true,
      flash_swf_url       : '/js/libs/plupload/plupload.flash.swf',
      silverlight_xap_url : '/js/libs/plupload/plupload.silverlight.xap',
      auto_upload         : true

    # see official documentation
    @predefinedBinds:
      'BeforeUpload':   (uploader, file)->
      'ChunkUploaded':  (uploader, file, response)->
      #'CancelUpload':  (uploader)->
      'Destroy':        (uploader)->

      # -- Error --
      'Error':          (uploader, error)->
        if error.file
          file_list       = uploader.settings.ForeShadowed.file_list
          item            = file_list.find("#uploader_file_id_" + error.file.id)
          item.find('.file_status').addClass('failure').text('Failed')
        else
          try
            console.log(error.code.toString() + ":" + error.message)
          catch e
            e

      # -- FilesAdded --
      'FilesAdded':     (uploader, files)->
        file_list = uploader.settings.ForeShadowed.file_list
        _.each files, (file, index)->
          view = new UploaderItemView(
            data:
              id: file.id,
              filename: file.name
              filesize: plupload.formatSize(file.size)
            uploader: uploader
            file: file
          )
          $(file_list).append(view.$el)
          uploader.refresh()

          if uploader.settings.auto_upload
            # cuz we binds FilesAdd before uploader.init()
            window.setTimeout(()->
              uploader.start()
              # window.event.preventDefault()
            ,100)

      # -- FilesRemoved --
      'FilesRemoved':   (uploader, files)->
        _.each files, (file)->
          file_list   = uploader.settings.ForeShadowed.file_list
          item = file_list.find("#uploader_file_id_" + file.id)

          if file.status is 5
            unless _.isEmpty(item)
              uploaded_file_id = item.attr("data")
              file_field        = uploader.settings.ForeShadowed.file_field
              value = file_field.val()
              value = '[]' if (value is null) || (value is '')
              value = $.parseJSON(value)
              unless _.isEmpty(value)
                kept = _.reject value, (data)->
                  data.id == uploaded_file_id
                file_field.val(JSON.stringify(kept))
          else
            uploader.stop()
            uploader.refresh()
            if uploader.files.length > 0
              if uploader.files.length > uploader.total.uploaded
                uploader.start()

      # -- FileUploaded --
      'FileUploaded':   (uploader, file, response)->
        data        = $.parseJSON(response.response)

        file_field  = uploader.settings.ForeShadowed.file_field
        file_list   = uploader.settings.ForeShadowed.file_list
        item        = file_list.find("#uploader_file_id_" + file.id)

        unless _.isEmpty(item)
          value = file_field.val()
          value = '[]' if (value is null) || (value is '')
          value = $.parseJSON(value)
          value.push data
          file_field.val(JSON.stringify(value))

          mime_types = {
            'image/png'                     : 'image',
            'application/png'               : 'image',
            'application/x-png'             : 'image',
            'image/gif'                     : 'image',
            'image/x-xbitmap'               : 'image',
            'image/gi_'                     : 'image',
            'image/jpeg'                    : 'image',
            'image/jpg'                     : 'image',
            'image/jpe_'                    : 'image',
            'image/pjpeg'                   : 'image',
            'image/vnd.swiftview-jpeg'      : 'image',
            'application/msword'            : 'doc',
            'application/doc'               : 'doc',
            'appl/text'                     : 'doc',
            'application/vnd.msword'        : 'doc',
            'application/vnd.ms-word'       : 'doc',
            'application/winword'           : 'doc',
            'application/word'              : 'doc',
            'application/x-msw6'            : 'doc',
            'application/x-msword'          : 'doc',
            'application/vnd.ms-powerpoint' : 'ppt',
            'application/mspowerpoint'      : 'ppt',
            'application/ms-powerpoint'     : 'ppt',
            'application/mspowerpnt'        : 'ppt',
            'application/vnd-mspowerpoint'  : 'ppt',
            'application/powerpoint'        : 'ppt',
            'application/x-powerpoint'      : 'ppt',
            'application/x-m'               : 'ppt',
            'application/vnd.ms-excel'      : 'xls',
            'application/msexcel'           : 'xls',
            'application/x-msexcel'         : 'xls',
            'application/x-ms-excel'        : 'xls',
            'application/x-excel'           : 'xls',
            'application/x-dos_ms_excel'    : 'xls',
            'application/xls'               : 'xls',
            'application/pdf'               : 'pdf',
            'application/x-pdf'             : 'pdf',
            'application/acrobat'           : 'pdf',
            'applications/vnd.pdf'          : 'pdf',
            'text/pdf'                      : 'pdf',
            'text/x-pdf'                    : 'pdf'
          }

          css_class = mime_types[data.mime_type]
          item.addClass(css_class)

          if css_class == 'image'
            thumbnail_href="/files/#{data.id}?version=mini&display=inline"
            item.prepend("<img src='#{thumbnail_href}' alt='thumbnail' />")

          item.attr("data", data.id)
          item.find(".file_name").html(
            "<a href='/files/#{data.id}'>#{data.original_filename}</a>"
          )
          item.find(".file_status").addClass('success').text('uploaded')
          item.find(".file_progress").addClass('success').text('100%')

      # -- Init --
      'Init':           (uploader, params)->
#        console.log('pluploader runtime:' + params.runtime)
      'PostInit':       (uploader)->
      'QueueChanged':   (uploader)->
      'Refresh':        (uploader)->
      'StateChanged':   (uploader)->
      'UploadComplete': (uploader, files)->
      'UploadFile':     (uploader, file)->
      'UploadProgress': (uploader, file)->
        file_list   = uploader.settings.ForeShadowed.file_list
        item = file_list.find("#uploader_file_id_" + file.id)
        item.find(".file_progress").text(file.percent + '%')

    # intialize a uploader form view
    # and binds plupload behavior to it
    @build: (view_options, uploader_options)->
      view_options     = {} unless view_options
      uploader_options = {} unless uploader_options
      view = @initView(view_options)

      uploader_options = _.extend(uploader_options, view_form: view.$el)
      uploader = @initUploader(uploader_options)

      view: view
      uploader: uploader

    @initView: (options={})->
      new UploaderFormView(options)

    @initUploader: (options={})->
      options.settings  = {} unless options.settings
      options.events    = {} unless options.events

      unless options.parent_form
        throw new Error('please specify the parent form')
      unless options.view_form
        throw new Error('please specify the uploader view form')

      @appendInputFilesToForm(options.parent_form)

      settings  = _.extend options.settings, @defaultSettings
      events    = _.extend options.events,   @predefinedBinds
      settings.container      = options.view_form.attr("id")
      settings.browse_button  = options.view_form.find(".pick_file")
                                  .attr("id")
      settings.ForeShadowed =
        file_list: options.view_form.find("ul.file_list")
        file_field: options.parent_form.find("input[name='files']")

      if settings.token
        settings.url = settings.url + settings.token

      uploader = new plupload.Uploader(settings)
      _.each events, (call_back, e)->
        uploader.bind(e, call_back)
      # document ready do this!
      # uploader.init()
      uploader

    @getUploader: (settings) ->
      settings = {} unless settings
      settings = _.extend @defaultSettings, settings
      if settings.token
        settings.url = settings.url + settings.token

      new plupload.Uploader(settings)

    @appendInputFilesToForm: (form)->
      unless _.isEmpty(form.find("input[name='files']"))
        form.append($("<input />", {
          type: 'hidden',
          name: 'files'
        }))

    @icon_class_for: (mime_type) ->
      types = {
        'image/png'                     : 'image',
        'application/png'               : 'image',
        'application/x-png'             : 'image',
        'image/gif'                     : 'image',
        'image/x-xbitmap'               : 'image',
        'image/gi_'                     : 'image',
        'image/jpeg'                    : 'image',
        'image/jpg'                     : 'image',
        'image/jpe_'                    : 'image',
        'image/pjpeg'                   : 'image',
        'image/vnd.swiftview-jpeg'      : 'image',
        'application/msword'            : 'doc',
        'application/doc'               : 'doc',
        'appl/text'                     : 'doc',
        'application/vnd.msword'        : 'doc',
        'application/vnd.ms-word'       : 'doc',
        'application/winword'           : 'doc',
        'application/word'              : 'doc',
        'application/x-msw6'            : 'doc',
        'application/x-msword'          : 'doc',
        'application/vnd.ms-powerpoint' : 'ppt',
        'application/mspowerpoint'      : 'ppt',
        'application/ms-powerpoint'     : 'ppt',
        'application/mspowerpnt'        : 'ppt',
        'application/vnd-mspowerpoint'  : 'ppt',
        'application/powerpoint'        : 'ppt',
        'application/x-powerpoint'      : 'ppt',
        'application/x-m'               : 'ppt',
        'application/vnd.ms-excel'      : 'xls',
        'application/msexcel'           : 'xls',
        'application/x-msexcel'         : 'xls',
        'application/x-ms-excel'        : 'xls',
        'application/x-excel'           : 'xls',
        'application/x-dos_ms_excel'    : 'xls',
        'application/xls'               : 'xls',
        'application/pdf'               : 'pdf',
        'application/x-pdf'             : 'pdf',
        'application/acrobat'           : 'pdf',
        'applications/vnd.pdf'          : 'pdf',
        'text/pdf'                      : 'pdf',
        'text/x-pdf'                    : 'pdf'
      }
      types[mime_type]
