define ["underscore", "backbone"], (_, Backbone) ->
  class Credential extends Backbone.Model
    urlRoot: "/credentials"
