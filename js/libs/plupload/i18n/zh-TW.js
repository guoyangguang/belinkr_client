// Chinese-Traditional
plupload.addI18n({
    'Select files' : '選擇文件',
    'Add files to the upload queue and click the start button.' : '添加文件到上傳隊列並點擊上傳按鈕',
    'Filename' : '文件名',
    'Status' : '狀態',
    'Size' : '大小',
    'Add files' : '添加文件',
    'Stop current upload' : '中止當前上傳',
    'Start uploading queue' : '開始隊列上傳',
    'Uploaded %d/%d files' : '上傳第 %d/%d 個文件',
    'N/A' : 'N/A',
    'Drag files here.' : '拖動文件到這裏',
    'File extension error.' : '文件擴展名錯誤',
    'File size error.' : '文件大小錯誤',
    'Init error.' : '上傳初始化錯誤',
    'HTTP Error.' : 'HTTP錯誤',
    'Security error.' : '安全性錯誤',
    'Generic error.' : '一般性錯誤',
    'IO error.' : 'IO錯誤',
    'Stop Upload' : '停止上傳',
    'Add Files' : '添加文件',
    'Start Upload' : '開始上傳',
    '%d files queued' : '隊列有%d個文件'
});