define [
  "order!libs/websocket/config",
  "order!libs/websocket/swfobject",
  "order!libs/websocket/FABridge",
  "order!libs/websocket/web_socket"
], () ->
  class NotificationWebSocket
    @log_output: (msg)->
      try
        console.log(msg)
      catch e
        e

    @init: (options={}, synchronizer)=>
      WebSocket.__initialize() if HAS_WINDOW_WEBSOCKET_OBJECT is false

      ws = new WebSocket("ws://" + window.location.hostname + ":8080/?token=" + options.token)
      ws.onopen = options.ws_onopen || ()=>
        @log_output('websocket open')

      ws.onmessage = (e) ->
        synchronizer.refresh(JSON.parse(e.data))

      ws.onclose = options.ws_onclose || ()=>
        synchronizer.running = false if synchronizer
        @log_output('websocket closed')

      ws.onerror = options.ws_onerror || ()=>
        @log_output('websocket error')

      window.NotificationWebSocket = ws