/* =========================================================
 * jquery.click-modal.js v0.1 simple beta
 * Author: Elvuel
 * Mail:   elvuel(at)gmail.com
 * http://www.belinkr.com
 * =========================================================
 * Copyright 2012 belinkr, Inc.
 * ========================================================= */
//define('click_modal',['jquery'], function (jQuery) {
(function($) {

    $.fn.click_modal = function(options) {
        var $this = $(this);
        options = $.extend({}, $.fn.click_modal.defaults, options);
        $this.attr("data-toggle", "modal");
        $this.attr("data-target", options.prefix + options.modal.target);

        //$(function() {
            $this.on("click", function(e) {
                e.preventDefault();
                $('body').find(".modal[id^='" + options.prefix + "']").remove();

                var modal = $(options.modal.template);
                modal.attr("id", options.prefix + options.modal.target);

                $('body').append(modal);

                for (var key in options.modal.events) {
                    var invoke = options.modal.events[key];
                    // modal.find(key).on('click', $.proxy(options.parent[invoke], options.parent));
                    modal.find(key).on('click', function(e){
                        e.preventDefault();
                        options.parent[invoke].call();
                    });
                };
                modal.modal();
            });
        //});
    };
    $.fn.click_modal.defaults = {
        prefix : "click_modal_"
    };
}(jQuery));
//});
