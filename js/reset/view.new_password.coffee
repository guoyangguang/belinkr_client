define [
  'jquery',
  'underscore',
  'backbone',
  'cs!../external_view',
  'cs!./model',
  'cs!../alerter/helper',
  'text!../../templates/new_password.html'
], ($, _, Backbone, ExternalView, Reset, Alerter, Template) ->
  class NewPasswordView extends ExternalView
    template:         jQuery(Template).html()
    el:               'div.container'
    events:
      'change .password'                : 'password_change'
      'change .password_confirmation'   : 'password_confirmation_change'
      'click button.save'               : 'save'
      'submit form'                     : 'save'
      'click a.login'                   : 'go_login'

    initialize: () ->
      @events = _.extend(@events, @locale_events)
      _.bindAll @
      @model.fetch( error: (model, response) => @go_login() )

    render: () ->
      @$el.html(@template)
      @$el = @localize(@$el)
      @

    password_change: (event) ->
      @model.set(password: $(event.currentTarget).val())
      
    password_confirmation_change: (event) ->
      @model.set(password_confirmation: $(event.currentTarget).val())

    save: (e) =>
      e.preventDefault()
      $('form .alert').remove()
      $('button.save').attr('disable', true)
      @model.save({ wait: true }
        success: (model, response) =>
          alerter = new Alerter()
          div = $("<div />")
          login_link = $("<a />", class: 'login')
          login_link.html(@getLocalizeTextByKey("reset.alerter.resetting.succeed.here"))
          div.append(login_link)
          message = @getLocalizeTextByKey("reset.alerter.resetting.succeed.success")
          message += div.html()
          message += @getLocalizeTextByKey("reset.alerter.resetting.succeed.log_in")
          div.empty()

          $('form h2').after(alerter.success(message).render())
          @clear_form()
          
        error: (model, response) =>
          alerter = new Alerter()
          $('form h2').after(alerter.error(
            [
              @getLocalizeTextByKey("reset.alerter.resetting.failed")
            ]
          ).render())
      )

      $('button.save').attr('disable', false)
      false

    clear_form: () ->
      $('form input[type="password"]').val('')
      @model.set('password', '')
      @model.set('password_confirmation', '')
          
    go_login: (e) ->
      e.preventDefault()
      window.App.navigate( "/login", trigger: true)
      false
