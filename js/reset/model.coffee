define ["underscore", "backbone"], (_, Backbone) ->
  class Reset extends Backbone.Model
    urlRoot: '/resets'
    defaults:
      email:                  ''
      password:               ''
      password_confirmation:  ''
