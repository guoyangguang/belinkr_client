define [
  'jquery',
  'underscore',
  'backbone',
  'cs!../external_view',
  'cs!./model',
  'cs!../alerter/helper',
  'text!../../templates/reset.html'
], ($, _, Backbone, ExternalView, Reset, Alerter, Template) ->
  class ResetView extends ExternalView
    template:         jQuery(Template).html()
    el:               'div.container'

    events:
      'click a.go_login'    : 'go_login'
      'change .email'       : 'email_change'
      'click button.reset'  : 'reset'
      'submit form'         : 'reset'

    initialize: () ->
      @events = _.extend(@events, @locale_events)
      _.bindAll @
      @model = new Reset

    render: () ->
      @$el.html(@template)
      @$el = @localize @$el
      @

    email_change: (event) ->
      @model.set(email: $(event.currentTarget).val())

    reset: (e) =>
      e.preventDefault()
      $('form .alert').remove()
      $('button.reset').attr('disabled', true)
      @model.save {},
        wait: true
        success: (model, response) =>
          alerter = new Alerter()
          $('form h2').after(alerter.success(
            @getLocalizeTextByKey('reset.alerter.email_sent')
          ).render())
          @model = new Reset
          @clear_form()
        error:   (model, response) =>
          alerter = new Alerter()
          @model = new Reset
          $('form h2').after(alerter.error(
            [
              @getLocalizeTextByKey('reset.alerter.email_unregistered')
            ]
          ).render())
          
      $('button.reset').removeAttr('disabled')
      false

    clear_form: () ->
      $('form input[type="text"]').val('')

    go_login: (e) ->
      e.preventDefault()
      window.App.navigate( "/login", { trigger: true })
      false
