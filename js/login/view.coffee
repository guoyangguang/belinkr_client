define [
  'jquery',
  'underscore',
  'backbone',
  'text!../../templates/login.html',
  'cs!../external_view',
  'cs!../localize',
  'jqueryui/effects/shake'
], ($, _, Backbone, Template, ExternalView) ->
  class LoginView extends ExternalView
    template: jQuery(Template).html()
    el:       'div.container'
    events:
      'click  a.go_reset'         : 'go_reset'
      'click  .signin button'     : 'login'
      'change .signin .email'     : 'email_change'
      'change .signin .password'  : 'password_change'
      'change .signin .remember'  : 'remember_change'

    initialize: () ->
      @events = _.extend(@events, @locale_events)
      _.bindAll @

    render: () ->
      @$el.html(@template)
      @$el = @localize @$el
      @

    email_change: (event) ->
      @model.set(email: $(event.currentTarget).val())

    password_change: (event) ->
      @model.set(password: $(event.currentTarget).val())

    remember_change: (event) ->
      checked = $(event.currentTarget).attr('checked') ? true : false
      @model.set(remember: checked)

    login: (e) ->
      e.preventDefault()
      window.xmpp_password = @model.get('password')

      $.ajax(
        type:     'post'
        url:      '/sessions'
        async:    false
        data:     JSON.stringify(
                    email:    @model.get('email'),
                    password: @model.get('password')
                    remember: @model.get('remember')
                  )
        dataType: 'json'
        success:  (data, text_status, jq_xhr) ->
          window.xmpp_account = data.jid
          # store session_data
          window.localStorage.setItem('session_data', JSON.stringify(data))
          # fetch user_data
          user_id = data.user_id
          $.ajax(
            url: "/users/#{user_id}"
            async:    false
            dataType: 'json'
            success:  (data, text_status, jq_xhr) ->
              # store user_data
              window.localStorage.setItem('user_data', JSON.stringify(data))
              window.App.navigate('/statuses')
              window.App.initialize()
              window.App.navigate('/statuses', { trigger: true })
            error:  (data, test_status, jq_xhr) ->
              $('.signin').effect('shake', { times: 4, distance: 4 }, 100)
          )
        error:  (data, test_status, jq_xhr) ->
          $('.signin').effect('shake', { times: 4, distance: 4 }, 100)

      )
      false

    go_reset: () ->
      window.App.navigate('/reset', { trigger: true })
      false
