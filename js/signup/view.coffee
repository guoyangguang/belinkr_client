define [
  'jquery',
  'underscore',
  'backbone',
  'cs!../external_view',
  'cs!../invitation/model',
  'cs!../alerter/helper',
  'text!../../templates/signup.html',
], ($, _, Backbone, ExternalView, Invitation, Alerter, Template) ->
  class SignupView extends ExternalView
    template:         jQuery(Template).html()
    el:               'div.container'
    events:
      'click button.btn-primary' : 'signup'
      'change .email'            : 'email_change'
      'change .firstname'        : 'firstname_change'
      'change .lastname'         : 'lastname_change'
      'change .password'         : 'password_change'
      'click a.login'            : 'nav_login'

    initialize: () ->
      @events = _.extend(@events, @locale_events)
      _.bindAll @
      @invitation = @options.invitation
      @invitation.urlRoot = '/invitations'
      @fetch_invitation_or_redirect()

    render: () ->
      @$el.html(@template)
      @$el = @localize @$el
      @

    email_change: (event) ->
      @model.set(email: $(event.currentTarget).val())

    firstname_change: (event) ->
      @model.set(first: $(event.currentTarget).val())

    lastname_change: (event) ->
      @model.set(last: $(event.currentTarget).val())

    password_change: (event) ->
      @model.set(password: $(event.currentTarget).val())

    signup: (e) =>
      e.preventDefault()
      $('form .alert').remove()
      $('button.btn-primary').attr('disabled', true)
      @model.save {},
        wait: true
        success: (model, response) =>
          alerter = new Alerter()
          message = @getLocalizeTextByKey('signup.alerter.succeed.created')
          message += @getLocalizeTextByKey('signup.alerter.succeed.start')
          div = $("<div />")
          link = $("<a />",
            class: "login"
            text: @getLocalizeTextByKey('signup.alerter.succeed.login')
            href: "/w/login"
          )
          div.append(link)
          message += div.html()
          div.empty()
          $('form h2').after(alerter.success(message).render())
          @reset_form()
        error: (model, response) =>
          if response && response.responseText
            attrs_with_errors = JSON.parse(response.responseText).errors
            alerter = new Alerter()
            $('form h2').after(alerter.error(attrs_with_errors).render())
      
      $('button.btn-primary').removeAttr('disabled')
      false

    fetch_invitation_or_redirect: () ->
      @invitation.fetch(
        success: (model, response) =>
          @model = new Invitation(
            id        : @invitation.get('id')
            entity_id : @invitation.get('entity_id')
          )
          @model.urlRoot = @invitation.urlRoot
          @render()

        error: () =>
          window.App.navigate('/login', { trigger: true })
      )

    nav_login: (e)->
      e.preventDefault()
      window.App.navigate('login', trigger: true)

    reset_form: () ->
      $('form input').each (index, element) ->
        $(element).val('')
