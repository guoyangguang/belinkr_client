define ["underscore", "backbone"], (_, Backbone) ->
  class Workspace extends Backbone.Model
    urlRoot: "/workspaces"
    defaults: 
      name: 'workspace'
      entity_id: 1
      created_at: new Date()
      updated_at: new Date()
