define ["underscore", "backbone"], (_, Backbone) ->
  class WorkspaceSummary extends Backbone.Model
    url: ()->
      "/workspaces/" + @id + "/summary"
    defaults:
      name: 'workspace'
      entity_id: 1
      created_at: new Date()
      updated_at: new Date()
