define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../../composite_view",
  "../../../templates/directives",
  "text!../../../templates/workspace_user.html",
  "cs!../../prefetch_data/prefetch_collections",
  "cs!../../alerter/helper",
  "../../aura/mediator",
  "cs!../../modules",
  "../../libs/pure/pure"
], ($, _, Backbone, CompositeView, Directives, Template, PrefetchCollections, Alerter, mediator) ->
  class WorkspaceUserBoxView extends Backbone.View
    events:
      "click img.avatar"    : "nav_user"
      "click li.user_name"  : "nav_user"
      "click button"        : "demote_or_promote"

    _objectName: "WorkspaceUserBoxView"


    template:             jQuery(Template)
    tagName:              "li"
    className:            "card"
    initialize: () ->
      _.bindAll @
      @model.bind "change", @render
      @template.directives(Directives.workspace_user)

    render: () ->
      @$el.html(@template.render(@model.toJSON()).html())
      @

    remove: () ->
      @$el.remove() # no fadeout

    leave: ()->
      @unbind()
      @remove()

    nav_user: (e) ->
      e.preventDefault()
      window.App.navigate("/users/" + @model.get("id"), trigger: true )
      false

    demote_or_promote: ()->
      @get_sidebar_summary_view()
      @get_sidebar_collaborators_view()
      @get_sidebar_administrators_view()

      if @model.get("_render_to") == "administrator"
        if @model.get("_visitor_workspace_relationship") is "administrator"
          unless @model.get("relationship") is "self"
            @demote()
      else if @model.get("_render_to") == "collaborator"
        if @model.get("_visitor_workspace_relationship") is "administrator"
          @promote()
      false

    get_sidebar_summary_view: ()->
      return @_sidebar_summary_view if @_sidebar_summary_view
      @_sidebar_summary_view = @parent.static_children.find (child)->
        child._objectName is "WorkspacesSidebarSummaryView"
      @_sidebar_summary_view

    get_sidebar_collaborators_view: ()->
      return @_sidebar_collaborators_view if @_sidebar_collaborators_view
      summary_view = @get_sidebar_summary_view()
      if summary_view
        @_sidebar_collaborators_view = summary_view
          .static_children.find (child)->
            child._objectName is "WorkspaceSidebarCollaboratorsView"
      @_sidebar_collaborators_view

    get_sidebar_administrators_view: ()->
      return @_sidebar_administrators_view if @_sidebar_administrators_view
      summary_view = @get_sidebar_summary_view()
      if summary_view
        @_sidebar_administrators_view = summary_view
          .static_children.find (child)->
            child._objectName is "WorkspaceSidebarAdministratorsView"
      @_sidebar_administrators_view

    promote: ()->
      $.ajax
        url: "/workspaces/" + @model.get("_workspace_id") + "/administrators/" + @model.get("id")
        type: "PUT"
        success: (response) =>
          @parent.collection.remove(@model)
          @_sidebar_collaborators_view?.collection.remove(@model)
          if @_sidebar_administrators_view?.collection.size() < 10
            @model.set("_visitor_workspace_relationship", "administrator")
            @model.set('relationship', 'administrator')
            @model.set("_render_to", "administrator")
            @_sidebar_administrators_view?.collection.add(@model)
          @promote_success_flash(@model)
          @leave()
        error: (xhr, text_status, error_message)=>
          @promote_error_flash(error_message)

    promote_success_flash: (model)->
      p = $("<p />")
      p1 = $("<p />")
      user_link = $("<a />",
        href: "/users/" + model.get("id")
        id: "flash_username"
        text: model.get("name")
      )
      p.append(user_link)
      administrators_link = $("<a />",
        href: "/workspaces/" + model.get("_workspace_id") + "/administrators"
        id: "flash_administrators"
        text: 'here'
      )
      p1.append(administrators_link)
      message = p.html() + "has been promoted to administrator."
      message += " You can see all administrators in "
      message += p1.html() + "."
      alerter = new Alerter()
      @parent.$el.find("div.flash").html(alerter.success(message).render())

    promote_error_flash: (message)->
      alerter = new Alerter()
      @parent.$el.find("div.flash").html(alerter.error([message]).render())

    demote: ()->
      $.ajax
        url: "/workspaces/" + @model.get("_workspace_id") + "/administrators/" + @model.get("id")
        type: "DELETE"
        success: (response) =>
          @parent.collection.remove(@model)
          @_sidebar_administrators_view?.collection.remove(@model)
          if @_sidebar_collaborators_view?.collection.size() < 10
            @model.set('relationship', 'collaborator')
            @model.set("_render_to", "collaborator")
            @_sidebar_collaborators_view?.collection.add(@model)
          @demote_success_flash(@model)
          @leave()
        error: (xhr, text_status, error_message)=>
          @demote_error_flash(error_message)

    demote_success_flash: (model)->
      p = $("<p />")
      p1 = $("<p />")
      user_link = $("<a />",
        href: "/users/" + model.get("id")
        id: "flash_username"
        text: model.get("name")
      )
      p.append(user_link)
      collaborators_link = $("<a />",
        href: "/workspaces/" + model.get("_workspace_id") + "/collaborators"
        id: "flash_collaborators"
        text: 'here'
      )
      p1.append(collaborators_link)
      message = p.html() + "has been demoted to collaborator."
      message += " You can see all collaborators in "
      message += p1.html() + "."
      alerter = new Alerter()
      @parent.$el.find("div.flash").html(alerter.success(message).render())

    demote_error_flash: (message)->
      alerter = new Alerter()
      @parent.$el.find("div.flash").html(alerter.error([message]).render())