define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../../composite_view",
  "../../../templates/directives",
  "text!../../../templates/workspace_sidebar_user.html",
  "cs!../../alerter/helper",
  "../../aura/mediator",
  "cs!../../modules",
  "../../libs/pure/pure"
], ($, _, Backbone, CompositeView, Directives, Template, Alerter, mediator) ->
  class WorkspaceSidebarUserView extends CompositeView
    template:             jQuery(Template)
    tagName:              "li"

    _objectName: "WorkspaceSidebarUserView"

    events:
      "click a" : "nav_user"
      "click button" : "demote_or_promote"

    initialize: () ->
      _.bindAll @
      @model.bind "change", @render
      @template.directives(Directives.workspace_sidebar_user)

    render: () ->
      @$el.html(@template.render(@model.toJSON()))
      @

    remove: () ->
      @$el.remove() # no fadeout

    leave: ()->
      @unbind()
      @remove()

    nav_user: (e)->
      e.preventDefault()
      window.App.navigate("/users/" + @model.id, trigger: true)
      false

    demote_or_promote: ()->
      @get_sidebar_summary_view_parent()
      if @model.get("_render_to") == "administrator"
        if @model.get("_visitor_workspace_relationship") is "administrator"
          unless @model.get("relationship") is "self"
            @get_sidebar_collaborators_view()
            @demote()
      else if @model.get("_render_to") == "collaborator"
        if @model.get("_visitor_workspace_relationship") is "administrator"
          @get_sidebar_administrators_view()
          @promote()
      false

    get_ancestor: (level) ->
      return @parent if level < 1
      object = @
      for num in [1..level]
        object = object.parent if object?.parent
      object

    get_sidebar_administrators_view: ()->
      return @_sidebar_administrators_view if @_sidebar_administrators_view
      summary_view = @get_ancestor(2)
      if summary_view
        @_sidebar_administrators_view = summary_view.static_children
          .find (child)->
            child._objectName is "WorkspaceSidebarAdministratorsView"
      @_sidebar_administrators_view

    get_sidebar_collaborators_view: ()->
      return @_sidebar_collaborators_view if @_sidebar_collaborators_view
      summary_view = @get_ancestor(2)
      if summary_view
        @_sidebar_collaborators_view = summary_view.static_children
          .find (child)->
            child._objectName is "WorkspaceSidebarCollaboratorsView"
      @_sidebar_collaborators_view

    get_sidebar_summary_view_parent: ()->
      @_sidebar_summary_view_parent ||= @get_ancestor(3)

    update_administrators_view_for_promote: (model)->
      if @_sidebar_summary_view_parent._objectName is "WorkspaceAdministratorsView"
        @_sidebar_summary_view_parent?.collection.add(model)

    update_collaborators_view_for_promote: (model)->
      if @_sidebar_summary_view_parent._objectName is "WorkspaceCollaboratorsView"
        @_sidebar_summary_view_parent?.collection.remove(model)

    update_administrators_view_for_demote: (model)->
      if @_sidebar_summary_view_parent._objectName is "WorkspaceAdministratorsView"
        @_sidebar_summary_view_parent?.collection.remove(model)

    update_collaborators_view_for_demote: (model)->
      if @_sidebar_summary_view_parent._objectName is "WorkspaceCollaboratorsView"
        @_sidebar_summary_view_parent?.collection.add(model)

    promote: ()->
      $.ajax
        url: "/workspaces/" + @model.get("_workspace_id") + "/administrators/" + @model.get("id")
        type: "PUT"
        success: (response) =>
          @parent.collection.remove(@model)
          @update_administrators_view_for_promote(@model)
          @update_collaborators_view_for_promote(@model)
          if @_sidebar_administrators_view?.collection.size() < 10
            @model.set("_visitor_workspace_relationship", "administrator")
            @model.set('relationship', 'administrator')
            @model.set("_render_to", "administrator")
            @_sidebar_administrators_view?.collection.add(@model)
          @promote_success_modal(@model)
        error: (xhr, text_status, error_message)=>
          @promote_error_modal(error_message)

    promote_success_modal: (model)->
      modal = @parent.$el.find("#collaborator_promote_modal")
      p = $("<p />")
      p1 = $("<p />")
      user_link = $("<a />",
        href: "/users/" + model.get("id")
        id: "modal_body_username"
        text: model.get("name")
      )
      p.append(user_link)
      administrators_link = $("<a />",
        href: "/workspaces/" + model.get("_workspace_id") + "/administrators"
        id: "modal_body_administrators"
        text: 'here'
      )
      p1.append(administrators_link)
      message = p.html() + "has been promoted to administrator."
      message += " You can see all administrators in "
      message += p1.html() + "."
      alerter = new Alerter()
      @parent.$el.find(".modal-body").html(alerter.success(message).render())
      modal.modal()

    promote_error_modal: (message) ->
      modal = @parent.$el.find("#collaborator_promote_modal")
      alerter = new Alerter()
      @parent.$el.find("div.modal-body").html(alerter.error([message]).render())
      modal.modal()

    demote: ()->
      $.ajax
        url: "/workspaces/" + @model.get("_workspace_id") + "/administrators/" + @model.get("id")
        type: "DELETE"
        success: (response) =>
          @parent.collection.remove(@model)
          @update_administrators_view_for_demote(@model)
          @update_collaborators_view_for_demote(@model)
          if @_sidebar_collaborators_view?.collection.size() < 10
            @model.set('relationship', 'collaborator')
            @model.set("_render_to", "collaborator")
            @_sidebar_collaborators_view?.collection.add(@model)
          @demote_success_modal(@model)
        error: (xhr, text_status, error_message)=>
          @demote_error_modal(error_message)

    demote_success_modal: (model)->
      modal = @parent.$el.find("#administrator_demote_modal")
      p = $("<p />")
      p1 = $("<p />")
      user_link = $("<a />",
        href: "/users/" + model.get("id")
        id: "modal_body_username"
        text: model.get("name")
      )
      p.append(user_link)
      collaborators_link = $("<a />",
        href: "/workspaces/" + model.get("_workspace_id") + "/collaborators"
        id: "modal_body_collaborators"
        text: 'here'
      )
      p1.append(collaborators_link)
      message = p.html() + "has been demoted to collaborator."
      message += " You can see all collaborators in "
      message += p1.html() + "."
      alerter = new Alerter()
      @parent.$el.find(".modal-body").html(alerter.success(message).render())
      modal.modal()

    demote_error_modal: (message) ->
      modal = @parent.$el.find("#administrator_demote_modal")
      alerter = new Alerter()
      @parent.$el.find("div.modal-body").html(alerter.error([message]).render())
      modal.modal()
