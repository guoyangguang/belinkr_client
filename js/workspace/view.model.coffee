define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../composite_view",
  "cs!./model.summary"
  "../../templates/directives",
  "text!../../templates/workspace.html",
  "cs!./status/collection",
  "cs!./status/view.collection",
  "cs!./view.sidebar.summary",
  "../aura/mediator",
  "cs!../modules",
  "../libs/pure/pure"
], ($, _, Backbone, CompositeView, Model, Directives, Template, WorkspaceStatuses, WorkspaceStatusesView, SidebarView, mediator) ->
  class WorkspaceView extends CompositeView
    template:             jQuery(Template)

    initialize: () ->
      @is_demonstrated = false
      _.bindAll @
      @model = new Model(id: @options.workspace_id)
      @model.bind("change", @render)
      @template.directives(Directives.workspace)

    render: () ->
      $(@el).html(@template.render(@model.toJSON()))
      @_after()
      @render_statuses()
      @

    _after: ()->
      if @is_demonstrated is false
        @is_demonstrated = true
      else
        @render_sidebar()
        if @model.get("relationship")
          unless (@model.get("relationship") is 'administrator') or (@model.get("relationship") is 'collaborator')
            window.App.navigate('/workspaces', trigger: true)

    render_sidebar: () ->
      view = new SidebarView(workspace: @model)
      @replaceWithStaticChildInto(view, @$('div#sidebar'))

    render_statuses: ()->
      workspace_statuses = new WorkspaceStatuses([],
        workspace_id: @options.workspace_id
      )
      workspace_statuses.fetch()
      view = new WorkspaceStatusesView(collection: workspace_statuses)
      @appendStaticChildInto(view, @$('div#main'))
