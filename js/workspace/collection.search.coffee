define ["underscore", "backbone", "cs!./model", "paginator"], (_, Backbone, Workspace) ->
  class WorkspaceCollection extends Backbone.Paginator.requestPager
    model:      Workspace
    url:        => "/workspaces/search"
    pageAttribute: 'page'
    perPageAttribute: 'perPage'
    queryAttribute: 'name'

    page: 0
    firtPage: 0
    perPage: 20

    comparator: (model) ->
      model.get("updated_at")

    initialize: (models, options) ->
      unless _.isEmpty(arguments)
        @options = _(arguments).last()
        @kind = @options.kind
        @perPage = @options.perPage if @options.perPage
        @query= options['query']
