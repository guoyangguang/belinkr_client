define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../../composite_view",
  "text!../../../templates/workspace_header.html",
  "../../../templates/directives",
  "cs!../../alerter/helper",
  "../../aura/mediator",
  "cs!../../modules",
  "../../libs/pure/pure"
], ($, _, Backbone, CompositeView, Template, Directives, Alerter, mediator) ->
  class WorkspaceHeaderView extends CompositeView
    template: jQuery(Template)

    events:
      "click h1 a": "nav_workspace"
      "click #update_workspace_modal button.submit" : "update_workspace"
      "click #delete_workspace_modal a[data-context='delete-workspace']" : "delete_workspace"

    initialize: () ->
      _.bindAll @
      # reset the model url(without summary)
      @model.url = "/workspaces/" + @model.get("id")
      @model.bind('chnage', @render)

    render: () ->
      @template = jQuery(Template)
      @template = @localize(@template)
      text = @getLocalizeTextByKey("workspace.header")
      workspace_link = '<a href="/workspaces/' + @model.get('id') + '">'
      workspace_link += text + " : " + @model.get("name")
      workspace_link += '</a>'
      @template.find("#header h1").html(workspace_link)

      unless @model.get('relationship') is 'administrator'
        @template.find("a[data-context='edit-workspace']").remove()
        @template.find("a[data-context='delete-workspace']").remove()
        @template.find("div#delete_workspace_modal").remove()
        @template.find("div#update_workspace_modal").remove()
      else
        @template.find("#header h1").attr("class", "span10")
        @template.find("#update_workspace_modal form input")
          .val(@model.get("name"))

      @$el.html(@template)
      @

    update_workspace: (e)->
      e.preventDefault()
      @model.save(
        name: @$el.find("#update_workspace_modal form input").val(),
        {
          wait: true
          success: ()=>
            alerter = new Alerter()
            message = @getLocalizeTextByKey("workspace.alerter.update.succeed")
            @$el.find("#update_workspace_modal form")
            .before(alerter.success(message).render())

          error: (model, response, xhr)=>
            if response && response.responseText
              attrs_with_errors = JSON.parse(response.responseText).errors
              alerter = new Alerter()
              @$el.find("#update_workspace_modal form")
              .before(alerter.error(attrs_with_errors).render())
        }
      )

    delete_workspace: (e)->
      e.preventDefault()
      @model.destroy(
        success: ()=>
#          @$el.find('#delete_workspace_modal').modal('toggle')
          @$el.find('#delete_workspace_modal div.modal-footer a').trigger('click')
          window.setTimeout ()->
            window.App.navigate('workspaces', trigger: true)
          ,1500

        error: (model, response, xhr)=>
          if response && response.responseText
            attrs_with_errors = JSON.parse(response.responseText).errors
            alerter = new Alerter()
            @$el.find("#delete_workspace_modal form")
            .before(alerter.error(attrs_with_errors).render())
      )

    nav_workspace: (e)->
      e.preventDefault()
      window.App.navigate('workspaces/' + @model.get('id'), trigger: true)