define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../../composite_view",
  "text!../../../templates/workspace_toolbar.html",
  "../../../templates/directives",
  "../../aura/mediator",
  "cs!../../modules",
  "../../libs/pure/pure"
], ($, _, Backbone, CompositeView, Template, Directives, mediator) ->
  class WorkspaceToolbarView extends CompositeView
    template: jQuery(Template)

    events:
      'click div.btn-group a' : 'switch_workspace_nav'

    initialize: () ->
      _.bindAll @
      @template.directives(Directives.workspace_toolbar)
      @model = new Backbone.Model(
        workspace_id: @options.workspace_id
        active: @options.active
      )
      @model.bind('change', 'render')

    render: () ->
      @$el.html(@template.render(@model.toJSON()).html())
      @$el = @localize(@$el)
      @toolbar_active()
      @

    toolbar_active: ()->
      switch @options.active
        when "invitations"
          link = @$el.find("a[data-context='invitations']")
        when "autoinvitations"
          link = @$el.find("a[data-context='autoinvitations']")
        when "files"
          link = @$el.find("a[data-context='files']")
        else
          link = @$el.find("a[data-context='timeline']")

      link.siblings().removeClass('active')
      link.addClass('active')

    switch_workspace_nav: (e)->
      e.preventDefault()
      el = $(e.target)
      el = el.parent() if el.is('span')
      window.App.navigate(el.attr("href"), trigger: true)
      false