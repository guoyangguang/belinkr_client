define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../composite_view",
  "../../templates/directives",
  "text!../../templates/workspace_sidebar_counter.html",
  "../aura/mediator",
  "cs!../modules"
], ($, _, Backbone, CompositeView, Directives, Template, mediator) ->
  class WorkspaceSidebarCounterView extends CompositeView
    template: jQuery(Template)
    tagName: "div"
    className: "box row-fluid"

    initialize: () ->
      _.bindAll @
      @template.directives(Directives.workspace_sidebar_counter)
      @$el.html(@template.render(@model.toJSON()))
      @$el = @localize(@$el)

    render: () ->
      @
