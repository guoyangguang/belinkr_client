define [
  "jquery",
  "underscore",
  "backbone",
  "cs!./view.model.partial",
  "cs!./collection",
  "cs!../composite_view",
  'cs!../alerter/helper',
  "text!../../templates/workspaces_sidebar_partial.html",
  "../aura/mediator",
  "cs!../modules"
], ($, _, Backbone, WorkspacePartialView, WorkspaceCollection, CompositeView, Alerter, Template, mediator) ->
  class WorkspacesSidebarPartialView extends CompositeView
    className: "workspaces_sidebar row-fluid"
    events:
      "click .modal button.submit"    : "create"
      "click button.all_workspaces"   : 'nav_workspaces'
      "click a.workspace"             : 'enter_workspace'

    initialize: () ->
      _.bindAll @
      @collection = new WorkspaceCollection([], kind: 'own', perPage: 5)
      @collection.perPage = 5
      @collection.kind = 'own'
      @collection.bind("add", @render_one)
      @collection.bind("reset", @render)
      @collection.fetch()
      @workspace_id = @options?.workspace_id
      @$el.html(Template)
      @$el = @localize(@$el)

      # Markup hooks
      @form           = @$("#create_workspace_modal form")
      @name_field     = @$("input[name='name']")
      @name_filler    = @name_field.data("filler")
      @submit_button  = @$("button.submit")
      @workspace_list = @$("ul.workspaces")

      @reset_form()

    render: () =>
      @collection.each(@render_one)
      @

    render_one: (model) ->
      view = new WorkspacePartialView(model:model, parent:@)
      model.bind("remove", view.fadeOutremove)
      @prependChildInto(view, @workspace_list)

    create: () ->
      @submit_button.attr("disabled", "disabled")
      @$("#create_workspace_modal ul.alert").remove()

      if (@name_field.attr("value") != @name_filler)  &&
      (@name_field.attr("value") != "")

        @collection.create({ name: @name_field.val() }, {
          wait:     true,
          success:  @success,
          error:    @errors
        })

      @submit_button.removeAttr("disabled")
      false

    success: (a, model)->
      alerter = new Alerter()
      div = $("<div />")
      workspace_link = $("<a />", class: 'workspace', href: "workspaces/#{model.id}")
      workspace_link.html(@getLocalizeTextByKey("workspace.alerter.new.here"))
      div.append(workspace_link)
      message = @getLocalizeTextByKey("workspace.alerter.new.your_workspace")
      message += model.name
      message += @getLocalizeTextByKey("workspace.alerter.new.created")
      message += div.html()
      message += @getLocalizeTextByKey("workspace.alerter.new.enter")
      div.empty()

      @form.before(alerter.success(message).render())
      @render_to_parent(model)
      @reset_form()

    render_to_parent: (model)->
      switch Backbone.history.fragment
        when "workspaces", "own"
          if @parent? and @parent.parent?
            workspaces_view = @parent.parent
            workspaces_view.collection._added_to_prepend = true
            workspaces_view.collection.add(model)
            workspaces_view.endless_pagination?()
        else
          ''

    errors: (model, response, xhr) ->
      if response && response.responseText
        attrs_with_errors = JSON.parse(response.responseText).errors
        alerter = new Alerter()
        @form.before(alerter.error(attrs_with_errors).render())

    nav_workspaces: ()->
      window.App.navigate("workspaces/own", trigger: true)

    enter_workspace: (e)->
      e.preventDefault()
      @$el.find('div.modal-footer a').trigger('click')
      window.App.navigate($(e.target).attr('href'), trigger: true)

    reset_form: () ->
      @name_field.attr("value", @name_filler)
      .focusin (event) =>
        $(event.target).attr("value", "")
        .focusout (event) =>
          if $(event.target).attr("value") == ""
            $(event.target).attr("value", @name_filler)
