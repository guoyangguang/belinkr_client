define [
  "jquery",
  "underscore",
  "backbone",
  "cs!./view.model.box",
  "cs!./view.sidebar",
  "cs!../toolbar/view.workspaces.toolbar",
  "cs!../toolbar/view.search_results.toolbar",
  "cs!../prefetch_data/prefetch_models",
  "cs!../composite_view",
  "text!../../templates/workspaces.html",
  "../aura/mediator",
  "cs!../modules"
], ($, _, Backbone, WorkspaceView, WorkspacesSidebarView, ToolbarView, SearchResultsToolbarView, PrefetchModels, CompositeView, Template, mediator) ->
  class WorkspacesView extends CompositeView
    template: jQuery(Template)

    events:
      'click div.btn-group a'     : 'nav_workspaces'
      "click button.servernext"   : "next_page"
      "click #accept_invitation_modal a.workspace"   : 'enter_workspace'

    initialize: ()->
      _.bindAll @

      @collection.bind("add", @render_one)
      @collection.bind("reset", @render)

      @collection.pager = @collection_pager
      @$el.html(@template)
      @$el = @localize(@$el)

      @workspace_list = @$("ul.workspaces")
      @error_list     = @$("ul.errors")
      @render_sidebar()
      @render_toolbar()

    render: () ->
      @render_all()
      @endless_pagination()
      if @collection.length <= @collection.perPage
        @hide_next_page_button()
      else
        @show_next_page_button()
      @

    render_all: ()->
      @collection.each(@render_one)

    render_toolbar: () ->
      search_results = PrefetchModels.search_results()
      if search_results.get('search_terms')
        toolbar_view = new SearchResultsToolbarView()
      else
        toolbar_view = new ToolbarView()
      @appendStaticChildInto(toolbar_view, @$(".btn-toolbar"))

    show_next_page_button: ()->
      @$el.find("button.servernext").removeClass("hide")

    hide_next_page_button: ()->
      @$el.find("button.servernext").addClass("hide")

    disable_next_page_button: ()->
      @$el.find("button.servernext").attr("disabled", "disabled")
#      @$el.find("button.servernext").html("No more results")

    enable_next_page_button: ()->
      @$el.find("button.servernext").removeAttr("disabled")
#      @$el.find("button.servernext").html("The next page")

    onShow: ->
      @accept_invitation_modal_swith()
      @toolbar_active()

    render_one: (model) ->
      view = new WorkspaceView(model:model, parent:@)
      view.render()
      model.bind("remove", view.fadeOutremove)
      if @collection._added_to_prepend is true
        @prependChildInto(view, @workspace_list)
        @collection._added_to_prepend = false
      else
        @appendChildInto(view, @workspace_list)

    render_sidebar: () ->
      sidebar_view = new WorkspacesSidebarView
      @replaceWithStaticChildInto(sidebar_view, @$("#sidebar"))

    toolbar_active: () ->
      switch @collection.kind
        when "invited"
          link = @$el.find("a[data-context='invited']")
        when "autoinvited"
          link = @$el.find("a[data-context='autoinvited']")
        when "own"
          link = @$el.find("a[data-context='own']")
        else
          link = @$el.find("a[data-context='all']")
      link.siblings().removeClass('active')
      link.addClass('active')

    accept_invitation_modal_swith: ()->
      if @collection.kind is 'all' or @collection.kind is 'invited'
        @$el.find('#accept_invitation_modal').addClass('modal')
      else
        @$el.find('#accept_invitation_modal').removeClass('modal')

    nav_workspaces: (e)->
      @_leaveChildren()
      @workspace_list.empty()
      @collection = []
      @collection.page = 0

      el = $(e.target)
      el = el.parent() if el.is('span')
      window.App.navigate(el.attr("href"), trigger: true)
      false

    enter_workspace: (e)->
      e.preventDefault()
      @$el.find('div.modal-footer a').trigger('click')
      window.App.navigate($(e.target).attr('href'), trigger: true)

    endless_pagination: ()->
      @collection.no_more_results = false
      $(window).unbind('scroll')
      $(window).scroll ()=>
        return if @collection.no_more_results
        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10)
            # @collection mightbe a []
            @collection.requestNextPage() if @collection.kind

    collection_pager: ()->
      @collection.fetch success: (collection, resp)=>
        if collection.length == 0
          @show_next_page_button()
          @disable_next_page_button()
          @collection.page -= 1
          @collection.no_more_results = true

    next_page: () ->
      @hide_next_page_button()
      @collection.requestNextPage()
      $ () =>
        if $(document).height() > $(window).height()
          @endless_pagination()
        else
          @show_next_page_button()
          @enable_next_page_button()
      false