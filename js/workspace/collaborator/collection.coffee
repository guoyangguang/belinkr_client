define ["underscore", "backbone", "cs!../../user/model.summary", "paginator"], (_, Backbone, User) ->
  class WorkspaceCollaboratorCollection extends Backbone.Paginator.requestPager
    model:      User
    url: ()->
      "/workspaces/" + @workspace_id + "/collaborators"

    pageAttribute: 'page'
    perPageAttribute: 'perPage'

    page: 0
    firtPage: 0
    perPage: 20

    comparator: (model) ->
      model.get("updated_at")

    initialize: (models,options)->
      @options = options
      @workspace_id = @options.workspace_id
      @perPage = @options.perPage if @options.perPage