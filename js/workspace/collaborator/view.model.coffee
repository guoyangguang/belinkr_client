define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../../composite_view",
  "../../../templates/directives",
  "text!../../../templates/workspace_collaborator_box.html",
  "text!../../../templates/workspace_remove_collaborator_modal.html",
  "cs!../../prefetch_data/prefetch_collections",
  "cs!../../alerter/helper",
  "../../aura/mediator",
  "cs!../../modules",
  "../../libs/pure/pure"
], ($, _, Backbone, CompositeView, Directives, Template, RemoveModalTemplate, PrefetchCollections, Alerter, mediator) ->
  class WorkspaceCollaboratorView extends CompositeView
    events:
      "click img.avatar"    : "nav_user"
      "click li.user_name"  : "nav_user"
      "click button[data-context='promote']"  : "click_promote"
      "click button[data-context='remove']"   : "show_remove_modal"

    template:             jQuery(Template)
    tagName:              "li"
    className:            "card"
    initialize: () ->
      _.bindAll @
      @model.bind "change", @render
      @template.directives(Directives.workspace_collaborator_box)

    render: () ->
      @$el.html(@template.render(@model.toJSON()).html())
      @$el = @localize(@$el)
      @localize_button_text()
      @

    localize_button_text: ()->
      if @model.get("relationship") is 'self'
        translation_key = "self"
      else
        switch @model.get("_render_to")
          when 'administrator'
            translation_key = 'demote'
          when 'collaborator'
            translation_key = "promote"
          else
            translation_key = ''
      if translation_key is ''
        text = ''
      else
        text = @getLocalizeTextByKey("button.workspace.all_users.#{translation_key}")

      @$el.find("button[data-context='promote']").html(text)


    remove: () ->
      @$el.remove() # no fadeout

    leave: ()->
      @unbind()
      @remove()

    nav_user: (e) ->
      e.preventDefault()
      window.App.navigate("/users/" + @model.get("id"), trigger: true )
      false

    show_remove_modal: ()->
      @get_sidebar_summary_view()
      @get_sidebar_collaborators_view()
      @get_sidebar_administrators_view()

      if @model.get("_render_to") == "collaborator"
        if @model.get("_visitor_workspace_relationship") is "administrator"
          @add_remove_modal_to_parent()

    add_remove_modal_to_parent: ()->
      modal = jQuery(RemoveModalTemplate)
      modal = @localize(modal)
      btn = modal.find("a[data-context='delete-collaborator']")
      btn.off('click')
      btn.on('click', @remove_collaborator)

      el = @parent.$el
      modal_container = el.find("#remove_modal_container")
      modal_container.empty()
      modal_container.append(
        modal
      )
      modal.modal('show')

    click_promote: ()->
      @get_sidebar_summary_view()
      @get_sidebar_collaborators_view()
      @get_sidebar_administrators_view()

      if @model.get("_render_to") == "collaborator"
        if @model.get("_visitor_workspace_relationship") is "administrator"
          @promote()
      false

    get_sidebar_summary_view: ()->
      return @_sidebar_summary_view if @_sidebar_summary_view
      @_sidebar_summary_view = @parent.static_children.find (child)->
        child._objectName is "WorkspacesSidebarSummaryView"
      @_sidebar_summary_view

    get_sidebar_collaborators_view: ()->
      return @_sidebar_collaborators_view if @_sidebar_collaborators_view
      summary_view = @get_sidebar_summary_view()
      if summary_view
        @_sidebar_collaborators_view = summary_view
        .static_children.find (child)->
          child._objectName is "WorkspaceSidebarCollaboratorsView"
      @_sidebar_collaborators_view

    get_sidebar_administrators_view: ()->
      return @_sidebar_administrators_view if @_sidebar_administrators_view
      summary_view = @get_sidebar_summary_view()
      if summary_view
        @_sidebar_administrators_view = summary_view
        .static_children.find (child)->
          child._objectName is "WorkspaceSidebarAdministratorsView"
      @_sidebar_administrators_view

    promote: ()->
      $.ajax
        url: "/workspaces/" + @model.get("_workspace_id") + "/administrators/" + @model.get("id")
        type: "PUT"
        success: (response) =>
          @parent.collection.remove(@model)
          @_sidebar_collaborators_view?.collection.remove(@model)
          if @_sidebar_administrators_view?.collection.size() < 10
            @model.set("_visitor_workspace_relationship", "administrator")
            @model.set('relationship', 'administrator')
            @model.set("_render_to", "administrator")
            @_sidebar_administrators_view?.collection.add(@model)
          @promote_success_flash(@model)
          @leave()
        error: (xhr, text_status, error_message)=>
          @promote_error_flash(error_message)

    promote_success_flash: (model)->
      p = $("<p />")
      user_link = $("<a />",
        href: "/users/" + model.get("id")
        id: "flash_username"
        text: model.get("name")
      )
      p.append(user_link)
      message = p.html()
      message += @getLocalizeTextByKey(
        "workspace.collaborators.alerter.promote.succeed.has_promoted"
      )
      p.empty()
      alerter = new Alerter()
      @parent.$el.find("div.flash").html(alerter.success(message).render())

    promote_error_flash: (message)->
      alerter = new Alerter()
      @parent.$el.find("div.flash").html(alerter.error([message]).render())

    remove_collaborator: (e)->
      e.preventDefault()
      $.ajax
        url: "/workspaces/" + @model.get("_workspace_id") + "/collaborators/" + @model.get("id")
        type: "DELETE"
        success: @remove_success
        error: @remove_error

    remove_success: ()->
      @parent.collection.remove(@model)
      @_sidebar_collaborators_view.collection.remove(@model)
      # alerter = new Alerter()
      # message = "The collaborator already removed from this workspace"
      # @render_message_to_modal(
      #   alerter.success(message).render()
      # )
      el = @parent.$el
      el.find("#remove_collaborator_modal").modal("toggle")

      @leave()
      @empty_modal_container()

    remove_error: (xhr, text_status, error_message)->
        alerter = new Alerter()
        @render_message_to_modal(
          alerter.error([error_message]).render()
        )

    render_message_to_modal: (msg_dom)->
      el = @parent.$el
      el.find("#remove_collaborator_modal form").before(msg_dom)

    empty_modal_container: ()->
      el = @parent.$el
      modal_container = el.find("#remove_modal_container")
      modal_container.empty()
