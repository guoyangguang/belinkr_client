define [
  "jquery",
  "underscore",
  "backbone",
  "../../templates/directives",
  "text!../../templates/workspace_partial.html",
  "cs!../composite_view",
  "../aura/mediator",
  "cs!../modules",
  "../libs/pure/pure"
], ($, _, Backbone, Directives, Template,CompositeView,mediator) ->
  class WorkspacePartialView extends CompositeView
    template:             jQuery(Template)
    tagName:              "li"

    events:
      "click a"   : "show"

    initialize: () ->
      _.bindAll(@, "render", "fadeOutremove", "delete", "show")
      @model.bind("change", @render)
      @template.directives(Directives.workspace_partial)
    render: () ->
      $(@el).html( @template.render @model.toJSON() )
      @

    fadeOutremove: () ->
      $(@el).fadeOut(250,=>@remove())

    delete: () ->
      @model.destroy({},{wait:true})
      false

    show: (e) ->
      e.preventDefault()
      window.App.navigate("/workspaces/" + @model.id, trigger: true)
      false