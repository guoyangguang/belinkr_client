define [
  "jquery",
  "underscore",
  "backbone",
  "../../templates/directives",
  "text!../../templates/workspace_box.html",
  "cs!../composite_view",
  'cs!../alerter/helper',
  "../aura/mediator",
  "cs!../modules",
  "../libs/pure/pure"
], ($, _, Backbone, Directives, Template,CompositeView, Alerter, mediator) ->
  class WorkspaceViewBox extends CompositeView
    template:             jQuery(Template)
    tagName:              "li"
    className:            "workspace"

    events:
      "click button"        : 'btn_click'
      "click h3.truncate a" : 'nav_workspace'

    initialize: () ->
      @_demonstrated = false
      _.bindAll @
      @model.bind("change", @render)
      @template.directives(Directives.workspace_box)

    render: () ->
      @_after()
      @

    remove: () ->
      @$el.remove()

    leave: ()->
      @unbind()
      @remove()

    _after: ()->
      if @_demonstrated is false
        @_demonstrated = true
      else
        @$el.html( @template.render @model.toJSON() )
        @$el = @localize(@$el)
        @update_button_text()
        @add_box_class_in()
        @add_reject_button_for_invited()


    fadeOutremove: () ->
      $(@el).fadeOut(250,=>@remove())

    delete: () ->
      @model.destroy({},{wait:true})
      false

    update_button_text: ()->
      switch @model.get("relationship")
        when 'administrator', 'collaborator', 'none', 'autoinvited', 'invited'
          valid = true
        else
          valid = false
      if valid
        text = @getLocalizeTextByKey(
          "button.workspace_relationship.#{@model.get("relationship")}"
        )
      else
        text = ''
      @$el.find('button').append(text)


    add_box_class_in: ()->
      if @model.get('relationship') is ('administrator' or 'collaborator')
        @$el.addClass('in')

    add_reject_button_for_invited: ()->
      if @model.get('relationship') is 'invited'
        @$el.find(".btn-group").prepend("<button class='btn btn-danger'>
          <i class='icon icon-ban-circle icon-white'></i>
          #{@getLocalizeTextByKey('button.workspace_relationship.rejected')}
          </button>"
        )

    show: () ->
      false

    btn_click: (e)->
      switch @model.get("relationship")
        when 'administrator', 'collaborator' then @_navigate()
        when 'none' then @_request_access()
#        when 'autoinvited' then ''
        when 'invited'
          btn = $(e.target)
          if btn.hasClass('btn-success')
            @_accept_invitation()
          else if btn.hasClass('btn-danger')
            @_reject_invitation()
        else ''
      false

    nav_workspace: (e)->
      e.preventDefault()
      switch @model.get("relationship")
        when 'administrator', 'collaborator' then @_navigate()
        else false
      false

    _navigate: ()->
      window.App.navigate('/workspaces/' + @model.id, trigger: true)

    _request_access: ()->
      $.ajax
        url: '/workspaces/'+ @model.id + '/autoinvitations'
        type:'POST'
        dataType: 'json'
        success: (response)=>
          @model.set('relationship', 'autoinvited')
        error: ()=>

    _accept_invitation: ()->
      $.ajax
        url: @model.get('links').invitation_accept
        type:'PUT'
        dataType: 'json'
        success: (response)=>
          modal = @parent.$el.find("#accept_invitation_modal")
          modal.modal()
          alerter = new Alerter()

          div = $("<div />")
          link = $("<a />", class: 'workspace', href: "workspaces/#{@model.get("id")}")
          link.html(@getLocalizeTextByKey("workspace.invitation.alerter.accept.succeed.here"))
          div.append(link)
          message = @getLocalizeTextByKey("workspace.invitation.alerter.accept.succeed.accepted")
          message += div.html()
          message += @getLocalizeTextByKey("workspace.invitation.alerter.accept.succeed.enter")
          div.empty()

          modal.find("div.modal-body")
          .html(alerter.success(message).render())

          @model.set('relationship', 'collaborator')
        error: (xhr, text_status, error_message)=>
          modal = @parent.$el.find("#accept_invitation_modal")
          modal.modal()
          alerter = new Alerter()
          message = "Accept workspace(" + @model.get("name")
          message += ") invitation error: " + error_message
          modal.find("div.modal-body")
          .html(alerter.error([message]).render())

    _reject_invitation: ()->
      $.ajax
        url: @model.get('links').invitation_reject
        type:'PUT'
        dataType: 'json'
        success: (response)=>
          modal = @parent.$el.find("#accept_invitation_modal")
          modal.modal()
          alerter = new Alerter()
          message = @getLocalizeTextByKey("workspace.invitation.alerter.reject.succeed")
          modal.find("div.modal-body")
          .html(alerter.success(message).render())

          @model.set('relationship', 'none')
        error: (xhr, text_status, error_message)=>
          modal = @parent.$el.find("#accept_invitation_modal")
          modal.modal()
          alerter = new Alerter()
          message = "reject workspace(" + @model.get("name")
          message += ") invitation error: " + error_message
          modal.find("div.modal-body")
          .html(alerter.error([message]).render())