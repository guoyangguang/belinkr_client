define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../composite_view",
  "cs!../user/model.summary",
  "cs!./user/view.sidebar.model",
  "text!../../templates/workspace_sidebar_administrators.html",
  "../aura/mediator",
  "cs!../modules"
], ($, _, Backbone, CompositeView, User, SidebarUserView, Template, mediator) ->
  class WorkspaceSidebarAdministratorsView extends CompositeView

    events:
      "click h3 a"                        : "nav_workspace_administrators"
      "click a#modal_body_username"       : "nav_user"
      "click a#modal_body_collaborators" : "nav_workspace_collaborators"

    _objectName: "WorkspaceSidebarAdministratorsView"

    initialize: () ->
      _.bindAll @
      @collection   = new Backbone.Collection(@model.get("administrators"))
      @collection.bind 'remove', @render
      @collection.bind 'reset',  @render
      @collection.bind 'add',    @render
      @workspace_id = @model.get("id")
      @$el.html(Template)
      @$el = @localize(@$el)

    render: () ->
      if @collection.size() > 0
        @_leaveChildren()
        @render_all_users()
      @

    render_all_users: ()->
      @collection.each(@render_user)

    render_user: (model)->
      workspace_relationship = model.get("_visitor_workspace_relationship")
      unless workspace_relationship
        model.set("_visitor_workspace_relationship",
          @model.get("relationship")
        )
      model.set("_render_to",   "administrator")
      model.set("_workspace_id", @model.get("id"))
      view = new SidebarUserView(model: new User(model.attributes))
      @appendChildInto(view, @$("ul.avatars"))

    nav_workspace_administrators: (e)->
      e.preventDefault()
      window.App.navigate(
        "/workspaces/" + @workspace_id + "/administrators",
        trigger: true
      )

    nav_workspace_collaborators: (e)->
      e.preventDefault()
      @$(".modal-footer a.btn").trigger('click')
      window.App.navigate(
        "/workspaces/" + @workspace_id + "/collaborators",
        trigger: true
      )

    nav_user: (e)->
      e.preventDefault()
      @$(".modal-footer a.btn").trigger('click')
      window.App.navigate(
        $(e.target).attr('href'),
        trigger: true
      )