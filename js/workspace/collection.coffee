define ["underscore", "backbone", "cs!./model", "paginator"], (_, Backbone, Workspace) ->
  class WorkspaceCollection extends Backbone.Paginator.requestPager
    model:      Workspace
    url: ()->
      switch @kind
        when 'own'          then '/workspaces/own'
        when 'invited'      then '/workspaces/invited'
        when 'autoinvited'  then '/workspaces/autoinvited'
        else '/workspaces'

    pageAttribute: 'page'
    perPageAttribute: 'perPage'

    page: 0
    firtPage: 0
    perPage: 20

    comparator: (model) ->
      model.get("updated_at")

    initialize: () ->
      unless _.isEmpty(arguments)
        @options = _(arguments).last()
        @kind = @options.kind
        @perPage = @options.perPage if @options.perPage