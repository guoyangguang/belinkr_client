define ["underscore", "backbone", "cs!./model"], (_, Backbone, Reply) ->
  class ReplyCollection extends Backbone.Collection
    model:      Reply
    url:        ->
      "/workspaces/" + @workspace_id + "/statuses/" + @status_id + "/replies"
    comparator: (model) ->
      model.get("updated_at")

    initialize: (models,options)->
      if options['status_id'] 
        @status_id=options["status_id"]
      if options['workspace_id'] 
        @workspace_id=options["workspace_id"]
      
