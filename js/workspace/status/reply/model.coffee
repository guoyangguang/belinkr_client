define ["underscore", "backbone"], (_, Backbone) ->
  class Reply extends Backbone.Model
    urlRoot: -> "/workspaces/" + @collection.workspace_id + "/statuses/" + @get("status_id") + "/replies"
    defaults: {
      created_at: new Date()
      updated_at: new Date()
    }
