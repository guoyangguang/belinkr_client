define ["underscore", "backbone", "cs!./model", "paginator"], (_, Backbone, WorkspaceStatus) ->
  class WorkspaceFileStatusCollection extends Backbone.Paginator.requestPager
    model:      WorkspaceStatus
    url:        ->
      "/workspaces/" + @workspace_id + "/timelines/files"

    pageAttribute: 'page'
    perPageAttribute: 'perPage'

    page: 0
    firtPage: 0
    perPage: 20

    comparator: (model) ->
      model.get("updated_at")

    initialize: (models,options)->
      @options = options
      @workspace_id = @options.workspace_id

