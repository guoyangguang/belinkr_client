define ["underscore", "backbone", "cs!./reply/collection"], (_, Backbone, Replies) ->
  class WorkspaceStatus extends Backbone.Model
    urlRoot: ()->
      "/workspaces/" + @get("workspace_id") + "/statuses"
    defaults: {
      # FIXME: the default values are fixed, not the current time, but it works
      # in /status/model.coffee, so comment these code now and let api to
      # generate the timestamp
      #created_at: new Date()
      #updated_at: new Date()
    }
    get_replies: ->
      if @get('replies') and @get('replies').length > 0
        new Replies(@get('replies'),
          status_id: @get('id')
          workspace_id: @get("workspace_id")
        )
      else
        new Replies([],
          status_id: @get('id')
          workspace_id: @get("workspace_id")
        )


