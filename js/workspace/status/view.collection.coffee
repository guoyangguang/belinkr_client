define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../model.summary",
  "../../../templates/directives"
  "cs!../../composite_view",
  "cs!../../status/view.model",
  "cs!../toolbar/view.toolbar",
  "cs!../toolbar/view.workspace.header",
  "cs!../../scrapbook/scrap/view.collection",
  'text!../../../templates/workspace_statuses.html',
  'cs!../../uploader',
  "cs!../view.sidebar.summary",
  "../../aura/mediator",
  "cs!../../modules"
], ($, _, Backbone, Workspace, Directives, CompositeView, StatusView, ToolbarView, WorkspaceHeaderView, ScrapsView, Template, Uploader, SidebarView, mediator) ->
  class WorkspaceStatusesView extends CompositeView
    template: jQuery(Template)
    events:
      'click button.submit'       : 'create'
      'click button.servernext'   : 'next_page'

    initialize: () ->
      _.bindAll @
      @collection.bind 'reset', @render
      @collection.bind 'add',   @render_one
      @collection.pager     = @collection_pager

      @model = new Workspace(id: @collection.workspace_id)
      @model.fetch()
      @model.bind 'change', @render_workspace

      @$el.html(@template.html())
      @$el = @localize(@$el)

#      @render_html_editor(
#        id: "editor-toolbar-workspace-statuses"
#        editorContainer: "#editor-toolbar-workspace-statuses"
#      )

      # Markup hooks
      @text_field     = @$('textarea')
      @text_filler    = @text_field.attr('placeholder')
      @submit_button  = @$('button.submit')
      @status_list    = @$('ul.statuses')
      @error_list     = @$('ul.errors')
#      @render_header()
      @render_sidebar()
      @render_toolbar()

    onShow: ()=>
#      @editor = new wysihtml5.Editor @$el.find("#workspace-status")[0],
#        toolbar:      "editor-toolbar-workspace-statuses",
#        parserRules:  wysihtml5ParserRules
      @render_upload_form()

    render: () =>
      @reset_form()
      @render_all()
      if @collection.length <= @collection.perPage
        @hide_next_page_button()
      else
        @show_next_page_button()
      @endless_pagination()
      @

    render_upload_form: ()=>
      view_options =
        data:
          id: 'workspace_statuses'

      uploader_options =
        parent_form: @$el.find("form.statuses").first()
        settings:
          token: '?auth_token=' + $.cookie('belinkr.auth_token')

      built = Uploader.build(view_options, uploader_options)
      @replaceWithStaticChildInto(built.view, @$("div.uploader"))
      # document ready!
      $ ()->
        built.uploader.init()

    show_next_page_button: ()->
      @$el.find("button.servernext").removeClass("hide")

    hide_next_page_button: ()->
      @$el.find("button.servernext").addClass("hide")

    disable_next_page_button: ()->
      @$el.find("button.servernext").attr("disabled", "disabled")
#      @$el.find("button.servernext").html("No more results")

    enable_next_page_button: ()->
      @$el.find("button.servernext").removeAttr("disabled")
#      @$el.find("button.servernext").html("The next page")

    render_sidebar: () ->
      sidebar_view = new SidebarView(model: @model)
      @replaceWithStaticChildInto(sidebar_view, @$("#sidebar"))

    render_header: ()->
      header_view = new WorkspaceHeaderView(
        model: @model
      )
      @replaceWithStaticChildInto(header_view, @$("#header"))

    enforce_relationship: ()->
      if ['administrator', 'collaborator'].indexOf(@model.get("relationship")) < 0
        window.App.navigate('workspaces', trigger: true)

    render_workspace: ()->
      @enforce_relationship()
      @render_sidebar()
      @render_header()

    render_toolbar: ()->
      view = new ToolbarView(
        workspace_id: @collection.workspace_id
        active: 'timeline'
      )
      view.render()
      @renderChildInto(view, @$('div.btn-toolbar'))

    create: () =>
      @collection._added_to_prepend = true
      mediator.publish "createWorkspaceStatus", @
      @editor_reset()
      false

    editor_reset: ()->
      @editor?.clear()
      @editor?.fire("set_placeholder")

    render_all: () ->
      @collection.each(@render_one)

    render_one: (model) =>
      view = new StatusView(model: model)
      if @collection._added_to_prepend is true
        @prependChildInto(view, @status_list)
        @collection._added_to_prepend = false
      else
        @appendChildInto(view, @status_list)

    errors: (model, response, xhr) =>
      if response && response.responseText
        attrs_with_errors = JSON.parse(response.responseText).errors
        _.each(attrs_with_errors, (errors) ->
          _.each(errors, (error) ->
            @$('ul.errors').append('<li>' + error + '</li>')
          )
        )

    reset_form: () =>
      @$el.find("input[name='files']").val('')
      @$el.find(".uploader_form ul.file_list").empty()

      @text_field.val(@text_filler)
        .focusin (event) =>
          @$(event.target).val('') if $(event.target).val() == @text_filler
        .focusout (event) =>
          @$(event.target).val(@text_filler) if $(event.target).val() == ''

    endless_pagination: ()->
      @collection.no_more_results = false
      $(window).unbind('scroll')
      $(window).scroll ()=>
        return if @collection.no_more_results
        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10)
          @collection.requestNextPage()

    collection_pager: ()->
      @collection.fetch success: (collection, resp)=>
        if collection.length == 0
          @show_next_page_button()
          @disable_next_page_button()
          @collection.page -= 1
          @collection.no_more_results = true

    next_page: () ->
      @hide_next_page_button()
      @collection.requestNextPage()
      $ () =>
        if $(document).height() > $(window).height()
          @endless_pagination()
        else
          @show_next_page_button()
          @enable_next_page_button()
      false