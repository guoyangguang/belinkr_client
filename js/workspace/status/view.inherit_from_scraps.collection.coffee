define [
  "jquery",
  "underscore",
  "backbone",
  "../../../templates/directives"
  "cs!../../composite_view",
  "cs!../../scrapbook/scrap/view.collection",
  "text!../../../templates/scraps.html",
  'cs!../../uploader',
  "../../aura/mediator",
  "cs!../../modules"
], ($, _, Backbone, Directives, CompositeView, ScrapsView, Template, Uploader, mediator) ->
  class WorkspaceStatusesView extends ScrapsView
    create: ()->
      mediator.publish "createWorkspaceStatus", @
      false


