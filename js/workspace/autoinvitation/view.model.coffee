define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../../composite_view"
  "../../../templates/directives",
  "text!../../../templates/workspace_autoinvitation_pending.html",
  "text!../../../templates/workspace_autoinvitation.html",
  "../../libs/pure/pure"
], ($, _, Backbone, CompositeView, Directives, TemplatePending, Template) ->
  class WorkspaceAutoInvitationView extends CompositeView

#    template:         jQuery(Template)
#    template_pending: jQuery(TemplatePending)
    template: ()->
      if (@model.get("state") is "pending") && (@model.get("relationship") is "administrator")
        template = jQuery(TemplatePending)
        template.directives(Directives.workspace_autoinvitation_pending)
        template
      else
        template = jQuery(Template)
        template.directives(Directives.workspace_autoinvitation)
        template

    tagName:    "li"
    className:  "activity article span12 row-fluid"

    events:
      "click button.btn-success" : "accept"
      "click button.btn-danger"  : "reject"
      "click a.actor"            : "nav_invited"
      "click img.avatar"         : "nav_invited"
      "click a.object"           : "nav_workspace"

    initialize: ()->
      _.bindAll @
      @model.bind 'change', @render

    render: () ->
      @$el.html(@template().render(@model.toJSON()).html())
      @$el = @localize(@$el)
      @localize_invitation_state()
      @invoke_timeago()
      @

    localize_invitation_state: ()->
      @$el.find("p.state span").html(
        @getLocalizeTextByKey(
          "workspace.auto_invitation.state.#{@model.get("state")}"
        )
      )

    nav_invited: (e)->
      e.preventDefault()
      window.App.navigate(@model.get("links").invited, trigger: true)

    nav_workspace: (e)->
      e.preventDefault()
      window.App.navigate(@model.get('links').workspace, trigger: true)

    accept: () ->
      if @model.get('state') is "pending"
        $.ajax
          url: '/workspaces/' + @model.get("workspace_id") + '/autoinvitations/accepted/' + @model.get("id")
          type: "PUT"
          success: (response) =>
            @model.set(JSON.parse(response))
          error: ()=>
      false

    reject: () ->
      if @model.get('state') is "pending"
        $.ajax
          url: '/workspaces/' + @model.get("workspace_id") + '/autoinvitations/rejected/' + @model.get("id")
          type: "PUT"
          success: (response) =>
            @model.set(JSON.parse(response))
          error: ()=>
      false
