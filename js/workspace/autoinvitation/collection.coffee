define ["underscore", "backbone", "cs!./model", "paginator"], (_, Backbone, Invitation) ->
  class WorkspaceAutoInvitationCollection extends Backbone.Paginator.requestPager
    model:      Invitation
    url: ()->
      "/workspaces/" + @workspace_id + "/autoinvitations"

    pageAttribute: 'page'
    perPageAttribute: 'perPage'

    page: 0
    firtPage: 0
    perPage: 20

    comparator: (model) ->
      model.get("updated_at")

    initialize: (models,options)->
      @options = options
      @workspace_id = @options.workspace_id
      @perPage = @options.perPage if @options.perPage