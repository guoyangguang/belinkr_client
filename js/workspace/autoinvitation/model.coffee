define ["underscore", "backbone"], (_, Backbone) ->
  class WorkspaceAutoInvitation extends Backbone.Model
    urlRoot: () ->
      "/workspaces/" + @get("workspace_id") + "/autoinvitations"