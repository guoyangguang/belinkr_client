define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../model.summary",
  "cs!./view.model",
  "cs!../toolbar/view.toolbar",
  "cs!../../composite_view",
  "text!../../../templates/workspace_autoinvitations.html",
  "cs!../view.sidebar.summary",
  "../../aura/mediator",
  "cs!../../modules"
], ($, _, Backbone, Workspace, InvitationView, ToolbarView, CompositeView, Template, SidebarView, mediator) ->
  class WorkspaceAutoInvitationsView extends CompositeView
    events:
      "click button.servernext"   : "next_page"

    initialize: () ->
      _.bindAll @
      @model = new Workspace(id: @collection.workspace_id)
      @model.fetch()
      @model.bind 'change', @render_workspace

      @collection.bind "reset", @render
      @collection.bind "add",   @render_one

      @collection.pager = @collection_pager

      @$el.html(Template)
      @$el = @localize(@$el)

    render: () ->
      @render_all()
      if @collection.length <= @collection.perPage
        @hide_next_page_button()
      else
        @show_next_page_button()
      @endless_pagination()
      @

    onShow: ()->
      @render_toolbar()

    show_next_page_button: ()->
      @$el.find("button.servernext").removeClass("hide")

    hide_next_page_button: ()->
      @$el.find("button.servernext").addClass("hide")

    disable_next_page_button: ()->
      @$el.find("button.servernext").attr("disabled", "disabled")
#      @$el.find("button.servernext").html("No more results")

    enable_next_page_button: ()->
      @$el.find("button.servernext").removeAttr("disabled")
#      @$el.find("button.servernext").html("The next page")

    render_all: () ->
      @collection.each(@render_one)

    render_one: (model) ->
      view = new InvitationView(model: model)
      @appendChildInto(view, @$("ul.activities"))

    render_toolbar: () ->
      view = new ToolbarView(
        workspace_id: @collection.workspace_id
        active: "autoinvitations"
      )
      view.render()
      @renderChildInto(view, @$('div.btn-toolbar'))

    render_sidebar: () ->
      sidebar_view = new SidebarView(model: @model)
      @replaceWithStaticChildInto(sidebar_view, @$("#sidebar"))

    enforce_relationship: ()->
      if ['administrator', 'collaborator'].indexOf(@model.get("relationship")) < 0
        window.App.navigate('workspaces', trigger: true)

    render_workspace: ()->
      @enforce_relationship()
      @render_sidebar()
      @render_workspace_name()

    render_workspace_name: ()->
      text = @getLocalizeTextByKey("workspace.header")
      @$el.find("#header h1").html(
        text + " : " + @model.get('name')
      )

    endless_pagination: ()->
      @collection.no_more_results = false
      $(window).unbind('scroll')
      $(window).scroll ()=>
        return if @collection.no_more_results
        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10)
          @collection.requestNextPage()

    collection_pager: ()->
      @collection.fetch success: (collection, resp)=>
        if collection.length == 0
          @show_next_page_button()
          @disable_next_page_button()
          @collection.page -= 1
          @collection.no_more_results = true

    next_page: () ->
      @hide_next_page_button()
      @collection.requestNextPage()
      $ () =>
        if $(document).height() > $(window).height()
          @endless_pagination()
        else
          @show_next_page_button()
          @enable_next_page_button()
      false