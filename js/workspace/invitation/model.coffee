define ["underscore", "backbone"], (_, Backbone) ->
  class WorkspaceInvitation extends Backbone.Model
    urlRoot: ()->
      "/workspaces/" + @get("workspace_id") + "/invitations"