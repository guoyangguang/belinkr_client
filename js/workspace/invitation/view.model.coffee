define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../../composite_view",
  "../../../templates/directives",
  "text!../../../templates/workspace_invitation.html",
  "../../libs/pure/pure"
], ($, _, Backbone, CompositeView, Directives, Template) ->
  class WorkspaceInvitationView extends CompositeView
    template:     jQuery(Template)

    tagName:    "li"
    className:  "activity article span12 row-fluid"

    events:
      "click a.actor[data-context='inviter']"   : "nav_inviter"
      "click img.avatar"                : "nav_inviter"
      "click a.actor[data-context='invited']"   : "nav_invited"
      "click a.object"                  : "nav_workspace"

    initialize: ()->
      _.bindAll @
      @model.bind 'change', @render
      @template.directives(Directives.workspace_invitation)

    render: () ->
      @$el.html(@template.render(@model.toJSON()).html())
      @$el = @localize(@$el)
      @localize_invitation_state()
      @invoke_timeago()
      @

    localize_invitation_state: ()->
      @$el.find("p.state span").html(
        @getLocalizeTextByKey("workspace.invitation.state.#{@model.get("state")}")
      )

    nav_inviter: (e)->
      e.preventDefault()
      window.App.navigate(@model.get("links").inviter, trigger: true)

    nav_invited: (e)->
      e.preventDefault()
      window.App.navigate(@model.get("links").invited, trigger: true)

    nav_workspace: (e)->
      e.preventDefault()
      window.App.navigate(@model.get('links').workspace, trigger: true)
