define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../model.summary",
  "cs!./view.model",
  "cs!../../toolbar/view.users.toolbar",
  "cs!../../composite_view",
  "text!../../../templates/workspace_administrators.html",
  "cs!../view.sidebar.summary",
  "../../aura/mediator",
  "cs!../../modules"
], ($, _, Backbone, Workspace, UserView, ToolbarView, CompositeView, Template, SidebarView, mediator) ->
  class WorkspaceAdministratorsView extends CompositeView

    events:
      "click button.servernext"     : "next_page"
      "click a#flash_username"      : "nav_user"
      "click a#flash_collaborators" : "nav_workspace_collaborators"

    _objectName: "WorkspaceAdministratorsView"

    initialize: () ->
      _.bindAll @
      @model = new Workspace(id: @collection.workspace_id)
      @model.bind 'change', @render_workspace
      @model.fetch()
      @collection.bind "remove", @render
      @collection.bind "reset", @render
      @collection.bind "add",   @render_one
      @collection.pager = @collection_pager
      @$el.html(Template)
      @$el = @localize(@$el)

    render: () ->
      @render_all()

      if @collection.size() <= @collection.perPage
        @hide_next_page_button()
      else
        @show_next_page_button()

      @endless_pagination()
      @

    onShow: ()->

    render_all: () ->
      @_leaveChildren() if @collection.page is 0
      @collection.each(@render_one)

    render_one: (model) ->
      workspace_relationship = model.get("_visitor_workspace_relationship")
      unless workspace_relationship
        model.set("_visitor_workspace_relationship",
          @model.get("relationship")
        )
      model.set("_render_to",   "administrator")
      model.set("_workspace_id", @model.get("id"))
      view = new UserView(model: model)
      @appendChildInto(view, @$("ul.cards"))

    show_next_page_button: ()->
      @$el.find("button.servernext").removeClass("hide")

    hide_next_page_button: ()->
      @$el.find("button.servernext").addClass("hide")

    disable_next_page_button: ()->
      @$el.find("button.servernext").attr("disabled", "disabled")
#      @$el.find("button.servernext").html("No more results")

    enable_next_page_button: ()->
      @$el.find("button.servernext").removeAttr("disabled")
#      @$el.find("button.servernext").html("The next page")

    render_sidebar: () ->
      sidebar_view = new SidebarView(model: @model)
      @replaceWithStaticChildInto(sidebar_view, @$("#sidebar"))

    enforce_relationship: ()->
      if ['administrator', 'collaborator'].indexOf(@model.get("relationship")) < 0
        window.App.navigate('workspaces', trigger: true)

    render_workspace: ()->
      @enforce_relationship()
      @render_all()
      @render_sidebar()
      @render_workspace_name()

    render_workspace_name: ()->
      workspace_header = @getLocalizeTextByKey("workspace.header")
      administrators_header = @getLocalizeTextByKey("workspace.administrators.header")
      @$el.find("#header h1").html(
        workspace_header + " : " + @model.get('name') + " " + administrators_header
      )

    nav_user: (e)->
      e.preventDefault()
      window.App.navigate(
        $(e.target).attr('href'),
        trigger: true
      )

    nav_workspace_collaborators: (e)->
      e.preventDefault()
      window.App.navigate(
        "/workspaces/" + @model.get("id") + "/collaborators",
        trigger: true
      )

    endless_pagination: ()->
      @collection.no_more_results = false
      $(window).unbind('scroll')
      $(window).scroll ()=>
        return if @collection.no_more_results
        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10)
          @collection.requestNextPage()

    collection_pager: ()->
      @collection.fetch success: (collection, resp)=>
        if collection.length == 0
          @show_next_page_button()
          @disable_next_page_button()
          @collection.page -= 1
          @collection.no_more_results = true

    next_page: () ->
      @hide_next_page_button()
      @collection.requestNextPage()
      $ () =>
        if $(document).height() > $(window).height()
          @endless_pagination()
        else
          @show_next_page_button()
          @enable_next_page_button()
      false