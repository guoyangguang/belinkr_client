define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../../composite_view",
  "../../../templates/directives",
  "text!../../../templates/workspace_administrator_box.html",
  "cs!../../prefetch_data/prefetch_collections",
  "cs!../../alerter/helper",
  "../../aura/mediator",
  "cs!../../modules",
  "../../libs/pure/pure"
], ($, _, Backbone, CompositeView, Directives, Template, PrefetchCollections, Alerter, mediator) ->
  class WorkspaceAdministratorView extends CompositeView
    events:
      "click img.avatar"    : "nav_user"
      "click li.user_name"  : "nav_user"
      "click button"        : "click_demote"

    template:             jQuery(Template)
    tagName:              "li"
    className:            "card"
    initialize: () ->
      _.bindAll @
      @model.bind "change", @render
      @template.directives(Directives.workspace_administrator_box)

    render: () ->
      @$el.html(@template.render(@model.toJSON()).html())
      @$el = @localize(@$el)
      @localize_button_text()
      @

    localize_button_text: ()->
      if @model.get("relationship") is 'self'
        translation_key = "self"
      else
        switch @model.get("_render_to")
          when 'administrator'
            translation_key = 'demote'
          when 'collaborator'
            translation_key = "promote"
          else
            translation_key = ''

      if translation_key is ''
        text = ''
      else
        text = @getLocalizeTextByKey("button.workspace.all_users.#{translation_key}")

      @$el.find('button.btn').html(text)

    remove: () ->
      @$el.remove() # no fadeout

    leave: ()->
      @unbind()
      @remove()

    nav_user: (e) ->
      e.preventDefault()
      window.App.navigate("/users/" + @model.get("id"), trigger: true )
      false

    click_demote: ()->
      @get_sidebar_summary_view()
      @get_sidebar_collaborators_view()
      @get_sidebar_administrators_view()

      if @model.get("_render_to") == "administrator"
        if @model.get("_visitor_workspace_relationship") is "administrator"
          unless @model.get("relationship") is "self"
            @demote()
      false

    get_sidebar_summary_view: ()->
      return @_sidebar_summary_view if @_sidebar_summary_view
      @_sidebar_summary_view = @parent.static_children.find (child)->
        child._objectName is "WorkspacesSidebarSummaryView"
      @_sidebar_summary_view

    get_sidebar_collaborators_view: ()->
      return @_sidebar_collaborators_view if @_sidebar_collaborators_view
      summary_view = @get_sidebar_summary_view()
      if summary_view
        @_sidebar_collaborators_view = summary_view
        .static_children.find (child)->
          child._objectName is "WorkspaceSidebarCollaboratorsView"
      @_sidebar_collaborators_view

    get_sidebar_administrators_view: ()->
      return @_sidebar_administrators_view if @_sidebar_administrators_view
      summary_view = @get_sidebar_summary_view()
      if summary_view
        @_sidebar_administrators_view = summary_view
        .static_children.find (child)->
          child._objectName is "WorkspaceSidebarAdministratorsView"
      @_sidebar_administrators_view

    demote: ()->
      $.ajax
        url: "/workspaces/" + @model.get("_workspace_id") + "/administrators/" + @model.get("id")
        type: "DELETE"
        success: (response) =>
          @parent.collection.remove(@model)
          @_sidebar_administrators_view?.collection.remove(@model)
          if @_sidebar_collaborators_view?.collection.size() < 10
            @model.set('relationship', 'collaborator')
            @model.set("_render_to", "collaborator")
            @_sidebar_collaborators_view?.collection.add(@model)
          @demote_success_flash(@model)
          @leave()
        error: (xhr, text_status, error_message)=>
          @demote_error_flash(error_message)

    demote_success_flash: (model)->
      p = $("<p />")
      user_link = $("<a />",
        href: "/users/" + model.get("id")
        id: "flash_username"
        text: model.get("name")
      )
      p.append(user_link)
      message = p.html()
      message += @getLocalizeTextByKey(
        "workspace.administrators.alerter.demote.succeed.has_demoted"
      )
      p.empty()
      alerter = new Alerter()
      @parent.$el.find("div.flash").html(alerter.success(message).render())

    demote_error_flash: (message)->
      alerter = new Alerter()
      @parent.$el.find("div.flash").html(alerter.error([message]).render())
