define [
  "jquery",
  "underscore",
  "backbone",
  'cs!../sidebar/view.sidebar.search',
  "cs!./view.sidebar.invitation",
  "cs!./view.sidebar.administrators",
  "cs!./view.sidebar.collaborators",
  "cs!./view.sidebar.counter",
  "cs!../composite_view",
  "text!../../templates/workspace_sidebar_summary.html",
  "../aura/mediator",
  "cs!../modules"
], ($, _, Backbone, SearchSidebar, SidebarInvitationView, SidebarAdministratorsView, SidebarCollaboratorsView, SidebarCounterView, CompositeView,Template,mediator) ->
  class WorkspacesSidebarSummaryView extends CompositeView

    _objectName: "WorkspacesSidebarSummaryView"

    initialize: () ->
      _.bindAll @
      @model.bind('change', @render)

    render: ()->
      @$el.html(Template)
      @render_search_sidebar()
      @render_invitation()
      @render_counts()
      @render_administrators()
      @render_collaborators()
      @

    render_search_sidebar: ()->
      search_sidebar = new SearchSidebar()
      @replaceWithStaticChildInto(search_sidebar, @$("#sidebar div.header"))

    render_invitation: () ->
      @invitation_view = new SidebarInvitationView(workspace_id: @model.get("id"))
      @replaceWithStaticChildInto(@invitation_view, @$el.find("div.invitation"))

    render_administrators: () ->
      view = new SidebarAdministratorsView(model: @model)
      @replaceWithStaticChildInto(view, @$el.find("div.administrators"))

    render_collaborators: () ->
      view = new SidebarCollaboratorsView(model: @model)
      @replaceWithStaticChildInto(view, @$el.find("div.collaborators"))

    render_counts: () ->
      view = new SidebarCounterView(model: @model)
      @replaceWithStaticChildInto(view, @$el.find("div.statistics"))
