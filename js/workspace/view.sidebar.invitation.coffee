define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../composite_view",
  "cs!../user/model.summary",
  "cs!./invitation/model",
  "cs!../user/view.sidebar.avatar.model",
  "text!../../templates/workspace_sidebar_invitation.html",
  'cs!../alerter/helper',
  "../aura/mediator",
  "ajax_chosen",
  "cs!../modules"
], ($, _, Backbone, CompositeView, User, WorkspaceInvitation, UserAvatarView, Template, Alerter, mediator) ->
  class WorkspaceSidebarInvitationView extends CompositeView
    events:
      'click a.invite_user': 'send_invitation'
      'click #invite_modal': 'invite_user'

    initialize: (options) ->
      _.bindAll @
      @workspace_id = options.workspace_id
      @$el.html(jQuery(Template).html())
      @$el = @localize(@$el)
      @invite_modal = @$el.find("#invite_modal")
      @form         = @invite_modal.find("form")

    render: () ->
      @

    invite_user: (e) ->
      e.preventDefault()
      @render_chosen()

    render_chosen: ()->
      ajaxChosenOption =
        minTermLength: 1
        method:       'GET'
        url:          '/users/autocomplete'
        jsonTermKey:  'name'
        dataType:     'json'
      @$('select#invite_user').ajaxChosen ajaxChosenOption, (data)->
        terms = {}
        $.each data.page, (i,value)-> 
          terms[value[1]] = value[0]
        terms

    send_invitation: ()->
      invited_ids = @$el.find('select').val()
      _.each invited_ids, (id)=>
        invitation = new WorkspaceInvitation
          workspace_id: @workspace_id
          invited_id: id
          
        invitation.save({ wait: true }, 
          success:  @success
          error:    @errors
        )
    success: (model, response)->
      alerter = new Alerter()
      message = @getLocalizeTextByKey("workspace.invitation.alerter.inviting.succeed.invited")
      message += model.get('invited_name')
      message += @getLocalizeTextByKey("workspace.invitation.alerter.inviting.succeed.has_sent")
      @form.before(alerter.success(message).render())
      @reset_form()

    reset_form: ->
      @$el.find('li.search-choice').remove()
      @$el.find('select').val('')
      
    errors: (model, response) ->
      if response && response.responseText
        attrs_with_errors = JSON.parse(response.responseText).errors
        alerter = new Alerter()
        @form.before(alerter.error(attrs_with_errors).render())


