define [
  "jquery",
  "underscore",
  "backbone",
  'cs!../sidebar/view.sidebar.search',
  "cs!../sidebar/view.counter.sidebar.partial",
  "cs!../sidebar/view.sidebar.followers",
  "cs!../sidebar/view.sidebar.following",
  "cs!../scrapbook/view.sidebar.partial",
  "cs!./view.sidebar.partial",
  "cs!../composite_view",
  "text!../../templates/workspaces_sidebar.html",
  "../aura/mediator",
  "cs!../modules"
], ($, _, Backbone, SearchSidebar, CounterSidebar, FollowersSidebar, FollowingSidebar, ScrapbooksSidebar, WorkspacesSidebar, CompositeView,Template,mediator) ->
  class WorkspacesSidebarView extends CompositeView
    initialize: () ->
      _.bindAll @
      @workspace_id = @options?.workspace_id
      @$el.html(Template)
      @render_search_sidebar()
      @render_counter_sidebar()
      @render_following_sidebar()
      @render_followers_sidebar()
      @render_scrapbooks_sidebar()
      @render_workspaces_sidebar()
    # Markup hooks
    render: ()->
      @

    render_search_sidebar: ()->
      search_sidebar = new SearchSidebar()
      @replaceWithStaticChildInto(search_sidebar, @$("#sidebar div.header"))

    render_counter_sidebar: ->
      counter_sidebar = new CounterSidebar()
      @replaceWithStaticChildInto(counter_sidebar, @$("ul.counters"))

    render_following_sidebar: ->
      following_sidebar = new FollowingSidebar()
      @replaceWithStaticChildInto(following_sidebar, @$("div.following"))

    render_followers_sidebar: ->
      followers_sidebar = new FollowersSidebar()
      @replaceWithStaticChildInto(followers_sidebar, @$("div.followers"))

    render_scrapbooks_sidebar: ()=>
      scrapbooks_sidebar = new ScrapbooksSidebar
      @replaceWithStaticChildInto(scrapbooks_sidebar, @$(".scrapbooks_sidebar"))

    render_workspaces_sidebar: ()->
      workspaces_sidebar = new WorkspacesSidebar(workspace_id:@workspace_id)
      @replaceWithStaticChildInto(workspaces_sidebar, @$(".workspaces_sidebar"))
