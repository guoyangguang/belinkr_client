define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../composite_view",
  "text!../../templates/workspaces_toolbar.html",
  "../aura/mediator",
  "cs!../modules"
], ($, _, Backbone, CompositeView, Template,mediator) ->
  class WorkspacesToolbarView extends CompositeView
    initialize: () ->
      _.bindAll @
      @$el.html(Template)
      @$el = @localize(@$el)

    render: () ->
      @
    leave: () ->
      @unbind()
      @remove()
 

