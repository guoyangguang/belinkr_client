define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../composite_view",
  "text!../../templates/main_navigator_toolbar.html",
  "../aura/mediator",
  "cs!../notification/synchronizer",
  "cs!../modules"
], ($, _, Backbone, CompositeView, Template, mediator, NotificationSynchronizer) ->
  class NavigatorView extends CompositeView
    template: jQuery(Template)
    events:
      'click ul.nav li' : 'navigate_views'
      'click .settings' : 'settings'
      'click .logout'   : 'logout'
      "click .dropdown-menu a['data-context']" : "switch_locale"
      "click .dropdown-menu a['context']"      : "startAuthorization"

    initialize: (options) ->
      if NotificationSynchronizer.running is false
        NotificationSynchronizer.sync(
          token: $.cookie('belinkr.auth_token')
        )

      _.bindAll @
      @router = options['router']
      current_user = PrefetchModels.current_user()
      @template.find("div#menu img").attr("src", current_user.get("links")?.avatar)
      @template.find("div#menu span.user_name").html(current_user.get("name"))
      @$el.html(@template.html())
      @$el = @localize(@$el)

    navigate_views: (evt)->
      evt.preventDefault()
      #clear the search results because navigate from main navigator
      search_results = PrefetchModels.search_results()
      search_results.set('search_terms',null)
      classes = evt.currentTarget.className.split(' ')
      @set_active_class $(evt.currentTarget)
      if _.include(classes, 'activity')
        @router.navigate('statuses', trigger: true)
      else if _.include(classes, 'workspaces')
        @router.navigate('workspaces', trigger:true)
      else if _.include(classes, 'scrapbooks')
        @router.navigate('scrapbooks', trigger:true)
      else if _.include(classes, 'people')
        @router.navigate('users', trigger:true)
      else if _.include(classes, 'notifications')
        @$el.find("li.notifications span.badge").addClass('hide')
        @router.navigate('notifications', trigger:true)
        
    set_active_class: (target)->
      target.siblings().removeClass('active')
      target.addClass('active')

    render: () ->
      @

    logout: (e) ->
      e.preventDefault()
      $.ajax(
        type:     'delete'
        url:      '/sessions/0'
        async:    false
        dataType: 'json'
        success:  (data, text_status, jq_xhr) ->
          window.location.href = "http://www.belinkr.com"
        error:    () ->
          window.location.href = "http://www.belinkr.com"
      )

    settings: () -> @router.navigate('settings', trigger: true)

    switch_locale: (e)->
      e.preventDefault()
      locale = $(e.target).attr('data-context')
      window.App.setLocale(locale)

    startAuthorization: (e)->
      e.preventDefault()
      target = $(e.target)
      if target.prop("tagName") != 'A'
        target = target.parent()
      window.location.href = target.attr("href")

    leave: () ->
      @unbind()
      @reove()
