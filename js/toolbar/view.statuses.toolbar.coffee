define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../composite_view",
  "text!../../templates/statuses_toolbar.html",
  "../aura/mediator",
  "cs!../modules"
], ($, _, Backbone, CompositeView, Template,mediator) ->
  class StatusesToolbarView extends CompositeView
    template: jQuery(Template)

    events:
      "click .btn-group a" : 'nav_statuses'

    initialize: () ->
      @_get_current_fragment()
      _.bindAll @
      @$el.html(@template.html())
      @$el = @localize(@$el)
      @toolbar_active()

    render: () ->
      @

    leave: () ->
      @unbind()
      @remove()

    _get_current_fragment: ()->
      fragment = Backbone.history.fragment
      @_current_fragment = _(fragment.split('/')).last()

    toolbar_active: ()->
      switch @_current_fragment
        when "files"
          link = @$el.find("a[data-context='files']")
#        when "conversations"
#          link = @$el.find("a[data-context='conversations']")
        when "workspaces"
          link = @$el.find("a[data-context='workspaces']")
#        when "action_required"
#          link = @$el.find("a[data-context='action_required']")
        else
          link = @$el.find("a[data-context='statuses']")

      link.siblings().removeClass('active')
      link.addClass('active')

    nav_statuses: (e)->
      e.preventDefault()
      el = $(e.target)
      el = el.parent() if el.is('span')
      unless el.attr('href') is '#'
        window.App.navigate(el.attr("href"), trigger: true)
      false

