define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../prefetch_data/prefetch_models",
  "../../templates/directives",
  "text!../../templates/search_results_toolbar.html",
  "../aura/mediator",
  "cs!../modules"
], ($, _, Backbone, PrefetchModels, Directives, Template,mediator) ->
  class SearchResultsToolbarView extends Backbone.View
    template: jQuery(Template)
    events:
      'click .btn-group .btn': 'navigate_views'
    initialize: () ->
      _.bindAll @
      @router = window.App
      @model = PrefetchModels.search_results()
      @template.directives(Directives.search_results)
      @model.on 'change', @render
    render: () ->
      @$el.html(@template.render(@model.toJSON()).html())
      @set_active_class()
      @

    leave: () ->
      @unbind()
      @remove()
 
    navigate_views: (evt)->
      classes = evt.currentTarget.className.split(' ')
      if _.include(classes, 'activity')
        @router.navigate('statuses/search/'+@model.get('search_terms'), trigger: true)
      else if _.include(classes, 'workspaces')
        @router.navigate('workspaces/search/'+@model.get('search_terms'), trigger:true)
      else if _.include(classes, 'scrapbooks')
        @router.navigate('scrapbooks/search/'+@model.get('search_terms'), trigger:true)
      else if _.include(classes, 'users')
        @router.navigate('users/search/'+@model.get('search_terms'), trigger:true)
 
    set_active_class: ->
      fragment = Backbone.history.fragment
      fragment_list = fragment.split('/')
      @$el.find('.btn-group a').removeClass('active')
      if _.include(fragment_list, 'statuses')
        @$el.find('.btn-group a.activity').addClass('active')
      else if _.include(fragment_list, 'users')
        @$el.find('.btn-group a.users').addClass('active')
      else if _.include(fragment_list, 'workspaces')
        @$el.find('.btn-group a.workspaces').addClass('active')
      else if _.include(fragment_list, 'scrapbooks')
        @$el.find('.btn-group a.scrapbooks').addClass('active')



