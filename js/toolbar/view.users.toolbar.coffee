define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../composite_view",
  "text!../../templates/users_toolbar.html",
  "../aura/mediator",
  "cs!../modules"
], ($, _, Backbone, CompositeView, Template,mediator) ->
  class UsersToolbarView extends CompositeView
    initialize: () ->
      _.bindAll @
      #@$el.html(Template)
      @$el = @localize(@$el)

    render: () ->
      @
    leave: () ->
      @unbind()
      @remove()
 

