define ["jquery", "underscore", "backbone", "cs!router"],
($, _, Backbone, Router) ->
  $(document).ready ->
    window.App = new Router
    Backbone.history.start({ pushState: true, root: "/w/" })
