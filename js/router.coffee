define [
  'order!jquery',
  'order!underscore',
  'order!backbone',
  'cs!login/view',
  'cs!signup/view',
  'cs!reset/model',
  'cs!reset/view',
  'cs!reset/view.new_password',
  'cs!prefetch_data/prefetch_models',
  'cs!prefetch_data/prefetch_collections',
  'cs!status/model',
  'cs!status/collection',
  'cs!status/collection.search',
  'cs!status/collection.file',
  'cs!status/collection.workspace',
  'cs!status/view.collection',
  'cs!status/view.model.show',
  'cs!scrapbook/collection',
  'cs!scrapbook/collection.search',
  'cs!scrapbook/view.collection',
  'cs!scrapbook/view.collection.search',
  'cs!appointment/collection',
  'cs!appointment/view.collection'
  'cs!workspace/collection',
  'cs!workspace/collection.search',
  'cs!workspace/view.collection',
  'cs!workspace/view.model',
  'cs!workspace/administrator/collection',
  'cs!workspace/administrator/view.collection',
  'cs!workspace/collaborator/collection',
  'cs!workspace/collaborator/view.collection',
  'cs!workspace/autoinvitation/collection',
  'cs!workspace/autoinvitation/view.collection',
  'cs!workspace/invitation/collection',
  'cs!workspace/invitation/view.collection',
  'cs!workspace/status/model',
  'cs!workspace/status/view.mode.show',
  'cs!workspace/status/collection',
  'cs!workspace/status/view.collection',
  'cs!workspace/status/collection.file',
  'cs!workspace/status/view.collection.file',
  'cs!user/paginator.collection',
  'cs!user/paginator.collection.search',
  'cs!user/paginator.collection.followers',
  'cs!user/paginator.collection.following',
  'cs!user/view.collection',
  'cs!credential/model',
  'cs!invitation/model',
  'cs!toolbar/view.main_navigator_toolbar',
  'text!../templates/external.html',
  'text!../templates/internal.html',
  'cs!./xmpp/roster/view.roster',
  'cs!./user/view.profile',
  'cs!./user/view.settings',
  'cs!./user/collection.user.followers',
  'cs!./user/collection.user.following',
  'cs!./user/collection.user.workspaces',
  'cs!./notification/collection',
  'cs!./notification/view.collection',
  'cs!./aggregation/query/collection',
  'cs!./aggregation/query/view.collection',
  "cs!./libs/timeago/jquery.timeago.locales",
  'scrollto',
  'libs/bootstrap/bootstrap.min',
  'order!jqueryui',
  'order!jquery_cookie',
  'order!click_modal',
  'order!wysihtml5'
], ($, _, Backbone, LoginView, SignupView, ResetModel, ResetView,\
NewPasswordView, PrefetchModels, PrefetchCollections, StatusModel, StatusCollection,\
SearchStatusCollection, FileStatusCollection, TimelineWorkspaceStatusCollection, StatusesView, StatusView,\
ScrapbookCollection, SearchScrapbookCollection, ScrapbooksView,\
ScrapbooksSearchView, AppointmentCollection, AppointmentsView,\
WorkspaceCollection, SearchWorkspaceCollection, WorkspacesView,\
WorkspaceView, WorkspaceAministratorCollection, WorkspaceAdministratorsView,\
WorkspaceCollaboratorCollection, WorkspaceCollaboratorsView,\
WorkspaceAutoInvitationCollection, WorkspaceAutoInvitationsView,\
WorkspaceInvitationCollection, WorkspaceInvitationsView,\
WorkspaceStatusModel, WorkspaceStatusShowView,\
WorkspaceStatusCollection, WorkspaceStatusesView,\
WorkspaceFileStatusCollection, WorkspaceFileStatusesView,\
UserPaginatorCollection, SearchUserPaginatorCollection, Followers, Following,\
UsersView, CredentialModel, InvitationModel, NavigatorView, ExternalTemplate,\
InternalTemplate, RosterView,UserProfileView, UserSettingsView,\
UserFollowersCollection, UserFollowingCollection, UserWorkspaceCollection,\
NotificationCollection, NotificationsView, AggregationQueryCollection, AggregationQueryStatusesView, TimeAgoLocales) ->
  class SwappingRouter extends Backbone.Router
    swap: (newView)->
      @currentView?.leave?()
      @currentView = newView

      @currentView.render()
      $(@el).empty().append(@currentView.el)
      jQuery('body').ScrollTo(
        duration: 1000
      )
      @currentView.onShow?()

  class Router extends SwappingRouter
    routes:
      'login'                           : 'login'
      'signup/:id'                      : 'signup'
      'reset'                           : 'reset'
      'resets/:id'                      : 'new_password'
      'statuses'                        : 'statuses'
      "statuses/:id"                    : 'status'
      'statuses/:status_id/replies/:id' : 'status_reply'
      'statuses/search/:query'          : 'search_statuses'
      'timelines/files'                 : 'timelines_files'
      'timelines/workspaces'            : 'timelines_workspaces'
      'scrapbooks'                      : 'scrapbooks'
      'scrapbooks/search/:query'        : 'search_scrapbooks'
      'scrapbooks/:id'                  : 'scrapbook'
      'appointments'                    : 'appointments'
      'workspaces'                      : 'workspaces'
      'workspaces/search/:query'        : 'search_workspaces'
      'workspaces/own'                  : 'workspaces'
      'workspaces/invited'              : 'workspaces'
      'workspaces/autoinvited'          : 'workspaces'
      'workspaces/:id'                  : 'workspace'
      'workspaces/:workspace_id/statuses/:id' : 'workspace_status'
      'workspaces/:workspace_id/statuses/:status_id/replies/:id' : 'workspace_status_reply'
      'workspaces/:id/administrators'   : 'workspace_administrators'
      'workspaces/:id/collaborators'    : 'workspace_collaborators'
      'workspaces/:id/invitations'      : 'workspace_invitations'
      'workspaces/:id/autoinvitations'  : 'workspace_autoinvitations'
      'workspaces/:id/timelines/files'  : 'workspace_timelines_files'
      'users'                           : 'users'
      'users/:id/followers'             : 'user_followers'
      'users/:id/following'             : 'user_following'
      'users/:id/workspaces'            : 'user_workspaces'
      'users/search/:query'             : 'search_users'
      'following'                       : 'following'
      'followers'                       : 'followers'
      'users/:id'                       : 'user'
      'settings'                        : 'settings'
      'notifications'                   : 'notifications'
      'notification/:id'                : 'notification',
      'aggregation/query/:query'        : 'aggregationQuery'
      '*actions'                        : 'defaultRoute'

    initialize: () ->
      unless $.cookie('belinkr.locale')
        $.cookie('belinkr.locale', 'zh', { expires: null, path: '/' })

      @set_timeago_locale()
      window.RunningMode = "production"
      if @external()
        @set_external_template()
      else
        @redirect_unless_logged_in()
        @set_internal_template()

        @prefetch()

        @render_navigator_view()

#        #Chat.start()
#        if window.RunningMode is "production"
#          xmpp = new RosterView(
#            xmpp_url: "http://belinkr.dev:5280/http-bind"
#          )
#          $('div#roster').html(xmpp.render().el)

        @el = 'div#left-content'

        @statuses() if Backbone.history.fragment is "statuses"

    defaultRoute: (actions) ->
      @login()

    login: (action) ->
      #@set_external_template()
      view = new LoginView(model: new CredentialModel).render()
      @swap view
      view

    signup: (invitation_id) ->
      view = new SignupView(invitation: new InvitationModel(id: invitation_id))
      @swap view
      view

    reset: (action) ->
      view = new ResetView().render()
      @swap view
      view

    new_password: (reset_id) ->
      view = new NewPasswordView(model: new ResetModel(id: reset_id))
      @swap view
      view

    search_statuses: (query) ->
      collection = new SearchStatusCollection [], query: query
      view = new StatusesView(collection: collection)
      view.collection.fetch()
      @swap view
      view.invoke_chosen()
      view

    statuses: (action) ->
      view = new StatusesView(collection: new StatusCollection)
      view.collection.fetch()
      @swap view
      view.invoke_chosen()
      view

    status: (status_id) ->
      if status_id.indexOf('?') > 0
        paths = status_id.split('?')
        status_id = paths[0]
        query     = paths[1]
      view = new StatusView(model: new StatusModel(
        id: status_id, _query: query)
      )
      view.model.fetch()
      @swap view
      view

    workspace_status: (workspace_id, status_id) ->
      if status_id.indexOf('?') > 0
        paths = status_id.split('?')
        status_id = paths[0]
        query     = paths[1]
      view = new WorkspaceStatusShowView(model: new WorkspaceStatusModel(
        id: status_id, workspace_id: workspace_id, _query: query)
      )
      view.model.fetch()
      @swap view
      view

    status_reply: (status_id, reply_id) ->
      anchor = "?s" + status_id + "r" + reply_id
      window.App.navigate("statuses/" + status_id + anchor, trigger: true)

    workspace_status_reply: (workspace_id, status_id, reply_id) ->
      anchor = "?s" + status_id + "r" + reply_id
      window.App.navigate(
        "/workspaces/" + workspace_id + "/statuses/" + status_id + anchor,
        trigger: true
      )

    timelines_files: () ->
      view = new StatusesView(collection: new FileStatusCollection)
      view.collection.fetch()
      @swap view
      view.invoke_chosen()
      view

    timelines_workspaces: () ->
      view = new StatusesView(collection: new TimelineWorkspaceStatusCollection)
      view.collection.fetch()
      @swap view
      view.invoke_chosen()
      view

    scrapbooks: (action) ->
      view = new ScrapbooksView(collection: new ScrapbookCollection)
      view.collection.fetch()
      @swap view
      view

    search_scrapbooks: (query) ->
      collection = new SearchScrapbookCollection([],query:query)
      view = new ScrapbooksSearchView(collection: collection)
      view.collection.fetch()
      @swap view
      view

    scrapbook: (scrapbook_id)->
      view = new ScrapbooksView(
        collection:   new ScrapbookCollection,
        scrapbook_id: scrapbook_id
      )
      view.collection.fetch()
      @swap view
      view

    appointments: (action) ->
      view = new AppointmentsView(collection: new AppointmentCollection)
      view.collection.fetch()
      @swap view
      view

    workspaces: (action) ->
      fragment = Backbone.history.fragment
      if fragment.indexOf('/') > 0
        _type = _(fragment.split('/')).last()
        collection = new WorkspaceCollection([], kind: _type)
        collection.kind = _type
      else
        collection = new WorkspaceCollection([], kind: 'all')
        collection.kind = 'all'

      view = new WorkspacesView(collection: collection)
      view.collection.fetch()
      @swap view
      view

    search_workspaces: (query) ->
      collection = new SearchWorkspaceCollection([], kind: 'all',query:query)
      view = new WorkspacesView(collection: collection)
      view.collection.fetch()
      @swap view
      view


    workspace: (workspace_id) ->
      collection = new WorkspaceStatusCollection([],
        workspace_id: workspace_id
      )
      view = new WorkspaceStatusesView(collection: collection)
      view.collection.fetch()
      @swap view
      view

    workspace_administrators: (workspace_id) ->
      collection = new WorkspaceAministratorCollection([],
        workspace_id: workspace_id
      )
      view = new WorkspaceAdministratorsView(collection: collection)
      view.collection.fetch()
      @swap view
      view

    workspace_collaborators: (workspace_id) ->
      collection = new WorkspaceCollaboratorCollection([],
        workspace_id: workspace_id
      )
      view = new WorkspaceCollaboratorsView(collection: collection)
      view.collection.fetch()
      @swap view
      view

    workspace_invitations: (workspace_id) ->
      collection = new WorkspaceInvitationCollection([],
        workspace_id: workspace_id
      )
      view = new WorkspaceInvitationsView(collection: collection)
      view.collection.fetch()
      @swap view
      view

    workspace_autoinvitations: (workspace_id) ->
      collection = new WorkspaceAutoInvitationCollection([],
        workspace_id: workspace_id
      )
      view = new WorkspaceAutoInvitationsView(collection: collection)
      view.collection.fetch()
      @swap view
      view

    workspace_timelines_files: (workspace_id) ->
      collection = new WorkspaceFileStatusCollection([],
        workspace_id: workspace_id
      )
      view = new WorkspaceFileStatusesView(collection: collection)
      view.collection.fetch()
      @swap view
      view

    users: () ->
      view = new UsersView(collection: new UserPaginatorCollection())
      view.collection.fetch()
      @swap view
      view

    user_followers: (id)->
      collection = new UserFollowersCollection([], user_id: id)
      view = new UsersView(collection: collection)
      view.collection.fetch()
      @swap view
      view

    user_following: (id)->
      collection = new UserFollowingCollection([], user_id: id)
      view = new UsersView(collection: collection)
      view.collection.fetch()
      @swap view
      view

    user_workspaces: (id)->
      collection = new UserWorkspaceCollection([], user_id: id)
      view = new WorkspacesView(collection: collection)
      view.collection.fetch()
      @swap view
      view

    search_users: (query) ->
      collection = new SearchUserPaginatorCollection([],query:query)
      collection.fetch()

      view = new UsersView(collection: collection)
      @swap view
      view

    followers: () ->
      view = new UsersView(collection: new Followers())
      view.collection.fetch()
      @swap view
      view

    following: () ->
      view = new UsersView(collection: new Following())
      view.collection.fetch()
      @swap view
      view

    user: (user_id)->
      view = new UserProfileView(user_id: user_id)
      @swap view
      view

    settings: ()->
      view = new UserSettingsView()
      @swap view
      view

    notifications: ()->
      view = new NotificationsView(collection: new NotificationCollection())
      if window.Notifications && window.Notifications.length > 0
        view.collection.add(window.Notifications.reverse())
        window.Notifications = []
        view.collection.fetch(add: true)
      else
        view.collection.fetch()

      @swap view
      view

    notification: (notification_id) ->

    aggregationQuery: (query) ->
      collection = new AggregationQueryCollection [], query: query
      view = new AggregationQueryStatusesView(collection: collection)
      view.collection.fetch()
      @swap view
      view

    external: () ->
      pathname = _(window.location.href.split(window.location.host)).last()
      return true if pathname == '/'
      return true if pathname == '/w'
      return true if pathname == '/w/'
      return true if pathname == '/w/#'

      fragment = _(pathname.split(/\/w\/?/)).last()
      return true if fragment.match /^#?(login|signup|reset)/
      return false

    redirect_unless_logged_in: () ->
      window.location.pathname = "/" unless @logged_in()
      
    logged_in: () ->
      return false if @logged_out()
      return true if  $.cookie('belinkr.auth_token') ||
                      $.cookie('belinkr.remember_me') ||
                      window.testing_mode
      return false

    logged_out: () ->
      $.cookie('belinkr.auth_token') == '0'

    set_external_template: () ->
      $('body').removeClass('internal').addClass('external')
      $('body').html($(ExternalTemplate).html())

    set_internal_template: () ->
      $('body').html(jQuery(InternalTemplate).html())
      $('body').removeClass('external').addClass('internal')

    prefetch: () ->
      PrefetchModels.counter()
      PrefetchModels.current_user()
      window.PrefetchModels = PrefetchModels
      PrefetchCollections.following()
      window.PrefetchCollections = PrefetchCollections

    navigate: (fragment, options) ->
      routeStripper = /^[#\/]/
      frag = (fragment || '').replace(routeStripper, '')
      if Backbone.history.fragment is frag
        Backbone.history.loadUrl(Backbone.history.fragment)
      else
        super(fragment, options)

    render_navigator_view: ()->
      $('div.navbar-fixed-top').html('')
      navigator_view = new NavigatorView(router:@)
      $('div.navbar-fixed-top').html(navigator_view.render().el)

    set_timeago_locale: ()->
      jQuery.timeago.settings.strings = TimeAgoLocales[$.cookie('belinkr.locale') || 'zh']

    setLocale: (locale) ->
      $.cookie('belinkr.locale', locale, { expires: null, path: '/' })
      @set_timeago_locale()
      @render_navigator_view()
      Backbone.history.loadUrl(Backbone.history.fragment)
