define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../prefetch_data/prefetch_collections",
  "cs!../composite_view",
  "cs!../user/view.sidebar.avatar.model",
  "../../templates/directives",
  "text!../../templates/followers_sidebar_partial.html",
  "../aura/mediator",
  "cs!../modules"
], ($, _, Backbone, PrefetchCollections, CompositeView, UserAvtarView, Directives, Template, mediator) ->
  class FollowersSidebar extends CompositeView
    events:
      "click h3 a" : "nav_followers"

    initialize:() ->
      _.bindAll @
      @followers = PrefetchCollections.followers()
      @followers.bind 'all', @render
      @$el.html(Template)
      @$el = @localize(@$el)
      
    render: () ->
      #test the prefetch model before using to render,
      #otherwise, the spec will fail
      unless _.isEmpty(@followers)
        @_leaveChildren()
        @render_all_followers()
      @
    render_all_followers: ->
      @followers.each(@render_follower)

    render_follower: (model)->
      avatar_view = new UserAvtarView(model: model)
      @appendChildInto(avatar_view, @$("ul.followers"))

    nav_followers: (e)->
      e.preventDefault()
      window.App.navigate("followers", trigger: true)