define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../prefetch_data/prefetch_collections",
  "cs!../composite_view",
  "cs!../user/view.sidebar.avatar.model",
  "../../templates/directives",
  "text!../../templates/following_sidebar_partial.html",
  "../aura/mediator",
  "cs!../modules"
], ($, _, Backbone, PrefetchCollections, CompositeView, UserAvtarView, Directives, Template, mediator) ->
  class FollowingSidebar extends CompositeView
    events:
      "click h3 a" : "nav_following"

    initialize:() ->
      _.bindAll @
      @following = PrefetchCollections.following()
      @following.bind 'all', @render
      @$el.html(Template)
      @$el = @localize(@$el)
      
    render: () ->
      #test the prefetch model before using to render,
      #otherwise, the spec will fail
      unless _.isEmpty(@following)
        @_leaveChildren()
        @render_all_following()
      @
    render_all_following: ->
      @following.each(@render_following)

    render_following: (model)->
      avatar_view = new UserAvtarView(model: model)
      @appendChildInto(avatar_view, @$("ul.following"))

    nav_following: (e)->
      e.preventDefault()
      window.App.navigate("following", trigger: true)
