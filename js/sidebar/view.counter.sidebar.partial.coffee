define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../counter/model",
  "cs!../prefetch_data/prefetch_models",
  "cs!../prefetch_data/prefetch_collections",
  "cs!../composite_view",
  "cs!../user/view.sidebar.avatar.model",
  "../../templates/directives",
  "text!../../templates/counter_sidebar_partial.html",
  "../aura/mediator",
  "cs!../modules"
], ($, _, Backbone,Counter, PrefetchModels, PrefetchCollections, CompositeView, UserAvtarView, Directives, Template, mediator) ->
  class CounterSidebar extends CompositeView
    template:             jQuery(Template)
    events:
      'click p.counter.followers': 'navigate_to_followers'
      'click p.counter.following': 'navigate_to_following'

    initialize:() ->
      _.bindAll @
      @counter_model = PrefetchModels.counter()
      @counter_model.bind 'change', @render
      
      @template.directives(Directives.counter)

    navigate_to_followers: ->
      window.App.navigate 'followers', trigger: true
    navigate_to_following: ->
      window.App.navigate 'following', trigger: true
 
    render: () ->
      #test the prefetch model before using to render,
      #otherwise, the spec will fail
      unless _.isEmpty(@counter_model.attributes)
        @$el.html(@template.render(@counter_model.toJSON()))
        @$el = @localize(@$el)
      @

