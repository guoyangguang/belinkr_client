define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../counter/model",
  "cs!../prefetch_data/prefetch_models",
  "cs!../prefetch_data/prefetch_collections",
  "cs!../composite_view",
  "../../templates/directives",
  "text!../../templates/search_sidebar.html",
  "../aura/mediator",
  "cs!../modules"
], ($, _, Backbone,Counter, PrefetchModels, PrefetchCollections, CompositeView, Directives, Template, mediator) ->
  class SearchSidebar extends CompositeView
    template  : jQuery(Template)
    className : 'header row-fluid'
    events:
      'keypress input.search-query': 'search'

    initialize:() ->
      _.bindAll @
      @$el.html(@template.html())
      @$el = @localize(@$el)

    render: () ->
      @

    search: (event)->
      if event.keyCode == 13
        query = @$('input.search-query').val()
        query_text_types = ['statuses']
        PrefetchModels.search_results().set("search_terms", query)
        _.each query_text_types, (query_type)->
          $.get "/#{query_type}/autocomplete?text=#{query}",
            (data)->
              PrefetchModels.search_results().set("#{query_type}", data.hits)
            ,'json'
        query_name_types = ['users','workspaces','scrapbooks']
        _.each query_name_types, (query_type)->
          $.get "/#{query_type}/autocomplete?name=#{query}",
            (data)->
              PrefetchModels.search_results().set("#{query_type}", data.hits)
            ,'json'
 
        window.App.navigate('statuses/search/'+query, trigger:true)
        event.preventDefault()


