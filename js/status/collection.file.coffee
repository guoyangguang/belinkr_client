define ["underscore", "backbone", "cs!./model", "paginator"], (_, Backbone, Status) ->
  class FileStatusCollection extends Backbone.Paginator.requestPager
    model:      Status
    url:        "/timelines/files"
    comparator: (model) ->
      model.get("updated_at")

    pageAttribute: 'page'
    perPageAttribute: 'perPage'

    page: 0
    firtPage: 0
    perPage: 20
