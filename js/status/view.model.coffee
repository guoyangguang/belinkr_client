define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../composite_view",
  "cs!./reply/view.collection",
  "cs!../file/view.model",
  "cs!../prefetch_data/prefetch_models",
  "../../templates/directives",
  "text!../../templates/status.html",
  "text!../../templates/reply_form.html",
  "text!../../templates/status_delete_click_modal.html",
  "cs!./model",
  "cs!../uploader",
  "../aura/mediator",
  "cs!../modules",
  "../libs/pure/pure"
], ($, _, Backbone, CompositeView, RepliesView, FileView, PrefetchModels, Directives, Template, ReplyFormTemplate, StatusDeleteClickModal, Status, Uploader, mediator) ->
  class StatusView extends CompositeView
    template:             jQuery(Template)
    reply_form_template:  jQuery(ReplyFormTemplate)
    tagName:              "li"
    className:            "status article span12 row-fluid"

    events:
      "click .links .new_reply" : "new_reply"
      "click .links .retweet"   : "retweet"
      "click button.reply"      : "submit_new_reply"
      "click img.avatar"        : "nav_user"
      "click span.user_name"    : "nav_user"
      "click .icon-arrow-up"    : "cancel_reply"
      "click .offset8 button.btn" : "cancel_reply"

    initialize: () ->
      _.bindAll @
      @model.bind("change", @render)
      @model.bind("destroy", @leave)
      @template.directives(Directives.status)

    render: () ->
      @$el.html(@template.render(@model.toJSON()).html())
      @$el = @localize(@$el)
      @click_modal()
      @display_enforcer()
      @_leaveChildren()
      @render_files()
      @render_replies()
      @invoke_timeago()
      @

    click_modal: ()->
      @$el.find(".links .delete").click_modal
        parent: @
        modal:
          target: "for_status"
          template: @localize($(StatusDeleteClickModal))
          events:
            "button.btn-warning" : "delete"

    render_files: () ->
      files = @model.get('files')
      @file_list = @$("ul.files")
      unless _.isEmpty(files)
        _.each files, (data)=>
          file_view = new FileView(data: data)
          @appendChildInto(file_view, @file_list)

    render_replies: () ->
      @replies_view = new RepliesView(
                          collection: @model.get_replies(),
                          status_id:  @model.id
                        )
      @appendChildInto(@replies_view, @$el.find('ul.replies'))

    remove: () ->
      @$el.fadeOut('slow', => @$el.remove())

    delete: () ->
      mediator.publish "deleteStatus", @
      counter = PrefetchModels.counter()
      counter.set('statuses', counter.get('statuses')-1)
      false

    submit_new_reply: (e)->
      e.preventDefault()
      e.stopPropagation()
      files = @$("input[name='files']").val()
      files = '[]' if (files is null) || (files is '')
      @replies_view.collection.create(
        {
          text:       @text_field.val()
          status_id:  @model.id
          files:      $.parseJSON(files)
        },
        {
          wait: true
          success:    @render_saved_reply
          error:      @errors
        }
      )

    render_reply_upload_form: () ->
      view_options= { data: { id: 'status_' + @model.get("id") + "_reply" }}

      uploader_options=
        parent_form: @$el.find("form.reply").first()

      built = Uploader.build(view_options, uploader_options)
      @appendChildInto(built.view, @$el.find("form.reply ul.unstyled"))
      $ ()->
        built.uploader.init()

    new_reply: () ->
      @$el.find("a.new_reply").hide()
      if @$el.find("div.reply_form").length > 0
        @$el.find("div.reply_form").show()
        @$el.find('ul.replies').ScrollTo(
          duration: 1000
        )
      else
        reply_form = jQuery(ReplyFormTemplate)
        reply_form = @localize(reply_form)

#        @render_html_editor(
#          id: "editor-toolbar-status-reply" + @model.get('id')
#          editorContainer: reply_form.find("#editor-toolbar-status-reply")
#        )

#        editor_toolbar = reply_form
#          .find("div[id^='editor-toolbar-status-reply']")
        textarea = reply_form.find("textarea")
#        textarea.attr("id", "editor-status-reply" + @model.get("id"))

        @$el.find('ul.replies').after(reply_form)

#        @reply_editor = new wysihtml5.Editor textarea[0],
#          toolbar:      editor_toolbar[0],
#          parserRules:  wysihtml5ParserRules

        @text_field     = @$("textarea")
        @text_filler    = @text_field.attr("placeholder")
        @reset_form()
        @render_reply_upload_form()
        @$el.find('ul.replies').ScrollTo(
          duration: 1000
        )
      false

    render_saved_reply: () =>
      @model.fetch()
      @reset_form()

    retweet: () ->
      $.ajax
        url:        '/statuses/forwarded/' + @model.id
        type:       'PUT'
        dataType:   'json'
        success:    (response) =>
                      model = new Status(response)
                      view = new StatusView(model: model)
                      @appendChildInto(view, $("ul.statuses"))

    reset_form: () ->
      @text_field.val(@text_filler)
        .focusin (event) =>
          @$(event.target).val("") if $(event.target).val() == @text_filler
        .focusout (event) =>
          @$(event.target).val(@text_filler) if $(event.target).val() == ""

      @reply_editor?.clear()

    cancel_reply: ()->
      @$el.find("a.new_reply").show()
      if (@$el.find("form.reply textarea").val() != '') or (@$el.find("form.reply files").val() != '')
        @$el.find("div.reply_form").hide()
      else
        @$el.find("div.reply_form").remove()

    viewer_is_author: () ->
      current_user = PrefetchModels.current_user()
      current_user.get("id") == @model.get("user_id")

    display_enforcer: () ->
      if PrefetchModels.current_user_is_admin()
        if @viewer_is_author()
          @$el.find("a.retweet").parent().remove()
        else
          if @model.get('workspace_id') or @model.get('scrapbook_id')
            @$el.find("a.delete").parent().remove()
      else
        if @viewer_is_author()
          @$el.find("a.retweet").parent().remove()
        else
          @$el.find("a.delete").parent().remove()

    nav_user:(e) ->
      e.preventDefault()
      window.App.navigate("/users/" + @model.get('user_id'), trigger: true)
      false
