define [
  'jquery',
  'underscore',
  'backbone',
  'cs!../invitation/model',
  'cs!../sidebar/view.sidebar.search',
  'cs!../sidebar/view.counter.sidebar.partial',
  'cs!../sidebar/view.sidebar.followers',
  'cs!../sidebar/view.sidebar.following',
  'cs!./model',
  'cs!./view.model',
  'cs!../scrapbook/view.sidebar.partial',
  'cs!../workspace/view.sidebar.partial',
  'cs!../composite_view',
  'text!../../templates/statuses_sidebar.html',
  'cs!../alerter/helper',
  '../aura/mediator',
  'cs!../modules'
], ($, _, Backbone, Invitation, SearchSidebar, CounterSidebar, FollowersSidebar, FollowingSidebar, Model, StatusView,ScrapbooksSidebar, WorkspacesSidebar, CompositeView,Template, Alerter, mediator) ->
  class StatusesSidebarView extends CompositeView
    events: 
      'click #invite_modal .submit' : 'create_invitation'

    initialize: () ->
      _.bindAll @
      @$el.html(Template)
      @$el = @localize(@$el)
      @render_search_sidebar()
      @render_counter_sidebar()
      @render_following_sidebar()
      @render_followers_sidebar()
      @render_scrapbooks_sidebar()
      @render_workspaces_sidebar()

      # Markup hooks
      @invite_modal = @$el.find("#invite_modal")
      @form         = @invite_modal.find("form")

    render: ()->
      @

    render_search_sidebar: ()->
      search_sidebar = new SearchSidebar()
      @replaceWithStaticChildInto(search_sidebar, @$("#sidebar div.header"))


    render_counter_sidebar: ->
      counter_sidebar = new CounterSidebar()
      @replaceWithStaticChildInto(counter_sidebar, @$("ul.counters"))

    render_following_sidebar: ->
      following_sidebar = new FollowingSidebar()
      @replaceWithStaticChildInto(following_sidebar, @$("div.following"))

    render_followers_sidebar: ->
      followers_sidebar = new FollowersSidebar()
      @replaceWithStaticChildInto(followers_sidebar, @$("div.followers"))


    render_scrapbooks_sidebar: ()=>
      scrapbooks_sidebar = new ScrapbooksSidebar
      @replaceWithStaticChildInto(scrapbooks_sidebar, @$(".scrapbooks_sidebar"))

    render_workspaces_sidebar: ()->
      workspaces_sidebar = new WorkspacesSidebar
      @replaceWithStaticChildInto(workspaces_sidebar, @$(".workspaces_sidebar"))

    create_invitation: () =>
      @invite_modal.find('ul.alert').remove()
      invitation = new Invitation
        invited_email:  @invite_modal.find('.invited_email').val(),
        invited_name:   @invite_modal.find('.invited_name').val()
      
      invitation.save({ wait: true }, {
        success:  @success,
        error:    @errors
      })
      

    success: (a, model) ->
      alerter = new Alerter()
      message = @getLocalizeTextByKey("invitation.alerter.succeed.inverter")
      message += model.invited_name
      message += @getLocalizeTextByKey("invitation.alerter.succeed.has_sent")
      @form.before(alerter.success(message).render())
      @reset_form()

    errors: (model, response, xhr) ->
      if response && response.responseText
        attrs_with_errors = JSON.parse(response.responseText).errors
        alerter = new Alerter()
        @form.before(alerter.error(attrs_with_errors).render())

    reset_form: () ->
      @form.find('input').each (index, element) ->
        $(element).val('')
