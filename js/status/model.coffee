define ["underscore", "backbone", "cs!./reply/collection"], (_, Backbone, Replies) ->
  class Status extends Backbone.Model
    urlRoot: "/statuses"
    defaults: {
      created_at: new Date()
      updated_at: new Date()
    }

    get_replies: ->
      if @get('replies') and @get('replies').length > 0
        new Replies(@get('replies'), status_id: @get('id'))
      else
        new Replies([], status_id: @get('id'))
