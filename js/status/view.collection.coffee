define [
  'jquery',
  'underscore',
  'backbone',
  'cs!./model',
  'cs!./view.model',
  'cs!./view.sidebar',
  "cs!../toolbar/view.statuses.toolbar",
  "cs!../toolbar/view.search_results.toolbar",
  "cs!../prefetch_data/prefetch_models",
  'cs!../composite_view',
  'text!../../templates/statuses.html',
  'cs!../uploader',
  '../aura/mediator',
  'cs!../modules'
], ($, _, Backbone, Model, StatusView, StatusesSidebarView, ToolbarView, SearchResultsToolbarView, PrefetchModels, CompositeView,Template, Uploader, mediator) ->
  class StatusesView extends CompositeView
    template: jQuery(Template)
    events:
      'click button.submit'       : 'create'
      'click button.servernext'   : 'next_page'

    initialize: () ->
      _.bindAll @
      @collection.bind 'reset', @render
      @collection.bind 'add',   @render_one
      @collection.pager     = @collection_pager
        
      @$el.html(@template.html())
      @$el = @localize(@$el)
#      @render_html_editor(
#        id: "editor-toolbar-statuses"
#        editorContainer: @$el.find("#editor_toolbar_for_status")
#      )
     
      # Markup hooks
      @text_field     = @$('textarea')
      @text_filler    = @text_field.attr("placeholder")
      @submit_button  = @$('button.submit')
      @status_list    = @$('ul.statuses')
      @error_list     = @$('ul.errors')
      @render_sidebar()
      @render_toolbar()

    onShow: ()=>
#      @editor = new wysihtml5.Editor @$el.find("#wysiwyg-statuses")[0],
#        toolbar:      "editor-toolbar-statuses",
#        parserRules:  wysihtml5ParserRules

      @render_upload_form()

    render: () =>
      @reset_form()
      @render_all()
      if @collection.length <= @collection.perPage
        @hide_next_page_button()
      else
        @show_next_page_button()
      @endless_pagination()
      @

    render_upload_form: ()=>
      view_options =
        data:
          id: 'statuses'

      uploader_options =
        parent_form: @$el.find("div#main form").first()
        settings:
          token: '?auth_token=' + $.cookie('belinkr.auth_token')

      built = Uploader.build(view_options, uploader_options)
      @replaceWithStaticChildInto(built.view, @$("div.uploader"))
      # document ready!
      $ ()->
        built.uploader.init()


    show_next_page_button: ()->
      @$el.find("button.servernext").removeClass("hide")

    hide_next_page_button: ()->
      @$el.find("button.servernext").addClass("hide")

    disable_next_page_button: ()->
      @$el.find("button.servernext").attr("disabled", "disabled")
#      @$el.find("button.servernext").html("No more results")

    enable_next_page_button: ()->
      @$el.find("button.servernext").removeAttr("disabled")
#      @$el.find("button.servernext").html("The next page")

    render_sidebar: () =>
      sidebar_view = new StatusesSidebarView()
      @replaceWithStaticChildInto(sidebar_view, @$("#sidebar"))

    render_toolbar: () ->
      search_results = PrefetchModels.search_results()
      if search_results.get('search_terms')
        toolbar_view = new SearchResultsToolbarView()
        @$el.find('form.statuses').hide()
      else
        toolbar_view = new ToolbarView()
      @appendStaticChildInto(toolbar_view, @$(".btn-toolbar"))

    create: () =>
      fragment = Backbone.history.fragment
      referer  = _(fragment.split("/")).last()
      @collection.unbind('add') unless referer is "statuses"

      @collection._added_to_prepend = true
      @submit_button.attr("disabled", "disabled")
      @error_list.empty()

      if (@text_field.val() != @text_filler) && (@text_field.val() != "")
        files = @$("input[name='files']").val()
        files = '[]' if (files is null) || (files is '')
        @collection.create({
            "text": @text_field.val(),
            "files": $.parseJSON(files),
            "syn-sina": this.$el.find(".syn-sina input").attr('checked'),
            "syn-tencent": this.$el.find(".syn-tencent input").attr('checked'),
            "syn-netease": this.$el.find(".syn-netease input").attr('checked')
          },{
            wait:    true,
            success: @inc_count_reset_form,
            error:   @errors
        })
      @submit_button.removeAttr('disabled')
      window.App.navigate("/statuses", trigger: true) unless referer is "statuses"
      false

    render_all: () ->
      @collection.each(@render_one)

    render_one: (model) =>
      view = new StatusView(model: model)
      if @collection._added_to_prepend is true
        @prependChildInto(view, @status_list)
        @collection._added_to_prepend = false
      else
        @appendChildInto(view, @status_list)

    errors: (model, response, xhr) =>
      if response && response.responseText
        attrs_with_errors = JSON.parse(response.responseText).errors
        _.each(attrs_with_errors, (errors) ->
          _.each(errors, (error) ->
            @$('ul.errors').append('<li>' + error + '</li>')
          )
        )

    inc_count_reset_form: ()=>
      counter = PrefetchModels.counter()
      counter.set('statuses', counter.get('statuses')+1)
      @editor_reset()
      @reset_form()

    editor_reset: ()->
      @editor?.clear()
      @editor?.fire("set_placeholder")

    reset_form: () =>
      @$el.find("input[name='files']").val('')
      @$el.find(".uploader_form ul.file_list").empty()

      @text_field.val(@text_filler)
        .focusin (event) =>
          @$(event.target).val('') if $(event.target).val() == @text_filler
        .focusout (event) =>
          @$(event.target).val(@text_filler) if $(event.target).val() == ''

    endless_pagination: ()->
      @collection.no_more_results = false
      $(window).unbind('scroll')
      $(window).scroll ()=>
        return if @collection.no_more_results
        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10)
          @collection.requestNextPage()

    collection_pager: ()->
      @collection.fetch success: (collection, resp)=>
        if collection.length == 0
          @show_next_page_button()
          @disable_next_page_button()
          @collection.page -= 1
          @collection.no_more_results = true

    next_page: () ->
      @hide_next_page_button()
      @collection.requestNextPage()
      $ () =>
        if $(document).height() > $(window).height()
          @endless_pagination()
        else
          @show_next_page_button()
          @enable_next_page_button()
      false
