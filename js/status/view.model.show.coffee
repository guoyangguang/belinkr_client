define [
  "jquery",
  "underscore",
  "backbone",
  "cs!./model",
  "cs!./view.model",
  'cs!./view.sidebar',
  "text!../../templates/status_show.html",
], ($, _, Backbone, Status, StatusView, StatusesSidebarView, Template) ->
  class StatusShowView extends StatusView
    template:             jQuery(Template)
    tagName:              "div"
    className:            ""

    render: ()->
      if @model.hasChanged()
        super
        if @model.get('_query')
          @$el.find("#" + @model.get('_query')).ScrollTo(duration: 50)
        @render_sidebar()

    render_sidebar: () =>
      if @model.hasChanged()
        sidebar_view = new StatusesSidebarView()
        @replaceWithStaticChildInto(sidebar_view, @$("#sidebar"))