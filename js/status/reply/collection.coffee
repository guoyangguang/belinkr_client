define ["underscore", "backbone", "cs!./model"], (_, Backbone, Reply) ->
  class ReplyCollection extends Backbone.Collection
    model:      Reply
    url:        ->
      "/statuses/" + @status_id + "/replies"
    comparator: (model) ->
      model.get("updated_at")

    initialize: (models,options)->
      if options['status_id'] 
        @status_id=options["status_id"]
      
