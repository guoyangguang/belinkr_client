define [
  "jquery",
  "underscore",
  "backbone",
  "cs!./view.model",
  "cs!../../composite_view",
  "../../aura/mediator",
  "cs!../../modules"
], ($, _, Backbone, ReplyView, CompositeView, mediator) ->
  class RepliesView extends CompositeView
    initialize: () ->
      _.bindAll(@, "render", "render_one")
      @collection.bind "add",   @render_one
      @collection.bind "reset", @render

    render: () ->
      @_leaveChildren()
      @collection.each(@render_one)
      @invoke_timeago()
      @

    render_one: (model) ->
      if @collection.workspace_id
        model.set('workspace_id', @collection.workspace_id)
      view = new ReplyView(model:model)
      @appendChildInto(view, @$el)
