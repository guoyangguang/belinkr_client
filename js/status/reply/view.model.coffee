define [
  "jquery",
  "underscore",
  "backbone",
  "cs!../../composite_view",
  "../../../templates/directives",
  "text!../../../templates/reply.html",
  "text!../../../templates/reply_delete_click_modal.html",
  "cs!../../file/view.model",
  "../../aura/mediator",
  "cs!../../modules",
  "../../libs/pure/pure"
], ($, _, Backbone, CompositeView, Directives, Template, ReplyDeleteClickModal, FileView, mediator) ->
  class ReplyView extends CompositeView
    template:             jQuery(Template)
    tagName:              "li"
    className:            "reply row-fluid"

    events:
      "click img.avatar"        : "nav_user"
      "click span.user_name"    : "nav_user"

    initialize: () ->

      _.bindAll @#(@, "render","fadeOutremove","delete", "leave", "render_files")
      @model.bind("change", @render)
      @model.bind("destroy", @fadeOutremove)
      @template.directives(Directives.reply)

    render: () ->
      @$el.html( @template.render(@model.toJSON()).html() )
      @$el = @localize(@$el)
      @click_modal()
      @display_enforcer()
      @render_files()
      @

    render_files: () ->
      files = @model.get('files')
      @file_list = @$("ul.files")
      unless _.isEmpty(files)
        _.each files, (data)=>
          file_view = new FileView(data: data)
          @appendChildInto(file_view, @file_list)

    delete: () ->
      mediator.publish "deleteReply", @
      false

    click_modal: ()->
      link = @$el.find(".links .delete")
      link.click_modal
        parent: @
        modal:
          target: "for_status_reply"
          template: @localize($(ReplyDeleteClickModal))
          events:
            "button.btn-warning" : "delete"

    fadeOutremove: () ->
      @$el.fadeOut(250, => @remove())

    leave: () ->
      @unbind()
      @remove()

    viewer_is_author: () ->
      current_user = PrefetchModels.current_user()
      current_user.id == @model.get("user_id")

    display_enforcer: () ->
      if @model.get('timeliner') is 'status'
        unless PrefetchModels.current_user_is_admin()
          unless @viewer_is_author()
            @$el.find("a.delete").parent().remove()
      else
        unless @viewer_is_author()
          @$el.find("a.delete").parent().remove()

    nav_user:(e) ->
      e.preventDefault()
      third_party = @model.get('third_party')
      unless third_party?.user
        window.App.navigate("/users/" + @model.get('user_id'), trigger: true)
      false

