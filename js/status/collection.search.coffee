define ["underscore", "backbone", "cs!./model", "paginator"], (_, Backbone, Status) ->
  class SearchStatusCollection extends Backbone.Paginator.requestPager
    model:      Status
    url:        => "/statuses/search"
    comparator: (model) ->
      model.get("updated_at")

    pageAttribute: 'page'
    perPageAttribute: 'perPage'
    queryAttribute: 'text'

    page: 0
    firtPage: 0
    perPage: 20

    initialize: (models, options)->
      super
      @query=options['query']


