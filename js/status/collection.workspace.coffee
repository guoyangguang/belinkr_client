define ["underscore", "backbone", "cs!../workspace/status/model", "paginator"], (_, Backbone, Status) ->
  class WorkspaceStatusCollection extends Backbone.Paginator.requestPager
    model:      Status
    url:        "/timelines/workspaces"
    comparator: (model) ->
      model.get("updated_at")

    pageAttribute: 'page'
    perPageAttribute: 'perPage'

    page: 0
    firtPage: 0
    perPage: 20
