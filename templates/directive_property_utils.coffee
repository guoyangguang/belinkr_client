define
  user:
   name    : "name"
   position: "position"
   mobile  : "mobile"
   email   : "email"
   as_participant:
    name: "user_name"

  workspace:
   inviter_name: "inviter_name"
   invited_name: "invited_name"

  id   : "id"
  text : "text"
  name : "name"

  created_at: "created_at"
  updated_at: "updated_at"

  workspace_name: "workspace_name"

  links:
    self        : "links.self"
    inviter     : "links.inviter"
    invited     : "links.invited"
    workspace   : "links.workspace"
    workspaces  : "links.workspaces"
    followers   : "links.followers"
    following   : "links.following"