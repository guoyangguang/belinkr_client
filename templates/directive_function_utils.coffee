define
  thirdParty: (arg) ->
    third_party = arg.context.third_party
    if third_party
      "<img src='/img/microblog/#{third_party.type}/logo.png' class='micro-blog-icon'/>"
    else
      ''

  user:
    avatar: (arg)->
      if arg.context.links?.avatar?
        arg.context.links.avatar
      else
        '/img/default-avatar.png'

    button_class: (arg) ->
      switch arg.context.relationship
        when 'follower'
          'btn btn-success'
        when 'following'
          'btn btn-primary'
        when 'self'
          'btn'
        else
          'btn btn-primary'

    button_i_class: (arg) ->
      switch arg.context.relationship
        when 'follower'
          'icon-ok-sign icon-white'
        when 'following'
          'icon-plus-sign icon-white'
        when 'self'
          'icon-white'
        else
          'icon-plus-sign icon-white'

#    button_span_text: (arg) ->
#      switch arg.context.relationship
#        when 'follower'
#          'following'
#        when 'following'
#          'follow'
#        when 'self'
#          'You are here!'
#        else
#          'follow'

  status:
    span_label_style_append: (arg) ->
      if arg.context.workspace_name
        ''
      else
        'display: none'
    div_class_append: (arg) ->
      if (arg.context.forwarder_id)
        ' forwarded'
      else
        ''

  reply:
    span_label_style_append: (arg)->
      if arg.context.workspace_name
        ''
      else
        'display: none'

    a_delete_href: (arg) ->
      if arg.context.workspace_id
        "/workspaces" + arg.context.links.self
      else
        arg.context.links.self

    anchor: (arg) ->
      return "s" + arg.context.status_id + "r" + arg.context.id

  scrap:
    span_label_style_append: (arg) ->
      if arg.context.workspace_name
        ''
      else
        'display: none'

  user_status:
    span_label: (arg) ->
      if arg.context.workspace_name
        arg.context.workspace_name
      else
        ''

    span_label_class_append: (arg) ->
      if arg.context.workspace_id
        ''
      else
        ' hide'

  scrap_reply:
    span_label_style_append: (arg)->
      if arg.context.workspace_name
        ''
      else
        'display: none'

  scrap_form:
    form_action_append: (arg) ->
      arg.context.scrapbook_id + '/scraps'

  workspace_box:
#    button_text_append: (arg) ->
#      switch arg.context.relationship
#        when 'administrator', 'collaborator'
#          'enter'
#        when 'none'
#          'request access'
#        when 'autoinvited'
#          'awaiting confirmation'
#        when 'invited'
#          'accept invitation'
#        else
#          ''

    button_class: (arg) ->
      switch arg.context.relationship
        when 'administrator', 'collaborator', 'invited'
          'btn btn-success'
        when 'none'
          'btn btn-primary'
        when 'autoinvited'
          'btn btn-warning'
        else
          ''

    button_i_class: (arg) ->
      switch arg.context.relationship
        when 'administrator', 'collaborator'
          'icon-ok icon-white'
        when 'none'
          'icon-plus-sign icon-white'
        when 'autoinvited'
          'icon-exclamation-sign icon-white'
        when 'invited'
          'icon-plus-sign icon-white'
        else
          ''

  workspace_autoinvitation:
    state_span_class_append: (arg) ->
      switch arg.context.state
        when 'accepted'
          ' label-success'
        when 'rejected'
          ' label-important'
        when 'pending'
          ' label-info'
        else
          ' hide'

  workspace_invitation:
    state_span_class_append: (arg) ->
      switch arg.context.state
        when 'accepted'
          ' label-success'
        when 'rejected'
          ' label-important'
        when 'pending'
          ' label-info'
        else
          ' hide'

  workspace_sidebar_user:
    button_class_append: (arg) ->
      if arg.context._visitor_workspace_relationship is 'administrator'
        if arg.context._render_to is 'administrator'
          if arg.context.relationship is 'self'
            ' hide'
          else
            ''
      else
        ' hide'

    button_text: (arg) ->
      switch arg.context._render_to
        when 'administrator'
          'demote'
        when 'collaborator'
          'promote'
        else
          ''

  workspace_user:
    button_class_append: (arg) ->
      if arg.context._visitor_workspace_relationship is 'administrator'
        if arg.context._render_to is 'administrator'
          if arg.context.relationship is 'self'
            ''
          else
            ''
      else
        ' hide'

  profile:
    div_picture_p_class: (arg) ->
      if arg.context.relationship is 'self'
        'hide'

    div_picture_p_span_text: (arg) ->
      switch arg.context.relationship
        when 'follower'
          'unfollow'
        when 'following', 'none'
          'follow'
        else
          ''

    prepend_div_picture_p_i_class: (arg) ->
      switch arg.context.relationship
        when 'follower'
          'icon-minus-sign'
        when 'following', 'none'
          'icon-plus-sign';
        else
          ''

  card:
    div_template_div_class_append: (arg) ->
      ' ' + arg.context.status

  message:
    message_from_text: (arg) ->
      if arg.context.direction is 'in'
        arg.context.name + ':'
      else
        'Me:'

  composing_message:
    div_composing_message_body_text: (arg) ->
      arg.context.name + ' is typing...'

  search_results:
    a_activity_span_text: (arg) ->
      if arg.context.statuses
        arg.context.statuses
      else
        0

    a_users_span_text: (arg) ->
      if arg.context.users
        arg.context.users
      else
        0

    a_workspaces_span_text: (arg) ->
      if arg.context.workspaces
        arg.context.workspaces
      else
        0

    a_scrapbooks_span_text: (arg) ->
      if arg.context.scrapbooks
        arg.context.scrapbooks
      else
        0

  file:
    a_link_append: (arg) ->
      images =
        'image/png'         : 'image',
        'application/png'   : 'image',
        'application/x-png' : 'image',
        'image/gif'         : 'image',
        'image/x-xbitmap'   : 'image',
        'image/gi_'         : 'image',
        'image/jpeg'        : 'image',
        'image/jpg'         : 'image',
        'image/jpe_'        : 'image',
        'image/pjpeg'       : 'image',
        'image/vnd.swiftview-jpeg':'image'
      is_img = images[arg.context.mime_type];

      if is_img
        arg.context.id + '?inline=1'
      else
        arg.context.id

    prepend_a_class: (arg) ->
      # see: http://filext.com/file-extension
      #      http://filext.com/faq/office_mime_types.php
      mimes =
        'image/png'                     : 'image',
        'application/png'               : 'image',
        'application/x-png'             : 'image',
        'image/gif'                     : 'image',
        'image/x-xbitmap'               : 'image',
        'image/gi_'                     : 'image',
        'image/jpeg'                    : 'image',
        'image/jpg'                     : 'image',
        'image/jpe_'                    : 'image',
        'image/pjpeg'                   : 'image',
        'image/vnd.swiftview-jpeg'      : 'image',
        'application/msword'            : 'doc',
        'application/doc'               : 'doc',
        'appl/text'                     : 'doc',
        'application/vnd.msword'        : 'doc',
        'application/vnd.ms-word'       : 'doc',
        'application/winword'           : 'doc',
        'application/word'              : 'doc',
        'application/x-msw6'            : 'doc',
        'application/x-msword'          : 'doc',
        'application/vnd.ms-powerpoint' : 'ppt',
        'application/mspowerpoint'      : 'ppt',
        'application/ms-powerpoint'     : 'ppt',
        'application/mspowerpnt'        : 'ppt',
        'application/vnd-mspowerpoint'  : 'ppt',
        'application/powerpoint'        : 'ppt',
        'application/x-powerpoint'      : 'ppt',
        'application/x-m'               : 'ppt',
        'application/vnd.ms-excel'      : 'xls',
        'application/msexcel'           : 'xls',
        'application/x-msexcel'         : 'xls',
        'application/x-ms-excel'        : 'xls',
        'application/x-excel'           : 'xls',
        'application/x-dos_ms_excel'    : 'xls',
        'application/xls'               : 'xls',
        'application/pdf'               : 'pdf',
        'application/x-pdf'             : 'pdf',
        'application/acrobat'           : 'pdf',
        'applications/vnd.pdf'          : 'pdf',
        'text/pdf'                      : 'pdf',
        'text/x-pdf'                    : 'pdf'
      match = mimes[arg.context.mime_type]
      if match
        match
      else
        'other'