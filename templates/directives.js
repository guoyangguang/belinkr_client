define(["cs!./directive_utils"], function (Utils) {
  var Directives = {
    "status":{
      "span.user_name"  : Utils.user.as_participant.name,
      "img.avatar@src"  : Utils.user.avatar,
      ".timeago@title"  : Utils.created_at,
      "span.label"      : Utils.workspace_name,
      "span.label@style+":  Utils.status.span_label_style_append,
      "a.delete@href"    :  Utils.links.self,
      "div.text"         :  Utils.text,
      "div[class='data']@class+": Utils.status.div_class_append,
      "p.author+"        : Utils.thirdParty
    },

    "scrapbook":{
      "a"       : "name",
      "a@href"  : Utils.links.self
    },

    "scrap":{
      '.timeago@title'  : Utils.created_at,
      "a.delete@href"   : Utils.links.self,
      "img@src"         : Utils.user.avatar,
      'span.user_name'  : Utils.user.as_participant.name,
      'div.text'        : Utils.text,
      "span.label@style+": Utils.scrap.span_label_style_append
    },

    "user_status":{
      "img.avatar@src"  : Utils.user.avatar,
      'span.user_name'  : Utils.user.as_participant.name,
      '.timeago@title'  : Utils.created_at,
      "span.label"      : Utils.user_status.span_label,
      "span.label@class+": Utils.user_status.span_label_class_append,
      "a.delete@href" : Utils.links.self,
      'div.text'      : Utils.text,
      "ul.author+"        : Utils.thirdParty
    },

    "user_status_reply":{
      "img.avatar@src"  : function(arg) {
        if (arg.context.third_party && arg.context.third_party.user) {
          return arg.context.third_party.user.profile_image_url;
        } else {
          return Utils.user.avatar(arg);
        }
      },
      '.timeago@title'  : Utils.created_at,
      'span.user_name'  : function(arg) {
        if (arg.context.third_party && arg.context.third_party.user) {
          return arg.context.third_party.user.screen_name;
        } else {
          return arg.context.user_name;
        }
      },
      'div.text'        : Utils.text,
      '.author+'       : Utils.thirdParty
    },

    "scrap_reply":{
      '.timeago@title'  : Utils.created_at,
      "img@src"         : Utils.user.avatar,
      'span.user_name'  : Utils.user.as_participant.name,
      'div.text'        : Utils.text,
      "span.label@style+": Utils.scrap_reply.span_label_style_append
    },

    "scrap_form":{
      "form@action+": Utils.scrap_form.form_action_append
    },

    "reply":{
      "span.user_name"  : function(arg) {
        if (arg.context.third_party && arg.context.third_party.user) {
          return arg.context.third_party.user.screen_name;
        } else {
          return arg.context.user_name;
        }
      },
      "img@src"         : function(arg) {
        if (arg.context.third_party && arg.context.third_party.user) {
          return arg.context.third_party.user.profile_image_url;
        } else {
          return Utils.user.avatar(arg);
        }
      },
      ".timeago@title"  : Utils.updated_at,
      "li a.delete@href": Utils.reply.a_delete_href,
      "div.text"        : Utils.text,
      "span.label@style+": Utils.reply.span_label_style_append,
      "div.text@id"     : Utils.reply.anchor,
      'p.author+'       : Utils.thirdParty
    },

    "appointment":{
      "#title@value":"title",
      "#color@value":"color"
    },

    "workspace":{
      "#header h1+":": #{name}"
    },

    "workspace_box":{
      "a@href+" : Utils.id,
      "a"       : "name",
      "span[data-context='user']+"    : 'people_count',
      "span[data-context='comment']+" : 'statuses_count',
      "button@class": Utils.workspace_box.button_class,
      "button i@class": Utils.workspace_box.button_i_class
    },

    "scrapbook_box":{
      "a@href+" : Utils.id,
      "a"       : "name",
      "span[data-context='comment']+" : 'statuses_count',
      "button@class"                  : 'btn btn-primary'
    },

    "workspace_partial":{
      "a"       : "name",
      "a@href+" : Utils.id
    },

    "workspace_sidebar_counter":{
      "p.people"  : "people_count",
      "p.updates" : "statuses_count"
    },

    "workspace_toolbar":{
      "a[data-context='timeline']@href"       : "/workspaces/#{workspace_id}",
      "a[data-context='invitations']@href"    : "/workspaces/#{workspace_id}/invitations",
      "a[data-context='files']@href"          : "/workspaces/#{workspace_id}/timelines/files",
      "a[data-context='autoinvitations']@href": "/workspaces/#{workspace_id}/autoinvitations"
    },

    "workspace_autoinvitation_pending":{
      "img.avatar@src"      : Utils.user.avatar,
      "abbr.timeago@title"  : Utils.created_at,
      "a.actor"             : Utils.workspace.invited_name,
      "a.actor@href"        : Utils.links.invited,
      "a.object"            : Utils.workspace_name,
      "a.object@href"       : Utils.links.workspace
    },

    "workspace_autoinvitation":{
      "img.avatar@src"      : Utils.user.avatar,
      "abbr.timeago@title"  : Utils.created_at,
      "a.actor"             : Utils.workspace.invited_name,
      "a.actor@href"        : Utils.links.invited,
      "a.object"            : Utils.workspace_name,
      "a.object@href"       : Utils.links.workspace,
      "p.state span@class+" : Utils.workspace_autoinvitation.state_span_class_append
    },

    "workspace_invitation":{
      "img.avatar@src"                  : Utils.user.avatar,
      "abbr.timeago@title"              : Utils.created_at,
      "a.actor[data-context='inviter']" : Utils.workspace.inviter_name,
      "a.actor[data-context='invited']" : Utils.workspace.invited_name,
      "a.actor[data-context='inviter']@href"  : Utils.links.inviter,
      "a.actor[data-context='invited']@href"  : Utils.links.invited,
      "a.object"      : Utils.workspace_name,
      "a.object@href" : Utils.links.workspace,
      "p.state span@class+" : Utils.workspace_invitation.state_span_class_append
    },

    "workspace_sidebar_user":{
      "a@href+" : Utils.id,
      "img@src" : Utils.user.avatar,
      "button@class+": Utils.workspace_sidebar_user.button_class_append,
      "button": Utils.workspace_sidebar_user.button_text
    },

    "workspace_administrator_box":{
      "img.avatar@src"  : Utils.user.avatar,
      "li.user_name"    : Utils.user.name,
      "li.title"        : Utils.user.position,
      "li.email"        : Utils.user.email,
      "li.mobile"       : Utils.user.mobile,
      "button@class+" : Utils.workspace_user.button_class_append
    },

    "workspace_collaborator_box":{
      "img.avatar@src"  : Utils.user.avatar,
      "li.user_name"    : Utils.user.name,
      "li.title"        : Utils.user.position,
      "li.email"        : Utils.user.email,
      "li.mobile"       : Utils.user.mobile,
      "button@class+" : Utils.workspace_user.button_class_append
    },

    "user":{
      "img.avatar@src"  : Utils.user.avatar,
      "li.followers span.followers_count"    : "#{followers_count}",
      "li.following span.following_count"    : "#{following_count}",
      "li.updates span.updates_count"        : "#{statuses}",
      "li.first"        : "first",
      "li.last"         : "last",
      "li.title"        : Utils.user.position,
      "li.email"        : Utils.user.email,
      "li.mobile"       : Utils.user.mobile,
      "button@class"    : Utils.user.button_class,
      "button i@class"  : Utils.user.button_i_class
    },

    "user_sidebar_avatar":{
      ".@href+" : Utils.id,
      "img@src" : Utils.user.avatar
    },

    "profile":{
      "div#header h1+" : Utils.user.name,

      // card
      "div.picture img@src" : Utils.user.avatar,
      "div.picture p@class" : Utils.profile.div_picture_p_class,

      "div.picture p span"  : Utils.profile.div_picture_p_span_text,

      "+div.picture p i@class" : Utils.profile.prepend_div_picture_p_i_class,

      "div.info p[data-context='followers']"  : "followers_count",
      "div.info p[data-context='following']"  : "following_count",
      "div.info p[data-context='updates']"    : "statuses",
      "div.info p.name"       : Utils.user.name,
      "div.info ul li.title"  : Utils.user.position,
      "div.info ul li.mobile" : Utils.user.mobile,

      "div.workspaces p[data-context='workspaces'] a@href" : Utils.links.workspaces,
      "div.connections p[data-context='followers'] a@href"  : Utils.links.followers,
      "div.connections p[data-context='following'] a@href"  : Utils.links.following
    },

    "settings":{
      "div.avatar img@src"            : Utils.user.avatar,
      "input[name='avatar']@value"    : Utils.user.avatar,
      "input[name='first']@value"     : "first",
      "input[name='last']@value"      : "last",
      "input[name='position']@value"  : Utils.user.position,
      "input[name='mobile']@value"    : Utils.user.mobile,
      "input[name='landline']@value"  : "landline",
      "input[name='fax']@value"       : "fax"
    },

    "card":{
      "div.template div@data-jid":"jid",
      "div.template div@class+" : Utils.card.div_template_div_class_append,
      "div.template div div img@src"    : "avatar",
      "div.template div div span.name"  : "name"
    },

    "message":{
      "div.template li div.message_from": Utils.message.message_from_text,
      "div.template li div.message_body":"body"
    },

    "composing_message":{
      "div.template li div.composing_message_body": Utils.composing_message.div_composing_message_body_text
    },

    "conversation":{
      "div.template div.conversation@data-jid":"jid"
    },

    "counter":{
      "p.followers" : "followers",
      "p.following" : "following",
      "p.updates"   : "statuses"
    },

    "search_results":{
      "a.activity span" : Utils.search_results.a_activity_span_text,
      "a.users span"    : Utils.search_results.a_users_span_text,
      "a.workspaces span" : Utils.search_results.a_workspaces_span_text,
      "a.scrapbooks span" : Utils.search_results.a_scrapbooks_span_text
    },

    "uploader_form":{
      ".pick_file@id+"  : Utils.id,
      ".file_list@id+"  : Utils.id,
      "span@id+"        : Utils.id,
      "i@id+"           : Utils.id
    },

    "uploader_item":{
      ".file_name@id+"      : Utils.id,
      ".file_size@id+"      : Utils.id,
      ".file_progress@id+"  : Utils.id,
      ".file_status@id+"    : Utils.id,
      ".file_name"          : "filename",
      ".file_size"          : "filesize"
    },

    "file":{
      ".@id+" : Utils.id,
      "a"     : "original_filename",
      "a@href+"  : Utils.file.a_link_append,
      "+a@class" : Utils.file.prepend_a_class
    },

    "notification": {
      "div.avatar img@src"    : function(arg) {
        if (arg.context.actor && arg.context.actor.links && arg.context.actor.links.avatar) {
          return arg.context.actor.links.avatar;
        } else {
          return '/img/default-avatar.png';
        }
      },
      "abbr@title"    : Utils.created_at
    },

    "aggregation": {
      "span.user_name"  : 'user_nick',
      "img.avatar@src"  : 'head_url',
      ".timeago@title"  : 'create_date',
      "div.text"        : function(arg) {
        var content = arg.context.content;
        content = content.replace(arg.context.key_word, "<b style='color:orange'>"+ arg.context.key_word +"</b>");
        return content;
      },
      "p.author+"      : function(arg) {
        var icon = undefined;
        switch (arg.context.come_from) {
          case "sohu" :
            icon = 'sohu';
            break;
          case 'qq' :
            icon = 'tencent';
            break;
          case 'tencent':
            icon = 'tencent';
            break;
          case '163':
            icon = 'netease';
            break;
          case 'netease':
            icon = 'netease';
            break;
        }
        if (icon) {
          return '<i class="'+ icon +'-microblog"></i>';
        } else {
          return '';
        }
      }
    }
  }; // End Directives

  return Directives;
});
