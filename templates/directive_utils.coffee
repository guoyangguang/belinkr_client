define ["underscore", 'cs!./directive_property_utils', 'cs!./directive_function_utils'], (_, AllProperty, AllFunction)->
  class DirectiveUtils
    @simple_merge: (object1, object2)->
      new_object = _.extend({}, object1, object2)
      for key in _.keys(object1)
        if new_object[key]
          if typeof(object1[key]) is "object"
            new_object[key] = DirectiveUtils.simple_merge(
              new_object[key], object1[key]
            )
        else
          new_object[key] = object1[key]
      new_object

  # $p => PUREJS
  if $p
    if $p.__directives
      $p.__directives
    else
      $p.__directives = DirectiveUtils.simple_merge(AllProperty, AllFunction)
      $p.__directives
  else
    {}
