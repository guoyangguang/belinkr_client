describe 'Contact', ->
  require [
    "cs!xmpp/contact",
    "cs!xmpp/xml"
  ], (Contact, XML) ->
    describe 'Contact', ->
      beforeEach ->
        window.Chat = {
          localStorage: localStorage
        }
        window.Chat.Contact = Contact
        window.Chat.XML = XML
     
        node = window.Chat.XML.parse """
          <item jid="alice@belinkr.com"
                name="Alice" />
        """
        @contact = new window.Chat.Contact(node)
    
      describe '#online/#offline', ->
        describe 'when the contact is online', ->
          it 'returns true/false', ->
            node = window.Chat.XML.parse """
              <item jid="alice@belinkr.com"
                    name="Alice" />
            """
            @contact = new window.Chat.Contact(node)
    
            @contact.presence = ['presence']
            expect(@contact.online()).toEqual(true)
            expect(@contact.offline()).toEqual(false)
    
        describe 'when the contact is offline', ->
          it 'returns false/true', ->
            @contact.presence = []
            expect(@contact.online()).toEqual(false)
            expect(@contact.offline()).toEqual(true)
    
      describe '#status', ->
        describe 'when the contact is online', ->
          describe 'and available', ->
            it 'returns online', ->
              @contact.presence = [
                { away: false }
              ]
              expect(@contact.status()).toEqual('Available')
    
          describe 'but away', ->
            it 'returns away', ->
              @contact.presence = [
                { away: true }
              ]
              expect(@contact.status()).toEqual('Away')
    
        describe 'when the contact is offline', ->
          it 'returns offline', ->
            @contact.online = -> false
            expect(@contact.status()).toEqual('Offline')
    
      describe '#available/#away', ->
        describe 'when the contact is available', ->
          it 'returns true/false', ->
            @contact.presence = [
              { away: false }
            ]
            expect(@contact.available()).toEqual(true)
            expect(@contact.away()).toEqual(false)
    
        describe 'when the contact is offline', ->
          it 'returns false/true', ->
            @contact.presence = [
              { away: true }
            ]
            expect(@contact.available()).toEqual(false)
            expect(@contact.away()).toEqual(true)
    
      describe '#update', ->
        describe 'when the presence is a logout', ->
          it 'clears the presence', ->
            @contact.presence = [
              { away: true }
            ]
    
            presence =
              type: 'unsubscribed'
    
            @contact.update(presence)
            expect(@contact.presence).toEqual([])
    
        describe 'otherwise', ->
          it 'updates the presence of the status', ->
            @contact.presence = [
              { away: false }
            ]
    
            presence =
              away: true
    
            @contact.update(presence)
            expect(@contact.presence[0]).toEqual(presence)
    
      describe '#statusClass', ->
        describe 'when the contact is available', ->
          it 'returns available', ->
            @contact.available = -> true
            @contact.away = -> false
            expect(@contact.statusClass()).toEqual 'available'
    
        describe 'when the contact is away', ->
          it 'returns away', ->
            @contact.available = -> false
            @contact.away = -> true
            expect(@contact.statusClass()).toEqual 'away'
    
        describe 'when the contact is offline', ->
          it 'returns offline', ->
            @contact.available = -> false
            @contact.away = -> false
            expect(@contact.statusClass()).toEqual 'offline'
