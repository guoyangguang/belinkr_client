describe 'Conversation', ->
  require [
    "cs!xmpp/card",
    "cs!xmpp/conversation",
    "cs!xmpp/session",
    "cs!xmpp/contact"
  ], (Card, Conversation, Session, Contact) ->
    describe 'Conversation', ->
      beforeEach ->
        Chat = {
          localStorage: window.localStorage
        }

        Chat.Card = Card
        Chat.Conversation = Conversation
        Chat.Session = Session
        Chat.Contact= Contact
        window.Chat = Chat

        window.Chat.localStorage['vcard:1234'] = '{"name":"bar","jid":"test@belinkr.dev"}'
        @session = { }
        @contact = { jid: '1234' }
        window.Chat.Conversation.all = []

      describe '.findOrCreate', ->
        describe 'when the conversation already exists', ->
          it 'it returns the existing conversation', ->
            
            window.Chat.Conversation.all = [
              {
                session: @session
                contact: @contact
              }
            ]

            conversation = window.Chat.Conversation.findOrCreate @session, @contact
            expect(conversation).toEqual window.Chat.Conversation.all[0]

        describe 'when the conversation does not exist', ->
          it 'it creates a new one and returns it', ->
            window.Chat.Conversation.all = []

            conversation = window.Chat.Conversation.findOrCreate @session, @contact
            expect(conversation).toEqual new window.Chat.Conversation(@session, @contact)

      describe '.destroy', ->
        it 'destroys a Conversation from a jid', ->
          window.Chat.Conversation.all = [
            {
              session: @session
              contact: @contact
            }
          ]

          window.Chat.Conversation.destroy('1234')

          expect(window.Chat.Conversation.all).toEqual []

      describe 'a new Conversation', ->
        it 'registers itself in .all', ->
          conversation = new window.Chat.Conversation(@session, @contact)

          expect(window.Chat.Conversation.all).toEqual [conversation]

          #describe '#focus', ->
          #  template 'conversation.html'

          #  beforeEach ->
          #    @session = { }
          #    @contact = { jid: '1234' }
          #    @conversation = new window.Chat.Conversation(@session, @contact)

          #  # TODO: Make it work with the CLI runner (works on Evergreen Serve only)
          #  #
          #  # it 'focuses on the input', ->
          #  #   $("#chat").append "<div class=\\"conversation\\" data-jid=\\"#{@contact.jid}\\"><input type=\\"text\\"/></div>"
          #  #   @conversation.focus()

          #  #   expect($("#chat input").first().is(':focus')).toEqual true

          #describe '#render', ->
          #  template 'conversation.html'

          #  beforeEach ->
          #    @session = { }
          #    @contact = { jid: '1234' }
          #    @conversation = new window.Chat.Conversation(@session, @contact)

          #  describe 'when the conversation is already open', ->
          #    it 'refreshes it with the new messages', ->
          #      $("#chat").append "<div class=\"conversation\" data-jid=\"#{@contact.jid}\"><div class=\"messages\"></div></div>"
          #      @conversation.messages = [
          #        { who: 'ours', body: 'hello!' },
          #        { who: 'theirs', body: 'hey' },
          #      ]

          #      @conversation.render()

          #      expect($("#chat .conversation:first .messages p").length).toEqual(2)

          #  describe 'when the conversation does not exist', ->
          #    it 'creates it with the new messages', ->
          #      $("#chat").html ''

          #      @conversation.messages = [
          #        { who: 'ours', body: 'hello!' },
          #        { who: 'theirs', body: 'hey' },
          #      ]

          #      @conversation.render()

          #      expect($("div.conversation[data-jid=\"1234\"] .messages p").length).toEqual(2)
