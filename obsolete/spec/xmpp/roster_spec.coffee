describe 'Roster', ->
  require [
    "cs!xmpp/session",
    "cs!xmpp/roster",
    "cs!xmpp/contact",
    "cs!xmpp/card",
    "cs!xmpp/xml"
  ], (Session, Roster, Contact, Card, XML) ->
    describe 'Roster', ->

      beforeEach ->
        window.Chat or= {}
        window.Chat.Session = Session
        window.Chat.Roster = Roster
        window.Chat.Contact= Contact
        window.Chat.Card= Card
        window.Chat.XML = XML


        @session = new window.Chat.Session('')
        @roster = new window.Chat.Roster(@session)

      it 'initializes with an empty list', ->
        @session = new window.Chat.Session('')
        @roster = new window.Chat.Roster(@session)

        expect(@roster.list).toEqual {}

      describe '#populate', ->
        #FIXME: It can pass the test in browser but can't pass using jasmine-headless
        #So disable the code
        #it 'populates the list from a raw IQ response', ->
        #  callback = sinon.spy()

        #  node = window.Chat.XML.parse """
        #    <roster>
        #      <item jid="alice@belinkr.com"
        #            name="Alice" />

        #      <item jid="bob@belinkr.com"
        #            name="Bob" />
        #    </roster>
        #  """
        #  # Stub
        #  @session.sendIQ = (n, r) -> r(node)

        #  @roster.populate(callback)
        #  expect(callback).toHaveBeenCalled()

        #  alice = @roster.list['alice@belinkr.com']
        #  bob   = @roster.list['bob@belinkr.com']

        #  expect(alice.name).toEqual 'Alice'
        #  expect(bob.name).toEqual 'Bob'

      describe '#render', ->
        #TODO: ignore the template first
        #template 'roster.html'

        #it 'renders the roster', ->
        #  alice =
        #    jid: 'jid123'
        #    name: 'Alice'
        #    available: -> false
        #    away: -> true
        #    offline: -> false
        #    statusClass: -> 'away'
        #  bob =
        #    jid: 'jid456'
        #    name: 'Bob'
        #    available: -> false
        #    away: -> false
        #    offline: -> true
        #    statusClass: -> 'offline'

        #  @roster.list = 'jid123': alice, 'jid456': bob
        #  @roster.render()

        #  alice = $("#roster #contact-list .contact:first .info .name")
        #  expect(alice.text().trim()).toEqual('Alice')

        #  bob = $("#roster #contact-list .contact:last .info .name")
        #  expect(bob.text().trim()).toEqual('Bob')

      describe '#cards', ->
        it 'populates cards for all the contacts and triggers notifications on those', ->
          stored_cards = []
          notified_cards = []

          store_stub = sinon.stub window.Chat.Card,"store", (card) ->
            stored_cards.push card

          @session.notify = (type, card) ->
            notified_cards.push card if type == 'card'

          find_stub = sinon.stub window.Chat.Card, "find", (jid, session, cb) ->
            card =
              photo:
                type: 'PNG'
                binval: jid
            cb(card)

          @roster.list =
            'jid123': {}
            'jid456': {}

          callback = sinon.spy()

          @roster.cards(callback)

          expect(stored_cards.length).toEqual(3)
          expect(notified_cards.length).toEqual(3)

          expect(callback).toHaveBeenCalled()

          find_stub.restore()
          store_stub.restore()
