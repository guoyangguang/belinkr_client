describe 'Session', ->
  require [
    "jquery",
    "cs!xmpp/session",
    "cs!xmpp/card",
    "cs!xmpp/message",
    "cs!xmpp/presence",
    "cs!xmpp/roster",
    "cs!xmpp/xml"
  ], (jquery, Session, Card, Message, Presence, Roster, XML) ->
    describe 'Session', ->

      beforeEach ->
        window.$ = jquery
        window.Chat or= {}
        window.Chat.Session = Session
        window.Chat.Card= Card
        window.Chat.Presence= Presence
        window.Chat.Message= Message
        window.Chat.XML = XML

        @url = 'http://belinkr.dev/xmpp-httpbind'
        #window.Chat.Roster = sinon.spy()

      it 'initializes with a new Strophe connection', ->
        session = new window.Chat.Session(@url)
        expect(session.xmpp.connect).toBeDefined()

      it 'works on real connections', ->
        session = new window.Chat.Session(@url)
        server =sinon.fakeServer.create()
        server.respondWith(
          [
            200,
            "Content-Type": "applilcation/xml",
            ""
          ])

        session.connect 'test@belinkr.dev', 'test', (success) ->
          expect(success).toBeTruthy()
        server.respond()
        server.restore()

      describe '#connect', ->
        beforeEach ->
          @session  = new window.Chat.Session(@url)
          
          @session.roster.render = ->

        afterEach ->
          @populate_stub.restore()

        describe 'when the connection is successful', ->
          it 'calls back with true', ->
            # Fake Strophe connection to return the callback with an ok status
            @populate_stub = sinon.stub @session.roster, 'populate', (callback) ->
            @callback = sinon.spy()
            spyOn(@session.xmpp, 'connect').andCallFake (jid, password, callback) ->
              callback(Strophe.Status.CONNECTED)

            @session.connect('test@belinkr.dev', 'test', @callback)
            #expect(@callback).toHaveBeenCalled()
            expect(@session.roster.populate).toHaveBeenCalled()

        describe 'when the connection is failed', ->
          it 'calls back with false', ->
            @populate_stub = sinon.stub @session.roster, 'populate', (callback) ->
              callback()
            @callback = sinon.spy()
            # Fake Strophe connection to return the callback with a failed status
            spyOn(@session.xmpp, 'connect').andCallFake (jid, password, callback) ->
              callback(Strophe.Status.AUTHFAIL)

            @session.connect('test@belinkr.com', '1234', @callback)
            expect(@callback).toHaveBeenCalledWith(false)

      describe '#disconnect', ->
        beforeEach ->
          @session  = new window.Chat.Session
          sinon.spy(@session.xmpp, 'disconnect')

        it 'disconnects the Strophe connection', ->
          @session.disconnect()
          expect(@session.xmpp.disconnect).toHaveBeenCalled()

      describe '#bareJid', ->
        beforeEach ->
          @session = new window.Chat.Session
          @session.xmpp.jid = "test@belinkr.com/whatever"

        it "returns the session's bare JID", ->
          expect(@session.bareJid()).toEqual('test@belinkr.com')

      describe '#avatar', ->
        beforeEach ->
          @session = new window.Chat.Session

        describe 'when an avatar exists', ->
          it "returns the user's avatar URL", ->
            load_stub = sinon.stub window.Chat.Card,"load", (jid) ->
              
              photo:
                type: 'PNG'
                binval: '123456789'

            url = @session.avatar('test@belinkr.com')
            expect(url).toEqual('data:PNG;base64,123456789')
            load_stub.restore()

        describe 'otherwise', ->
          it "returns a fallback avatar", ->
            load_stub = sinon.stub window.Chat.Card,"load", (jid) -> {}

            url = @session.avatar('test@belinkr.com')
            expect(url).toEqual('http://viajeteca.com/avatars/default_avatar.gif')
            load_stub.restore()
      describe '#connected', ->
        beforeEach ->
          @session  = new window.Chat.Session('')

        describe 'when the user is connected', ->
          it 'returns true', ->
            @session.xmpp = { jid: 'user@domain.com' }
            expect(@session.connected()).toBeTruthy()

        describe 'otherwise', ->
          it 'returns false', ->
            @session.xmpp = { jid: undefined }
            expect(@session.connected()).toBeFalsy()

      describe '#notify', ->
        beforeEach ->
          @session = new window.Chat.Session('')
          @message_callback = sinon.spy()
          @presence_callback = sinon.spy()

        it 'runs all the callbacks of a given type', ->
          @session.listeners = {
            message: [ @message_callback ],
            presence: [ @presence_callback ]
          }

          @session.notify('message', { my: 'message' })
          @session.notify('presence', { some: 'presence' })

          expect(@message_callback).toHaveBeenCalledWith({ my: 'message' })
          expect(@presence_callback).toHaveBeenCalledWith({ some: 'presence' })

      describe '#onMessage', ->
        beforeEach ->
          @session = new window.Chat.Session('')

        it 'adds a message callback', ->
          @session.onMessage (message) ->
            message.type

          message_callbacks = @session.listeners['message']
          expect(message_callbacks.length).toEqual(1)
          expect(message_callbacks[0]({type: 'foo'})).toEqual('foo')

      describe '#onRoster', ->
        beforeEach ->
          @session = new window.Chat.Session('')

        it 'adds a roster callback', ->
          @session.onRoster (roster) ->
            roster.type

          roster_callbacks = @session.listeners['roster']
          expect(roster_callbacks.length).toEqual(1)
          expect(roster_callbacks[0]({type: 'foo'})).toEqual('foo')

      describe '#onPresence', ->
        beforeEach ->
          @session = new window.Chat.Session('')

        it 'adds a presence callback', ->
          @session.onPresence (presence) ->
            presence.type

          presence_callbacks = @session.listeners['presence']
          expect(presence_callbacks.length).toEqual(1)
          expect(presence_callbacks[0]({type: 'foo'})).toEqual('foo')

      describe '#onCard', ->
        beforeEach ->
          @session = new window.Chat.Session('')

        it 'adds a card callback', ->
          @session.onCard (card) ->
            card.type

          card_callbacks = @session.listeners['card']
          expect(card_callbacks.length).toEqual(1)
          expect(card_callbacks[0]({type: 'foo'})).toEqual('foo')

      describe '#handleMessage', ->
        beforeEach ->
          @session = new window.Chat.Session('')
          @session.notify = sinon.spy()

        it 'creates a new message from an XML node and calls #notify with it', ->
          @node = """
            <message
              from="alice@jabber.org/home"
              to="bob@jabber.org"
              type="chat">
              <crap>Hello bob!</crap>
              <body>Hello bob!</body>
            </message>
          """
          @session.handleMessage(@node)

          expect(@session.notify).toHaveBeenCalled

      describe '#handlePresence', ->
        beforeEach ->
          @session = new window.Chat.Session('')
          @session.notify = sinon.spy()
          @session.roster.render = ->

        it 'creates a new presence from an XML node and calls #notify with it', ->
          #TODO: can't test in following way because not use global scope now.
          #updateSpy = sinon.spy()
          #presence = (node) -> { parsed: node, updateContact: updateSpy }

          #window.Chat.Presence = (node) -> presence(node)
          #@session.handlePresence("<foo>bar</foo>")

          #expect(@session.notify).toHaveBeenCalledWith('presence', presence("<foo>bar</foo>"))
          #expect(updateSpy).toHaveBeenCalled()

      describe '#sendMessage', ->
        beforeEach ->
          @session = new window.Chat.Session('')

        it 'sends a message to another user', ->
          stub = sinon.stub(@session.xmpp, "send", (a_node)->)
          @session.sendMessage("someone@belinkr.com", "hello there!")

          expect(stub).toHaveBeenCalledOnce()
          #FIXME: stub.args[0] can't get the arguments pass to stub
          #node = stub.args[0]
          #expect($(node).attr('id')).toBeDefined()
          #expect($(node).attr('to')).toEqual "someone@belinkr.com"
          #expect($(node).attr('type')).toEqual "chat"

          #expect($('body', node).text()).toEqual "hello there!"
          stub.restore()

      describe '#sendIQ', ->
        beforeEach ->
          @session = new window.Chat.Session('')
          @spy =sinon.spy()
          @session.xmpp.sendIQ = @spy
          @node = window.Chat.XML.parse """
            <iq id='foo' type="get">
              <query xmlns="jabber:iq:bar"/>
            </iq>
          """
          @callback = jasmine.createSpy 'callback'

        it 'sends an IQ query', ->
          @session.sendIQ @node, @callback
          expect(@spy).toHaveBeenCalledWith(@node, @callback, @callback, 5000)
