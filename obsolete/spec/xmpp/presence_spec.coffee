describe 'Presence', ->
  require [
    "cs!xmpp/presence",
    "cs!xmpp/session",
    "cs!xmpp/roster",
    "cs!xmpp/xml"
  ], (Presence, Session, Roster, XML) ->
    describe 'Presence', ->
      beforeEach ->
        window.Chat or= {}
        window.Chat.Presence = Presence
        window.Chat.Session = Session
        window.Chat.Roster= Roster
        window.Chat.XML= XML
     
        @session = new window.Chat.Session('http://belinkr.com/xmpp-httpbind')
        @session.roster.list =
          "txus@belinkr.com":
            jid: 'txus@belinkr.com'
            update: sinon.spy()
    
        @node = window.Chat.XML.parse """
          <presence to="test@belinkr.com"
                    from="txus@belinkr.com"
                    type="available">
    
            <show>dnd</show>
            <status>XMPP ROCKZ</status>
          </presence>
        """
    
        @presence = new window.Chat.Presence(@node, @session)
    
      it 'initializes with a raw XML node', ->
        expect(@presence.to).toEqual('test@belinkr.com')
        expect(@presence.from).toEqual('txus@belinkr.com')
        expect(@presence.type).toEqual('available')
        expect(@presence.show).toEqual('dnd')
        expect(@presence.status).toEqual('XMPP ROCKZ')
    
        expect(@presence.offline).toEqual(false)
        expect(@presence.away).toEqual(false)
        expect(@presence.dnd).toEqual(true)
    
      describe '#updateContact', ->
        it 'updates the contact if it exists', ->
          @presence.updateContact()
          expect(@session.roster.list["txus@belinkr.com"].update).toHaveBeenCalledWith(@presence)
