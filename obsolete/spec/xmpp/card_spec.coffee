describe 'Card', ->
  require [
    "cs!xmpp/card",
    "cs!xmpp/session",
    "cs!xmpp/roster",
    "cs!xmpp/xml"
  ], (Card, Session, Roster, XML) ->
    describe 'Card', ->

      beforeEach ->
        window.Chat = {
          localStorage: window.localStorage
        }
        window.Chat.Session = Session
        window.Chat.Card= Card
        window.Chat.Roster = Roster
        window.Chat.XML = XML

        @session = new window.Chat.Session('http://belinkr.dev/xmpp-httpbind')
        @node = window.Chat.XML.parse """
          <card>
            <vCard>
              <FN>Tim White</FN>
              <NICKNAME>littletim</NICKNAME>
              <PHOTO>
                <TYPE>PNG</TYPE>
                <BINVAL>123456789</BINVAL>
              </PHOTO>
            </vCard>
          </card>
        """

        @card = new window.Chat.Card('test@belinkr.dev/resource', @node)

      it 'initializes with a raw XML node', ->
        expect(@card.jid).toEqual('test@belinkr.dev/resource')
        expect(@card.bareJid).toEqual('test@belinkr.dev')
        expect(@card.photo.type).toEqual('PNG')
        expect(@card.nick_name).toEqual('littletim')
        expect(@card.full_name).toEqual('Tim White')
        
        expect(@card.photo.binval).toEqual('123456789')

      describe '#find', ->
        describe 'when there is a card', ->
          it 'calls back with the card', ->
            
            sendIQ_stub = sinon.stub @session, "sendIQ", (node, sendIQcallback) =>
              spycallback(@card)

            callbackFn = (card) ->
              
              expect(card.photo.type).toEqual('PNG')
              expect(card.photo.binval).toEqual('123456789')

            spycallback = sinon.spy(callbackFn)

            window.Chat.Card.find('test@belinkr.dev/resource', @session, spycallback)
            expect(spycallback).toHaveBeenCalled()
            sendIQ_stub.restore()
            

        describe 'otherwise', ->
          it 'calls back with null', ->
            find_stub = sinon.stub window.Chat.Card, "find", (jid, session, findcallback) =>
              findcallback(null)

            callback = sinon.spy()

            window.Chat.Card.find('test@belinkr.dev/resource', @session, callback)

            expect(callback).toHaveBeenCalledWith(null)
            find_stub.restore()

      describe '#load', ->
        it 'loads a card from the local storage', ->
          window.Chat.localStorage['vcard:test@belinkr.dev'] = '{"foo":"bar","bar":"baz"}';
          result =window.Chat.Card.load('test@belinkr.dev/resource') 
          
          expect(result).toEqual({foo: 'bar', bar: 'baz'})

      describe '#store', ->
        it 'stores a card to the local storage', ->
          window.Chat.Card.store
            jid: 'test@belinkr.dev/resource'
            foo: 'bar'
            bar: 'baz'

          expect(window.Chat.localStorage['vcard:test@belinkr.dev']).toEqual '{"jid":"test@belinkr.dev/resource","foo":"bar","bar":"baz"}'
