describe "MyClass", ->
  require [ "cs!xmpp/myclass" ], (MyClass) ->
    describe "MyClass", ->
      beforeEach ->
        @myClass = MyClass
      describe "mytest", ->
        it "do nothing", ->
          expect(1).toEqual(1)
        it "still do nothing", ->
          expect(@myClass.p).toEqual(1)

