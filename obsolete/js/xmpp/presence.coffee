# A presence is a kind of XMPP node that identifies online/offline statuses
# and other metainformation that users want to provide.
#
define ["jquery"], ($) ->
  class Presence
    # Initializes from a raw presence node and a session.
    #
    # @api public
    constructor: (node, @session) ->
      node   = $(node)
      @to     = node.attr 'to'
      @from   = node.attr 'from'
      @type   = node.attr 'type'
      @show   = node.find('show').first().text()
      if @show == ""
        @show = "online"
      @status = node.find('status').first().text()
  
      @offline = @type == 'unavailable' || @type == 'error'
      @away =    @show == 'away' || @show == 'xa'
      @dnd =     @show == 'dnd'
  
    # Updates a contact with the presence if it exists.
    updateContact: ->
      @contact().update this if @contact()
  
    # @return [Contact]
    #   The populated contact.
    #
    # @api private
    contact: ->
      contact = @session.roster.list[@from.split('/')[0]]
      @jid = contact.jid if contact
      contact
  
  # Exports
  Presence
