# An cross-cutting XML helper used by many classes in the chat system.
#
define ["jquery"], ($) ->
  class XML
    # Parses XML text and returns a proper node.
    #
    # @param [String] text
    #   The XML raw text to parse.
    #
    # @api public
    @parse: (text) -> $.parseXML(text).documentElement

  # Exports
  XML
