# A contact.
#
define ["jquery"], ($) ->
  class Contact
    # Initializes with a node.
    #
    # @api public
    constructor: (node) ->
      # warning, in a real message, only jid and subscription are available
      node  = $(node)
      @jid  = node.attr('jid').split('/')[0]
      @name = node.attr 'name'
      @ask  = node.attr 'ask'
      @subscription = node.attr 'subscription'
      @groups = $('group', node).map(-> $(this).text()).get()
      @presence = []
  
    # @return [true, false]
    #   Whether the contact is online.
    #
    # @api public
    online: ->
      @presence.length > 0
  
    # @return [true, false]
    #   Whether the contact is offline.
    #
    # @api public
    offline: -> !@online()
  
    # @return [true, false]
    #   Whether the contact is available (online and not away).
    #
    # @api public
    available: ->
      @online() && (p for p in @presence when !p.away).length > 0
  
    # @return [true, false]
    #   Whether the contact is away (online and not available).
    #
    # @api public
    away: -> @online() && !@available()
  
    # @return [String]
    #   The contact status.
    #
    # @api public
    status: ->
      available = (p.status for p in @presence when p.status && !p.away)[0] || 'Available'
      away = (p.status for p in @presence when p.status && p.away)[0] || 'Away'
      if this.offline()   then 'Offline'
      else if this.away() then away
      else available
  
    # Updates the presence of a contact, or if it is a logout, just clears it.
    #
    # @param [Presence]
    #   The presence used to update the contact.
    #
    # @api public
    update: (presence) ->
      @presence = (p for p in @presence when p.from != presence.from)
      @presence.push presence unless presence.type
      @presence = [] if presence.type == 'unsubscribed'
  
    # @return [String]
    #   The computer-friendly class of status.
    #
    # @api public
    statusClass: ->
      return "available" if @available()
      return "away" if @away()
      return "offline"
  
  # Exports
  Contact
