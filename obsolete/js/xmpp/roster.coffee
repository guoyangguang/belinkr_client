# An XMPP Roster, namely a contact list.
#
define ["jquery","cs!./card","cs!./xml", "cs!./contact"], ($,Card, XML,Contact) ->
  class Roster
    # Initializes from a Session object.
    #
    # @api public
    constructor: (@session) ->
      @list = {}
  
    # Populates the list from a raw IQ response.
    #
    # @param [Session] session
    #   The Session object.
    #
    # @param [Function] callback
    #   A function to call back.
    #
    # @api public
    populate: (callback) ->
      node = $iq({type:'get'}).c("query",{xmlns:"jabber:iq:roster"})
      @session.sendIQ node, (response) =>
        contacts = $('item', response).map(-> new Contact this )
        @list[contact.jid] = contact for contact in contacts
  
        @cards(callback) # Load the cards to storage for later use
  
    # Renders the roster in a roster URL.
    #
    # @api public
    render: ->
      # $("#roster #contact-list").html('')
  
      for own jid, contact of @list when contact.available()
        @renderContact(jid, contact, 'available')
  
      for own jid, contact of @list when contact.away()
        @renderContact(jid, contact, 'away')
  
      for own jid, contact of @list when contact.offline()
        @renderContact(jid, contact, 'offline')
  
    # Populates cards for all the contacts and triggers notifications for those.
    #
    # @param [Function] callback
    #   A function to call back when finished.
    #
    # @api public
    cards: (callback) ->
      jids = (jid for own jid, contact of @list when !Card.load jid)
      jids.push @session.bareJid() if !Card.load(@session.bareJid())
  
      success = (card) =>
        if card
          Card.store card
          @session.notify 'card', card
  
      for jid in jids
        # window.Chat.Card.find jid, @session, success for jid in jids
        Card.find jid, @session, success
  
      callback()
  
    # Renders a contact given a jid, contact and status.
    #
    # @param [String] jid
    #   The contact jid.
    #
    # @param [Contact] contact
    #   The contact itself.
    #
    # @param [String] status
    #   The underscored status to add as a CSS class.
    #
    # @api private
    renderContact: (jid, contact, status) ->
      if $("#roster #contact-list .contact[data-jid=\"#{jid}\"]").length == 1
        if $("#roster #contact-list .contact[data-jid=\"#{jid}\"] a.chat").length == 0
          $("#roster #contact-list .contact[data-jid=\"#{jid}\"]").prepend """
            <a href="#" class="chat">
              <span class="chat">Chat</span>
            </a>
          """
      else if @session.xmpp.jid.split('/')[0] != jid 
        if $("#roster #contact-list #contact-list-offline").length == 1
          $default_list = $("#roster #contact-list-offline")
        else
          $default_list = $("#roster #contact-list")
        $default_list.append """
          <div style='display: block' class='contact #{status}' data-jid='#{jid}'>
            <a href="#" class="chat">
              <span class="chat">Chat</span>
            </a>
            <div class="avatar">
              <img src=\"#{@session.avatar(jid)}\" width=\"33\" height=\"33\"/>
            </div>
            <div class="info">
              <span class="name">
                #{contact.name}
              </span>
            </div>
          </div>
        """
  
  # Exports
  Roster
