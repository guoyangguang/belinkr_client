# A wrapper for the Strophe Connection.
#
# For now, it just connects and calls back with true or false.
#
# @example
#
#   uri = "http://my-xmpp-server.com:5280/http-bind"
#   session = new window.Chat.Session(uri)
#
#   session.onMessage (msg) ->
#     alert("#{msg.date}: #{msg.from} => #{msg.to}: #{msg.body}")
#
#   session.connect 'user@my-xmpp-server.com', 'password', (success) ->
#     # Write your callback here.
#     # The +success+ variable will be either true or false.
#
define ["jquery", "underscore", "backbone", "strophe",
  "cs!./xml",
  "cs!./message",
  "cs!./presence",
  "cs!./card",
  "cs!./roster",
  'jquery_cookie'
],
($, _, Backbone, Strophe, XML, Message, Presence, Card, Roster) ->
  class Session
    # Initializes with a BOSH URI.
    #
    # @param [String] uri
    #   The BOSH server URI.
    #
    # @api public
    constructor: (uri)->
      @xmpp = new Strophe.Connection(uri)
      @roster = new Roster(this)
      @listeners =
        message: []
        roster: []
        presence: []
        card: []
      
      
  
    # Connects to the XMPP server using Strophe and calls back.
    #
    # It also attachs message handlers,
    #
    # @param [String] jid
    #   The full jid (user@domain.com)
    #
    # @param [String] password
    #   The password (1234)
    #
    # @param [Function] callback
    #   A callback to pass true or false depending on whether the connection was
    #   successful or not.
    #
    # @api public
    connect: (jid, password, callback) ->
      @xmpp.connect jid, password, (status) =>
        switch status
          when Strophe.Status.AUTHFAIL or Strophe.Status.CONNFAIL
            callback false
          when Strophe.Status.CONNECTED
            @xmpp.addHandler ((node) => 
              console.log('receive xmpp')
              console.log(node)
              $.cookie('belinkr.xmpp.rid', @xmpp.rid))
            #@roster.populate =>
            #  @xmpp.addHandler ((node) => @handlePresence(node)), null, 'presence'
            #  @xmpp.addHandler ((node) => @handleMessage(node)), null, 'message'
            #  @xmpp.send $pres() #XML.parse '<presence/>'
            #  @roster.render()
            callback true
  
    attach: (jid, sid, rid, callback) ->
      @xmpp.attach jid, sid, rid, (status) =>
        switch status
          when Strophe.Status.DISCONNECTED
            $.cookie('belinkr.xmpp.sid','')
            $.cookie('belinkr.xmpp.rid','')
            $.cookie('belinkr.xmpp.jid','')
            callback false
          when Strophe.Status.AUTHFAIL or Strophe.Status.CONNFAIL
            callback false
          when Strophe.Status.ATTACHED
            @xmpp.addHandler ((node) => $.cookie('belinkr.xmpp.rid', @xmpp.rid))
            populate_callback = =>
              @roster.populate =>
                @xmpp.addHandler ((node) => @handlePresence(node)), null, 'presence'
                @xmpp.addHandler ((node) => @handleMessage(node)), null, 'message'
                @roster.render()
                @xmpp.send $pres() #XML.parse '<presence/>'
                callback true
            setTimeout(populate_callback,5000)
 
    # Disconnects from the XMPP server.
    #
    # @api public
    disconnect: -> 
      @xmpp.disconnect()
      $.cookie('belinkr.xmpp.sid','')
      $.cookie('belinkr.xmpp.rid','')
      $.cookie('belinkr.xmpp.jid','')
 
  
    # Returns whether the session is connected or not.
    #
    # @api public
    connected: -> @xmpp.jid && @xmpp.jid.length > 0
  
    # Adds a callback to the message callback list.
    #
    # @param [Function] callback
    #   The callback to add.
    #
    # @api public
    onMessage: (callback) ->
      @listeners['message'].push callback
  
    # Adds a callback to the roster callback list.
    #
    # @param [Function] callback
    #   The callback to add.
    #
    # @api public
    onRoster: (callback) ->
      @listeners['roster'].push callback
  
    # Adds a callback to the presence callback list.
    #
    # @param [Function] callback
    #   The callback to add.
    #
    # @api public
    onPresence: (callback) ->
      @listeners['presence'].push callback
  
    # Adds a callback to execute on a Card event.
    #
    # @param [Function] callback
    #   The callback to add.
    #
    # @api public
    onCard: (callback) ->
      @listeners['card'].push callback
  
    # @return [String]
    #   The session's unique Id.
    #
    # @api public
    uniqueId: -> @xmpp.getUniqueId()
  
    # @return [String]
    #   The session's bare JID (without the resource part).
    #
    # @api public
    bareJid: -> @xmpp.jid.split('/')[0]
  
    # TODO: Move to its own class.
    #
    # @param [String]
    #   The jid from which to get the avatar.
    #
    # @return [String]
    #   The user avatar or a fallback.
    #
    # @api public
    avatar: (jid) ->
      card = Card.load(jid)
      
      if card && card.photo
        "data:#{card.photo.type};base64,#{card.photo.binval}"
      else
        'http://viajeteca.com/avatars/default_avatar.gif'
  
    # Sends a message to another user.
    #
    # @param [String] jid
    #   The receiver jid.
    #
    # @param [String] message
    #   The message to send.
    #
    # @api public
    sendMessage: (jid, message) ->
      #node = XML.parse """
      #  <message id="#{@uniqueId()}" to="#{jid}" type="chat">
      #    <body></body>
      #  </message>
      #"""
      #$('body', node).text message
      node = $msg(
        to: jid
        type: "chat"
      ).c('body').t(message)
      @xmpp.send node
  
    # Sends an IQ query.
    #
    # @param [Node] node
    #   An XML node containing the query.
    #
    # @param [Function] callback
    #   A function to call back.
    #
    # @api public
    sendIQ: (node, callback) ->
      @xmpp.sendIQ node, callback, callback, 5000
  
    ##
    # Private members
    ##
  
    # Creates a new message from an XML node and calls #notify with it.
    #
    # @param [String] node
    #   An XML node with all the message information.
    #
    # @api private
    handleMessage: (node) ->
      message = new Message(node)
      @notify('message', message)
      true # keep handler alive
  
    # Creates a new presence from an XML node and calls #notify with it.
    #
    # @param [String] node
    #   An XML node with all the presence information.
    #
    # @api private
    handlePresence: (node) ->
      presence = new Presence(node, this)
      presence.updateContact()
      @notify('presence', presence)
      @roster.render()
      true # keep handler alive
  
    # Calls each registered callback for a given event type.
    #
    # @param [String] type
    #   The event type (message, presence...)
    #
    # @param [Object] object
    #   The event object.
    #
    # @api private
    notify: (type, object) ->
      for callback in (@listeners[type] || [])
        callback(object)
  
  # Exports
  Session
