# A class representing a Message. It handles all the conversion from XML node
# to a real Message object.
#
define ["jquery"], ($) ->
  class Message
    # Initializes with an XML node.
    #
    # @param [String] node
    #   The XML node representing the raw message.
    #
    # @api public
    constructor: (node)->
      xml       = $(node)
      @from       = xml.attr('from')
      @to       = xml.attr('to')
      @type     = xml.attr('type')
      @body     = $('body', xml).text()
      @received = new Date()
  
      composing = $('composing',xml)
      @composing = composing.length > 0
  
      error = $('error',xml)
      @error = error.length > 0
      
  # Exports
  Message
