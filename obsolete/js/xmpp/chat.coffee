# Chat is the parent namespace for every chat class.
#
define ["jquery", "underscore", "backbone", 
  "cs!./card",
  "cs!./session",
  "cs!./conversation",
  "jqueryui/accordion",
  "jqueryui/autocomplete",
  "jqueryui/dialogr"
  'jquery_cookie'
], ($, _, Backbone, Card, Session, Conversation) ->
  Chat = {}
  window.Chat = Chat
 
  Chat.attach = (session) ->
    rid = $.cookie('belinkr.xmpp.rid')
    rid = (parseInt(rid,10)+0).toString()
    $.cookie('belinkr.xmpp.rid',rid)
    
    session.attach(
      $.cookie('belinkr.xmpp.jid'),
      $.cookie('belinkr.xmpp.sid'),
      rid,
      (success)-> 
        console.log(success)
        if success
          $("div#login").hide()
          self_card =Card.load(session.bareJid())
          if self_card.full_name?
            $("#roster #user a").text self_card.full_name 
          else
            $("#roster #user a").text self_card.jid 
  
          $("#roster #user div img").attr("src", session.avatar(session.bareJid()))
   
          # display the name of following user from the localStorage cards
          stored_cards = (Card.load(contact.jid) for own jid, contact of session.roster.list)
          for card in stored_cards
            if card? and (card.name? or card.nick_name? or card.full_name?)
              session.notify 'card', card
        else
          alert "XMPP connection lost, please login again"
          $("div#login").show()
 
    )


  Chat.login = (session) ->
    user = $('#login_jid').attr('value')
    password = $('#login_password').attr('value')
    #password = $('#roster .info').attr('data-password')
    #user = 'test@belinkr.dev'
    #password = 'test'
    session.connect user, password, (success) ->
      # $('#avatar').html "<img src=\"#{session.avatar(session.bareJid())}\" width=\"50\" height=\"50\"/>"
      # save to cookie
      if success
        $("div#login").hide()
        $.cookie('belinkr.xmpp.sid',session.xmpp.sid)
        $.cookie('belinkr.xmpp.jid',session.xmpp.jid)
        $.cookie('belinkr.xmpp.rid',session.xmpp.rid)
        # display self name
        self_card =Card.load(session.bareJid())
        if self_card.full_name?
          $("#roster #user a").text self_card.full_name 
        else
          $("#roster #user a").text self_card.jid 
  
        $("#roster #user div img").attr("src", session.avatar(session.bareJid()))
   
        # display the name of following user from the localStorage cards
        stored_cards = (Card.load(contact.jid) for own jid, contact of session.roster.list)
        for card in stored_cards
          if card? and (card.name? or card.nick_name? or card.full_name?)
            session.notify 'card', card
  
  Chat.start = ->
    #$(document).ready ->
    
    $("#accordion").accordion({autoHeight: false})
    uri = "http://belinkr.dev:5280/http-bind"
    session = new Session(uri)
    window.session = session
  
    if $.cookie('belinkr.xmpp.sid') &&
    $.cookie('belinkr.xmpp.rid') &&
    $.cookie('belinkr.xmpp.jid')
      Chat.attach(session)

    # Receiving chat messages
    session.onMessage (msg) ->
      jid  = msg.from.split('/')[0]
      name = $("#roster .contact[data-jid=\"#{jid}\"] .info .name").text()
      contact = session.roster.list[jid]
      conversation =Conversation.findOrCreate(session, contact)
      mymsg=msg

      unless msg.error
        if msg.composing
            conversation.composing = true
            conversation.render()
        else
          conversation.composing = false
          conversation.push('theirs', msg.body, name)
          conversation.render()
          conversation.focus()
  
    # Receiving cards
    session.onCard (card) ->
      #$("#roster .contact[data-jid=\"#{card.bareJid}\"] .picture").html "<img src=\"#{card.photo.binval}\" />"
      #$('#avatar').html "<img src=\"#{session.avatar(session.bareJid())}\" />"
      if card?.jid == session.bareJid()
        $("#roster #user a").text card.jid
      else
        if card?.name?.length > 0
          $("#roster .contact[data-jid=\"#{card.bareJid}\"] .info .name").text card.name
        else if card?.nick_name?.length > 0
          $("#roster .contact[data-jid=\"#{card.bareJid}\"] .info .name").text card.nick_name
        else if card?.full_name?.length > 0
          $("#roster .contact[data-jid=\"#{card.bareJid}\"] .info .name").text card.full_name
  
    # Receiving presences
    session.onPresence (presence) ->
      if presence.jid != session.xmpp.jid.split('/')[0]
        if presence.contact()
          $("#roster .contact[data-jid=\"#{presence.jid}\"]").removeClass 'available'
          $("#roster .contact[data-jid=\"#{presence.jid}\"]").removeClass 'offline'
          $("#roster .contact[data-jid=\"#{presence.jid}\"]").removeClass 'away'
          $("#roster .contact[data-jid=\"#{presence.jid}\"]").addClass presence.contact().statusClass()
          if presence.contact().statusClass() == 'available'
            $("#roster #contact-list #contact-list-online").append(
              $("#roster .contact[data-jid=\"#{presence.jid}\"]"))
          else if presence.contact().statusClass() == 'offline'
            $("#roster #contact-list #contact-list-offline").append(
              $("#roster .contact[data-jid=\"#{presence.jid}\"]"))
      else
        $("#avatar").addClass presence.contact().statusClass()
  
    # Starting a conversation with a contact
    $('#roster .contact a.chat').live 'click', ->
      jid = $(this).parent('.contact').attr('data-jid')
      contact = session.roster.list[jid]
      conversation =Conversation.findOrCreate(session, contact)
      conversation.render()
      conversation.focus()
  
    # Message sending
    $('div.conversation input').live 'keypress', (event) ->
      if event.which == 13
        event.preventDefault()
  
        jid = $(this).parent().parent('.conversation').attr('data-jid')
        contact = session.roster.list[jid]
        conversation =Conversation.findOrCreate(session, contact)
  
        msg = $(this).attr('value')
        session.sendMessage jid, msg
  
        conversation.push('ours', msg, 'Me')
        conversation.render()
        $(this).attr('value', '')
        $(this).focus()
        $(this).parent().parent('.conversation').data('composing', false)
      else
        unless $(this).parent().parent('.conversation').data('composing')
          #FIXME  in session class outgoing message don't set xmlns correctly
          #send composing
          jid = $(this).parent().parent('.conversation').attr('data-jid')
          notify = $msg({to: jid, "type": "chat"})
                      .c('composing', {xmlns: "http://jabber.org/protocol/chatstates"})
          session.xmpp.send(notify);
  
          $(this).parent().parent('.conversation').data('composing', true)
          setTimeout(
            =>
              $(this).parent().parent('.conversation').data('composing',false)
            5000)
  
          
    # Finally, log in to the chat
    #window.Chat.login(session)
    $("input#login_button").click ->
      if $.cookie('belinkr.xmpp.sid') &&
      $.cookie('belinkr.xmpp.rid') &&
      $.cookie('belinkr.xmpp.jid')
        Chat.attach(session)
      else
        Chat.login(session)
    # bind logout button
    $("a#logout").click ->
        session.disconnect()
        setTimeout("location.href = '/logout'", 3000)
  
    $("#roster_search_input").focus ->
      $(this).unbind('focus')
      $online_contact_list = $("#roster #contact-list-online .contact[data-jid]")
      online_contact_names = []
      online_contact_jids = []
      $online_contact_list.each (index)->
        online_contact_names.push $('.info span',$(this)).text()
        online_contact_jids.push $(this).data('jid')
  
      $(this).autocomplete
        source: online_contact_names,
        select: (event, ui) ->
          index = online_contact_names.indexOf(ui.item.value)
          jid = online_contact_jids[index]
          $("#roster div.contact[data-jid=\"#{jid}\"] a.chat").click()
  Chat
