# A contact vCard that identifies the users.
#
define ["jquery", "cs!./xml"], ($, XML) ->
  class Card
    # Initializes from a JID and a raw card node.
    #
    # @api public
    constructor: (jid, node) ->
      card     = $('vCard', node)
      photo    = $('PHOTO', card)
      name     = $('name', card).text()
      nick_name = $('NICKNAME',card).text()
      full_name = $('FN',card).text()
      type     = $('TYPE', photo).text()
      bin      = $('BINVAL', photo).text()
  
      @jid = jid
      @name = name
      @nick_name = nick_name
      @full_name = full_name
      @bareJid = @jid.split('/')[0]
  
      @photo =
        if type && bin
          type: type, binval: bin.replace(/\n/g, '')
        else null
  
      @retrieved = new Date()
  
    # Requests a card from a JID and if found, calls back with it - otherwise
    # calls back with null.
    #
    # @param [String] jid
    #   The user JID.
    #
    # @param [Session] session
    #   The session from which to request the card.
    #
    # @param [Function] callback
    #   A function to call back.
    #
    # @api public
    @find: (jid, session, callback) ->
      return unless jid
      #node = XML.parse """
      #  <iq id="#{session.uniqueId()}" to="#{jid}" type="get">
      #    <vCard xmlns="vcard-temp"/>
      #  </iq>
      #"""
      node = $iq(
        type: "get"
        to:    jid
      ).c("vCard",{xmlns:"vcard-temp"})
      session.sendIQ node, (result) ->
        
        card = new Card(jid, result)
        if card.photo or card.name or card.full_name or card.nick_name
          callback(card)
        else
          callback(null)
  
    # Loads a card from the local storage.
    #
    # @param [String] jid
    #   The user jid.
    #
    # @api public
    @load: (jid) ->
      jid = jid.split('/')[0]
      found = Card.localStorage['vcard:' + jid]
      JSON.parse found if found
  
    # Stores a card to the local storage.
    #
    # @param [Card] card
    #   The card to store.
    #
    # @api public
    @store: (card) ->
      jid = card.jid.split('/')[0]
      Card.localStorage['vcard:' + jid] = JSON.stringify card
  
  # Exports
  Card.localStorage = localStorage
  Card
