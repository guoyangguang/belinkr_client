# A conversation is a chat between two people.
#
define ["jquery", "cs!./card"], ($, Card) ->
  class Conversation
    # An array to store all the conversations.
    @all: []
  
    # Finds an active Conversation or creates it if it does not exist.
    #
    # @param [Session] session
    #   The current session.
    #
    # @param [Contact] contact
    #   The contact with whom the conversation is.
    #
    # @return [Conversation]
    #   The existing or new conversation.
    #
    # @api public
    @findOrCreate: (session, contact) ->
      for conversation in @all
        return conversation if conversation.contact.jid == contact.jid
  
      return new Conversation(session, contact)
  
    # Destroys a Conversation (closes it) and forgets all the messages.
    #
    # @param [String] jid
    #   The user's jid.
    #
    # @api public
    @destroy: (jid)->
      @all = (conversation for conversation in @all when conversation.contact.jid != jid)
  
    # Initializes with a session and a contact.
    #
    # @param [Session] session
    #   The session.
    #
    # @param [Contact] contact
    #   The contact.
    #
    # @api public
    constructor: (@session, @contact) ->
      @messages = []
      Conversation.all.push this
  
      # a flag for composing
      @composing = false
  
  
    # Adds a message to the conversation.
    #
    # @param ['theirs', 'ours'] who
    #   Whose message it is.
    #
    # @param [String] message
    #   The message.
    #
    # @api public
    push: (who, message, name) ->
      @messages.push
        who: who
        body: message
        name: name
  
    # Changes the focus to the conversation input.
    #
    # @api public
    focus: ->
      query = "div.conversation[data-jid=\"#{@contact.jid}\"]"
      $(query).dialogr('open')
      $(query).dialogr('restore')
      $("input", query).focus()
  
    # Get user name
    #
    # @api public
    get_user_name: ->
        card = Card.load(@contact.jid)
        #name = $("#roster .contact[data-jid=\"#{@contact.jid}\"] .info .name").text()
        if card.name?.length > 0
          name = card.name 
        else
          name = card.full_name 
  
    # Renders the conversation with all the messages.
    #
    # @api public
    render: ->
      query = "div.conversation[data-jid=\"#{@contact.jid}\"]"
  
      # Create the conversation if it does not exist.
      if $(query).length == 0
        name = @get_user_name()
        new_div = $("<div>")
        window.new_div=new_div
        new_div.attr('title',name)
        new_div.attr('data-jid',@contact.jid)
        new_div.addClass "conversation"
        new_div.append """
          <div data-jid="#{@contact.jid}" class='message_container'><div class=\"messages\"></div><input class=\"chat_input\" type=\"text\"/></div>
        """
        jid = @contact.jid
        new_div.dialogr({maximized:true, minimized:true, minHeight: 140, minWidth: 310, height: 160, width: 330})
  
      conversation = $(".messages", query)
      conversation.html ''
      for message in @messages
        conversation.append("<p class=\"#{message.who}\"><strong>#{message.name}:</strong><br /> #{message.body}</p>")
  
      name = @get_user_name()
      if @composing
        chat_event = "<div class='chat-event'>" + name + " is typing...</div>"
        conversation.append(chat_event)
        setTimeout(
          => 
            $("div[data-jid=\"#{@contact.jid}\"] div.chat-event").remove()
          5000)
      else
        $("div[data-jid=\"#{@contact.jid}\"] div.chat-event").remove()
  
      $(query).scrollTop($(".message_container").height())
  
    @close: (jid)->
      $("div.conversation[data-jid=\"#{jid}\"]").dialogr('close')
  
  # Exports
  Conversation


